﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;

namespace assetmanager
{
    /// <summary>
    /// Asset enums used internally by the AssetManager.
    /// Typed assets can be accessed elsewhere using the typed enums.
    /// 
    /// Assets are generally /shared/ resources, this entire bit is here only to save multiple loads / copies in memory.
    /// Some places will create assets individually because they're not made to be shared.
    /// </summary>
    public enum Asset
    {
        SH_START = 0,
        SH_POS_NORM_MAP_TEX,
        SH_POS_NORM_TEX,
        SH_POS_TEX,
        SH_DISPLACEMENT_MAP,
        SH_LIGHT_POINT,
        SH_LIGHT_DIRECTIONAL,
        SH_COMPILE,
        SH_DEBUG_COLOR,
        SH_DEBUG_DEPTH,
        SH_DEBUG_NORMAL,
        SH_DEBUG_LIGHT,
        SH_FONT_BITMAP,
        SH_FONT_SDF,
        SH_ROUNDED_RECTANGLE_2D,
        SH_ROUNDED_RECTANGLE_3D,
        SH_LINE,
        SH_LINE_COLORED,
        SH_GRADIENT_CIRCLE,
        SH_DASHED_CIRCLE,
        SH_BORDERED_CIRCLE,
        SH_ENEMY,
        SH_TEST,
        SH_END,

        TEX_START = 4096,
        TEX_WHITEPIXEL,
        TEX_DUCK,
        TEX_TEST,
        TEX_END,

        VB_START = 8192,
        VB_QUAD_POS_TEX_UNIT,
        VB_CIRLCE_POS_TEX_UNIT,
        VB_CIRLCE_POS_TEX_NORM_UNIT,
        VB_END,

        F_START = 12288,
        F_CALLI_SDF_128,
        F_CALLI_SDF_64,
        F_CALLI_SDF_32,
        F_CALLI_SDF_16,
        F_CALLI_BMP_128,
        F_CALLI_BMP_64,
        F_CALLI_BMP_32,
        F_CALLI_BMP_16,
        F_SEGOEUI_SDF_128,
        F_SEGOEUI_SDF_64,
        F_SEGOEUI_SDF_32,
        F_SEGOEUI_SDF_16,
        F_SEGOEUI_BMP_128,
        F_SEGOEUI_BMP_64,
        F_SEGOEUI_BMP_32,
        F_SEGOEUI_BMP_16,
        F_END,

        B_START = 16384,
        B_WORLD,
        B_CAM_VIEWPROJ,
        B_CAM_INVVIEWPROJ,
        B_FONT,
        B_COLOR,
        B_POINT_LIGHT,
        B_DIRECTIONAL_LIGHT,
        B_ROUNDED_RECT,
        B_ROUNDED_RECT_3D,
        B_LINE_SEGMENT,
        B_LINE,
        B_QUAD_INDEX,
        B_LINE_COLORED_SEGMENT,
        B_LINE_COLORED,
        B_GRADIENT_CIRCLE_DATA,
        B_GRADIENT_CIRCLE,
        B_DASHED_CIRCLE_DATA,
        B_DASHED_CIRCLE,
        B_BORDERED_CIRCLE_DATA,
        B_BORDERED_CIRCLE,
        B_ENEMY_DRAW,
        B_ENEMY_DATA,
        B_END
    }

    public enum TextureAssets
    {
        WHITEPIXEL = Asset.TEX_WHITEPIXEL,
        DUCK = Asset.TEX_DUCK,
        TEST = Asset.TEX_TEST
    }

    public enum ShaderAssets
    {
        POS_NORM_MAP_TEX = Asset.SH_POS_NORM_MAP_TEX,
        POS_NORM_TEX = Asset.SH_POS_NORM_TEX,
        POS_TEX = Asset.SH_POS_TEX,
        DISPLACEMENT_MAP = Asset.SH_DISPLACEMENT_MAP,
        LIGHT_POINT = Asset.SH_LIGHT_POINT,
        LIGHT_DIRECTIONAL = Asset.SH_LIGHT_DIRECTIONAL,
        COMPILE = Asset.SH_COMPILE,
        DEBUG_COLOR = Asset.SH_DEBUG_COLOR,
        DEBUG_DEPTH = Asset.SH_DEBUG_DEPTH,
        DEBUG_NORMAL = Asset.SH_DEBUG_NORMAL,
        DEBUG_LIGHT = Asset.SH_DEBUG_LIGHT,
        FONT_BITMAP = Asset.SH_FONT_BITMAP,
        FONT_SDF = Asset.SH_FONT_SDF,
        ROUNDED_RECTANGLE_2D = Asset.SH_ROUNDED_RECTANGLE_2D,
        ROUNDED_RECTANGLE_3D = Asset.SH_ROUNDED_RECTANGLE_3D,
        LINE = Asset.SH_LINE,
        LINE_COLORED = Asset.SH_LINE_COLORED,
        GRADIENT_CIRCLE = Asset.SH_GRADIENT_CIRCLE,
        DASHED_CIRCLE = Asset.SH_DASHED_CIRCLE,
        BORDERED_CIRCLE = Asset.SH_BORDERED_CIRCLE,
        ENEMY = Asset.SH_ENEMY,
        TEST = Asset.SH_TEST
    }

    public enum VertexBufferAssets
    {
        QUAD_POS_TEX_UNIT = Asset.VB_QUAD_POS_TEX_UNIT,
        CIRCLE_POS_TEX_UNIT = Asset.VB_CIRLCE_POS_TEX_UNIT,
        CIRCLE_POS_TEX_NORM_UNIT = Asset.VB_CIRLCE_POS_TEX_NORM_UNIT
    }

    public enum FontAssets
    {
        CALLI_SDF_128 = Asset.F_CALLI_SDF_128,
        CALLI_SDF_64 = Asset.F_CALLI_SDF_64,
        CALLI_SDF_32 = Asset.F_CALLI_SDF_32,
        CALLI_SDF_16 = Asset.F_CALLI_SDF_16,
        CALLI_BMP_128 = Asset.F_CALLI_BMP_128,
        CALLI_BMP_64 = Asset.F_CALLI_BMP_64,
        CALLI_BMP_32 = Asset.F_CALLI_BMP_32,
        CALLI_BMP_16 = Asset.F_CALLI_BMP_16,
        SEGOEUI_SDF_128 = Asset.F_SEGOEUI_SDF_128,
        SEGOEUI_SDF_64 = Asset.F_SEGOEUI_SDF_64,
        SEGOEUI_SDF_32 = Asset.F_SEGOEUI_SDF_32,
        SEGOEUI_SDF_16 = Asset.F_SEGOEUI_SDF_16,
        SEGOEUI_BMP_128 = Asset.F_SEGOEUI_BMP_128,
        SEGOEUI_BMP_64 = Asset.F_SEGOEUI_BMP_64,
        SEGOEUI_BMP_32 = Asset.F_SEGOEUI_BMP_32,
        SEGOEUI_BMP_16 = Asset.F_SEGOEUI_BMP_16,
    }

    public enum BufferAssets
    {
        WORLD = Asset.B_WORLD,
        CAM_VIEWPROJ = Asset.B_CAM_VIEWPROJ,
        CAM_INVVIEWPROJ = Asset.B_CAM_INVVIEWPROJ,
        FONT = Asset.B_FONT,
        COLOR = Asset.B_COLOR,
        POINT_LIGHT = Asset.B_POINT_LIGHT,
        DIRECTIONAL_LIGHT = Asset.B_DIRECTIONAL_LIGHT,
        ROUNDED_RECT = Asset.B_ROUNDED_RECT,
        ROUNDED_RECT_3D = Asset.B_ROUNDED_RECT_3D,
        LINE_SEGMENT = Asset.B_LINE_SEGMENT,
        LINE = Asset.B_LINE,
        QUAD_INDEX = Asset.B_QUAD_INDEX,
        LINE_COLORED_SEGMENT = Asset.B_LINE_COLORED_SEGMENT,
        LINE_COLORED = Asset.B_LINE_COLORED,
        GRADIENT_CIRCLE_DATA = Asset.B_GRADIENT_CIRCLE_DATA,
        GRADIENT_CIRCLE = Asset.B_GRADIENT_CIRCLE,
        DASHED_CIRCLE_DATA = Asset.B_DASHED_CIRCLE_DATA,
        DASHED_CIRCLE = Asset.B_DASHED_CIRCLE,
        BORDERED_CIRCLE_DATA = Asset.B_BORDERED_CIRCLE_DATA,
        BORDERED_CIRCLE = Asset.B_BORDERED_CIRCLE,
        ENEMY_DRAW = Asset.B_ENEMY_DRAW,
        ENEMY_DATA = Asset.B_ENEMY_DATA
    }

    public abstract class AssetHelper
    {
        public static string GetPath(Asset asset)
        {
            switch (asset)
            {
                case Asset.SH_COMPILE:
                    return "Content/Effects/Compile.fx";
                case Asset.SH_DEBUG_COLOR:
                    return "Content/Effects/ColorDebug.fx";
                case Asset.SH_DEBUG_DEPTH:
                    return "Content/Effects/DepthDebug.fx";
                case Asset.SH_DEBUG_LIGHT:
                    return "Content/Effects/LightDebug.fx";
                case Asset.SH_DEBUG_NORMAL:
                    return "Content/Effects/NormalDebug.fx";
                case Asset.SH_DISPLACEMENT_MAP:
                    return "Content/Effects/DisplacementMap.fx";
                case Asset.SH_LIGHT_POINT:
                    return "Content/Effects/PointLight.fx";
                case Asset.SH_LIGHT_DIRECTIONAL:
                    return "Content/Effects/DirectionalLight.fx";
                case Asset.SH_POS_NORM_MAP_TEX:
                    return "Content/Effects/PosNormMapTex.fx";
                case Asset.SH_POS_NORM_TEX:
                    return "Content/Effects/PosNormTex.fx";
                case Asset.SH_POS_TEX:
                    return "Content/Effects/PosTex.fx";
                case Asset.SH_FONT_BITMAP:
                    return "Content/Effects/FontBitmap.fx";
                case Asset.SH_FONT_SDF:
                    return "Content/Effects/FontSDF.fx";
                case Asset.SH_ROUNDED_RECTANGLE_2D:
                    return "Content/Effects/RoundedRectangle2D.fx";
                case Asset.SH_ROUNDED_RECTANGLE_3D:
                    return "Content/Effects/RoundedRectangle.fx";
                case Asset.SH_LINE:
                    return "Content/Effects/Line.fx";
                case Asset.SH_LINE_COLORED:
                    return "Content/Effects/LineColored.fx";
                case Asset.SH_GRADIENT_CIRCLE:
                    return "Content/Effects/GradientCircle.fx";
                case Asset.SH_DASHED_CIRCLE:
                    return "Content/Effects/DashedCircle.fx";
                case Asset.SH_BORDERED_CIRCLE:
                    return "Content/Effects/BorderedCircle.fx";
                case Asset.SH_ENEMY:
                    return "Content/Effects/Enemy.fx";
                case Asset.SH_TEST:
                    return "Content/Effects/TestEffect.fx";
                case Asset.TEX_WHITEPIXEL:
                    return "Content/Textures/WhitePixel.bmp";
                case Asset.TEX_DUCK:
                    return "Content/Textures/angelduck.png";
                case Asset.TEX_TEST:
                    return "Content/Textures/test.png";
                case Asset.F_CALLI_SDF_128:
                    return "Content/Fonts/callisdf128.cyf";
                case Asset.F_CALLI_SDF_64:
                    return "Content/Fonts/callisdf64.cyf";
                case Asset.F_CALLI_SDF_32:
                    return "Content/Fonts/callisdf32.cyf";
                case Asset.F_CALLI_SDF_16:
                    return "Content/Fonts/callisdf16.cyf";
                case Asset.F_CALLI_BMP_128:
                    return "Content/Fonts/callibmp128.cyf";
                case Asset.F_CALLI_BMP_64:
                    return "Content/Fonts/callibmp64.cyf";
                case Asset.F_CALLI_BMP_32:
                    return "Content/Fonts/callibmp32.cyf";
                case Asset.F_CALLI_BMP_16:
                    return "Content/Fonts/callibmp16.cyf";
                case Asset.F_SEGOEUI_SDF_128:
                    return "Content/Fonts/segoeuisdf128.cyf";
                case Asset.F_SEGOEUI_SDF_64:
                    return "Content/Fonts/segoeuisdf64.cyf";
                case Asset.F_SEGOEUI_SDF_32:
                    return "Content/Fonts/segoeuisdf32.cyf";
                case Asset.F_SEGOEUI_SDF_16:
                    return "Content/Fonts/segoeuisdf16.cyf";
                case Asset.F_SEGOEUI_BMP_128:
                    return "Content/Fonts/segoeuibmp128.cyf";
                case Asset.F_SEGOEUI_BMP_64:
                    return "Content/Fonts/segoeuibmp64.cyf";
                case Asset.F_SEGOEUI_BMP_32:
                    return "Content/Fonts/segoeuibmp32.cyf";
                case Asset.F_SEGOEUI_BMP_16:
                    return "Content/Fonts/segoeuibmp16.cyf";
                default:
                    throw new Exception("Missing asset path case! " + asset);
            }
        }

        public static InputElement[] GetVertexElements(ShaderAssets shader)
        {
            switch (shader)
            {
                case ShaderAssets.COMPILE:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.DEBUG_COLOR:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.DEBUG_DEPTH:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.DEBUG_LIGHT:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.DEBUG_NORMAL:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.DISPLACEMENT_MAP:
                    return VertexPositionNormalMapTexture.vertexElements;
                case ShaderAssets.LIGHT_POINT:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.LIGHT_DIRECTIONAL:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.POS_NORM_MAP_TEX:
                    return VertexPositionNormalMapTexture.vertexElements;
                case ShaderAssets.POS_NORM_TEX:
                    return VertexPositionNormalTexture.vertexElements;
                case ShaderAssets.POS_TEX:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.FONT_BITMAP:
                    return null;
                case ShaderAssets.FONT_SDF:
                    return null;
                case ShaderAssets.LINE:
                    return null;
                case ShaderAssets.LINE_COLORED:
                    return null;
                case ShaderAssets.GRADIENT_CIRCLE:
                    return null;
                case ShaderAssets.DASHED_CIRCLE:
                    return null;
                case ShaderAssets.BORDERED_CIRCLE:
                    return null;
                case ShaderAssets.ENEMY:
                    return null;
                case ShaderAssets.ROUNDED_RECTANGLE_3D:
                    return null;
                case ShaderAssets.ROUNDED_RECTANGLE_2D:
                    return VertexPositionTexture.vertexElements;
                case ShaderAssets.TEST:
                    return VertexPositionTexture.vertexElements;
                default:
                    throw new Exception("Missing shader vert ele case! " + shader);
            }
        }
    }
}
