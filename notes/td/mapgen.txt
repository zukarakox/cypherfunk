Goal of map generation is to have a controllable level of 'difficulty' with a degree of randomness providing new flavor.

Difficulty should be represented as a number, which increases linearly as the player continues to beat levels.

A good map gen system should:
--A 'consistent' player should always reach around the same highest-difficulty--
	a. Every level should be beatable, up until 'perfect play' difficulty.
		(Levels won't go on infinitely, so there will be a point where it's just not possible to win.)
	b. There shouldn't be large difficulty swings depending on key map features having a 'good roll'
		(If we only generate a single boost zone per level, it shouldn't matter *which* boost gets generated)


Current gen plan--
'Map Difficutly' we care about it represented as 0 to 100
Difficutly can go beyond 100, where existing trends just keep going, but no major map generation shifts occur.
	(Everything in unlocked, we're just tweaking numbers)

Tweakable map gen features:
	Map scale
		= From ~15x15 min size to ~50x50 max size
	Gold
		= A 'full' maze will be around map scale * 10
		= Should give significantly less than that on easier waves, as using large amounts of gold effectively is difficult!
		= Should scale up with difficutly (past the full point even, to allow for many upgrades)
		= Should be calculated after other major features, varying to compensate
			(Say you can get 1 to 3 boost zones -- all else equal, 1 boost should have more gold than 3 boost)
	Enemies
		- Numbers
		- Affixes (per mob, and number of spawns)
		- Scaling (difficulty)
			= Should go up to compensate for map features as the occur
	Travel Nodes
		- Number
		- Min distance between path nodes
		- Min distance between all nodes
		- Node size
	Teleporters
		- Number
		- Min Distance
		- Ent/Exit size
	Boost Zones
		- Number
		- Radius
		- Strength
		- Position (How much is available to build on...)
	Pathing Blockers (rocks)
		- Number
		- Scale

Division of 'Map Difficulty' features:
	0 - 10 = 'Easiest Mode'
		- Scale: ~15x15 to ~20x20
		- Three Travel Nodes
		- One Enemy Spawn
		- 0-1 of:
			- Teleporter
			- Boost Zone
			- Enemy Affix
	10 - 20 = 'Easy Mode'
		- Scale: ~18x18 to ~22x22
	20 - 30 = 'Less Easy'
		- Scale: ~21x21 to ~25x25
	30 - 40 = 'Almost Medium'
		- Scale: ~23x23 to ~27x27
	40 - 50 = 'Medium'
		- Scale: ~27x27 to ~31x31
		- 4 to 6 Travel Nodes
		- 1-2 Spawns, with 2x Affixes
		- 0-2 Teleporters
		- 0-2 Boost Zones
	50 - 60 = 'Mediumish'
		- Scale: ~32x32 to ~36x36
		- First possible 3x Affix
	60 - 70 = 'Tepid'
		- Scale: ~36x36 to ~40x40
	70 - 80 = 'Almost Hard'
		- Scale: ~39x39 to ~43x43
	80 - 90 = 'Hard'
		- Scale: ~43x43 to ~47x47
	90 - 100 = 'Very Hard'
		- Scale: ~46x46 to ~50x50
		- 6 to 10 Travel Nodes
		- 1-3 Spawns, with 3x Affixes
		- 1-3 Teleporters
		- 1-3 Boost Zones