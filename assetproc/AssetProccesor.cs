﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using SharpFont;
using System.IO;
using assetmanager;

using Encoding = System.Text.Encoding;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

namespace assetproc
{
    class AssetProccesor
    {
        static string root = @"C:\Users\Owner\Source\Repos\cypherfunk\";
        static string contentDir = @"cypherfunk\Content\";
        static string fontDir = @"Fonts\";

        #region Font Gen
        static void MakeAllFonts()
        {
            string basePath = root + contentDir + fontDir;
            string supportedChars = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=[];',./\\~!@#$%^&*()_+{}:\"<>?|";

            MakeBmpFontSet(@"Content\Fonts\segoeui.ttf", basePath, "segoeui", supportedChars);
            MakeBmpFontSet(@"Content\Fonts\calligraphic-421-bt.ttf", basePath, "calli", supportedChars);

            MakeSDFFontSet(@"Content\Fonts\segoeui.ttf", basePath, "segoeui", supportedChars);
            MakeSDFFontSet(@"Content\Fonts\calligraphic-421-bt.ttf", basePath, "calli", supportedChars);
        }

        static void MakeBmpFontSet(string fontPath, string outputPath, string name, string supportedChars)
        {
            int maxAtlasWidth = 1024;
            int packingBuffer = 4; //number of blank pixels added on all sides of a char in the atlas to ensure linear sampling doesn't pick up another char
            int renderHeight = 1024;
            float searchDist = 64f;

            var p = new FontProcessor(false, supportedChars, maxAtlasWidth, packingBuffer, renderHeight, 16, searchDist);
            p.ProcessFont(fontPath, outputPath + name + "bmp16.cyf");

            p = new FontProcessor(false, supportedChars, maxAtlasWidth, packingBuffer, renderHeight, 32, searchDist);
            p.ProcessFont(fontPath, outputPath + name + "bmp32.cyf");

            p = new FontProcessor(false, supportedChars, maxAtlasWidth, packingBuffer, renderHeight, 64, searchDist);
            p.ProcessFont(fontPath, outputPath + name + "bmp64.cyf");

            p = new FontProcessor(false, supportedChars, maxAtlasWidth, packingBuffer, renderHeight, 128, searchDist);
            p.ProcessFont(fontPath, outputPath + name + "bmp128.cyf");
        }

        static void MakeSDFFontSet(string fontPath, string outputPath, string name, string supportedChars)
        {
            int maxAtlasWidth = 1024;
            int packingBuffer = 4; //number of blank pixels added on all sides of a char in the atlas to ensure linear sampling doesn't pick up another char

            var p = new FontProcessor(true, supportedChars, maxAtlasWidth, packingBuffer, 128, 16, 64f);
            p.ProcessFont(fontPath, outputPath + name + "sdf16.cyf");

            p = new FontProcessor(true, supportedChars, maxAtlasWidth, packingBuffer, 256, 32, 64);
            p.ProcessFont(fontPath, outputPath + name + "sdf32.cyf");

            p = new FontProcessor(true, supportedChars, maxAtlasWidth, packingBuffer, 512, 64, 64f);
            p.ProcessFont(fontPath, outputPath + name + "sdf64.cyf");

            p = new FontProcessor(true, supportedChars, maxAtlasWidth, packingBuffer, 1024, 128, 64f);
            p.ProcessFont(fontPath, outputPath + name + "sdf128.cyf");
        }
#endregion

        static void MakeAssetBlob(string outputFile)
        {
            string basePath = root + @"cypherfunk\";
            
            //list of temp files we've created during asset processing
            //we delete them after we're done
            List<string> tempFiles = new List<string>();

            using (BinaryWriter wr = new BinaryWriter(new FileStream(outputFile, FileMode.Create), Encoding.Unicode))
            {
                List<Asset> fullAssetList = new List<Asset>();
                List<FileInfo> assetPaths = new List<FileInfo>();
                
                //first build the list of all assets and files, so we can write the blob header
                for (Asset a = Asset.F_START + 1; a < Asset.F_END; a++)
                {
                    fullAssetList.Add(a);
                    assetPaths.Add(new FileInfo(basePath + AssetHelper.GetPath(a)));
                }

                //for shaders specifically, we want to compile them and save the bytecode
                //so compile each shader asset, save the bytecode to a temporary file
                for (Asset a = Asset.SH_START + 1; a < Asset.SH_END; a++)
                {
                    string tempName = "shader" + (int)a + ".tmp";
                    string shPath = basePath + AssetHelper.GetPath(a);

                    using (BinaryWriter shWr = new BinaryWriter(new FileStream(tempName, FileMode.Create)))
                    {
                        CompilationResult vs = ShaderBytecode.CompileFromFile(shPath, "VS", "vs_5_0", ShaderFlags.OptimizationLevel3, EffectFlags.None);
                        CompilationResult ps = ShaderBytecode.CompileFromFile(shPath, "PS", "ps_5_0", ShaderFlags.OptimizationLevel3, EffectFlags.None);

                        byte[] vsBytes = vs.Bytecode.Data;
                        byte[] psBytes = ps.Bytecode.Data;
                        
                        shWr.Write(vsBytes.Length);
                        shWr.Write(vsBytes);
                        shWr.Write(psBytes.Length);
                        shWr.Write(psBytes);
                    }

                    fullAssetList.Add(a);
                    assetPaths.Add(new FileInfo(tempName));
                    tempFiles.Add(tempName);
                }

                for (Asset a = Asset.TEX_START + 1; a < Asset.TEX_END; a++)
                {
                    fullAssetList.Add(a);
                    assetPaths.Add(new FileInfo(basePath + AssetHelper.GetPath(a)));
                }

                //write the blob header
                //first the number of assets stored within
                wr.Write(fullAssetList.Count);

                //write the per-asset header data
                long offset = 0;
                for (int i = 0; i < fullAssetList.Count; i++)
                {
                    wr.Write((int)fullAssetList[i]); //enum id
                    wr.Write(offset); //offset from end of header
                    wr.Write(assetPaths[i].Length); //length of file

                    offset += assetPaths[i].Length;
                }

                //now write each asset sequentially
                offset = 0;
                for (int i = 0; i < fullAssetList.Count; i++)
                {
                    byte[] bytes = File.ReadAllBytes(assetPaths[i].FullName);
                    
                    wr.BaseStream.Write(bytes, 0, bytes.Length);

                    offset += bytes.Length;
                }
            }

            //clean up the temp files
            for (int i = 0; i < tempFiles.Count; i++)
            {
                File.Delete(tempFiles[i]);
            }
        }

        static void FullBuild()
        {
            MakeAllFonts();

            MakeAssetBlob(root + contentDir + "blob.cy");
        }

        static void Main(string[] args)
        {
            MakeAssetBlob(root + contentDir + "blob.cy");
        }
    }
}
