﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using SharpFont;

namespace assetproc
{
    class FontProcessor
    {
        public class GlyphData
        {
            //char metrics
            public float bearingX;
            public float bearingY;
            public float advanceX;

            public float gWidth;
            public float gHeight;

            public Dictionary<char, float> kerningMap = new Dictionary<char, float>();

            //Atlas positioning
            public int aPosX;
            public int aPosY;

            public float aWidth;
            public float aHeight;

            public GlyphData()
            {

            }
        }

        //string of supported characters
        //whitespace characters handled specially by the renderer
        //space is in sup char list to gen kerning/advanceX info
        //tab should just be expanded to howevermany spaces, newlines handled however
        readonly string supportedChars = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ`1234567890-=[];',./\\~!@#$%^&*()_+{}:\"<>?|";
        readonly int maxAtlasWidth = 1024;
        readonly int packingBuffer = 4; //number of blank pixels added on all sides of a char in the atlas to ensure linear sampling doesn't pick up another char
        readonly int renderHeight = 4096;
        readonly int atlasHeight = 16;
        readonly float sdfRange = 128f;
        readonly float heightRatio;
        float ascenderHeight;
        float descenderHeight;
        readonly Library lib;

        bool isSDF = false;

        Dictionary<char, GlyphData> glyphs = new Dictionary<char, GlyphData>();

        public FontProcessor(bool isSDF, string chars, int maxAtlasWidth, int packingBuffer, int renderHeight, int atlasHeight, float sdfRange)
        {
            this.isSDF = isSDF;
            this.supportedChars = chars;
            this.maxAtlasWidth = maxAtlasWidth;
            this.packingBuffer = packingBuffer;
            this.renderHeight = renderHeight;
            this.atlasHeight = atlasHeight;
            this.sdfRange = sdfRange;

            heightRatio = 1 / (float)renderHeight;

            lib = new Library();
        }

        public void ProcessFont(string path, string outPath)
        {
            Face face = new Face(lib, path);
            face.SetPixelSizes(0, (uint)renderHeight);

            BuildGlyphList(face, supportedChars);

            int atlasWidth = 0;
            int atlasHeight = 0;

            GetAtlasDim(face, supportedChars, out atlasWidth, out atlasHeight);

            Bitmap bOut = new Bitmap(atlasWidth, atlasHeight);
            Graphics g = Graphics.FromImage(bOut);

            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            g.Clear(Color.Transparent);

            RenderAtlas(face, supportedChars, g);

            SaveFont(bOut, outPath);

            bOut.Save(outPath + ".bmp");
        }

        /// <summary>
        /// Saves the processed font to a packed file.
        /// Format:
        ///     1. Font Data
        ///         int32 type: mostly for possible future use
        ///             0 = normal atlas
        ///             1 = sdf atlas
        ///         int32 AtlasHeight
        ///         float ascenderHeight
        ///         float descenderHeight
        ///         int32 width
        ///         int32 height
        ///         int32 numChars
        ///         GlyphData[] chars
        ///             char c
        ///             int32 atlasPosX
        ///             int32 atlasPosY
        ///             float aWidth
        ///             float aHeight
        ///             float bearingX
        ///             float bearingY
        ///             float advanceX
        ///             float gWidth
        ///             float gHeight
        ///             int32 numKerns
        ///             Kern[] kernChars
        ///                 char next
        ///                 float kern
        /// 
        ///         byte[] RGBA bitmap
        /// 
        /// </summary>
        private void SaveFont(Bitmap atlas, string outPath)
        {
            using (BinaryWriter wr = new BinaryWriter(new FileStream(outPath, FileMode.Create), System.Text.Encoding.Unicode))
            {
                int type = 0;
                if (isSDF)
                    type = 1;

                wr.Write(type);
                wr.Write(atlasHeight);
                wr.Write(ascenderHeight);
                wr.Write(descenderHeight);
                wr.Write(packingBuffer);
                wr.Write(atlas.Width);
                wr.Write(atlas.Height);
                wr.Write(glyphs.Count);

                foreach (KeyValuePair<char, GlyphData> vals in glyphs)
                {
                    GlyphData g = vals.Value;
                    wr.Write(vals.Key);
                    wr.Write(g.aPosX);
                    wr.Write(g.aPosY);
                    wr.Write(g.aWidth);
                    wr.Write(g.aHeight);
                    wr.Write(g.bearingX);
                    wr.Write(g.bearingY);
                    wr.Write(g.advanceX);
                    wr.Write(g.gWidth);
                    wr.Write(g.gHeight);
                    wr.Write(g.kerningMap.Count);

                    foreach (KeyValuePair<char, float> kerns in g.kerningMap)
                    {
                        wr.Write(kerns.Key);
                        wr.Write(kerns.Value);
                    }
                }

                int stride = atlas.Width * 4;
                for (int y = 0; y < atlas.Height; y++)
                {
                    int yi = y * stride;
                    for (int x = 0; x < atlas.Width; x++)
                    {
                        int index = yi + x * 4;
                        Color c = atlas.GetPixel(x, y);

                        wr.Write(c.R);
                        wr.Write(c.G);
                        wr.Write(c.B);
                        wr.Write(c.A);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the texture atlas dimensions. Packs the characters as closely as possible to eachother, ignoring the baseline.
        /// This should mirror render atlas code pretty closely.
        /// </summary>
        private void GetAtlasDim(Face f, string chars, out int x, out int y)
        {
            int width = 0;
            int height = 0;
            int totalHeight = 0;
            bool maxWidth = false;

            for (int i = 0; i < chars.Length; i++)
            {
                int w = 0;
                int h = 0;

                getCharAtlasDim(f, chars[i], out w, out h);

                if (w == packingBuffer * 2 || h == packingBuffer * 2)
                    continue;

                if (width + w > maxAtlasWidth)
                {
                    maxWidth = true;
                    width = w;
                    totalHeight += height;
                    height = h;
                }
                else
                {
                    width += w;
                    height = Math.Max(height, h);
                }
            }

            x = maxWidth ? maxAtlasWidth : width;
            y = totalHeight + height;
        }

        /// <summary>
        /// Gets the char bitmap dimensions, rounded up to nearest pixel integer with added packing buffer.
        /// </summary>
        private void getCharAtlasDim(Face f, char a, out int x, out int y)
        {
            uint gid = f.GetCharIndex(a);
            f.LoadGlyph(gid, LoadFlags.Default, LoadTarget.Normal);
            
            x = (int)Math.Ceiling((float)f.Glyph.Metrics.Width * heightRatio * atlasHeight) + packingBuffer * 2;
            y = (int)Math.Ceiling((float)f.Glyph.Metrics.Height * heightRatio * atlasHeight) + packingBuffer * 2;
        }

        /// <summary>
        /// Renders the character atlas.
        /// Should mirror the atlas dim code pretty closely.
        /// </summary>
        private void RenderAtlas(Face f, string chars, Graphics g)
        {
            int maxHeight = 0;
            int x = 0;
            int y = 0;

            for (int i = 0; i < chars.Length; i++)
            {
                int w = 0;
                int h = 0;

                getCharAtlasDim(f, chars[i], out w, out h);

                if (w == packingBuffer * 2 || h == packingBuffer * 2)
                {
                    var glyph = glyphs[chars[i]];
                    glyph.aPosX = x;
                    glyph.aPosY = y;
                    continue;
                }

                if (x + w > maxAtlasWidth)
                {
                    x = 0;
                    y += maxHeight;
                    maxHeight = h;
                }
                else
                {
                    maxHeight = Math.Max(maxHeight, h);
                }
                
                RenderChar(f, chars[i], g, x, y, w, h);
                x += w;
            }
        }
        
        private void RenderChar(Face f, char a, Graphics g, int x, int y, int rW, int rH)
        {
            if (isSDF)
            {
                uint gid = f.GetCharIndex(a);
                f.LoadGlyph(gid, LoadFlags.NoHinting, LoadTarget.Normal);
                f.Glyph.RenderGlyph(RenderMode.Normal);

                FTBitmap ftbmp = f.Glyph.Bitmap;
                Bitmap bmp = ftbmp.ToGdipBitmap(Color.White);

                int width = bmp.Width;
                int height = bmp.Height;
                byte[] sdf = new byte[rW * rH];
                byte[] src = new byte[width * height];

                //prep bitmaps
                for (int h = 0; h < height; h++)
                {
                    int hi = h * width;
                    for (int w = 0; w < width; w++)
                    {
                        src[hi + w] = bmp.GetPixel(w, h).A;
                    }
                }

                //calc the sdf
                int mul = renderHeight / atlasHeight;
                int mulh = mul / 2;
                int s = (int)Math.Ceiling(sdfRange);
                for (int h = 0; h < rH; h++)
                {
                    int hi = h * rW;
                    for (int w = 0; w < rW; w++)
                    {
                        int iSDF = hi + w;

                        //xy for center of our sample
                        int ch = (h - packingBuffer) * mul + mulh;
                        int cw = (w - packingBuffer) * mul + mulh;

                        //first, get the average alpha
                        //of all of the pixels our sdfpixel is covering in the bmp
                        int numPix = 0;
                        int sumAlpha = 0;

                        int minh = ch - mulh;
                        int maxh = ch + mulh;
                        int minw = cw - mulh;
                        int maxw = cw + mulh;
                        for (int hh = minh; hh < maxh; hh++)
                        {
                            int hi2 = hh * width;
                            for (int ww = minw; ww < maxw; ww++)
                            {
                                if (hh < 0 || hh >= height || ww < 0 || ww >= width)
                                {
                                }
                                else
                                {
                                    sumAlpha += src[hi2 + ww];
                                }
                                numPix++;
                            }
                        }

                        float avgAlpha = sumAlpha / (float)numPix;

                        //now find closest opposite-alpha pixel in bmp
                        if (avgAlpha < 128)
                        {
                            float cl = float.PositiveInfinity;
                            minh = Math.Max(0, ch - s);
                            maxh = Math.Min(height, ch + s);
                            minw = Math.Max(0, cw - s);
                            maxw = Math.Min(width, cw + s);
                            for (int hh = minh; hh < maxh; hh++)
                            {
                                int hi2 = hh * width;
                                for (int ww = minw; ww < maxw; ww++)
                                {
                                    if (src[hi2 + ww] >= 128)
                                    {
                                        float dx = ww - cw + 0.5f;
                                        float dy = hh - ch + 0.5f;
                                        float d = dx * dx + dy * dy;

                                        if (d < cl)
                                            cl = d;
                                    }
                                }
                            }

                            sdf[iSDF] = (byte)(128 - (byte)Math.Min(128, 128 * Math.Sqrt(cl) / sdfRange));
                        }
                        else if (avgAlpha > 128)
                        {
                            float cl = float.PositiveInfinity;
                            minh = ch - s;
                            maxh = ch + s;
                            minw = cw - s;
                            maxw = cw + s;
                            for (int hh = minh; hh < maxh; hh++)
                            {
                                int hi2 = hh * width;
                                for (int ww = minw; ww < maxw; ww++)
                                {
                                    if (hh < 0 || hh >= height || ww < 0 || ww >= width)
                                    {//if we're out of image bounds, that counts as 0 alpha
                                        float dx = ww - cw + 0.5f;
                                        float dy = hh - ch + 0.5f;
                                        float d = dx * dx + dy * dy;

                                        if (d < cl)
                                            cl = d;
                                    }
                                    else
                                    {
                                        if (src[hi2 + ww] <= 128)
                                        {
                                            float dx = ww - cw + 0.5f;
                                            float dy = hh - ch + 0.5f;
                                            float d = dx * dx + dy * dy;

                                            if (d < cl)
                                                cl = d;
                                        }
                                    }
                                }
                            }

                            sdf[iSDF] = (byte)(128 + (byte)Math.Min(127, 127 * Math.Sqrt(cl) / sdfRange));
                        }
                        else
                        {
                            sdf[iSDF] = 128;
                        }
                    }
                }

                Bitmap bSDF = new Bitmap(rW, rH);

                for (int h = 0; h < rH; h++)
                {
                    int hi = h * rW;
                    for (int w = 0; w < rW; w++)
                    {
                        bSDF.SetPixel(w, h, Color.FromArgb(sdf[hi + w], 255, 255, 255));
                    }
                }


                g.DrawImageUnscaled(bSDF, x, y);

                var glyph = glyphs[a];
                glyph.aPosX = x + packingBuffer;
                glyph.aPosY = y + packingBuffer;
            }
            else
            {
                f.SetPixelSizes(0, (uint)atlasHeight);
                uint gid = f.GetCharIndex(a);
                f.LoadGlyph(gid, LoadFlags.NoHinting, LoadTarget.Normal);
                f.Glyph.RenderGlyph(RenderMode.Normal);

                FTBitmap ftbmp = f.Glyph.Bitmap;
                Bitmap bmp = ftbmp.ToGdipBitmap(Color.White);

                g.DrawImageUnscaled(bmp, x + packingBuffer, y + packingBuffer);

                var glyph = glyphs[a];
                glyph.aPosX = x + packingBuffer;
                glyph.aPosY = y + packingBuffer;
                f.SetPixelSizes(0, (uint)renderHeight);
            }
        }

        private void BuildGlyphList(Face f, string chars)
        {
            float r = heightRatio; // 1f / ((float)f.Size.Metrics.Ascender - (float)f.Size.Metrics.Descender);

            ascenderHeight = (float)f.Size.Metrics.Ascender * r;
            descenderHeight = (float)f.Size.Metrics.Descender * r;

            Console.WriteLine("test: " + f.Size.Metrics.Ascender + " " + f.Size.Metrics.Descender + " " + f.Size.Metrics.Height + " " + f.Size.Metrics.ScaleX + " " + f.Size.Metrics.ScaleY + " " + f.Size.Metrics.NominalHeight);

            for (int i = 0; i < chars.Length; i++)
            {
                uint gid = f.GetCharIndex(chars[i]);
                f.LoadGlyph(gid, LoadFlags.NoHinting, LoadTarget.Normal);

                var g = new GlyphData();
                g.advanceX = (float)f.Glyph.Metrics.HorizontalAdvance * r;
                g.bearingX = (float)f.Glyph.Metrics.HorizontalBearingX * r;
                g.bearingY = (float)f.Glyph.Metrics.HorizontalBearingY * r;
                g.gWidth = (float)f.Glyph.Metrics.Width * r;
                g.gHeight = (float)f.Glyph.Metrics.Height * r;
                g.aWidth = (float)f.Glyph.Metrics.Width * heightRatio * atlasHeight;
                g.aHeight = (float)f.Glyph.Metrics.Height * heightRatio * atlasHeight;

                for (int t = 0; t < chars.Length; t++)
                {
                    uint id = f.GetCharIndex(chars[t]);
                    g.kerningMap.Add(chars[t], (float)f.GetKerning(gid, id, KerningMode.Unfitted).X * r);
                }

                glyphs.Add(chars[i], g);
            }
        }
    }
}
