﻿using deck;
using log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace freeside
{
    /// <summary>
    /// Game server!
    /// </summary>
    class Freeside : IFreesideFromCypher, IFreesideFromFreesidemaster, IFreesideFromProxy
    {
        static void Main(string[] args)
        {
            new Freeside("freeside1.cypherfunk.com", 8013);
        }

        FreesidetoProxyHost proxyHost;
        FreesidetoFreesidemasterClient masterClient;
        FreesidetoCypherHost cypherHost;

        readonly string privHostname;
        readonly int privPort;
        Freeside(string privHostname, int privPort)
        {
            this.privHostname = privHostname;
            this.privPort = privPort;

            proxyHost = new FreesidetoProxyHost(this, privPort, this, out cypherHost);
            masterClient = new FreesidetoFreesidemasterClient(this);

            new Thread(Run).Start();
        }

        public void cypher_onConnectionChange(ProxConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "cypher client connection change. New Status: " + status);
            if (status == ConnectionStatus.CONNECTED)
            {
                cypherHost.sendObjPos(conn, "naw brah", 80085);
            }
        }

        public void cypher_PlayerInput(ProxConn conn, string msg, int b)
        {
            Logger.WriteLine(LogType.DEBUG, "Got message from cypher?: " + msg + " " + b);
        }

        public void freesidemaster_onConnectionChange(FreesidetoFreesidemasterClient conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Freesidemaster connection change. New Status: " + status);
            if (status == ConnectionStatus.CONNECTED)
            {
                masterClient.sendPublicInformation(privHostname, privPort);
            }
        }

        public void proxy_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "proxy client connection change. New Status: " + status);
        }

        void Run()
        {
            while (true)
            {
                proxyHost.Update();
                masterClient.Update();
                Thread.Sleep(1);
            }
        }
    }
}
