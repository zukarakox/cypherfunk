﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using SharpDX.Direct3D11;
using System.IO;

using assetmanager;
using log;

namespace cypherfunk
{
    class AssetManager
    {
        #region AssetBlob Reader
        /// <summary>
        /// A class for reading from one of our asset blob files.
        /// Keeps a file handle open until disposed.
        /// </summary>
        class AssetBlob : IDisposable
        {
            struct AssetDat
            {
                public long offset;
                public long len;

                public AssetDat(long offset, long len)
                {
                    this.offset = offset;
                    this.len = len;
                }
            }

            Dictionary<Asset, AssetDat> assetToOffset = new Dictionary<Asset, AssetDat>();
            Stream stream;
            long headerOffset;
            LimitStream pubStream;
            
            public AssetBlob(string file)
            {
                stream = new FileStream(file, FileMode.Open);

                //read the header
                BinaryReader fr = new BinaryReader(stream, Encoding.Unicode, true);
                int numAssets = fr.ReadInt32();

                for (int i = 0; i < numAssets; i++)
                {
                    Asset a = (Asset)fr.ReadInt32();
                    long offset = fr.ReadInt64();
                    long len = fr.ReadInt64();

                    assetToOffset.Add(a, new AssetDat(offset, len));
                }

                fr.Dispose();
                
                //finish init
                headerOffset = stream.Position;
                pubStream = new LimitStream(stream);
            }

            public Stream getAssetStream(Asset a)
            {
                AssetDat dat;
                if (assetToOffset.TryGetValue(a, out dat))
                {
                    pubStream.SetLimits(headerOffset + dat.offset, dat.len);
                    return pubStream;
                }

                return null;
            }

            public void Dispose()
            {
                stream.Dispose();
            }
        }
        #endregion

        private Dictionary<Asset, IDisposable> loadedAssets = new Dictionary<Asset, IDisposable>();
        private GameStage stage;
        private AssetBlob blob;

        private bool inPreload = false;
        private bool inLoad = false;
        private HashSet<Asset> assetsAddedDuringLoad = new HashSet<Asset>();

        public AssetManager(GameStage stage, string blob)
        {
            this.stage = stage;
            this.blob = new AssetBlob(blob);
        }

        /// <summary>
        /// Called before the main load, to verify all assets required for the loading screen are already in memory.
        /// Should be called from main thread.
        /// </summary>
        /// <param name="preloadAssets">Assets needed for use while loading</param>
        public void PreLoad(HashSet<Asset> preloadAssets)
        {
            if (inLoad)
                throw new ArgumentException("Called PreLoad while inLoad is true");
            if (inPreload)
                throw new ArgumentException("Called PreLoad while inPreload is true");

            inPreload = true;

            //we want to load loading screen assets
            var s_toLoad = new HashSet<Asset>(preloadAssets.Except(loadedAssets.Keys));

            foreach (Asset a in s_toLoad)
            {
                loadedAssets.Add(a, loadAsset(a));
            }
        }

        /// <summary>
        /// Should be called from load thread.
        /// </summary>
        /// <param name="keepAssets"></param>
        public void StartLoad(HashSet<Asset> keepAssets, HashSet<Asset> preloadAssets)
        {
            if (inLoad)
                throw new ArgumentException("Called StartLoad while inLoad is true");
            if (!inPreload)
                throw new ArgumentException("Called StartLoad while inPreload is false");

            inPreload = false;
            inLoad = true;

            //here we want to load everything requested, and dispose things not needed anymore
            var s_Keep = new HashSet<Asset>(keepAssets.Union(preloadAssets));
            var s_toLoad = new HashSet<Asset>(s_Keep.Except(loadedAssets.Keys));
            var s_toDipose = new HashSet<Asset>(loadedAssets.Keys.Except(s_Keep));

            foreach (Asset s in s_toDipose)
            {
                loadedAssets[s].Dispose();
                loadedAssets.Remove(s);
            }

            foreach (Asset a in s_toLoad)
            {
                loadedAssets.Add(a, loadAsset(a));
            }

            assetsAddedDuringLoad.Clear();
        }

        /// <summary>
        /// Called after load is finished, to dispose of assets needed during load but not elsewhere.
        /// Should be called from main thread.
        /// </summary>
        /// <param name="keepAssets">Assets needed in general</param>
        /// <param name="preloadAssets">Assets needed during load</param>
        public void EndLoad(HashSet<Asset> keepAssets, HashSet<Asset> preloadAssets)
        {
            if (!inLoad)
                throw new ArgumentException("Called EndLoad while inLoad is false");
            if (inPreload)
                throw new ArgumentException("Called EndLoad while inPreload is true"); //???

            inLoad = false;

            //the assets we needed during load, but not those needed after
            //have to be careful with assets that were dynamically added during load time
            var s_toDipose = new HashSet<Asset>(preloadAssets.Except(keepAssets.Union(assetsAddedDuringLoad)));

#if DEBUG
            foreach (Asset a in assetsAddedDuringLoad.Except(keepAssets))
            {
                Logger.WriteLine(LogType.DEBUG, "Asset added dynamically during load: " + a);
            }
#endif

            foreach (Asset s in s_toDipose)
            {
                loadedAssets[s].Dispose();
                loadedAssets.Remove(s);
            }

            assetsAddedDuringLoad.Clear();
        }

        private IDisposable loadAsset(Asset a)
        {
#if DEBUG
            if (!inLoad && !inPreload)
                Logger.WriteLine(LogType.ERROR, "Loading resource during runtime, add it to load list: " + a);
#else
            if (!inLoad && !inPreload)
                throw new Exception("Loading an asset while not in asset load mode?");
#endif

            if (a > Asset.SH_START && a < Asset.SH_END)
                return loadShader((ShaderAssets)a);
            if (a > Asset.TEX_START && a < Asset.TEX_END)
                return loadTexture((TextureAssets)a);
            if (a > Asset.VB_START && a < Asset.VB_END)
                return loadVertexBuffer((VertexBufferAssets)a);
            if (a > Asset.F_START && a < Asset.F_END)
                return loadFont((FontAssets)a);
            if (a > Asset.B_START && a < Asset.B_END)
                return loadBuffer((BufferAssets)a);

            throw new Exception("Invalid asset type, can't load");
        }

        private IDisposable getAsset(Asset a)
        {
            IDisposable toReturn = null;

            if (inLoad)
                assetsAddedDuringLoad.Add(a);

            if (loadedAssets.TryGetValue(a, out toReturn))
                return toReturn;

#if DEBUG
            {
                toReturn = loadAsset(a);
                loadedAssets.Add(a, toReturn);
                return toReturn;
            }
#else

            if (inLoad)
            {
                toReturn = loadAsset(a);
                loadedAssets.Add(a, toReturn);
                return toReturn;
            }
#endif

            throw new Exception("Asset not loaded, attempting to be used: " + a);
        }

        public Shader getAsset(ShaderAssets a)
        {
            return (Shader)getAsset((Asset)a);
        }

        public Texture getAsset(TextureAssets a)
        {
            return (Texture)getAsset((Asset)a);
        }

        public VertexBuffer getAsset(VertexBufferAssets a)
        {
            return (VertexBuffer)getAsset((Asset)a);
        }

        public Font getAsset(FontAssets a)
        {
            return (Font)getAsset((Asset)a);
        }

        public ConstBuffer<T> getAsset<T>(BufferAssets a) where T : struct
        {
            return (ConstBuffer<T>)getAsset((Asset)a);
        }

        private Shader loadShader(ShaderAssets shader)
        {
#if DEBUG
            return new Shader(stage, AssetHelper.GetPath((Asset)shader), AssetHelper.GetVertexElements(shader));
#else
            return new Shader(stage, blob.getAssetStream((Asset)shader), AssetHelper.GetVertexElements(shader));
#endif
        }

        private Texture loadTexture(TextureAssets tex)
        {
            Stream str = blob.getAssetStream((Asset)tex);
            if (str == null)
            {
#if DEBUG
                Logger.WriteLine(LogType.DEBUG, "Can't find shader in asset blob, loading from file. " + tex);
                return new Texture(stage, AssetHelper.GetPath((Asset)tex));
#else
                Logger.WriteLine(LogType.ERROR, "Can't find shader in asset blob, loading from file. " + tex);
                return null; //welp, probably just gonna crash here
#endif
            }

            return new Texture(stage, str);
        }

        private Font loadFont(FontAssets font)
        {
            Stream str = blob.getAssetStream((Asset)font);
            if (str == null)
            {
#if DEBUG
                Logger.WriteLine(LogType.DEBUG, "Can't find shader in asset blob, loading from file. " + font);
                return new Font(stage, AssetHelper.GetPath((Asset)font));
#else
                Logger.WriteLine(LogType.ERROR, "Can't find shader in asset blob, loading from file. " + font);
                return null; //welp, probably just gonna crash here
#endif
            }

            return new Font(stage, str);
        }

        private VertexBuffer loadVertexBuffer(VertexBufferAssets vb)
        {
            switch (vb)
            {
                case VertexBufferAssets.QUAD_POS_TEX_UNIT:
                    return VertexBuffer.CreatePosTexQuad(stage, Vector2.Zero, Vector2.One);
                case VertexBufferAssets.CIRCLE_POS_TEX_UNIT:
                    return VertexBuffer.CreatePosTexCircle(stage, Vector3.Zero, new Vector3(0, 0.5f, 0), new Vector3(0, 0, 1), 36);
                case VertexBufferAssets.CIRCLE_POS_TEX_NORM_UNIT:
                    return VertexBuffer.CreatePosTexNormCircle(stage, Vector3.Zero, new Vector3(0, 0.5f, 0), new Vector3(0, 0, 1), 36);
                default:
                    throw new Exception("Missing vb case! " + vb);
            }
        }

        private IDisposable loadBuffer(BufferAssets buf)
        {
            switch (buf)
            {
                case BufferAssets.WORLD:
                    return new ConstBuffer<Matrix>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.CAM_VIEWPROJ:
                    return new ConstBuffer<Matrix>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.CAM_INVVIEWPROJ:
                    return new ConstBuffer<Matrix>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.FONT:
                    return new ConstBuffer<FontGlyphBuffer>(stage, 6 * 32, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.LINE_SEGMENT:
                    return new ConstBuffer<LineSegmentData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.LINE_COLORED_SEGMENT:
                    return new ConstBuffer<ColoredLineSegmentData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.GRADIENT_CIRCLE:
                    return new ConstBuffer<GradientCircleData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.DASHED_CIRCLE:
                    return new ConstBuffer<DashedCircleData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.BORDERED_CIRCLE:
                    return new ConstBuffer<BorderedCircleData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.ENEMY_DATA:
                    return new ConstBuffer<EnemyVSData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.ROUNDED_RECT_3D:
                    return new ConstBuffer<RoundedRectangleData>(stage, 128, ResourceUsage.Dynamic, BindFlags.ShaderResource, CpuAccessFlags.Write, ResourceOptionFlags.BufferStructured);
                case BufferAssets.POINT_LIGHT:
                    return new ConstBuffer<PointLightBuffer>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.DIRECTIONAL_LIGHT:
                    return new ConstBuffer<DirectionalLightBuffer>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.COLOR:
                    return new ConstBuffer<ColorBuffer>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.ROUNDED_RECT:
                    return new ConstBuffer<RoundedRectData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.LINE:
                    return new ConstBuffer<LineData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.QUAD_INDEX:
                    return new ConstBuffer<ushort>(buf, stage, 128 * 6, ResourceUsage.Immutable, BindFlags.IndexBuffer, CpuAccessFlags.None, ResourceOptionFlags.None);
                case BufferAssets.LINE_COLORED:
                    return new ConstBuffer<ColoredLineData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.GRADIENT_CIRCLE_DATA:
                    return new ConstBuffer<GradientCircleDrawData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.DASHED_CIRCLE_DATA:
                    return new ConstBuffer<DashedCircleDrawData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.BORDERED_CIRCLE_DATA:
                    return new ConstBuffer<BorderedCircleDrawData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                case BufferAssets.ENEMY_DRAW:
                    return new ConstBuffer<EnemyDrawData>(stage, 1, ResourceUsage.Dynamic, BindFlags.ConstantBuffer, CpuAccessFlags.Write, ResourceOptionFlags.None);
                default:
                    throw new Exception("Missing buffer case! " + buf);
            }
        }
    }
}
