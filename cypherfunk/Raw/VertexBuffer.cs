﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer = SharpDX.Direct3D11.Buffer;
using Color = SharpDX.Color;

using assetmanager;

namespace cypherfunk
{

    static class VertexHelper
    {
        static VertexPositionNormalMapTexture[] calcMapping(VertexPositionNormalTexture[] tri, int offset)
        {//takes a triangle and outputs a triangle with binormals and tangents!
            VertexPositionNormalMapTexture[] toReturn = new VertexPositionNormalMapTexture[3];

            Vector3 D = tri[1 + offset].pos - tri[offset].pos;
            Vector3 E = tri[2 + offset].pos - tri[offset].pos;

            Vector2 F = tri[1 + offset].tex - tri[offset].tex;
            Vector2 G = tri[2 + offset].tex - tri[offset].tex;

            float r = 1f / (F.X * G.Y - G.X * F.Y);
            Vector3 tan = new Vector3((G.Y * D.X - F.Y * E.X) * r, (G.Y * D.Y - F.Y * E.Y) * r, (G.Y * D.Z - F.Y * E.Z) * r);
            Vector3 bin = new Vector3((F.X * E.X - G.X * D.X) * r, (F.X * E.Y - G.X * D.Y) * r, (F.X * E.Z - G.X * D.Z) * r);

            tan.Normalize();
            bin.Normalize();

            for (int x = 0; x < 3; x++)
            {
                toReturn[x] = new VertexPositionNormalMapTexture(tri[x + offset].pos, tri[x + offset].norm, bin, tan, tri[x + offset].tex);
            }

            return toReturn;
        }
    }

    class VertexBuffer : IDisposable
    {
        public VertexBufferBinding vbBinding;
        public Buffer vb;
        public int numVerts;

        private VertexBuffer(Buffer vb, VertexBufferBinding vbBinding, int numVerts)
        {
            this.vb = vb;
            this.vbBinding = vbBinding;
            this.numVerts = numVerts;
        }

        public static VertexBuffer CreateFromData<T>(GameStage stage, T[] data, int vertexStride) where T : struct
        {
            if (data.Length == 0)
                throw new Exception("Vertex buffer requires at least a single vertex.");

            Buffer vb = Buffer.Create(stage.Device, BindFlags.VertexBuffer, data, vertexStride * data.Length);
            VertexBufferBinding vbBinding = new VertexBufferBinding(vb, vertexStride, 0);
            return new VertexBuffer(vb, vbBinding, data.Length);
        }

        public static VertexBuffer CreatePosTexQuad(GameStage stage, Vector2 start, Vector2 end)
        {
            VertexPositionTexture[] quad = new VertexPositionTexture[6];
            quad[0] = new VertexPositionTexture(new Vector3(start.X, start.Y, 0), new Vector2(0, 0));
            quad[1] = new VertexPositionTexture(new Vector3(end.X, start.Y, 0), new Vector2(1, 0));
            quad[2] = new VertexPositionTexture(new Vector3(start.X, end.Y, 0), new Vector2(0, 1));
            quad[3] = new VertexPositionTexture(new Vector3(start.X, end.Y, 0), new Vector2(0, 1));
            quad[4] = new VertexPositionTexture(new Vector3(end.X, start.Y, 0), new Vector2(1, 0));
            quad[5] = new VertexPositionTexture(new Vector3(end.X, end.Y, 0), new Vector2(1, 1));

            return CreateFromData(stage, quad, VertexPositionTexture.sizeOf);
        }

        /// <summary>
        /// Creates a circle.
        /// </summary>
        /// <param name="stage">Context to create the buffer in</param>
        /// <param name="center">The Center of the circle, usually Vector3.Zero</param>
        /// <param name="point">A point on edge of the circle, matching texture coord (0.5, 1), will be rotated around the face vector</param>
        /// <param name="face">The direction the face should be pointing</param>
        /// <param name="numTris">Number of triangles in the circle. Must be at least 3</param>
        /// <returns></returns>
        public static VertexBuffer CreatePosTexCircle(GameStage stage, Vector3 center, Vector3 point, Vector3 face, int numTris)
        {
            if (numTris < 3)
                throw new ArgumentException("Number of Triangles for a circle must be at least 3");

            VertexPositionTexture[] cir = new VertexPositionTexture[numTris * 3];

            Vector3 curPoint = point;
            Vector3 nextPoint;

            Vector2 cenTex = new Vector2(0.5f, 0.5f);
            Vector2 curTex = new Vector2(0.5f, 1.0f);
            Vector2 nextTex;

            float rot = -(float)(Math.PI * 2.0 / numTris);

            for (int x = 0; x < numTris; x++)
            {
                nextPoint = Vector3.Transform(curPoint - center, Matrix3x3.RotationAxis(face, rot)) + center;
                nextTex = Vector2.Transform(curTex - cenTex, Quaternion.RotationMatrix(Matrix.RotationZ(rot))) + cenTex;

                cir[x * 3 + 0] = new VertexPositionTexture(center, cenTex);
                cir[x * 3 + 1] = new VertexPositionTexture(nextPoint, nextTex);
                cir[x * 3 + 2] = new VertexPositionTexture(curPoint, curTex);

                curPoint = nextPoint;
                curTex = nextTex;
            }

            return CreateFromData(stage, cir, VertexPositionTexture.sizeOf);
        }

        /// <summary>
        /// Creates a circle.
        /// </summary>
        /// <param name="stage">Context to create the buffer in</param>
        /// <param name="center">The Center of the circle, usually Vector3.Zero</param>
        /// <param name="point">A point on edge of the circle, matching texture coord (0.5, 1), will be rotated around the face vector</param>
        /// <param name="face">The direction the face should be pointing</param>
        /// <param name="numTris">Number of triangles in the circle. Must be at least 3</param>
        /// <returns></returns>
        public static VertexBuffer CreatePosTexNormCircle(GameStage stage, Vector3 center, Vector3 point, Vector3 face, int numTris)
        {
            if (numTris < 3)
                throw new ArgumentException("Number of Triangles for a circle must be at least 3");
            
            VertexPositionNormalTexture[] cir = new VertexPositionNormalTexture[numTris * 3];

            Vector3 curPoint = point;
            Vector3 nextPoint;

            Vector2 cenTex = new Vector2(0.5f, 0.5f);
            Vector2 curTex = new Vector2(0.5f, 0.0f);
            Vector2 nextTex;

            float rot = -(float)(Math.PI * 2.0 / numTris);

            for (int x = 0; x < numTris; x++)
            {
                nextPoint = Vector3.Transform(curPoint - center, Matrix3x3.RotationAxis(face, rot)) + center;
                nextTex = Vector2.Transform(curTex - cenTex, Quaternion.RotationMatrix(Matrix.RotationZ(rot))) + cenTex;

                cir[x * 3 + 0] = new VertexPositionNormalTexture(center, face, cenTex);
                cir[x * 3 + 1] = new VertexPositionNormalTexture(nextPoint, face, nextTex);
                cir[x * 3 + 2] = new VertexPositionNormalTexture(curPoint, face, curTex);

                curPoint = nextPoint;
                curTex = nextTex;
            }

            return CreateFromData(stage, cir, VertexPositionNormalTexture.sizeOf);
        }

        public void Dispose()
        {
            vb.Dispose();
        }
    }
}
