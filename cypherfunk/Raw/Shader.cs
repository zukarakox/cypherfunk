﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;

using SharpDX;
using SharpDX.D3DCompiler;
using SharpDX.Direct3D11;

using System.IO;

namespace cypherfunk
{
    class Shader : IDisposable
    {
        readonly VertexShader vs;
        readonly PixelShader ps;
        readonly InputLayout layout;

#if DEBUG
        public Shader(GameStage stage, string fileName, InputElement[] inputFormat)
        {//Assuming the shader is using a vertex shader and a pixel shader, named VS and PS respectivley
            CompilationResult vertShader = ShaderBytecode.CompileFromFile(fileName, "VS", "vs_5_0", ShaderFlags.Debug, EffectFlags.None);
            CompilationResult pixelShader = ShaderBytecode.CompileFromFile(fileName, "PS", "ps_5_0", ShaderFlags.Debug, EffectFlags.None);
            
            //should output errors/warnings here but whatever
            vs = new VertexShader(stage.Device, vertShader);

            if (inputFormat != null)
                layout = new InputLayout(stage.Device, vertShader, inputFormat);
            else
                layout = null;

            ps = new PixelShader(stage.Device, pixelShader);

            vertShader.Dispose();
            pixelShader.Dispose();
        }
#endif

        public Shader(GameStage stage, ShaderBytecode vertShader, ShaderBytecode pixelShader, InputElement[] inputFormat)
        {//Disposes the bytecode to make it work pretty with the other constructor
            vs = new VertexShader(stage.Device, vertShader);

            if (inputFormat != null)
                layout = new InputLayout(stage.Device, vertShader, inputFormat);
            else
                layout = null;
            
            ps = new PixelShader(stage.Device, pixelShader);

            vertShader.Dispose();
            pixelShader.Dispose();
        }

        public Shader(GameStage stage, Stream str, InputElement[] inputFormat)
        {
            BinaryReader fr = new BinaryReader(str, Encoding.Unicode, true);

            int vsLen = fr.ReadInt32();
            byte[] vsBytes = fr.ReadBytes(vsLen);

            int psLen = fr.ReadInt32();
            byte[] psBytes = fr.ReadBytes(psLen);

            ShaderBytecode vertShader = new ShaderBytecode(vsBytes);
            ShaderBytecode pixelShader = new ShaderBytecode(psBytes);

            vs = new VertexShader(stage.Device, vertShader);

            if (inputFormat != null)
                layout = new InputLayout(stage.Device, vertShader, inputFormat);
            else
                layout = null;

            ps = new PixelShader(stage.Device, pixelShader);

            fr.Dispose();
            vertShader.Dispose();
            pixelShader.Dispose();
        }

        public void Bind(DeviceContext context)
        {
            context.InputAssembler.InputLayout = layout;
            context.VertexShader.Set(vs);
            context.PixelShader.Set(ps);
        }

        public void Dispose()
        {
            vs.Dispose();
            ps.Dispose();

            if (layout != null)
                layout.Dispose();
        }
    }
}
