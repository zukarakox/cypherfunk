﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace cypherfunk
{
    class KeyboardHandler
    {
        InputHandler input;

        public KeyboardHandler(InputHandler input, Form form)
        {
            this.input = input;

            form.KeyDown += keyDown;
            form.KeyUp += keyUp;
        }

        void keyUp(object sender, KeyEventArgs e)
        {
            input.onKeyChange(e.KeyCode, false, e.Shift, e.Control, e.Alt);
            e.Handled = true;
        }

        void keyDown(object sender, KeyEventArgs e)
        {
            input.onKeyChange(e.KeyCode, true, e.Shift, e.Control, e.Alt);

            //tell windows the event was handled so it doesn't do some dumb default behaviours
            //like try to open a non-existant form menu when alt is pressed :|
            e.Handled = true;
        }
    }
}
