﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using log;
using SharpDX.Windows;

namespace cypherfunk
{
    public delegate bool OnPointerChange(PointerEventArgs args);
    public delegate bool OnFocusChange(bool hasFocus); //window focus, for when to eat/release mouse
    public delegate bool OnAxisMove(Axis axis, float val);
    public delegate bool OnTriggerMove(Trigger trig, float val);
    public delegate bool OnKeyChange(KeyData data, bool isDown);
    public delegate bool OnAction(ActionEventArgs args);

    /// <summary>
    /// Anything that interfaces with human input should have a priority.
    /// Any input is given through the list of interfaces, in order of priority, until the input has been handled.
    /// </summary>
    public enum InterfacePriority
    {
        HIGHEST = 0,
        HIGH = 1024,
        MEDIUM = 2048,
        LOW = 3072,
        LOWEST = 4096
    }

    public enum PointerButton
    {
        NONE,
        LEFT,
        MIDDLE,
        RIGHT,
        XBUTTON1,
        XBUTTON2
    }

    public enum Axis
    {
        X,
        Y,
        RX,
        RY
    }

    public enum Trigger
    {
        LEFT,
        RIGHT
    }

    public enum ControllerButton
    {
        X,
        Y,
        B,
        A,
        L3,
        R3,
        L2,
        R2,
        START,
        SELECT,
        GUIDE,
        DPAD_LEFT,
        DPAD_RIGHT,
        DPAD_UP,
        DPAD_DOWN
    }

    public enum PointerEventType
    {
        MOVE,
        BUTTON,
        MOUSEWHEEL,
        AIM
    }

    public struct KeyData
    {
        public readonly Keys k;
        public readonly bool shift;
        public readonly bool ctrl;
        public readonly bool alt;

        public KeyData(Keys k, bool shift, bool ctrl, bool alt)
        {
            this.k = k;
            this.shift = shift;
            this.ctrl = ctrl;
            this.alt = alt;
        }
    }

    public struct PointerEventArgs
    {
        /// <summary>
        /// The type of event.
        /// </summary>
        public readonly PointerEventType type;

        /// <summary>
        /// X Position of the pointer during the event.
        /// </summary>
        public readonly int x;

        /// <summary>
        /// Y Position of the pointer during the event.
        /// </summary>
        public readonly int y;

        /// <summary>
        /// The button involved during a 'BUTTON' event.
        /// </summary>
        public readonly PointerButton button;

        /// <summary>
        /// Whether the button is down or up, for 'BUTTON' events.
        /// </summary>
        public readonly bool isDown;

        /// <summary>
        /// Signed number of detents the mousewheel was changed by, for 'MOUSEWHEEL' events.
        /// </summary>
        public readonly int wheelClicks;

        /// <summary>
        /// The amount the pointer moved horizontally during an aim event, as a percentage of the screen.
        /// </summary>
        public readonly float aimDeltaX;

        /// <summary>
        /// The amount the pointer moved vertically during an aim event, as a percentage of the screen.
        /// </summary>
        public readonly float aimDeltaY;

        /// <summary>
        /// If the window had focus when this event was fired.
        /// </summary>
        public readonly bool windowInFocus;

        public PointerEventArgs(PointerEventType type, int x, int y, PointerButton button, bool isDown, int delta, float aimX, float aimY, bool windowInFocus)
        {
            this.type = type;
            this.x = x;
            this.y = y;
            this.button = button;
            this.isDown = isDown;
            this.wheelClicks = delta;
            this.aimDeltaX = aimX;
            this.aimDeltaY = aimY;
            this.windowInFocus = windowInFocus;
        }
    }

    /// <summary>
    /// Input maps raw input from devices into application actions.
    /// </summary>
    class InputHandler
    {
        public readonly MouseHandler mouse;
        public readonly KeyboardHandler keys;

        ActionMapper map;
        public EventManager events; //should be set by stage during constructor, then updated however

        public InputHandler(RenderForm form)
        {
            mouse = new MouseHandler(this, form);
            keys = new KeyboardHandler(this, form);
            map = new ActionMapper(this, "Content/Config/binds.cyb");

            form.GotFocus += onGainedFocus;
            form.LostFocus += onLostFocus;
        }

        public void Update()
        {//probably need this later for controller handling SDL calls

        }

        private void onLostFocus(object sender, EventArgs e)
        {
            mouse.LostFocus();
            foreach (OnFocusChange d in events.focusChangeList)
            {
                if (d(false))
                    return;
            }
        }

        private void onGainedFocus(object sender, EventArgs e)
        {
            mouse.GainedFocus();
            foreach (OnFocusChange d in events.focusChangeList)
            {
                if (d(true))
                    return;
            }
        }

        public void onPointerMovement(int posX, int posY, bool focus)
        {
            PointerEventArgs args = new PointerEventArgs(PointerEventType.MOVE, posX, posY, PointerButton.NONE, false, 0, 0, 0, focus);
            foreach (OnPointerChange e in events.pointerChangeList)
            {
                if (e(args))
                    return;
            }
        }

        public void onPointerMousewheel(int posX, int posY, int delta, bool focus)
        {
            PointerEventArgs args = new PointerEventArgs(PointerEventType.MOUSEWHEEL, posX, posY, PointerButton.NONE, false, delta, 0, 0, focus);
            foreach (OnPointerChange e in events.pointerChangeList)
            {
                if (e(args))
                    return;
            }
        }

        public void onPointerButton(PointerButton button, int posX, int posY, bool isDown, bool focus)
        {
            PointerEventArgs args = new PointerEventArgs(PointerEventType.BUTTON, posX, posY, button, isDown, 0, 0, 0, focus);
            foreach (OnPointerChange e in events.pointerChangeList)
            {
                if (e(args))
                    return;
            }
        }

        public void onPointerAim(float dX, float dY, bool focus)
        {
            PointerEventArgs args = new PointerEventArgs(PointerEventType.AIM, 0, 0, PointerButton.NONE, false, 0, dX, dY, focus);
            foreach (OnPointerChange e in events.pointerChangeList)
            {
                if (e(args))
                    return;
            }
        }

        public void onKeyChange(Keys k, bool isDown, bool shift, bool ctrl, bool alt)
        {
            KeyData key = new KeyData(k, shift, ctrl, alt);

            ActionEventArgs action;
            bool hasAction = map.tryGetAction(key, isDown, out action);

            if (!hasAction)
            {
                foreach (OnKeyChange e in events.keyChangeList)
                {
                    if (e(key, isDown))
                        return;
                }

                return;
            }
            else
            {
                foreach (Pair<OnKeyChange, OnAction> p in events.keyActionList)
                {
                    if (p.hasVal1)
                    {
                        if (p.val1(key, isDown))
                            return;
                    }
                    else
                    {
                        if (p.val2(action))
                            return;
                    }
                }
            }
        }

        public void onBindingChange(Keys k, KeyMap keyData, bool isBound)
        {
            //if the action was fired but not released, might want to call the release event
            //... can that even happen?
            if (isBound)
            {
                Logger.WriteLine(LogType.DEBUG, "Key binding added. Bind: " + keyData.getDisplay(k) + " Action: " + keyData.action);
            }
            else
            {
                Logger.WriteLine(LogType.DEBUG, "Key binding removed. Bind: " + keyData.getDisplay(k) + " Action: " + keyData.action);
            }
        }
    }
}
