﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using log;

using SharpDX.Windows;

namespace cypherfunk
{
    class MouseHandler
    {
        InputHandler input;
        RenderForm form;

        public MouseHandler(InputHandler input, RenderForm form)
        {
            this.input = input;
            this.form = form;

            form.MouseMove += Form_MouseMove;
            form.MouseDown += Form_MouseDown;
            form.MouseUp += Form_MouseUp;
            form.MouseWheel += Form_MouseWheel;
            form.LocationChanged += Form_LocationChanged;
        }

        private void Form_LocationChanged(object sender, EventArgs e)
        {
        }

        bool constrainMouse = false;
        bool fpvMode = false;
        bool lostFocus = false;
        int cursorHideCount = 0;

        private void HideCursor()
        {
            while (cursorHideCount <= 0)
            {
                Cursor.Hide();
                cursorHideCount++;
            }
        }

        private void ShowCursor()
        {
            while (cursorHideCount > 0)
            {
                Cursor.Show();
                cursorHideCount--;
            }
        }

        public void ConstrainMouseToWindow()
        {
            constrainMouse = true;
            Cursor.Clip = new Rectangle(form.PointToScreen(Point.Empty), form.ClientSize);
            lostFocus = false;
        }

        /// <summary>
        /// Stops constraining the mouse
        /// </summary>
        public void FreeTheMouse()
        {
            constrainMouse = false;
            Cursor.Clip = Rectangle.Empty;
        }

        public void EnterFPVMode()
        {
            HideCursor();
            ConstrainMouseToWindow();

            fpvMode = true;

            int w = form.ClientSize.Width / 2;
            int h = form.ClientSize.Height / 2;

            Cursor.Position = form.PointToScreen(new Point(w, h));
        }

        public void LeaveFPVMode()
        {
            ShowCursor();
            FreeTheMouse();

            fpvMode = false;
            lostFocus = false;
        }

        public void LostFocus()
        {
            if (fpvMode)
            {
                ShowCursor();
            }
            lostFocus = true;
        }

        public void GainedFocus()
        {
        }

        private void Form_MouseWheel(object sender, MouseEventArgs e)
        {
            input.onPointerMousewheel(e.X, e.Y, e.Delta, !lostFocus);
        }

        private void Form_MouseUp(object sender, MouseEventArgs e)
        {
            if (fpvMode)
            {
                Keys k;

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        k = Keys.LButton;
                        break;
                    case MouseButtons.Middle:
                        k = Keys.MButton;
                        break;
                    case MouseButtons.Right:
                        k = Keys.RButton;
                        break;
                    case MouseButtons.XButton1:
                        k = Keys.XButton1;
                        break;
                    case MouseButtons.XButton2:
                        k = Keys.XButton2;
                        break;
                    default:
                        Logger.WriteLine(LogType.DEBUG, "Unmapped mouse button down: " + e.Button);
                        return;
                }

                input.onKeyChange(k, false, false, false, false);
                return;
            }

            PointerButton b;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    b = PointerButton.LEFT;
                    break;
                case MouseButtons.Middle:
                    b = PointerButton.MIDDLE;
                    break;
                case MouseButtons.Right:
                    b = PointerButton.RIGHT;
                    break;
                case MouseButtons.XButton1:
                    b = PointerButton.XBUTTON1;
                    break;
                case MouseButtons.XButton2:
                    b = PointerButton.XBUTTON2;
                    break;
                default:
                    Logger.WriteLine(LogType.DEBUG, "Unmapped mouse button up: " + e.Button);
                    return;
            }
            
            input.onPointerButton(b, e.X, e.Y, false, !lostFocus);
        }

        private void Form_MouseDown(object sender, MouseEventArgs e)
        {
            if (lostFocus && fpvMode)
            {
                EnterFPVMode();
                return;
            }
            else if (lostFocus && constrainMouse)
            {
                ConstrainMouseToWindow();
            }

            if (fpvMode)
            {
                Keys k;

                switch (e.Button)
                {
                    case MouseButtons.Left:
                        k = Keys.LButton;
                        break;
                    case MouseButtons.Middle:
                        k = Keys.MButton;
                        break;
                    case MouseButtons.Right:
                        k = Keys.RButton;
                        break;
                    case MouseButtons.XButton1:
                        k = Keys.XButton1;
                        break;
                    case MouseButtons.XButton2:
                        k = Keys.XButton2;
                        break;
                    default:
                        Logger.WriteLine(LogType.DEBUG, "Unmapped mouse button down: " + e.Button);
                        return;
                }

                input.onKeyChange(k, true, false, false, false);
                return;
            }

            PointerButton b;
            
            switch (e.Button)
            {
                case MouseButtons.Left:
                    b = PointerButton.LEFT;
                    break;
                case MouseButtons.Middle:
                    b = PointerButton.MIDDLE;
                    break;
                case MouseButtons.Right:
                    b = PointerButton.RIGHT;
                    break;
                case MouseButtons.XButton1:
                    b = PointerButton.XBUTTON1;
                    break;
                case MouseButtons.XButton2:
                    b = PointerButton.XBUTTON2;
                    break;
                default:
                    Logger.WriteLine(LogType.DEBUG, "Unmapped mouse button down: " + e.Button);
                    return;
            }
            
            input.onPointerButton(b, e.X, e.Y, true, !lostFocus);
        }

        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            if (fpvMode && lostFocus)
                return;

            if (fpvMode)
            {
                int w = form.ClientSize.Width / 2;
                int h = form.ClientSize.Height / 2;

                if (e.X == w && e.Y == h)
                    return;

                Cursor.Position = form.PointToScreen(new Point(w, h));
                input.onPointerAim((e.X - w) / (float)w, (e.Y - h) / (float)h, !lostFocus);
                return;
            }

            input.onPointerMovement(e.X, e.Y, !lostFocus);
        }
    }
}
