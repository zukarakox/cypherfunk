﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using SharpDX.Windows;

namespace cypherfunk
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //Init Form
            form = new RenderForm("Graphics Testing");
            //Init everything else
            Init();

            //Init clocks for fps
            clock = new Stopwatch();
            clock.Start();
            fpsTimer = new Stopwatch();
            fpsTimer.Start();

            //form.Focus();
            RenderLoop.Run(form, Run);
        }

        static RenderForm form;
        static GameStage stage;

        static Stopwatch clock;
        static Stopwatch fpsTimer;
        static int frameCount = 0;
        static double dt;
        private static void Run()
        {
            dt = clock.Elapsed.TotalMilliseconds / 1000.0;
            clock.Restart();

            stage.Update(dt);
            stage.Draw();

            frameCount++;
            if (frameCount >= 60)
            {
                form.Text = frameCount * 1000.0f / fpsTimer.ElapsedMilliseconds + " " + (float)fpsTimer.ElapsedMilliseconds / frameCount;
                fpsTimer.Restart();
                frameCount = 0;
            }

            //Thread.Sleep((int)Math.Max(0, targetMilliseconds - clock.Elapsed.TotalMilliseconds - 1));
        }

        private static void Init()
        {
            Settings settings = new Settings();

            form.ClientSize = new Size(settings.resWidth, settings.resHeight);
            form.StartPosition = FormStartPosition.WindowsDefaultLocation;
            form.AllowUserResizing = false;
            form.FormClosing += Form_FormClosing;
            form.FormClosed += Form_FormClosed;

            InputHandler input = new InputHandler(form);

            stage = new GameStage(settings, input);
            stage.Load(form.Handle);
        }

        private static void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            stage.FormClosed();
        }

        private static void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            stage.Dispose();
        }

        public static void CloseForm()
        {
            form.Close();
        }
    }
}
