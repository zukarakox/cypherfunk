﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk
{
    /// <summary>
    /// Settings class, for user config options.
    /// 
    /// Should primarily be used for global config. Maybe add something for remembering game-specific prefs?
    /// </summary>
    public class Settings
    {
        public int resWidth = 1920;
        public int resHeight = 1080;
        public bool vSync = true;

        public Settings()
        {//this should read from a config file later to remember user settings.

        }
    }
}
