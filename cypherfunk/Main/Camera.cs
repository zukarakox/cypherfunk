﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

using SharpDX;

namespace cypherfunk
{
    interface ICamera
    {
        Matrix ViewProjMatrix();
        Matrix InvViewProjMatrix();
        Vector3 getForwardVec();
    }

    class FPVCamera : ICamera
    {
        Matrix worldMatrix;
        Matrix viewMatrix;
        Matrix projMatrix;
        Matrix viewProjMatrix;
        Matrix viewProjInvMatrix;

        float myYaw;
        float myPitch;
        Vector3 mypos;
        bool recalcWorld = true;

        public Matrix ViewProjMatrix()
        {
            recalcMatricies();
            return viewProjMatrix;
        }

        public Matrix InvViewProjMatrix()
        {
            recalcMatricies();
            return viewProjInvMatrix;
        }

        public Vector3 pos
        {
            get
            {
                return mypos;
            }
            set
            {
                recalcWorld = true;
                mypos = value;
            }
        }

        private const float minPitch = (float)(-Math.PI / 2.0 * 7.0 / 10.0);
        private const float maxPitch = (float)(Math.PI / 2.0 * 7.0 / 10.0);
        public float pitch
        {
            get
            {
                return myPitch;
            }
            set
            {
                recalcWorld = true;
                myPitch = Math.Min(Math.Max(value, minPitch), maxPitch);
            }
        }

        public float yaw
        {
            get
            {
                return myYaw;
            }
            set
            {
                recalcWorld = true;
                myYaw = value;
                if (myYaw < -Math.PI)
                    myYaw += (float)(2 * Math.PI);
                else if (myYaw > Math.PI)
                    myYaw -= (float)(2 * Math.PI);
            }
        }

        public Vector3 getForwardVec()
        {//row 3 of world, negative
            recalcMatricies();
            return -new Vector3(worldMatrix.Row3.X, worldMatrix.Row3.Y, worldMatrix.Row3.Z);
        }

        public Vector3 getRightVec()
        {//row 1
            recalcMatricies();
            return new Vector3(worldMatrix.Row1.X, worldMatrix.Row1.Y, worldMatrix.Row1.Z);
        }

        public Vector3 getUpVec()
        {//row 2
            recalcMatricies();
            return new Vector3(worldMatrix.Row2.X, worldMatrix.Row2.Y, worldMatrix.Row2.Z);
        }

        public FPVCamera(float aspect, Vector3 startPos, float yaw, float pitch)
        {
            mypos = startPos;
            this.yaw = yaw;
            this.pitch = pitch;
            projMatrix = Matrix.PerspectiveFovRH((float)(Math.PI * 41.625 / 180.0), aspect, 1000.0f, 0.1f);

            recalcMatricies();
        }

        private void recalcMatricies()
        {
            if (recalcWorld)
            {
                worldMatrix = Matrix.RotationAxis(Vector3.UnitX, pitch) * Matrix.RotationAxis(Vector3.UnitY, yaw) * Matrix.Translation(mypos);
                viewMatrix = Matrix.Invert(worldMatrix);
                viewProjMatrix = viewMatrix * projMatrix;
                viewProjInvMatrix = Matrix.Invert(viewProjMatrix);

                recalcWorld = false;
            }
        }
    }

    class OrthoCamera : ICamera
    {
        Matrix worldMatrix;
        Matrix viewMatrix;
        Matrix projMatrix;
        Matrix viewProjMatrix;
        Matrix viewProjInvMatrix;

        Vector2 myScale;
        Vector2 mypos;
        bool recalcWorld = true;

        public Matrix ViewProjMatrix()
        {
            recalcMatricies();
            return viewProjMatrix;
        }

        public Matrix InvViewProjMatrix()
        {
            recalcMatricies();
            return viewProjInvMatrix;
        }

        public Vector3 getForwardVec()
        {
            return -Vector3.UnitZ;
        }

        public Vector2 pos
        {
            get
            {
                return mypos;
            }
            set
            {
                recalcWorld = true;
                mypos = value;
            }
        }

        public Vector2 scale
        {
            get
            {
                return myScale;
            }
            set
            {
                recalcWorld = true;
                myScale = value;
            }
        }

        public OrthoCamera(Vector2 position, float width, float height)
        {
            this.pos = position;
            this.scale = new Vector2(width, height);
        }

        private void recalcMatricies()
        {
            if (recalcWorld)
            {
                worldMatrix = Matrix.Translation(mypos.X, mypos.Y, 0f);
                viewMatrix = Matrix.Invert(worldMatrix);
                projMatrix = Matrix.OrthoOffCenterRH(0, myScale.X, myScale.Y, 0, 1000.0f, 0.1f);
                viewProjMatrix = viewMatrix * projMatrix;
                viewProjInvMatrix = Matrix.Invert(viewProjMatrix);

                recalcWorld = false;
            }
        }
    }

    public static class CameraHelper
    {
        /// <summary>
        /// Returns the world-space coordinates of the given screen-space coordinates.
        /// </summary>
        public static Vector3 getWorldSpace(Vector3 screenSpace, Matrix invViewProj)
        {
            return Vector3.TransformCoordinate(screenSpace, invViewProj);
        }

        /// <summary>
        /// Returns the world-space coordinates of the mouse.
        /// Note: Depth out of screen is nonsensical, only really useful for an ortho camera, or normalizing as a direction after.
        /// </summary>
        public static Vector3 getWorldSpace(int mouseX, int mouseY, int screenWidth, int screenHeight, Matrix invViewProj)
        {
            //takes the mouseX and mouseY to screenspace coords, [-1, 1]
            Vector3 src = new Vector3(
                (mouseX / (float)screenWidth - 0.5f) * 2f,
                (mouseY / (float)screenHeight - 0.5f) * -2f,
                0.5f);

            return getWorldSpace(src, invViewProj);
        }

        /// <summary>
        /// Returns the raw pos * viewProj transform
        /// </summary>
        public static Vector3 getScreenSpace(Vector3 pos, Matrix viewProj)
        {
            return Vector3.TransformCoordinate(pos, viewProj);
        }

        /// <summary>
        /// Returns the pixel-space coords
        /// </summary>
        public static Vector2 getScreenSpace(Vector3 pos, int screenWidth, int screenHeight, Matrix viewProj)
        {
            Vector3 screen = getScreenSpace(pos, viewProj);

            return new Vector2(
                (screen.X + 1) * 0.5f * screenWidth,
                (screen.Y + 1) * -0.5f * screenHeight);
        }

        /// <summary>
        /// Returns the pixel-space coords, for a given world-space position.
        /// An insignificant depth is added to the world-space position, verify that you don't actually need that.
        /// </summary>
        public static Vector2 getScreenSpace(Vector2 pos, int screenWidth, int screenHeight, Matrix viewProj)
        {
            Vector3 screen = getScreenSpace(new Vector3(pos, 0.5f), viewProj);

            return new Vector2(
                (screen.X + 1) * 0.5f * screenWidth,
                (-screen.Y + 1) * 0.5f * screenHeight);
        }
    }
}
