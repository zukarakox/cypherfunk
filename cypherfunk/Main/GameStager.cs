﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

using SharpDX;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Buffer = SharpDX.Direct3D11.Buffer;
using Device = SharpDX.Direct3D11.Device;
using Resource = SharpDX.Direct3D11.Resource;
using MapFlags = SharpDX.Direct3D11.MapFlags;
using Color = SharpDX.Color;

using assetmanager;

namespace cypherfunk
{
    delegate void DrawDelegate();
    delegate void UpdateDelegate(float dt);

    class GameStage : IDisposable
    {
        const Scenes startScene = Scenes.TD_SCENE;
#if DEBUG
        const bool drawDebug = false;
#endif
        #region DX Main
        Device device;
        public Device Device
        {
            get
            {
                return device;
            }
        }

        DeviceContext context;
        public DeviceContext Context
        {
            get
            {
                return context;
            }
        }
        SwapChain swapChain;
        RenderTargetView renderView;
        DepthStencilState stencilDefault;
        #endregion
        #region Quads
        VertexBuffer quad_postex_unit;
        #endregion
        #region Shaders
        Shader s_Compile;
        Shader s_ColorDebug;
        Shader s_DepthDebug;
        Shader s_NormalDebug;
        Shader s_LightDebug;
        Shader s_PointLight;
        Shader s_DirectionalLight;
        #endregion
        #region Samplers
        SamplerState mySamplerAnisotropy;
        public SamplerState samplerAnisotropy
        {
            get
            {
                return mySamplerAnisotropy;
            }
        }
        SamplerState mySamplerLinear;
        public SamplerState samplerLinear
        {
            get
            {
                return mySamplerLinear;
            }
        }
        SamplerState mySamplerPoint;
        public SamplerState samplerPoint
        {
            get
            {
                return mySamplerPoint;
            }
        }
        #endregion
        #region Rasters
        RasterizerState myRasterDebug;
        public RasterizerState rasterDebug
        {
            get
            {
                return myRasterDebug;
            }
        }
        RasterizerState myRasterNormal;
        public RasterizerState rasterNormal
        {
            get
            {
                return myRasterNormal;
            }
        }
        RasterizerState myRasterNormalScissor;
        public RasterizerState rasterNormalScissor
        {
            get
            {
                return myRasterNormalScissor;
            }
        }
        #endregion
        #region Blend States
        BlendState blendLight;
        BlendState blendDefault;
        BlendState blendTransparent;
        BlendState blendAlphaTest; //not really used right now. turns on alpha-to-coverage, so low-alpha pixels get culled
        #endregion
        #region Render Targets
        ShaderResourceView depthSRV;
        DepthStencilView depthDSV;

        ShaderResourceView colorSRV;
        RenderTargetView colorRTV;

        ShaderResourceView normalSRV;
        RenderTargetView normalRTV;

        ShaderResourceView lightSRV;
        RenderTargetView lightRTV;
        #endregion
        #region Buffers
        Buffer fullScreenViewProjBuffer;
        ConstBuffer<Matrix> viewProjBuffer;
        ConstBuffer<Matrix> viewProjInvBuffer;
        ConstBuffer<Matrix> worldBuffer;
        #endregion
        #region Event Managers
        /// <summary>
        /// The manager actively being used for inputs/updates/rendering
        /// </summary>
        EventManager activeManager;

        /// <summary>
        /// The manager used for loading/preloading
        /// </summary>
        EventManager loadManager;

        /// <summary>
        /// The manager used by scenes
        /// </summary>
        EventManager sceneManager;
        #endregion

        public ICamera cam;

#if DEBUG
        public static float DEBUG_TIDI = 1f;
#endif

        public readonly Settings settings;
        InputHandler input;

        AssetManager assetManager;
        public AssetManager Assets
        {
            get
            {
                return assetManager;
            }
        }

        Scene currentScene;

        float loadingTime = 0;
        Thread loadingThread;

        public GameStage(Settings settings, InputHandler input)
        {
            this.settings = settings;
            this.input = input;

            assetManager = new AssetManager(this, @"Content\blob.cy");

            loadManager = new EventManager();
            sceneManager = new EventManager();

            activeManager = sceneManager;
            input.events = activeManager;
        }

        #region Loading
        public void Load(IntPtr formHandle)
        {
            #region InitDX
            //Init Swapchain and Device
            {
                var desc = new SwapChainDescription()
                {
                    BufferCount = 2,
                    ModeDescription = new ModeDescription(settings.resWidth, settings.resHeight, new Rational(60, 1), Format.R8G8B8A8_UNorm_SRgb),
                    IsWindowed = true,
                    OutputHandle = formHandle,
                    SampleDescription = new SampleDescription(1, 0),
                    SwapEffect = SwapEffect.Discard,
                    Usage = Usage.RenderTargetOutput
                };

#if DEBUG
                Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.Debug, desc, out device, out swapChain);
#else
                Device.CreateWithSwapChain(DriverType.Hardware, DeviceCreationFlags.None, desc, out device, out swapChain);
#endif

                var factory = swapChain.GetParent<Factory>();
                factory.MakeWindowAssociation(formHandle, WindowAssociationFlags.IgnoreAltEnter);

                factory.Dispose();

                //Init context
                context = Device.ImmediateContext;

                //Init primary render target
                var backBuffer = Resource.FromSwapChain<Texture2D>(swapChain, 0);
                renderView = new RenderTargetView(Device, backBuffer);
            }
            #endregion
            loadAssets(null); //load starting assets
            #region Raster Init
            myRasterDebug = new RasterizerState(Device, new RasterizerStateDescription()
            {
                CullMode = CullMode.None,
                FillMode = FillMode.Solid
            });
            myRasterNormal = new RasterizerState(Device, new RasterizerStateDescription()
            {
                CullMode = CullMode.Back,
                FillMode = FillMode.Solid
            });
            myRasterNormalScissor = new RasterizerState(Device, new RasterizerStateDescription()
            {
                CullMode = CullMode.Back,
                FillMode = FillMode.Solid,
                IsScissorEnabled = true
            });
            context.Rasterizer.State = rasterNormal;
            #endregion
            #region Sampler Init
            mySamplerAnisotropy = new SamplerState(Device, new SamplerStateDescription()
            {
                AddressU = TextureAddressMode.Wrap,
                AddressV = TextureAddressMode.Wrap,
                AddressW = TextureAddressMode.Wrap,
                Filter = Filter.Anisotropic,
                MaximumAnisotropy = 16
            });

            mySamplerLinear = new SamplerState(Device, new SamplerStateDescription()
            {
                AddressU = TextureAddressMode.Wrap,
                AddressV = TextureAddressMode.Wrap,
                AddressW = TextureAddressMode.Wrap,
                Filter = Filter.MinMagMipLinear,
                MaximumLod = float.MaxValue,
                MinimumLod = 0,
                MipLodBias = 0
            });

            mySamplerPoint = new SamplerState(Device, new SamplerStateDescription()
            {
                AddressU = TextureAddressMode.Wrap,
                AddressV = TextureAddressMode.Wrap,
                AddressW = TextureAddressMode.Wrap,
                Filter = Filter.MinMagMipPoint
            });
            #endregion
            #region Load Render Targets
            Texture2D depthTarget = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.R32_Typeless,
                BindFlags = BindFlags.DepthStencil | BindFlags.ShaderResource,
                Width = settings.resWidth,
                Height = settings.resHeight,
                MipLevels = 1,
                ArraySize = 1,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                Usage = ResourceUsage.Default,
                SampleDescription = new SampleDescription(1, 0)
            });

            depthSRV = new ShaderResourceView(Device, depthTarget, new ShaderResourceViewDescription()
            {
                Format = Format.R32_Float,
                Dimension = ShaderResourceViewDimension.Texture2D,
                Texture2D = { MostDetailedMip = 0, MipLevels = 1 }

            });
            depthDSV = new DepthStencilView(Device, depthTarget, new DepthStencilViewDescription()
            {
                Flags = DepthStencilViewFlags.None,
                Format = Format.D32_Float,
                Dimension = DepthStencilViewDimension.Texture2D,
                Texture2D = { MipSlice = 0 }
            });


            Texture2D colorTarget = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.R8G8B8A8_UNorm,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                Width = settings.resWidth,
                Height = settings.resHeight,
                MipLevels = 1,
                ArraySize = 1,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                Usage = ResourceUsage.Default,
                SampleDescription = new SampleDescription(1, 0)
            });

            colorSRV = new ShaderResourceView(Device, colorTarget);
            colorRTV = new RenderTargetView(Device, colorTarget);


            Texture2D normalTarget = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.R16G16_Float,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                Width = settings.resWidth,
                Height = settings.resHeight,
                MipLevels = 1,
                ArraySize = 1,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                Usage = ResourceUsage.Default,
                SampleDescription = new SampleDescription(1, 0)
            });

            normalSRV = new ShaderResourceView(Device, normalTarget);
            normalRTV = new RenderTargetView(Device, normalTarget);


            Texture2D lightTarget = new Texture2D(Device, new Texture2DDescription()
            {
                Format = Format.R8G8B8A8_UNorm,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                Width = settings.resWidth,
                Height = settings.resHeight,
                MipLevels = 1,
                ArraySize = 1,
                CpuAccessFlags = CpuAccessFlags.None,
                OptionFlags = ResourceOptionFlags.None,
                Usage = ResourceUsage.Default,
                SampleDescription = new SampleDescription(1, 0)
            });

            lightSRV = new ShaderResourceView(Device, lightTarget);
            lightRTV = new RenderTargetView(Device, lightTarget);
            #endregion
            #region Blend State Setup
            {
                BlendStateDescription desc = new BlendStateDescription();
                desc.AlphaToCoverageEnable = false;
                desc.IndependentBlendEnable = false;
                desc.RenderTarget[0].BlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].AlphaBlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].DestinationBlend = BlendOption.One;
                desc.RenderTarget[0].SourceBlend = BlendOption.One;
                desc.RenderTarget[0].SourceAlphaBlend = BlendOption.One;
                desc.RenderTarget[0].DestinationAlphaBlend = BlendOption.One;
                desc.RenderTarget[0].RenderTargetWriteMask = ColorWriteMaskFlags.All;
                desc.RenderTarget[0].IsBlendEnabled = true;
                blendLight = new BlendState(Device, desc);

                desc.RenderTarget[0].IsBlendEnabled = false;
                blendDefault = new BlendState(Device, desc);

                desc.AlphaToCoverageEnable = false;
                desc.IndependentBlendEnable = false;
                desc.RenderTarget[0].BlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].AlphaBlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].DestinationBlend = BlendOption.InverseSourceAlpha;
                desc.RenderTarget[0].SourceBlend = BlendOption.SourceAlpha;
                desc.RenderTarget[0].SourceAlphaBlend = BlendOption.SourceAlpha;
                desc.RenderTarget[0].DestinationAlphaBlend = BlendOption.DestinationAlpha;
                desc.RenderTarget[0].RenderTargetWriteMask = ColorWriteMaskFlags.All;
                desc.RenderTarget[0].IsBlendEnabled = true;
                blendTransparent = new BlendState(Device, desc);

                desc.AlphaToCoverageEnable = true;
                desc.IndependentBlendEnable = false;
                desc.RenderTarget[0].BlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].AlphaBlendOperation = BlendOperation.Add;
                desc.RenderTarget[0].DestinationBlend = BlendOption.InverseSourceAlpha;
                desc.RenderTarget[0].SourceBlend = BlendOption.SourceAlpha;
                desc.RenderTarget[0].SourceAlphaBlend = BlendOption.SourceAlpha;
                desc.RenderTarget[0].DestinationAlphaBlend = BlendOption.DestinationAlpha;
                desc.RenderTarget[0].RenderTargetWriteMask = ColorWriteMaskFlags.All;
                desc.RenderTarget[0].IsBlendEnabled = true;
                blendAlphaTest = new BlendState(Device, desc);
            }
            #endregion
            #region Buffer Setup
            fullScreenViewProjBuffer = new Buffer(Device,
                Utilities.SizeOf<Matrix>(),
                ResourceUsage.Default,
                BindFlags.ConstantBuffer,
                CpuAccessFlags.None,
                ResourceOptionFlags.None,
                0);
            updateSubresource(fullScreenViewProjBuffer, Matrix.OrthoOffCenterRH(0.0f, settings.resWidth, settings.resHeight, 0.0f, 0.0f, 1.0f));


            quad_postex_unit = Assets.getAsset(VertexBufferAssets.QUAD_POS_TEX_UNIT);

            viewProjBuffer = Assets.getAsset<Matrix>(BufferAssets.CAM_VIEWPROJ);
            viewProjInvBuffer = Assets.getAsset<Matrix>(BufferAssets.CAM_INVVIEWPROJ);
            worldBuffer = Assets.getAsset<Matrix>(BufferAssets.WORLD);
            #endregion
            #region Shader Setup
            s_Compile = Assets.getAsset(ShaderAssets.COMPILE);
            s_PointLight = Assets.getAsset(ShaderAssets.LIGHT_POINT);
            s_DirectionalLight = Assets.getAsset(ShaderAssets.LIGHT_DIRECTIONAL);

            s_ColorDebug = Assets.getAsset(ShaderAssets.DEBUG_COLOR);
            s_DepthDebug = Assets.getAsset(ShaderAssets.DEBUG_DEPTH);
            s_NormalDebug = Assets.getAsset(ShaderAssets.DEBUG_NORMAL);
            s_LightDebug = Assets.getAsset(ShaderAssets.DEBUG_LIGHT);
            #endregion
            #region Misc Stuff
            stencilDefault = new DepthStencilState(Device, new DepthStencilStateDescription()
            {
                IsStencilEnabled = false,
                StencilReadMask = 0,
                StencilWriteMask = 0,
                DepthComparison = Comparison.Greater,
                DepthWriteMask = DepthWriteMask.All,
                IsDepthEnabled = true
            });
            #endregion

            switchToScene(startScene);
        }

        private HashSet<Asset> getOurAssets()
        {
            return new HashSet<Asset>()
            {
                (Asset)ShaderAssets.COMPILE,
                (Asset)ShaderAssets.LIGHT_POINT,
                (Asset)ShaderAssets.LIGHT_DIRECTIONAL,
                (Asset)ShaderAssets.DEBUG_COLOR,
                (Asset)ShaderAssets.DEBUG_DEPTH,
                (Asset)ShaderAssets.DEBUG_NORMAL,
                (Asset)ShaderAssets.DEBUG_LIGHT,
                (Asset)VertexBufferAssets.QUAD_POS_TEX_UNIT,
                (Asset)BufferAssets.WORLD,
                (Asset)BufferAssets.CAM_VIEWPROJ,
                (Asset)BufferAssets.CAM_INVVIEWPROJ
            };
        }

        private void loadAssets(Scene scene)
        {
            //assets we want during the scene
            //note - any changes here should be reflected equally in the loadthread and endload method
            HashSet<Asset> assets = getOurAssets();

            //assets we want during load
            HashSet<Asset> loadAssets = new HashSet<Asset>(assets);

            if (scene == null)
            {//if this is the first load without a scene, just do this single-threaded
                Assets.PreLoad(loadAssets);
                Assets.StartLoad(assets, loadAssets);
                Assets.EndLoad(assets, loadAssets);
                return;
            }

            //construct asset list
            loadAssets.UnionWith(scene.getPreloadAssetList());
            assets.UnionWith(scene.getAssetList());

            //preload scene
            activeManager = loadManager;
            input.events = loadManager;
            Assets.PreLoad(loadAssets);
            cam = scene.getCamera();
            scene.Preload(loadManager);

            //now start the actual loading thread
            loadingTime = 0;
            loadingThread = new Thread(new ThreadStart(LoadThread));
            loadingThread.Start();
        }

        private void LoadThread()
        {
            HashSet<Asset> assets = getOurAssets();
            HashSet<Asset> loadAssets = new HashSet<Asset>(assets);
            loadAssets.UnionWith(currentScene.getPreloadAssetList());
            assets.UnionWith(currentScene.getAssetList());
            
            Assets.StartLoad(assets, loadAssets);
            currentScene.Load(sceneManager);
        }

        private void FinishLoad()
        {
            HashSet<Asset> assets = getOurAssets();
            HashSet<Asset> loadAssets = new HashSet<Asset>(assets);
            loadAssets.UnionWith(currentScene.getPreloadAssetList());
            assets.UnionWith(currentScene.getAssetList());

            //cleanup all of the load stuff now
            Assets.EndLoad(assets, loadAssets);
            currentScene.LoadEnd();
            activeManager = sceneManager;
            input.events = sceneManager;
            loadManager.Clear();
        }

        public void switchToScene(Scenes scene)
        {
            sceneManager.Clear();
            if (currentScene != null)
                currentScene.Dispose();

            Scene toCreate;
            switch (scene)
            {
                case Scenes.TEST_SCENE:
                    toCreate = new TestScene(this);
                    break;
                case Scenes.MAIN_MENU:
                    toCreate = new MainMenu(this);
                    break;
                case Scenes.TD_SCENE:
                    toCreate = new TDScene(this);
                    break;
                default:
                    throw new Exception("Missing scene case! " + scene);
            }

            currentScene = toCreate;
            loadAssets(toCreate);

        }
        #endregion

        #region Update
        double timeLeftover = 0;
        public void Update(double dt)
        {
            input.Update();

            float timeStep = 0.016667f;

#if DEBUG
            timeLeftover += dt * DEBUG_TIDI;
#else
            timeLeftover += dt;
#endif

            while (timeLeftover > timeStep)
            {
                foreach (UpdateDelegate d in activeManager.updateList)
                {
                    d(timeStep);
                }

                if (activeManager == loadManager)
                {
                    currentScene.LoadUpdate(timeStep);
                    loadingTime += timeStep;
                    if (!loadingThread.IsAlive && loadingTime > currentScene.loadTime())
                    {
                        FinishLoad();
                    }
                }
                else
                    currentScene.Update(timeStep);

                timeLeftover -= timeStep;
            }
        }
#endregion

        #region Drawing
        public void Draw()
        {
            //MRT Setup
            context.ClearRenderTargetView(colorRTV, Color.MidnightBlue);
            context.ClearRenderTargetView(normalRTV, Color.Black);
            context.ClearRenderTargetView(renderView, Color.Black);
            context.ClearDepthStencilView(depthDSV, DepthStencilClearFlags.Depth, 0.0f, 0);

            context.Rasterizer.SetViewport(0, 0, settings.resWidth, settings.resHeight);
            context.OutputMerger.SetDepthStencilState(stencilDefault);
            context.OutputMerger.SetTargets(depthDSV, colorRTV, normalRTV);
            context.InputAssembler.PrimitiveTopology = PrimitiveTopology.TriangleList;

            //Set 3D Camera
            viewProjBuffer.dat[0] = cam.ViewProjMatrix();
            viewProjBuffer.Write(Context);
            context.VertexShader.SetConstantBuffer(0, viewProjBuffer.buf);

            if (currentScene.draw3D())
            {
                viewProjInvBuffer.dat[0] = cam.InvViewProjMatrix();
                viewProjInvBuffer.Write(Context);

                //MRT Draw
                drawMRT();

                //MRT End
                context.OutputMerger.ResetTargets();
                context.VertexShader.SetConstantBuffer(0, fullScreenViewProjBuffer);
                context.PixelShader.SetShaderResources(0, colorSRV, depthSRV, normalSRV);

                //Light Setup
                context.OutputMerger.SetTargets(lightRTV);
                context.ClearRenderTargetView(lightRTV, Color.Black);
                context.VertexShader.SetConstantBuffer(0, fullScreenViewProjBuffer);
                context.PixelShader.SetConstantBuffer(2, viewProjInvBuffer.buf);
                context.PixelShader.SetSampler(0, samplerPoint);

                context.OutputMerger.SetBlendState(blendLight, Color.White, -1);
                context.InputAssembler.SetVertexBuffers(0, quad_postex_unit.vbBinding);

                worldBuffer.dat[0] = Matrix.Scaling(settings.resWidth, settings.resHeight, 1);
                worldBuffer.Write(Context);
                context.VertexShader.SetConstantBuffer(1, worldBuffer.buf);

                //Light Draw
                drawLights();

                //Light End
                context.OutputMerger.SetBlendState(null, null, -1);

                //Compile MRT
                context.OutputMerger.ResetTargets();
                context.OutputMerger.SetTargets(renderView);
                context.PixelShader.SetShaderResource(3, lightSRV);
                s_Compile.Bind(context);
                context.Draw(6, 0);

                //Post Process Setup
                context.VertexShader.SetConstantBuffer(0, viewProjBuffer.buf);
                context.PixelShader.SetShaderResources(0, 4, null, null, null, null);
                context.OutputMerger.SetTargets(renderView);
                context.OutputMerger.SetBlendState(blendTransparent, Color.White, -1);

                //Draw Post Process Here
                drawPostProcess3D();
            }

            //2D Setup
            context.OutputMerger.SetBlendState(blendTransparent, Color.White, -1);
            context.VertexShader.SetConstantBuffer(0, fullScreenViewProjBuffer);
            context.OutputMerger.SetTargets(renderView);

            //Draw 2D here
            draw2D();
#if DEBUG
            if (drawDebug && currentScene.draw3D())
                drawMRTOutput();
#endif

            //End 2D
            context.OutputMerger.SetBlendState(null, null, -1);

            swapChain.Present(settings.vSync ? 1 : 0, PresentFlags.None);
        }

        void drawMRT()
        {
            foreach (DrawDelegate d in activeManager.drawMRTList)
            {
                d();
            }
        }

        void drawLights()
        {
            s_PointLight.Bind(context);
            foreach (PointLight l in activeManager.pointLightList)
            {
                l.Draw();
            }

            s_DirectionalLight.Bind(context);
            foreach (DirectionalLight l in activeManager.directionalLightList)
            {
                l.Draw();
            }
        }

        void drawPostProcess3D()
        {//3D post process
            foreach (DrawDelegate d in activeManager.drawPostProcList)
            {
                d();
            }
        }

        void draw2D()
        {//2D post process
            foreach (DrawDelegate d in activeManager.draw2DList)
            {
                d();
            }
        }

#if DEBUG
        void drawMRTOutput()
        {
            context.PixelShader.SetShaderResources(0, colorSRV, depthSRV, normalSRV, lightSRV);
            context.InputAssembler.SetVertexBuffers(0, quad_postex_unit.vbBinding);
            context.VertexShader.SetConstantBuffer(1, worldBuffer.buf);

            s_ColorDebug.Bind(context);
            worldBuffer.dat[0] = Matrix.Scaling(settings.resWidth / 4f, settings.resHeight / 4f, 1) * Matrix.Translation(0, settings.resHeight * 3f / 4f, 0);
            worldBuffer.Write(Context);
            context.Draw(6, 0);

            s_DepthDebug.Bind(context);
            worldBuffer.dat[0] = Matrix.Scaling(settings.resWidth / 4f, settings.resHeight / 4f, 1) * Matrix.Translation(settings.resWidth / 4f, settings.resHeight * 3f / 4f, 0);
            worldBuffer.Write(Context);
            context.Draw(6, 0);

            s_NormalDebug.Bind(context);
            worldBuffer.dat[0] = Matrix.Scaling(settings.resWidth / 4f, settings.resHeight / 4f, 1) * Matrix.Translation(settings.resWidth / 2f, settings.resHeight * 3f / 4f, 0);
            worldBuffer.Write(Context);
            context.Draw(6, 0);

            s_LightDebug.Bind(context);
            worldBuffer.dat[0] = Matrix.Scaling(settings.resWidth / 4f, settings.resHeight / 4f, 1) * Matrix.Translation(settings.resWidth * 3f / 4f, settings.resHeight * 3f / 4f, 0);
            worldBuffer.Write(Context);
            context.Draw(6, 0);

            context.PixelShader.SetShaderResources(0, 4, null, null, null, null);
        }
#endif
#endregion

        public void EnterFPVMode()
        {
            input.mouse.EnterFPVMode();
        }

        public void LeaveFPVMode()
        {
            input.mouse.LeaveFPVMode();
        }

        public void ConstrainMouseToWindow()
        {
            input.mouse.ConstrainMouseToWindow();
        }

        public void StopConstrainingMouseToWindow()
        {
            input.mouse.FreeTheMouse();
        }

        public void updateSubresource<T>(Resource b, T value) where T : struct
        {
            context.UpdateSubresource(ref value, b);
        }

        public void mapSubresource<T>(Resource b, T value) where T : struct
        {
            var dataBox = context.MapSubresource(b, 0, MapMode.WriteDiscard, MapFlags.None);
            Utilities.Write(dataBox.DataPointer, ref value);
            context.UnmapSubresource(b, 0);
        }

        public void Exit()
        {
            //this kills the window, graphics context is now dead
            Program.CloseForm();
        }

        public void Dispose()
        {//called by Program.CloseForm() through FormClosing event.
            //nicely kill the current scene...
            currentScene.Dispose();

            //should probably kill all of our stuff too....
        }

        public void FormClosed()
        {
            //should probably check for loose threads/allocations here, for debugging

            //there's no *nice* way to find if all the current threads are daemons that are happy to shutdown when we do
            //so just kill everything, rip
            Environment.Exit(0);
        }
    }
}
