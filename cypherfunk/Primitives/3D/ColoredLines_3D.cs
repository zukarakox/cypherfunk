﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Color = SharpDX.Color;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 64)]
    struct ColoredLineSegmentData
    {
        [FieldOffset(0)]
        public Vector3 startPos;
        [FieldOffset(12)]
        float padding; //we want float4-aligned data whenever possible
        [FieldOffset(16)]
        public Vector3 endPos;
        [FieldOffset(28)]
        float padding2; //we want float4-aligned data whenever possible
        [FieldOffset(32)]
        public Vector4 startColor;
        [FieldOffset(48)]
        public Vector4 endColor;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    struct ColoredLineData
    {
        [FieldOffset(0)]
        public Vector3 cameraForward;
        [FieldOffset(12)]
        public float width;
    }

    struct ColoredLineSegment
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 startPos;
        public Vector4 startColor;
        public Vector3 endPos;
        public Vector4 endColor;

        public ColoredLineSegment(Vector3 startPos, Vector3 endPos, Color startColor, Color endColor)
        {
            this.startPos = startPos;
            this.endPos = endPos;
            this.startColor = Texture.convertToLinear(startColor);
            this.endColor = Texture.convertToLinear(endColor);
        }
    }

    /// <summary>
    /// Lines with individually-colored line segments
    /// </summary>
    class ColoredLines_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<ColoredLineSegmentData> lineSegmentBuf;
        private ConstBuffer<ColoredLineData> lineData;
        private ConstBuffer<ushort> indexBuffer;

        public List<ColoredLineSegment> lineSegments;
        public float lineWidth;
        public bool enabled = true;

        GameStage stage;
        EventManager em;

        public ColoredLines_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.LINE_COLORED);
            lineSegmentBuf = stage.Assets.getAsset<ColoredLineSegmentData>(BufferAssets.LINE_COLORED_SEGMENT);
            lineData = stage.Assets.getAsset<ColoredLineData>(BufferAssets.LINE_COLORED);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            lineSegments = new List<ColoredLineSegment>();
            lineWidth = 4;

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, lineSegmentBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);
            
            lineData.dat[0].width = lineWidth;
            lineData.dat[0].cameraForward = stage.cam.getForwardVec();
            lineData.Write(stage.Context);
            
            stage.Context.VertexShader.SetConstantBuffer(1, lineData.buf);

            int index = 0;

            while (index < lineSegments.Count)
            {
                int loops = Math.Min(index + lineSegmentBuf.numElements, lineSegments.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    lineSegmentBuf.dat[i].startPos = lineSegments[i + index].startPos;
                    lineSegmentBuf.dat[i].endPos = lineSegments[i + index].endPos;
                    lineSegmentBuf.dat[i].startColor = lineSegments[i + index].startColor;
                    lineSegmentBuf.dat[i].endColor = lineSegments[i + index].endColor;
                }

                lineSegmentBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
