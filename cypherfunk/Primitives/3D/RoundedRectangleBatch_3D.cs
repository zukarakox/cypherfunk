﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using SharpDX.DXGI;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 64)]
    struct RoundedRectangleData
    {
        [FieldOffset(0)]
        public Vector4 positionRadius;
        [FieldOffset(16)]
        public Vector3 scaleThickness;
        [FieldOffset(28)]
        float padding;
        [FieldOffset(32)]
        public Vector4 mainColor;
        [FieldOffset(48)]
        public Vector4 borderColor;
    }

    struct RoundedRectangle
    {
        public Vector3 position;
        public Vector2 scale;
        public float radius;
        public float borderThickness;
        public Vector4 main;
        public Vector4 border;

        public Color mainColor
        {
            set
            {
                main = Texture.convertToLinear(value);
            }
            get
            {
                return Texture.convertToSrgb(main);
            }
        }

        public Color borderColor
        {
            set
            {
                border = Texture.convertToLinear(value);
            }
            get
            {
                return Texture.convertToSrgb(border);
            }
        }

        public RoundedRectangle(Vector3 position, Vector2 scale, float radius, float borderThickness, Color mainColor, Color borderColor)
        {
            this.position = position;
            this.scale = scale;
            this.radius = radius;
            this.borderThickness = borderThickness;
            this.main = Texture.convertToLinear(mainColor);
            this.border = Texture.convertToLinear(borderColor);
        }
    }

    class RoundedRectangleBatch_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<RoundedRectangleData> rectBuf;
        private ConstBuffer<ushort> indexBuffer;

        public bool enabled = true;

        public List<RoundedRectangle> rects = new List<RoundedRectangle>();

        GameStage stage;
        EventManager em;

        public RoundedRectangleBatch_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.ROUNDED_RECTANGLE_3D);
            rectBuf = stage.Assets.getAsset<RoundedRectangleData>(BufferAssets.ROUNDED_RECT_3D);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, rectBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);

            int index = 0;

            while (index < rects.Count)
            {
                int loops = Math.Min(index + rectBuf.numElements, rects.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    var rect = rects[i + index];
                    RoundedRectangleData dat = new RoundedRectangleData();
                    dat.positionRadius = new Vector4(rect.position, rect.radius);
                    dat.scaleThickness = new Vector3(rect.scale, rect.borderThickness);
                    dat.mainColor = rect.main;
                    dat.borderColor = rect.border;

                    rectBuf.dat[i] = dat;
                }

                rectBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }

    /// <summary>
    /// A wrapper around the batch renderer to render a single rectangle.
    /// </summary>
    class RoundedRectangle_3D
    {
        RoundedRectangleBatch_3D rend;
        public Shader shader
        {
            get
            {
                return rend.shader;
            }
            set
            {
                rend.shader = value;
            }
        }

        public bool enabled
        {
            get
            {
                return rend.enabled;
            }
            set
            {
                rend.enabled = value;
            }
        }

        public Vector3 pos
        {
            get
            {
                return rend.rects[0].position;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.position = value;
                rend.rects[0] = rect;
            }
        }

        public Vector2 scale
        {
            get
            {
                return rend.rects[0].scale;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.scale = value;
                rend.rects[0] = rect;
            }
        }

        public float radius
        {
            get
            {
                return rend.rects[0].radius;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.radius = value;
                rend.rects[0] = rect;
            }
        }

        public float borderThickness
        {
            get
            {
                return rend.rects[0].borderThickness;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.borderThickness = value;
                rend.rects[0] = rect;
            }
        }

        public Color mainColor
        {
            get
            {
                return rend.rects[0].mainColor;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.mainColor = value;
                rend.rects[0] = rect;
            }
        }

        public Color borderColor
        {
            get
            {
                return rend.rects[0].borderColor;
            }
            set
            {
                RoundedRectangle rect = rend.rects[0];
                rect.borderColor = value;
                rend.rects[0] = rect;
            }
        }

        public RoundedRectangle_3D(GameStage stage, EventManager em, int priority)
        {
            rend = new RoundedRectangleBatch_3D(stage, em, priority);
            rend.rects.Add(new RoundedRectangle());
        }

        public void Dispose()
        {
            rend.Dispose();
        }
    }
}
