﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using SharpDX.DXGI;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 48)]
    struct DashedCircleData
    {
        [FieldOffset(0)]
        public Vector4 centerRadius;
        [FieldOffset(16)]
        public Vector4 dashColor;
        [FieldOffset(32)]
        public float dashLength;
        [FieldOffset(36)]
        public float dashWidth;
        [FieldOffset(40)]
        public float rot;
        [FieldOffset(44)]
        public float padding;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    struct DashedCircleDrawData
    {
        [FieldOffset(0)]
        public Vector3 cameraForward;
        [FieldOffset(12)]
        private float padding;
    }

    struct DashedCircle
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 center;
        public float radius;
        public float dashLength;
        public float dashWidth;
        public float rot;
        public Vector4 dashColor;

        public DashedCircle(Vector3 center, float radius, float dashLength, float dashWidth, float rot, Color dashColor)
        {
            this.center = center;
            this.radius = radius;
            this.dashLength = dashLength;
            this.dashWidth = dashWidth;
            this.rot = rot;
            this.dashColor = Texture.convertToLinear(dashColor);
        }
    }

    class DashedCircle_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<DashedCircleData> circleBuf;
        private ConstBuffer<DashedCircleDrawData> drawBuf;
        private ConstBuffer<ushort> indexBuffer;

        public bool enabled = true;

        public List<DashedCircle> circles = new List<DashedCircle>();

        GameStage stage;
        EventManager em;

        public DashedCircle_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.DASHED_CIRCLE);
            drawBuf = stage.Assets.getAsset<DashedCircleDrawData>(BufferAssets.DASHED_CIRCLE_DATA);
            circleBuf = stage.Assets.getAsset<DashedCircleData>(BufferAssets.DASHED_CIRCLE);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, circleBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);

            drawBuf.dat[0].cameraForward = stage.cam.getForwardVec();
            drawBuf.Write(stage.Context);

            stage.Context.VertexShader.SetConstantBuffer(1, drawBuf.buf);

            int index = 0;

            while (index < circles.Count)
            {
                int loops = Math.Min(index + circleBuf.numElements, circles.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    var circ = circles[i + index];
                    DashedCircleData dat = new DashedCircleData();
                    dat.centerRadius = new Vector4(circ.center, circ.radius);
                    dat.dashColor = circ.dashColor;
                    dat.dashWidth = circ.dashWidth;
                    dat.rot = circ.rot;

                    //we want an even number of dashes per circle, so fudge the dashLength so it's evenly divisble into the circum
                    float len = (float)((circ.radius * Math.PI * 1.0) / Math.Round(circ.radius * Math.PI * 1.0 / circ.dashLength));
                    dat.dashLength = len;

                    circleBuf.dat[i] = dat;
                }

                circleBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
