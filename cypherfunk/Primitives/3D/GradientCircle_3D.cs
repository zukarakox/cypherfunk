﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using SharpDX.DXGI;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 48)]
    struct GradientCircleData
    {
        [FieldOffset(0)]
        public Vector4 centerRadius;
        [FieldOffset(16)]
        public Vector4 startColor;
        [FieldOffset(32)]
        public Vector4 endColor;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    struct GradientCircleDrawData
    {
        [FieldOffset(0)]
        public Vector3 cameraForward;
        [FieldOffset(12)]
        private float padding;
    }

    struct GradientCircle
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 center;
        public float radius;
        public Vector4 startColor;
        public Vector4 endColor;

        public GradientCircle(Vector3 center, float radius, Color startColor, Color endColor)
        {
            this.center = center;
            this.radius = radius;
            this.startColor = Texture.convertToLinear(startColor);
            this.endColor = Texture.convertToLinear(endColor);
        }
    }

    class GradientCircle_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<GradientCircleData> circleBuf;
        private ConstBuffer<GradientCircleDrawData> drawBuf;
        private ConstBuffer<ushort> indexBuffer;
        
        public bool enabled = true;

        public List<GradientCircle> circles = new List<GradientCircle>();

        GameStage stage;
        EventManager em;

        public GradientCircle_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.GRADIENT_CIRCLE);
            drawBuf = stage.Assets.getAsset<GradientCircleDrawData>(BufferAssets.GRADIENT_CIRCLE_DATA);
            circleBuf = stage.Assets.getAsset<GradientCircleData>(BufferAssets.GRADIENT_CIRCLE);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, circleBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);
            
            drawBuf.dat[0].cameraForward = stage.cam.getForwardVec();
            drawBuf.Write(stage.Context);

            stage.Context.VertexShader.SetConstantBuffer(1, drawBuf.buf);

            int index = 0;

            while (index < circles.Count)
            {
                int loops = Math.Min(index + circleBuf.numElements, circles.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    circleBuf.dat[i].centerRadius = new Vector4(circles[i + index].center, circles[i + index].radius);
                    circleBuf.dat[i].startColor = circles[i + index].startColor;
                    circleBuf.dat[i].endColor = circles[i + index].endColor;
                }

                circleBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
