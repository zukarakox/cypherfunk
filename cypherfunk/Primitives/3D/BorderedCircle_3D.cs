﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using SharpDX.DXGI;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 64)]
    struct BorderedCircleData
    {
        [FieldOffset(0)]
        public Vector4 centerRadius;
        [FieldOffset(16)]
        public Vector4 centerColor;
        [FieldOffset(32)]
        public Vector4 borderColor;
        [FieldOffset(48)]
        public float borderWidth;
        [FieldOffset(52)]
        public Vector3 padding;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    struct BorderedCircleDrawData
    {
        [FieldOffset(0)]
        public Vector3 cameraForward;
        [FieldOffset(12)]
        private float padding;
    }

    struct BorderedCircle
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 center;
        public float radius;
        public Vector4 centerColor;
        public Vector4 borderColor;
        public float borderWidth;

        public BorderedCircle(Vector3 center, float radius, Color centerColor, Color borderColor, float borderWidth)
        {
            this.center = center;
            this.radius = radius;
            this.centerColor = Texture.convertToLinear(centerColor);
            this.borderColor = Texture.convertToLinear(borderColor);
            this.borderWidth = borderWidth;
        }
    }

    class BorderedCircle_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<BorderedCircleData> circleBuf;
        private ConstBuffer<BorderedCircleDrawData> drawBuf;
        private ConstBuffer<ushort> indexBuffer;

        public bool enabled = true;

        public List<BorderedCircle> circles = new List<BorderedCircle>();

        GameStage stage;
        EventManager em;

        public BorderedCircle_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.BORDERED_CIRCLE);
            drawBuf = stage.Assets.getAsset<BorderedCircleDrawData>(BufferAssets.BORDERED_CIRCLE_DATA);
            circleBuf = stage.Assets.getAsset<BorderedCircleData>(BufferAssets.BORDERED_CIRCLE);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, circleBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);

            drawBuf.dat[0].cameraForward = stage.cam.getForwardVec();
            drawBuf.Write(stage.Context);

            stage.Context.VertexShader.SetConstantBuffer(1, drawBuf.buf);

            int index = 0;

            while (index < circles.Count)
            {
                int loops = Math.Min(index + circleBuf.numElements, circles.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    circleBuf.dat[i].centerRadius = new Vector4(circles[i + index].center, circles[i + index].radius);
                    circleBuf.dat[i].centerColor = circles[i + index].centerColor;
                    circleBuf.dat[i].borderColor = circles[i + index].borderColor;
                    circleBuf.dat[i].borderWidth = circles[i + index].borderWidth;
                }

                circleBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
