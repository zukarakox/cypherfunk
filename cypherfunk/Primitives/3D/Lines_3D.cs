﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using Color = SharpDX.Color;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 32)]
    struct LineSegmentData
    {
        [FieldOffset(0)]
        public Vector3 startPos;
        [FieldOffset(12)]
        float padding; //we want float4-aligned data whenever possible
        [FieldOffset(16)]
        public Vector3 endPos;
        [FieldOffset(28)]
        float padding2; //we want float4-aligned data whenever possible
    }

    [StructLayout(LayoutKind.Explicit, Size = 32)]
    struct LineData
    {
        [FieldOffset(0)]
        public Vector4 color;
        [FieldOffset(16)]
        public Vector3 cameraPos;
        [FieldOffset(28)]
        public float width;
    }

    struct LineSegment
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 startPos;
        public Vector3 endPos;

        public LineSegment(Vector3 startPos, Vector3 endPos)
        {
            this.startPos = startPos;
            this.endPos = endPos;
        }
    }

    class Lines_3D : IDisposable
    {
        public Shader shader;
        private ConstBuffer<LineSegmentData> lineSegmentBuf;
        private ConstBuffer<LineData> lineData;
        private ConstBuffer<ushort> indexBuffer;

        public List<LineSegment> lineSegments;
        public float lineWidth;
        public Color color;
        public bool enabled = true;

        GameStage stage;
        EventManager em;

        public Lines_3D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.LINE);
            lineSegmentBuf = stage.Assets.getAsset<LineSegmentData>(BufferAssets.LINE_SEGMENT);
            lineData = stage.Assets.getAsset<LineData>(BufferAssets.LINE);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            lineSegments = new List<LineSegment>();
            color = Color.White;
            lineWidth = 4;

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, lineSegmentBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);

            lineData.dat[0].color = Texture.convertToLinear(color);
            lineData.dat[0].width = lineWidth;
            lineData.dat[0].cameraPos = stage.cam.getForwardVec();
            lineData.Write(stage.Context);

            stage.Context.PixelShader.SetConstantBuffer(1, lineData.buf);
            stage.Context.VertexShader.SetConstantBuffer(1, lineData.buf);

            int index = 0;

            while (index < lineSegments.Count)
            {
                int loops = Math.Min(index + lineSegmentBuf.numElements, lineSegments.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    lineSegmentBuf.dat[i].startPos = lineSegments[i + index].startPos;
                    lineSegmentBuf.dat[i].endPos = lineSegments[i + index].endPos;
                }

                lineSegmentBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
