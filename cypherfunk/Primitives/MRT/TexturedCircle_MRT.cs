﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

using SharpDX;
using SharpDX.Direct3D11;
using assetmanager;

namespace cypherfunk
{
    /// <summary>
    /// Assumes the buffer is a 3D circle with postex coords in the XY plane with radius = 1, facing positive Z.
    /// Position will translate the circle center.
    /// Scale will scale the XY-plane.
    /// Face will rotate the circle so that the face is pointing in the 'face' direction. Must be normalized.
    /// rot will rotate the circle along the other axis, 'spinning' the circle.
    /// </summary>
    class TexturedCircle_MRT
    {
        public Shader shader;
        public Texture tex;
        public VertexBuffer buf;
        public SamplerState sampler;
        private ConstBuffer<Matrix> worldBuffer;

        public Vector3 position;
        public Vector2 scale;
        public Vector3 face;
        public float rot;

        GameStage stage;
        EventManager em;

        public TexturedCircle_MRT(GameStage stage, EventManager em, int priority, Texture tex)
        {
            this.stage = stage;
            this.em = em;
            this.tex = tex;

            shader = stage.Assets.getAsset(ShaderAssets.POS_NORM_TEX);
            buf = stage.Assets.getAsset(VertexBufferAssets.CIRCLE_POS_TEX_NORM_UNIT);
            sampler = stage.samplerLinear;
            worldBuffer = stage.Assets.getAsset<Matrix>(BufferAssets.WORLD);

            position = new Vector3();
            scale = new Vector2();
            face = Vector3.UnitZ;
            rot = 0;

            em.addDrawMRT(priority, DrawMRT);
        }

        void DrawMRT()
        {
            shader.Bind(stage.Context);
            stage.Context.InputAssembler.SetVertexBuffers(0, buf.vbBinding);
            stage.Context.PixelShader.SetShaderResource(0, tex.view);
            stage.Context.PixelShader.SetSampler(0, sampler);

            worldBuffer.dat[0] = Matrix.Scaling(scale.X, scale.Y, 1) * Matrix.RotationZ(-rot) * Matrix.RotationAxis(Vector3.Cross(-Vector3.UnitZ, face), (float)Math.Acos(Vector3.Dot(-Vector3.UnitZ, face))) * Matrix.Translation(position.X, position.Y, position.Z);
            worldBuffer.Write(stage.Context);

            stage.Context.VertexShader.SetConstantBuffer(1, worldBuffer.buf);
            stage.Context.Draw(buf.numVerts, 0);
        }

        public void Dispose()
        {
            em.removeMRT(DrawMRT);
        }
    }
}