﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

using SharpDX;
using SharpDX.Direct3D11;
using assetmanager;

namespace cypherfunk
{
    class TexturedQuad_2D : IDisposable
    {
        public Shader shader;
        public Texture tex;
        public VertexBuffer buf;
        public SamplerState sampler;
        private ConstBuffer<Matrix> worldBuffer;

        public Vector2 position;
        public Vector2 scale;

        GameStage stage;
        EventManager em;

        public TexturedQuad_2D(GameStage stage, EventManager em, int priority, Texture tex)
        {
            this.stage = stage;
            this.em = em;
            this.tex = tex;

            shader = stage.Assets.getAsset(ShaderAssets.POS_TEX);
            buf = stage.Assets.getAsset(VertexBufferAssets.QUAD_POS_TEX_UNIT);
            sampler = stage.samplerLinear;
            worldBuffer = stage.Assets.getAsset<Matrix>(BufferAssets.WORLD);

            position = new Vector2();
            scale = new Vector2();

            em.addDraw2D(priority, Draw2D);
        }

        void Draw2D()
        {
            shader.Bind(stage.Context);
            stage.Context.InputAssembler.SetVertexBuffers(0, buf.vbBinding);
            stage.Context.PixelShader.SetShaderResource(0, tex.view);
            stage.Context.PixelShader.SetSampler(0, sampler);

            worldBuffer.dat[0] = Matrix.Scaling(scale.X, scale.Y, 1) * Matrix.Translation(position.X, position.Y, 0);
            worldBuffer.Write(stage.Context);
            
            stage.Context.VertexShader.SetConstantBuffer(1, worldBuffer.buf);
            stage.Context.Draw(buf.numVerts, 0);
        }

        public void Dispose()
        {
            em.remove2D(Draw2D);
        }
    }
}
