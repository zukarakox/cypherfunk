﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 112)]
    struct RoundedRectData
    {
        [FieldOffset(0)]
        internal Matrix world;
        [FieldOffset(64)]
        internal Vector2 scale;
        [FieldOffset(72)]
        internal Vector2 radius; //x = rectangle corner radius, y = border width
        [FieldOffset(80)]
        internal Vector4 mainColor;
        [FieldOffset(96)]
        internal Vector4 borderColor;

        internal RoundedRectData(Matrix world, Vector2 scale, Vector2 radius, Color mainColor, Color borderColor)
        {
            this.world = world;
            this.scale = scale;
            this.radius = radius;
            this.mainColor = Texture.convertToLinear(mainColor);
            this.borderColor = Texture.convertToLinear(borderColor);
        }
    }

    class RoundedRectangle_2D : IDisposable
    {
        public Shader shader;
        public VertexBuffer buf;
        private ConstBuffer<RoundedRectData> buffer;

        public Vector2 position;
        public Vector2 scale;
        public float radius;
        public float borderThickness;
        public Color mainColor;
        public Color borderColor;
        public bool enabled = true;

        GameStage stage;
        EventManager em;

        public RoundedRectangle_2D(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.ROUNDED_RECTANGLE_2D);
            buf = stage.Assets.getAsset(VertexBufferAssets.QUAD_POS_TEX_UNIT);
            buffer = stage.Assets.getAsset<RoundedRectData>(BufferAssets.ROUNDED_RECT);

            position = new Vector2();
            scale = new Vector2();
            radius = 20;
            borderThickness = 2;

            mainColor = Color.Black;
            borderColor = Color.White;

            em.addDraw2D(priority, Draw2D);
        }

        void Draw2D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.InputAssembler.SetVertexBuffers(0, buf.vbBinding);

            buffer.dat[0].world = Matrix.Scaling(scale.X * 1.2f, scale.Y * 1.2f, 1) * Matrix.Translation(position.X - scale.X * 0.1f, position.Y - scale.Y * 0.1f, 0);
            buffer.dat[0].scale = scale;
            buffer.dat[0].radius.X = radius;
            buffer.dat[0].radius.Y = borderThickness;
            buffer.dat[0].mainColor = Texture.convertToLinear(mainColor);
            buffer.dat[0].borderColor = Texture.convertToLinear(borderColor);

            buffer.Write(stage.Context);
            stage.Context.VertexShader.SetConstantBuffer(1, buffer.buf);
            stage.Context.PixelShader.SetConstantBuffer(1, buffer.buf);
            stage.Context.Draw(buf.numVerts, 0);
        }

        public void Dispose()
        {
            em.remove2D(Draw2D);
        }
    }
}
