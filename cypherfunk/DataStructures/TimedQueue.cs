﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk
{
    public class TimedQueue<T>
    {
        private List<T> data = new List<T>();
        private List<float> timers = new List<float>();

        public void Insert(T o, float timer)
        {
            //insert into appropriate place
            for (int i = 0; i < timers.Count; i++)
            {
                if (timer < timers[i])
                {
                    data.Insert(i, o);
                    timers.Insert(i, timer);
                    return;
                }
            }

            //didn't find a place to insert, add to end
            data.Add(o);
            timers.Add(timer);
        }

        public bool Contains(T o)
        {
            return data.Contains(o);
        }

        public void Remove(T o)
        {
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Equals(o))
                {
                    data.RemoveAt(i);
                    timers.RemoveAt(i);
                    return;
                }
            }
        }

        public void Clear()
        {
            data.Clear();
            timers.Clear();
        }

        public float MinTimer()
        {
            if (Count == 0)
                return 0;
            return timers[0];
        }

        public T Pop()
        {
            T toReturn = data[0];

            data.RemoveAt(0);
            timers.RemoveAt(0);

            return toReturn;
        }

        public void Update(float dt)
        {
            for (int i = 0; i < timers.Count; i++)
            {
                timers[i] -= dt; //does that work...
            }
        }

        public int Count
        {
            get { return data.Count; }
        }
    }
}
