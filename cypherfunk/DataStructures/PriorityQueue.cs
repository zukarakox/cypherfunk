﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk
{
    public class PriorityQueue<T> where T : IComparable<T>
    {
        private List<T> data = new List<T>();
        
        public void Insert(T o)
        {
            //we want to sort things such that the minimum item is at the end of the list
            //so 0.compareTo(1) should be > 0

            for (int i = 0; i < data.Count; i++)
            {
                if (o.CompareTo(data[i]) >= 0)
                {
                    data.Insert(i, o);
                    return;
                }
            }

            data.Add(o);
        }

        public void Remove(T toRemove)
        {
            data.Remove(toRemove);
        }

        public void PriorityChanged(T obj)
        {
            //could do something nicer here to speed it up a bit.
            Remove(obj);
            Insert(obj);
        }

        public T ExtractMin()
        {
            T toReturn = data[data.Count - 1];
            data.RemoveAt(data.Count - 1);

            return toReturn;
        }

        public int Count
        {
            get { return data.Count; }
        }
    }
}
