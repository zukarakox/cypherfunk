﻿struct VS_IN
{
	float4 posRadius;
	float4 centerColor;
	float4 borderColor;
	float borderWidth;
	float time;
	float shields;
	float padding;
};

struct PS_IN
{
	float4 pos : SV_POSITION;
	float2 localPos : TEXTURE; //xy position inside for the pixel
	float4 centerColor : TEXTURE1;
	float4 borderColor : TEXTURE2;
	float4 borderInfo : TEXTURE3; //radius, borderWidth
};

struct PS_OUT
{
	float4 color : SV_TARGET0;
};

cbuffer cCam : register(b0)
{
	float4x4 ViewProj : packoffset(c0);
};

cbuffer cWorld : register(b1)
{
	float3 camForward : packoffset(c0); //should be normalized
};

StructuredBuffer<VS_IN> vertData : register(t0);


PS_IN VS(uint vID : SV_VertexID)
{
	PS_IN output = (PS_IN)0;

	uint index = vID >> 2u;
	uint localID = vID & 3u;

	float3 center = vertData[index].posRadius.xyz;
	float radius = vertData[index].posRadius.w;

	//need *any* direction orthogonal to camForward
	float3 t = cross(float3(1, 0, 0), camForward);
	float tLen = length(t);
	if (tLen < 0.001)
	{
		t = cross(float3(0, 1, 0), camForward);
		tLen = length(t);
	}

	t = t / tLen;

	float3 u = cross(t, camForward) * radius;
	t = t * radius;

	//four verts are center + u + t, center + u - t, center - u + t, center - u - t
	float2 offset = 1 - float2(
		((localID) & 2),		//this should be 0 on [0, 1] and 2 on [2, 3]
		((localID << 1) & 2)	//this should be 0 on [0, 2] and 2 on [1, 3]
		);

	offset = offset * 1.5; //offset is [-1.1, 1.1]

	output.pos = mul(ViewProj, float4(center + u * offset.x + t * offset.y, 1));

	output.localPos = offset;
	output.centerColor = vertData[index].centerColor;
	output.borderColor = vertData[index].borderColor;
	output.borderInfo = float4(radius, vertData[index].borderWidth, vertData[index].time, vertData[index].shields);

	return output;
}

PS_OUT PS(PS_IN input) : SV_Target
{
	PS_OUT output = (PS_OUT)0;

//length(input.localPos) is [0, sqrt(2)]
//we want >0 to be inside the circle, <0 to be outside
//so we transform that to [1 - sqrt(2), 1]
float dis = 1 - length(input.localPos);

//we want an outline around the edge
//edge is >0 for center color, <0 for edge color
float edge = dis * input.borderInfo.x / input.borderInfo.y - 1;

float radThresh = length(float2(ddx(dis), ddy(dis)));
float edgeThresh = length(float2(ddx(edge), ddy(edge)));

output.color = lerp(input.borderColor, input.centerColor, smoothstep(-edgeThresh, edgeThresh, edge));

output.color.a = output.color.a * smoothstep(-radThresh, radThresh, dis);

//now we want to add a shield effect on top...
dis = dis + 0.4;

float4 c = saturate(float4(0, 0, 1.0, smoothstep(-0.01, 0.01, dis) * input.borderInfo.w * (0.5
	* (
	sin((dis + input.borderInfo.z * 0.6) * 5.0)
	* sin((input.localPos.x + input.borderInfo.z * 0.4) * 5.0 * 0.70)
	* sin((input.localPos.y + input.borderInfo.z * 0.3) * 5.0 * 0.5)
	)
	+ 0.5)));

output.color = saturate(float4(output.color.rgb * output.color.a * (1 - c.a) + c.rgb * c.a, output.color.a + c.a));

return output;
}