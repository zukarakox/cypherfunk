﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class PowTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.Chartreuse;
        public const float baseRange = 100f;

        Map map;
        
        ColoredLines_3D attackAnim;

        public PowTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Pow, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new ColoredLines_3D(map.stage, map.em, 3);
            attackAnim.enabled = false;
            attackAnim.lineSegments.Add(new ColoredLineSegment());

            damage = 250 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 2f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float damage;
        float animTimer = -1;
        public override void Update(float dt)
        {
            //this should be a standard 'homing' bullet attack
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Enemy e = map.FindClosestToEndEnemy(worldPos, attackRange);
                if (e != null)
                {
                    attackAnim.lineSegments[0] = new ColoredLineSegment(
                        new Vector3(worldPos, -10),
                        new Vector3(e.position, -10),
                        Color.Chartreuse,
                        Color.Chartreuse);

                    e.DealDamage(damage, DamageSource.NORMAL);
                    attackTimer = -attackCD;
                    animTimer = 0;
                    attackAnim.enabled = true;
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            damage += 100 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class PowPreview : TowerPreview
    {
        public PowPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return PowTower.width;
            }
        }

        public override int height
        {
            get
            {
                return PowTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return PowTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = PowTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return PowTower.makeDisp(map);
        }
    }
}
