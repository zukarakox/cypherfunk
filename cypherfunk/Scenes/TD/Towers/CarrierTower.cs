﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class CarrierTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.MistyRose;
        public const float baseRange = 40f;

        Map map;

        public CarrierTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Carrier, pos, width, height, blocks, cost)
        {
            this.map = map;

            damage = 40 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 0.5f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float damage;
        CarrierBullet bullet = null;
        public override void Update(float dt)
        {
            if (bullet != null && bullet.isDone)
            {
                bullet = null;
            }

            attackTimer += dt;
            if (attackTimer > 0 && bullet == null)
            {
                Enemy e = map.FindClosestToEndEnemy(worldPos, attackRange);
                if (e != null)
                {
                    bullet = map.FireCarrierBullet(e, worldPos, damage);
                    attackTimer = -attackCD;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            bullet = null;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            bullet = null;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            if (bullet != null)
            {
                bullet.isDone = true;
                bullet = null;
            }
        }

        protected override void TowerUpgraded()
        {
            damage += 20 * damageBoost;
        }

        public override void Dispose()
        {
        }
    }

    class CarrierBullet : Bullet
    {
        const float attackCD = 0.5f;
        const float speed = 200f;
        const float attackRange = 20f;

        Map map;
        Enemy target;

        public Vector2 position;
        float damage;
        public bool isDone = false;
        public CarrierBullet(Map map, Enemy target, Vector2 pos, float damage)
        {
            this.map = map;
            this.position = pos;
            this.target = target;
            this.damage = damage;

            disp = new RoundedRectangle();
            disp.scale = new Vector2(5, 5);
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -10);
            disp.radius = 2.5f;
            disp.borderThickness = 1;
            disp.mainColor = Color.Black;
            disp.borderColor = Color.DeepPink;
        }

        float attackTimer = 0;
        float goalOrbit = 0;
        public override bool Update(float dt)
        {
            if (target.isDead || isDone)
            {
                return true;
            }

            goalOrbit += dt * 10;
            Vector2 goal = target.position + Vector2.Transform(Vector2.UnitX * attackRange / 2f, Quaternion.RotationAxis(Vector3.UnitZ, goalOrbit));

            Vector2 dir = goal - position;
            float len = dir.Length();


            if (len > dt * speed)
            {
                position = position + dir / len * dt * speed;
            }
            else
            {
                position = goal;
            }
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -1f);


            attackTimer += dt;
            if (attackTimer > 0 && len < attackRange)
            {
                attackTimer = -attackCD;
                target.DealDamage(damage, DamageSource.NORMAL);
            }


            return false;
        }

        public override void Dispose()
        {
            isDone = true;
        }
    }

    class CarrierPreview : TowerPreview
    {
        public CarrierPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return CarrierTower.width;
            }
        }

        public override int height
        {
            get
            {
                return CarrierTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return CarrierTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = CarrierTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return CarrierTower.makeDisp(map);
        }
    }
}
