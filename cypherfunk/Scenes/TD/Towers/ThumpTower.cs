﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class ThumpTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                15f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.ForestGreen;
        public const float baseRange = 25f;

        Map map;
        
        GradientCircle_3D attackAnim;

        public ThumpTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Thump, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new GradientCircle_3D(map.stage, map.em, -1);
            attackAnim.enabled = false;
            attackAnim.circles.Add(new GradientCircle(Vector3.Zero, 0, Color.Red, Color.Green));

            effect.damage = 60 * damageBoost;

            //thump tower not affected by range/spd buffs, for obvious stun-lock related reasons
            attackRange = baseRange;
            attackCD = 1.5f;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float animTimer = -1;
        StunEffect effect = new StunEffect(0.25f, 0f);
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {
                if (map.AnyEnemyInRange(worldPos, attackRange))
                {
                    attackTimer = -attackCD;
                    map.ApplyAOEEffect(worldPos, attackRange, effect);
                    animTimer = 0;
                    attackAnim.enabled = true;
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
                else
                {
                    attackAnim.circles[0] = new GradientCircle(
                        new Vector3(worldPos, -20),
                        attackRange * (animTimer / 0.25f),
                        Color.Transparent,
                        Color.LightGreen);
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            effect.damage += 60 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class ThumpPreview : TowerPreview
    {

        public ThumpPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return ThumpTower.width;
            }
        }

        public override int height
        {
            get
            {
                return ThumpTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return ThumpTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                return ThumpTower.baseRange;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return ThumpTower.makeDisp(map);
        }
    }
}
