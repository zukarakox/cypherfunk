﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class BonkerTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }
        
        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.LightSalmon;
        public const float baseRange = 25f;

        Map map;
        GradientCircle_3D attackAnim;

        public BonkerTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Bonker, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new GradientCircle_3D(map.stage, map.em, 3);
            attackAnim.enabled = false;
            attackAnim.circles.Add(new GradientCircle(Vector3.Zero, 0, Color.Red, Color.Green));

            damage = 600 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 2f * atkSpeedBoost;
        }
        
        readonly float attackRange;
        readonly float attackCD;
        float damage;
        float animTimer = 0;
        float attackTimer = 0;
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Enemy e = map.FindClosestToEndEnemy(worldPos, attackRange);
                if (e != null)
                {
                    e.DealDamage(damage, DamageSource.NORMAL);
                    attackTimer = -attackCD;

                    animTimer = 0;
                    attackAnim.enabled = true;
                    attackAnim.circles[0] = new GradientCircle(
                        new Vector3(e.position, -20),
                        0f,
                        Color.Transparent,
                        Color.Coral);
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
                else
                {
                    attackAnim.circles[0] = new GradientCircle(
                        attackAnim.circles[0].center,
                        10f * (animTimer / 0.25f),
                        Color.Transparent,
                        Color.Coral);
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            damage += 60 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class BonkerPreview : TowerPreview
    {

        public BonkerPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return BonkerTower.width;
            }
        }

        public override int height
        {
            get
            {
                return BonkerTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return BonkerTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = BonkerTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return BonkerTower.makeDisp(map);
        }
    }
}
