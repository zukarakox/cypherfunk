﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class PoogasTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.LimeGreen;
        public const float baseRange = 40f;

        Map map;
        
        GradientCircle_3D attackAnim;

        public PoogasTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Poogas, pos, width, height, blocks, cost)
        {
            this.map = map;

            dps = 20 * damageBoost / atkSpeedBoost;
            attackRange = baseRange * rangeBoost;

            attackAnim = new GradientCircle_3D(map.stage, map.em, -600);
            attackAnim.enabled = false;

            attackAnim.circles.Add(new GradientCircle(
                        new Vector3(worldPos, -20),
                        attackRange,
                        Color.Transparent,
                        Color.LightSeaGreen));
        }
        
        readonly float attackRange;

        float dps;
        public override void Update(float dt)
        {
            map.DamageArea(worldPos, attackRange, dps * dt, DamageSource.DOT);
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        bool inRound = false;
        bool enabled = false;
        public override void OnRoundStart()
        {
            inRound = true;
            attackAnim.enabled = enabled;
        }

        public override void OnRoundEnd()
        {
            inRound = false;
            attackAnim.enabled = false;
        }

        public override void Enabled()
        {
            enabled = true;
            attackAnim.enabled = inRound;
        }

        public override void Disabled()
        {
            enabled = false;
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            dps += 20 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class PoogasPreview : TowerPreview
    {

        public PoogasPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return PoogasTower.width;
            }
        }

        public override int height
        {
            get
            {
                return PoogasTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return PoogasTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = PoogasTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return PoogasTower.makeDisp(map);
        }
    }
}
