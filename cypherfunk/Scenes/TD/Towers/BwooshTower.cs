﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class BwooshTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                10f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.OrangeRed;
        public const float baseRange = 60f;

        Map map;

        public BwooshTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Bwoosh, pos, width, height, blocks, cost)
        {
            this.map = map;

            damage = 45 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 2f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float damage;
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Enemy e = map.FindClosestToEndEnemy(worldPos, attackRange);
                if (e != null)
                {
                    map.FireBwooshBullet(e.position, worldPos, damage);
                    attackTimer = -attackCD;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
        }

        protected override void TowerUpgraded()
        {
            damage += 10 * damageBoost;
        }

        public override void Dispose()
        {
        }
    }

    class BwooshBullet : Bullet
    {
        public float speed = 50f;
        public float damage;
        public float range = 20f;

        Vector2 target;
        Map map;
        public Vector2 position;
        public BwooshBullet(Map map, Vector2 target, Vector2 pos, float damage)
        {
            this.map = map;
            this.position = pos;
            this.target = target;
            this.damage = damage;

            disp = new RoundedRectangle();
            disp.scale = new Vector2(10, 10);
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -10);
            disp.radius = 2.5f;
            disp.borderThickness = 1;
            disp.mainColor = Color.Black;
            disp.borderColor = Color.Purple;
        }

        public override bool Update(float dt)
        {
            Vector2 dir = target - position;
            float len = dir.Length();

            if (len < dt * speed)
            {
                map.DamageArea(target, range, damage, DamageSource.NORMAL);
                return true;
            }


            position = position + dir / len * dt * speed;
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -1f);

            return false;
        }

        public override void Dispose()
        {
        }
    }

    class BwooshPreview : TowerPreview
    {

        public BwooshPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return BwooshTower.width;
            }
        }

        public override int height
        {
            get
            {
                return BwooshTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return BwooshTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = BwooshTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return BwooshTower.makeDisp(map);
        }
    }
}
