﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class TrapTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = false;
        public static readonly Color mainColor = Color.Gray;
        public static readonly Color borderColor = Color.LightGray;
        public const float baseRange = 12f;

        Map map;
        
        GradientCircle_3D attackAnim;

        public TrapTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Trap, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new GradientCircle_3D(map.stage, map.em, -550);
            attackAnim.enabled = false;

            dps = 100 * damageBoost / atkSpeedBoost;
            attackRange = baseRange;

            attackAnim.circles.Add(new GradientCircle(
                new Vector3(worldPos, -20),
                attackRange,
                Color.Transparent,
                Color.Magenta));
        }
        
        readonly float attackRange;
        
        float dps;
        public override void Update(float dt)
        {
            map.DamageArea(worldPos, attackRange, dps * dt, DamageSource.DOT);
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        bool inRound = false;
        bool enabled = false;
        public override void OnRoundStart()
        {
            inRound = true;
            attackAnim.enabled = enabled;
        }

        public override void OnRoundEnd()
        {
            inRound = false;
            attackAnim.enabled = false;
        }

        public override void Enabled()
        {
            enabled = true;
            attackAnim.enabled = inRound;
        }

        public override void Disabled()
        {
            enabled = false;
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            dps += 50 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class TrapPreview : TowerPreview
    {
        public TrapPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return TrapTower.width;
            }
        }

        public override int height
        {
            get
            {
                return TrapTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return TrapTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                return TrapTower.baseRange;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return TrapTower.makeDisp(map);
        }
    }
}
