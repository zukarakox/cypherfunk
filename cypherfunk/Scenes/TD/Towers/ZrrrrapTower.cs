﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class ZrrrrapTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                10f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.GhostWhite;
        public static readonly Color attackStart = Color.GhostWhite;
        public static readonly Color attackEnd = Color.DarkOrange;
        public const float baseRange = 80f;

        Map map;

        ColoredLines_3D attackAnim;

        public ZrrrrapTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Zrrrrap, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new ColoredLines_3D(map.stage, map.em, 3);
            attackAnim.enabled = false;
            attackAnim.lineSegments.Add(new ColoredLineSegment());

            damage = 20 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 1f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float animTimer = -1;
        const int maxJumps = 10;
        float damage;
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Vector2 pos = worldPos;
                int j = 0;
                Enemy e;
                attackAnim.lineSegments.Clear();
                List<Enemy> toIgnore = new List<Enemy>();
                Color curColor = attackStart;
                float maxDist = attackRange;
                while ((e = map.FindNearestEnemyIgnoringEnemies(pos, maxDist, toIgnore)) != null && j < maxJumps)
                {
                    toIgnore.Add(e);

                    e.DealDamage(damage, DamageSource.NORMAL);

                    Color nextColor = Color.Lerp(attackStart, attackEnd, (j + 1f) / 10f);

                    attackAnim.lineSegments.Add(new ColoredLineSegment(
                        new Vector3(pos, -10),
                        new Vector3(e.position, -10),
                        curColor,
                        nextColor));

                    curColor = nextColor;
                    j++;
                    pos = e.position;
                    maxDist = maxDist * 0.7f;
                }

                if (j != 0)
                {
                    attackTimer = -attackCD;
                    animTimer = 0;
                    attackAnim.enabled = true;
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            damage += 10 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class ZrrrrapPreview : TowerPreview
    {

        public ZrrrrapPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return ZrrrrapTower.width;
            }
        }

        public override int height
        {
            get
            {
                return ZrrrrapTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return ZrrrrapTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = ZrrrrapTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return ZrrrrapTower.makeDisp(map);
        }
    }
}
