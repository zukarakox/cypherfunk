﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    enum TowerTypes
    {//totally legit
        TOWER_START = -1, //so first tower is zero-indexed
        Pewpew,
        Bwoosh,
        Brrrr,
        Zrrrrap,
        Bonker,
        Pow,
        Poison,
        Poogas,
        Thump,
        Laze,
        Trap,
        Carrier,
        TOWER_END
    }

    interface TowerAttack
    {
        void Update(float dt);
    }

    abstract class Tower : MapObject
    {
        public const float TowerBuildTime = 2.5f;

        public static int GetTowerCost(TowerTypes type)
        {
            switch (type)
            {
                case TowerTypes.Pewpew:
                    return 10;
                case TowerTypes.Bwoosh:
                    return 100;
                case TowerTypes.Brrrr:
                    return 100;
                case TowerTypes.Zrrrrap:
                    return 100;
                case TowerTypes.Bonker:
                    return 100;
                case TowerTypes.Pow:
                    return 100;
                case TowerTypes.Poison:
                    return 100;
                case TowerTypes.Poogas:
                    return 100;
                case TowerTypes.Thump:
                    return 100;
                case TowerTypes.Laze:
                    return 100;
                case TowerTypes.Trap:
                    return 100;
                case TowerTypes.Carrier:
                    return 100;
                default:
                    throw new ArgumentException("Missing tower creation case for: " + type);
            }
        }

        public static int GetUpgradeCost(TowerTypes type, int level)
        {
            return GetTowerCost(type) * (level + 1);
        }

        /// <summary>
        /// This creates a tower of the given type at the given location.
        /// This *shouldn't* have a display, map should handle batch rendering all tower visuals.
        /// </summary>
        public static Tower CreateTower(Map map, GridLoc loc, TowerTypes type)
        {
            int cost = GetTowerCost(type);
            switch (type)
            {
                case TowerTypes.Pewpew:
                    return new PewPewTower(loc, map, cost);
                case TowerTypes.Bwoosh:
                    return new BwooshTower(loc, map, cost);
                case TowerTypes.Brrrr:
                    return new BrrrrTower(loc, map, cost);
                case TowerTypes.Zrrrrap:
                    return new ZrrrrapTower(loc, map, cost);
                case TowerTypes.Bonker:
                    return new BonkerTower(loc, map, cost);
                case TowerTypes.Pow:
                    return new PowTower(loc, map, cost);
                case TowerTypes.Poison:
                    return new PoisonTower(loc, map, cost);
                case TowerTypes.Poogas:
                    return new PoogasTower(loc, map, cost);
                case TowerTypes.Thump:
                    return new ThumpTower(loc, map, cost);
                case TowerTypes.Laze:
                    return new LazeTower(loc, map, cost);
                case TowerTypes.Trap:
                    return new TrapTower(loc, map, cost);
                case TowerTypes.Carrier:
                    return new CarrierTower(loc, map, cost);
                default:
                    throw new ArgumentException("Missing tower creation case for: " + type);
            }
        }

        private bool blocks;
        private int cost;
        private int upgradeLevel = 0;
        private TowerTypes type;

        public readonly float damageBoost = 1;
        public readonly float rangeBoost = 1;
        public readonly float atkSpeedBoost = 1;

        protected readonly Vector2 worldPos;
        
        private readonly Color mainColor;

        public Vector2 pos
        {
            get
            {
                return worldPos;
            }
        }

        public abstract float range
        {
            get;
        }

        protected Tower(Map map, TowerTypes type, GridLoc pos, int width, int height, bool blocks, int cost)
            : base(pos, width, height)
        {
            this.blocks = blocks;
            this.cost = cost;
            this.type = type;

            rect = makeRect(map);
            worldPos = new Vector2(loc.x + width / 2f, loc.y + height / 2f) * map.gridScale;
            rect.position = new Vector3(worldPos.X - rect.scale.X / 2, worldPos.Y - rect.scale.Y / 2, -20);

            mainColor = rect.mainColor;

            for (int i = 0; i < map.towerBoosts.Count; i++)
            {
                var b = map.towerBoosts[i];

                if (b.touching(this))
                {
                    damageBoost *= b.damageBoost;
                    rangeBoost *= b.rangeBoost;
                    atkSpeedBoost *= b.atkSpeedBoost;
                }
            }

            state = TowerState.BUILD;
        }

        protected abstract RoundedRectangle makeRect(Map map);
        public abstract void Update(float dt);

        TowerState _state;
        public TowerState state
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
                switch (value)
                {
                    case TowerState.BUILD:
                        rect.mainColor = Color.Blue;
                        buildTimer = -TowerBuildTime;
                        break;
                    case TowerState.FINISH_BUILD:
                        rect.mainColor = Color.Red;
                        break;
                    case TowerState.ACTIVE:
                        rect.mainColor = mainColor;
                        Enabled();
                        break;
                    case TowerState.DESTROY:
                        rect.mainColor = Color.Green;
                        buildTimer = -TowerBuildTime;
                        Disabled();
                        break;
                    case TowerState.FINISH_DESTROY:
                        rect.mainColor = Color.Yellow;
                        break;
                    case TowerState.DEAD:
                        rect.mainColor = Color.Orange;
                        break;
                }

                StateChanged();
            }
        }

        float buildTimer = -TowerBuildTime;
        public void BuildUpdate(float dt)
        {
            buildTimer += dt;
            switch (state)
            {
                case TowerState.BUILD:
                    if (buildTimer > 0)
                        state = TowerState.FINISH_BUILD;
                    break;
                case TowerState.FINISH_BUILD:
                    if (buildTimer > NavGrid.buildDelay)
                        state = TowerState.ACTIVE;
                    break;
                case TowerState.DESTROY:
                    if (buildTimer > 0)
                        state = TowerState.FINISH_DESTROY;
                    break;
                case TowerState.FINISH_DESTROY:
                    if (buildTimer > NavGrid.buildDelay)
                        state = TowerState.DEAD;
                    break;
            }
        }

        public override bool blocksPathing()
        {
            return blocks;
        }

        public override bool mustBePathedTo()
        {
            return false;
        }

        public int totalCost
        {
            get
            {
                return cost;
            }
        }

        public int getUpgradeCost()
        {
            return GetUpgradeCost(type, upgradeLevel);
        }

        public void Upgrade()
        {
            if (state != TowerState.ACTIVE)
            {
                Logger.WriteLine(LogType.POSSIBLE_ERROR, "Upgrading a non-active tower??");
                //we let the upgrade go through because user resources have already been deducted.
            }

            cost += getUpgradeCost();
            upgradeLevel++;
            TowerUpgraded();
        }

        protected abstract void TowerUpgraded();
        
        public bool inNavi = false;

        public event Action<Tower> stateChanged;

        protected void StateChanged()
        {
            if (stateChanged != null)
                stateChanged(this);
        }

        public abstract void OnRoundStart();
        public abstract void OnRoundEnd();

        public abstract void Disabled();
        public abstract void Enabled();

        public abstract void Dispose();
    }

    enum TowerState
    {
        BUILD,
        FINISH_BUILD,
        ACTIVE,
        DESTROY,
        FINISH_DESTROY,
        DEAD
    }

    abstract class TowerPreview
    {
        public static TowerPreview CreatePreview(Map map, TowerTypes type)
        {
            switch (type)
            {
                case TowerTypes.Pewpew:
                    return new PewPewPreview(map);
                case TowerTypes.Bwoosh:
                    return new BwooshPreview(map);
                case TowerTypes.Brrrr:
                    return new BrrrrPreview(map);
                case TowerTypes.Zrrrrap:
                    return new ZrrrrapPreview(map);
                case TowerTypes.Bonker:
                    return new BonkerPreview(map);
                case TowerTypes.Pow:
                    return new PowPreview(map);
                case TowerTypes.Poison:
                    return new PoisonPreview(map);
                case TowerTypes.Poogas:
                    return new PoogasPreview(map);
                case TowerTypes.Thump:
                    return new ThumpPreview(map);
                case TowerTypes.Laze:
                    return new LazePreview(map);
                case TowerTypes.Trap:
                    return new TrapPreview(map);
                case TowerTypes.Carrier:
                    return new CarrierPreview(map);
                default:
                    throw new ArgumentException("Missing tower creation case for: " + type);
            }
        }

        protected Map map;

        RoundedRectangleBatch_3D disp;
        DashedCircle_3D rangeDisp;

        Vector2 worldPos;
        GridLoc loc;

        readonly Color main;
        readonly Color border;

        protected Vector2 pos
        {
            get
            {
                return worldPos;
            }
        }

        protected TowerPreview(Map map)
        {
            this.map = map;

            disp = new RoundedRectangleBatch_3D(map.stage, map.em, 0);
            disp.rects.Add(makeDisp());

            rangeDisp = new DashedCircle_3D(map.stage, map.em, 1);

            main = disp.rects[0].mainColor;
            border = disp.rects[0].borderColor;

            worldPos = new Vector2((loc.x + width / 2f) * map.gridScale, (loc.y + height / 2f) * map.gridScale);
            RoundedRectangle rect = disp.rects[0];
            rect.position = new Vector3(worldPos.X - rect.scale.X / 2, worldPos.Y - rect.scale.Y / 2, -20);
            disp.rects[0] = rect;

            rangeDisp.circles.Add(new DashedCircle(
                new Vector3(pos, 0),
                range,
                5f,
                1f,
                0f,
                Color.LightGoldenrodYellow
                ));

            map.em.addUpdateListener(0, Update);
        }

        public void Update(float dt)
        {
            DashedCircle c = rangeDisp.circles[0];

            c.rot += dt * 0.25f;

            rangeDisp.circles[0] = c;
        }

        public void Dispose()
        {
            map.em.removeUpdateListener(Update);
            disp.Dispose();
            rangeDisp.Dispose();
        }

        public bool enabled
        {
            get
            {
                return rangeDisp.enabled;
            }

            set
            {
                rangeDisp.enabled = value;
            }
        }

        /// <summary>
        /// Call to set the display to work as a preview of the tower.
        /// Should be semi-transparent, and have a red overlay if valid is false.
        /// </summary>
        public void SetPreview(bool valid)
        {
            Color m = main;
            Color b = border;
            if (valid)
            {//we can build, so just set the colors approp.
                m.A = 100;
                b.A = 100;
            }
            else
            {//can't build, so overlay red
                m.R = 255;
                b.R = 255;

                m.A = 100;
                b.A = 100;
            }

            RoundedRectangle rect = disp.rects[0];
            rect.mainColor = m;
            rect.borderColor = b;
            disp.rects[0] = rect;
        }

        public GridLoc position
        {
            get
            {
                return loc;
            }

            set
            {
                loc = value;

                worldPos = new Vector2((loc.x + width / 2f) * map.gridScale, (loc.y + height / 2f) * map.gridScale);
                RoundedRectangle rect = disp.rects[0];
                rect.position = new Vector3(worldPos.X - rect.scale.X / 2, worldPos.Y - rect.scale.Y / 2, -20);
                disp.rects[0] = rect;

                DashedCircle c = rangeDisp.circles[0];

                c.center = new Vector3(pos, 0);
                c.radius = range;

                rangeDisp.circles[0] = c;
            }
        }

        public abstract RoundedRectangle makeDisp();

        public abstract int width
        {
            get;
        }

        public abstract int height
        {
            get;
        }

        public abstract bool blocksPathing
        {
            get;
        }

        public abstract float range
        {
            get;
        }
    }
}
