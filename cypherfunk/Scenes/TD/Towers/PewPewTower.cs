﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class PewPewTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 1;
        public const int height = 1;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.Gold;
        public const float baseRange = 50f;

        Map map;

        public PewPewTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Pewpew, pos, width, height, blocks, cost)
        {
            this.map = map;

            damage = 20 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 1f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        float damage;
        public override void Update(float dt)
        {
            //this should be a standard 'homing' bullet attack
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Enemy e = map.FindClosestToEndEnemy(worldPos, attackRange);
                if (e != null)
                {
                    map.FirePewPewBullet(e, worldPos, damage);
                    attackTimer = -attackCD;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
        }

        protected override void TowerUpgraded()
        {
            damage += 10 * damageBoost;
        }

        public override void Dispose()
        {
        }
    }

    class PewPewBullet : Bullet
    {
        public float speed = 150f;
        public float damage;
        Enemy target;

        Map map;
        public Vector2 position;
        public PewPewBullet(Map map, Enemy target, Vector2 pos, float damage)
        {
            this.map = map;
            this.position = pos;
            this.target = target;
            this.damage = damage;

            disp = new RoundedRectangle();
            disp.scale = new Vector2(5, 5);
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -10);
            disp.radius = 2.5f;
            disp.borderThickness = 1;
            disp.mainColor = Color.Black;
            disp.borderColor = Color.Gold;
        }

        public override bool Update(float dt)
        {
            if (target.isDead)
            {
                return true;
            }

            Vector2 dir = target.position - position;
            float len = dir.Length();


            if (len < dt * speed)
            {
                target.DealDamage(damage, DamageSource.NORMAL);
                return true;
            }

            position = position + dir / len * dt * speed;
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -1f);

            return false;
        }

        public override void Dispose()
        {
        }
    }

    class PewPewPreview : TowerPreview
    {

        public PewPewPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return PewPewTower.width;
            }
        }

        public override int height
        {
            get
            {
                return PewPewTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return PewPewTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = PewPewTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return PewPewTower.makeDisp(map);
        }
    }
}
