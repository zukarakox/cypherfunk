﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class BrrrrTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                10f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.DeepSkyBlue;
        public static readonly Color attackColor = Color.SkyBlue;
        public const float baseRange = 40f;

        Map map;
        
        GradientCircle_3D attackAnim;

        public BrrrrTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Brrrr, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new GradientCircle_3D(map.stage, map.em, -1);
            attackAnim.enabled = false;
            attackAnim.circles.Add(new GradientCircle(Vector3.Zero, 0, Color.Red, Color.Green));

            effect.damage = 2 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 2f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        ColdEffect effect = new ColdEffect(0.75f, 3f, 2f);
        float animTimer = -1;
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {
                if (map.AnyEnemyInRange(worldPos, attackRange))
                {
                    attackTimer = -attackCD;
                    map.ApplyAOEEffect(worldPos, attackRange, effect);
                    animTimer = 0;
                    attackAnim.enabled = true;
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
                else
                {
                    attackAnim.circles[0] = new GradientCircle(
                        new Vector3(worldPos, -20),
                        attackRange * (animTimer / 0.25f),
                        Color.Transparent,
                        Color.LightSkyBlue);
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            effect.damage += 1 * damageBoost;
            effect.strength *= 0.9f;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class BrrrrPreview : TowerPreview
    {

        public BrrrrPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return BrrrrTower.width;
            }
        }

        public override int height
        {
            get
            {
                return BrrrrTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return BrrrrTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = BrrrrTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return BrrrrTower.makeDisp(map);
        }
    }
}
