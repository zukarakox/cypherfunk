﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class LazeTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.LemonChiffon;
        public const float baseRange = 100f;

        Map map;

        ColoredLines_3D attackAnim;

        public LazeTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Laze, pos, width, height, blocks, cost)
        {
            this.map = map;

            attackAnim = new ColoredLines_3D(map.stage, map.em, 3);
            attackAnim.enabled = false;
            attackAnim.lineSegments.Add(new ColoredLineSegment());
            attackAnim.lineWidth = attackWidth;

            damage = 80 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 2f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;
        
        const float attackWidth = 12f;
        float damage;
        float animTimer = 0;
        float attackTimer = 0;
        public override void Update(float dt)
        {
            attackTimer += dt;
            if (attackTimer > 0)
            {//attack right
                Vector2 atkStart = new Vector2(worldPos.X, worldPos.Y - attackWidth / 2);
                Vector2 atkEnd = new Vector2(worldPos.X + attackRange, worldPos.Y + attackWidth / 2);
                List<Enemy> enemies = map.FindAllEnemiesTouchingBox(atkStart, atkEnd);

                if (enemies.Count > 0)
                {
                    for (int i = 0; i < enemies.Count; i++)
                    {
                        enemies[i].DealDamage(damage, DamageSource.NORMAL);
                    }

                    attackTimer = -attackCD;

                    animTimer = 0;
                    attackAnim.enabled = true;
                    attackAnim.lineSegments[0] = new ColoredLineSegment(
                        new Vector3(worldPos, -20),
                        new Vector3(worldPos.X + attackRange, worldPos.Y, -20),
                        Color.Plum,
                        Color.Sienna);
                }
            }
            if (attackTimer > 0)
            {//attack left
                Vector2 atkStart = new Vector2(worldPos.X - attackRange, worldPos.Y - attackWidth / 2);
                Vector2 atkEnd = new Vector2(worldPos.X, worldPos.Y + attackWidth / 2);
                List<Enemy> enemies = map.FindAllEnemiesTouchingBox(atkStart, atkEnd);

                if (enemies.Count > 0)
                {
                    for (int i = 0; i < enemies.Count; i++)
                    {
                        enemies[i].DealDamage(damage, DamageSource.NORMAL);
                    }

                    attackTimer = -attackCD;

                    animTimer = 0;
                    attackAnim.enabled = true;
                    attackAnim.lineSegments[0] = new ColoredLineSegment(
                        new Vector3(worldPos, -20),
                        new Vector3(worldPos.X - attackRange, worldPos.Y, -20),
                        Color.Plum,
                        Color.Sienna);
                }
            }
            if (attackTimer > 0)
            {//attack down
                Vector2 atkStart = new Vector2(worldPos.X - attackWidth / 2, worldPos.Y);
                Vector2 atkEnd = new Vector2(worldPos.X + attackWidth / 2, worldPos.Y + attackRange);
                List<Enemy> enemies = map.FindAllEnemiesTouchingBox(atkStart, atkEnd);

                if (enemies.Count > 0)
                {
                    for (int i = 0; i < enemies.Count; i++)
                    {
                        enemies[i].DealDamage(damage, DamageSource.NORMAL);
                    }

                    attackTimer = -attackCD;

                    animTimer = 0;
                    attackAnim.enabled = true;
                    attackAnim.lineSegments[0] = new ColoredLineSegment(
                        new Vector3(worldPos, -20),
                        new Vector3(worldPos.X, worldPos.Y + attackRange, -20),
                        Color.Plum,
                        Color.Sienna);
                }
            }
            if (attackTimer > 0)
            {//attack up
                Vector2 atkStart = new Vector2(worldPos.X - attackWidth / 2, worldPos.Y - attackRange);
                Vector2 atkEnd = new Vector2(worldPos.X + attackWidth / 2, worldPos.Y);
                List<Enemy> enemies = map.FindAllEnemiesTouchingBox(atkStart, atkEnd);

                if (enemies.Count > 0)
                {
                    for (int i = 0; i < enemies.Count; i++)
                    {
                        enemies[i].DealDamage(damage, DamageSource.NORMAL);
                    }

                    attackTimer = -attackCD;

                    animTimer = 0;
                    attackAnim.enabled = true;
                    attackAnim.lineSegments[0] = new ColoredLineSegment(
                        new Vector3(worldPos, -20),
                        new Vector3(worldPos.X, worldPos.Y - attackRange, -20),
                        Color.Plum,
                        Color.Sienna);
                }
            }

            if (animTimer >= 0)
            {
                animTimer += dt;
                if (animTimer > 0.25f)
                {
                    animTimer = -1;
                    attackAnim.enabled = false;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
            attackAnim.enabled = false;
            animTimer = -1;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
            attackAnim.enabled = false;
        }

        protected override void TowerUpgraded()
        {
            damage += 60 * damageBoost;
        }

        public override void Dispose()
        {
            attackAnim.Dispose();
        }
    }

    class LazePreview : TowerPreview
    {
        public LazePreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return LazeTower.width;
            }
        }

        public override int height
        {
            get
            {
                return LazeTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return LazeTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = LazeTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return LazeTower.makeDisp(map);
        }
    }
}
