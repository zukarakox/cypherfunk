﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class PoisonTower : Tower
    {
        public static RoundedRectangle makeDisp(Map map)
        {
            return new RoundedRectangle(
                Vector3.Zero,
                new Vector2(width, height) * map.gridScale,
                5f,
                3f,
                mainColor,
                borderColor);
        }

        protected override RoundedRectangle makeRect(Map map)
        {
            return makeDisp(map);
        }

        public const int width = 2;
        public const int height = 2;
        public const bool blocks = true;
        public static readonly Color mainColor = Color.Black;
        public static readonly Color borderColor = Color.Crimson;
        public const float baseRange = 60f;

        Map map;

        public PoisonTower(GridLoc pos, Map map, int cost)
            : base(map, TowerTypes.Poison, pos, width, height, blocks, cost)
        {
            this.map = map;

            effect.dps = 50 * damageBoost;
            attackRange = baseRange * rangeBoost;
            attackCD = 1f * atkSpeedBoost;
        }

        readonly float attackCD;
        readonly float attackRange;

        float attackTimer = 0;
        PoisonEffect effect = new PoisonEffect(50, 5);
        public override void Update(float dt)
        {
            //this should be a standard 'homing' bullet attack
            attackTimer += dt;
            if (attackTimer > 0)
            {
                Enemy e = map.FindClosestToEndEnemyWithWeakerPoison(worldPos, attackRange, effect);
                if (e != null)
                {
                    map.FirePoisonBullet(e, worldPos, effect);
                    attackTimer = -attackCD;
                }
            }
        }

        public override float range
        {
            get
            {
                return attackRange;
            }
        }

        public override void OnRoundStart()
        {
            attackTimer = 0;
        }

        public override void OnRoundEnd()
        {
            attackTimer = 0;
        }

        public override void Enabled()
        {
        }

        public override void Disabled()
        {
        }

        protected override void TowerUpgraded()
        {
            effect.dps += 20 * damageBoost;
        }

        public override void Dispose()
        {
        }
    }

    class PoisonBullet : Bullet
    {
        public float speed = 150f;
        Enemy target;

        Map map;
        public Vector2 position;
        PoisonEffect effect;
        public PoisonBullet(Map map, Enemy target, Vector2 pos, PoisonEffect effect)
        {
            this.map = map;
            this.position = pos;
            this.target = target;
            this.effect = effect;

            disp = new RoundedRectangle();
            disp.scale = new Vector2(5, 5);
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -10);
            disp.radius = 2.5f;
            disp.borderThickness = 1;
            disp.mainColor = Color.Black;
            disp.borderColor = Color.MediumPurple;
        }

        public override bool Update(float dt)
        {
            if (target.isDead)
            {
                return true;
            }

            Vector2 dir = target.position - position;
            float len = dir.Length();


            if (len < dt * speed)
            {
                effect.Apply(target);
                return true;
            }

            position = position + dir / len * dt * speed;
            disp.position = new Vector3(position.X - disp.scale.X / 2, position.Y - disp.scale.Y / 2, -1f);

            return false;
        }

        public override void Dispose()
        {
        }
    }

    class PoisonPreview : TowerPreview
    {

        public PoisonPreview(Map map)
            : base(map)
        {
        }

        public override int width
        {
            get
            {
                return PoisonTower.width;
            }
        }

        public override int height
        {
            get
            {
                return PoisonTower.height;
            }
        }

        public override bool blocksPathing
        {
            get
            {
                return PoisonTower.blocks;
            }
        }

        public override float range
        {
            get
            {
                float r = PoisonTower.baseRange;

                for (int i = 0; i < map.towerBoosts.Count; i++)
                {
                    var b = map.towerBoosts[i];

                    if (b.touching(pos, width, height))
                    {
                        r *= b.rangeBoost;
                    }
                }

                return r;
            }
        }

        public override RoundedRectangle makeDisp()
        {
            return PoisonTower.makeDisp(map);
        }
    }
}
