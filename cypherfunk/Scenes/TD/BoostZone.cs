﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Diagnostics;

using SharpDX;
using log;

namespace cypherfunk.TD
{
    abstract class BoostZone
    {
        protected Vector2 position;
        protected float radius;
        protected Map map;

        Color color;

        public DashedCircle disp;

        protected BoostZone(Map map, Vector2 position, float radius, Color color)
        {
            this.map = map;
            this.position = position;
            this.radius = radius;
            this.color = color;
            
            disp = new DashedCircle(
                new Vector3(position, 0),
                radius,
                5f,
                1f,
                0f,
                color
                );

            map.em.addUpdateListener(0, Update);
        }

        public void Update(float dt)
        {
            disp.rot += dt * 0.25f;
        }

        public void Dispose()
        {
            map.em.removeUpdateListener(Update);
        }
    }

    class EnemyBoostZone : BoostZone
    {
        /// <summary>
        /// Boost less than one slows enemies.
        /// </summary>
        public readonly float speedBoost;

        /// <summary>
        /// Strength greater than one slows enemies.
        /// </summary>
        public static EnemyBoostZone makeEnemySpeedBoost(Map map, Vector2 position, float radius, float strength)
        {
            return new EnemyBoostZone(map, position, radius, Color.Blue, strength);
        }

        public EnemyBoostZone(Map map, Vector2 position, float radius, Color color, float speedBoost)
            : base(map, position, radius, color)
        {
            this.speedBoost = 1 / speedBoost; //see atk speed tower boost for why inverted
        }

        public bool touching(Enemy e)
        {
            float dSquared = Vector2.DistanceSquared(position, e.position);

            return dSquared < (radius + e.radius) * (radius + e.radius);
        }
    }

    class TowerBoostZone : BoostZone
    {
        public readonly float damageBoost;

        /// <summary>
        /// Multiplied to attack cooldown, so values less than 1 make the tower attack faster.
        /// </summary>
        public readonly float atkSpeedBoost;
        public readonly float rangeBoost;

        public static TowerBoostZone makeDamageBoost(Map map, Vector2 position, float radius, float strength)
        {
            return new TowerBoostZone(map, position, radius, Color.Gold, strength, 1, 1);
        }

        public static TowerBoostZone makeRangeBoost(Map map, Vector2 position, float radius, float strength)
        {
            return new TowerBoostZone(map, position, radius, Color.Purple, 1, 1, strength);
        }

        /// <summary>
        /// To make the tower attack 2x faster, strength = 2.0.
        /// 
        /// This gets inverted in the constructor, as towers use attackCD * atkSpdBoost.
        /// 
        /// I just want all constructors to have strength greater than 1 be 'good' for the player, so we don't have to worry about it elsewhere.
        /// </summary>
        public static TowerBoostZone makeAttackSpeedBoost(Map map, Vector2 position, float radius, float strength)
        {
            return new TowerBoostZone(map, position, radius, Color.Red, 1, strength, 1);
        }

        private TowerBoostZone(Map map, Vector2 position, float radius, Color color, float damageBoost, float atkSpeedBoost, float rangeBoost)
            : base(map, position, radius, color)
        {
            this.damageBoost = damageBoost;
            this.atkSpeedBoost =  1 / atkSpeedBoost;
            this.rangeBoost = rangeBoost;
        }

        public bool touching(MapObject o)
        {
            GridLoc loc = o.getLocation();
            Vector2 oPos = new Vector2(loc.x + o.getWidth() / 2f, loc.y + o.getHeight() / 2f) * map.gridScale;

            float dSquared = Vector2.DistanceSquared(position, oPos);
            float innerRad = Math.Min(o.getWidth(), o.getHeight()) * map.gridScale * 0.5f;

            if (dSquared < (innerRad + radius) * (innerRad + radius))
                return true;

            float outerRad = (float)Math.Sqrt(o.getWidth() * o.getWidth() + o.getHeight() * o.getHeight()) * map.gridScale * 0.5f;

            if (dSquared > (outerRad + radius) * (outerRad + radius))
                return false;

            //TODO: do a finer circle v aabb test here
            //see if the point on the circle closest to the aabb center is inside of the aabb
            return true;
        }

        public bool touching(Vector2 oPos, int width, int height)
        {
            float dSquared = Vector2.DistanceSquared(position, oPos);
            float innerRad = Math.Min(width, height) * map.gridScale * 0.5f;

            if (dSquared < (innerRad + radius) * (innerRad + radius))
                return true;

            float outerRad = (float)Math.Sqrt(width * width + height * height) * map.gridScale * 0.5f;

            if (dSquared > (outerRad + radius) * (outerRad + radius))
                return false;

            //TODO: do a finer circle v aabb test here
            //see if the point on the circle closest to the aabb center is inside of the aabb
            return true;
        }
    }
}
