﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using log;

namespace cypherfunk.TD
{
    struct GridLoc
    {
        public int x;
        public int y;

        public GridLoc(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public float Dist(GridLoc o)
        {
            return Dist(o.x, o.y);
        }

        public float Dist(int oX, int oY)
        {
            float dx = oX - x;
            float dy = oY - y;
            return (float)Math.Sqrt(dx * dx + dy * dy);
        }

        public float DistSquared(int oX, int oY)
        {
            float dx = oX - x;
            float dy = oY - y;
            return dx * dx + dy * dy;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GridLoc))
                return false;

            return Equals((GridLoc)obj);
        }

        public override int GetHashCode()
        {
            return x * 5827 + y;
        }

        public bool Equals(GridLoc o)
        {
            return x == o.x && y == o.y;
        }

        public static bool operator ==(GridLoc a, GridLoc b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(GridLoc a, GridLoc b)
        {
            return !a.Equals(b);
        }
    }

    abstract class MapObject
    {//static objects that inhabit the map, max of 1 per grid cell
        //everything in here *NEEDS* to be thread-safe with the navigation thread.
        protected readonly GridLoc loc;
        readonly int width;
        readonly int height;

        public RoundedRectangle rect;

        public MapObject(GridLoc loc, int width, int height)
        {
            this.loc = loc;
            this.width = width;
            this.height = height;
        }

        public GridLoc getLocation()
        {
            return loc;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public bool covers(GridLoc o)
        {
            return (o.x >= loc.x && o.x < loc.x + width
                && o.y >= loc.y && o.y < loc.y + height);
        }

        public abstract bool blocksPathing();
        public abstract bool mustBePathedTo();
    }

    class PathNode : MapObject
    {
        public bool first;

        public PathNode(Map map, int order, int numNodes, GridLoc loc, int width, int height)
            : base(loc, width, height)
        {
            first = order == 0;

            rect = new RoundedRectangle();
            rect.scale = new Vector2(width * map.gridScale, height * map.gridScale);
            rect.radius = rect.scale.X / 2f;
            rect.borderThickness = 0;

            if (order == 0)
                rect.mainColor = Color.Green;
            else if (order < numNodes - 1)
                rect.mainColor = Color.Yellow;
            else
                rect.mainColor = Color.Red;

            rect.borderColor = Color.Black;
            rect.position = new Vector3(loc.x * map.gridScale, loc.y * map.gridScale, -20f);
        }

        public override bool mustBePathedTo()
        {
            return !first;
        }

        public override bool blocksPathing()
        {
            return false;
        }
    }

    class TeleporterEntrance
    {
        public RoundedRectangle rect;
        public TeleporterExit exit;
        public int num;

        public GridLoc loc;
        public int width;
        public int height;

        public TeleporterPather telePath;
        public Vector2 center;
        public float radius;

        public TeleporterEntrance(Map map, GridLoc loc, int siz, TeleporterExit exit, int num)
        {
            this.exit = exit;
            this.num = num;

            this.loc = loc;
            this.width = siz;
            this.height = siz;

            rect = new RoundedRectangle();
            rect.scale = new Vector2(siz * map.gridScale, siz * map.gridScale);
            rect.radius = rect.scale.X / 2f;
            rect.borderThickness = 0;

            rect.mainColor = Color.CornflowerBlue;

            rect.borderColor = Color.Black;
            rect.position = new Vector3(loc.x * map.gridScale, loc.y * map.gridScale, -20f);

            center = new Vector2(rect.position.X, rect.position.Y) + rect.scale / 2;
            radius = rect.scale.X * 0.5f * 0.9f; //make it slightly smaller than a full square, enemies don't barely touch it outside of a square

            telePath = new TeleporterPather(map, this);
        }

        public void Dispose()
        {
            telePath.Dispose();
        }
    }

    /// <summary>
    /// Class to display the tele-pairings during the build phase
    /// Or always?
    /// </summary>
    class TeleporterPather
    {
        const float speed = 150f;

        float timer = 0;
        Vector2 startPos;
        Vector2 goalPos;
        Vector2 pos;

        EventManager em;
        RoundedRectangle_3D disp;
        TeleporterEntrance ent;
        TeleporterExit ext;

        public TeleporterPather(Map map, TeleporterEntrance ent)
        {
            this.ent = ent;
            this.ext = ent.exit;
            this.em = map.em;
            
            startPos = ent.center;

            GridLoc extPos = ext.getLocation();
            goalPos = new Vector2(extPos.x + ext.getWidth() * 0.5f, extPos.y + ext.getHeight() * 0.5f) * map.gridScale;

            pos = startPos;

            disp = new RoundedRectangle_3D(map.stage, map.em, 1);
            disp.scale = new Vector2(map.gridScale, map.gridScale) * 0.75f;
            disp.pos = new Vector3(pos.X - disp.scale.X / 2, pos.Y - disp.scale.Y / 2, -10);
            disp.radius = disp.scale.X / 2f;
            disp.borderThickness = 0;
            disp.mainColor = new Color(0, 255, 0, 100);
            disp.borderColor = disp.mainColor;
            
            disp.enabled = false;

            em.addUpdateListener(1, Update);
        }

        public void Update(float dt)
        {
            if (timer > 0)
            {
                Vector2 d = goalPos - pos;
                float dist = d.Length();

                if (dist < speed * dt)
                {//we reached the end
                    pos = startPos;
                    disp.enabled = false;
                    timer = -1f;
                    return;
                }

                pos = pos + d / dist * speed * dt;
                
                disp.pos = new Vector3(pos.X - disp.scale.X / 2, pos.Y - disp.scale.Y / 2, -10);
            }
            else
            {
                timer += dt;
                if (timer > 0)
                {
                    disp.enabled = true;
                }
            }
        }


        public void Dispose()
        {
            em.removeUpdateListener(Update);
            disp.Dispose();
        }
    }

    class TeleporterExit : MapObject
    {
        public int num;

        public TeleporterExit(Map map, GridLoc loc, int width, int height, int num)
            : base(loc, width, height)
        {
            this.num = num;

            rect = new RoundedRectangle();
            rect.scale = new Vector2(width * map.gridScale, height * map.gridScale);
            rect.radius = rect.scale.X / 2f;
            rect.borderThickness = 0;

            rect.mainColor = Color.CadetBlue;

            rect.borderColor = Color.Black;
            rect.position = new Vector3(loc.x * map.gridScale, loc.y * map.gridScale, -20f);
        }

        public override bool mustBePathedTo()
        {
            return true;
        }

        public override bool blocksPathing()
        {
            return false;
        }
    }
}
