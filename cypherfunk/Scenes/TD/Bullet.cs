﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;

namespace cypherfunk.TD
{
    abstract class Bullet
    {
        public RoundedRectangle disp;

        /// <summary>
        /// Return true if this bullet should be disposed after.
        /// </summary>
        abstract public bool Update(float dt);

        public abstract void Dispose();
    }
}
