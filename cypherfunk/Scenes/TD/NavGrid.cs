﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Diagnostics;

using SharpDX;
using log;

namespace cypherfunk.TD
{
    /// <summary>
    /// A navigation grid for the map.
    /// 
    /// Has two different sets of grids, that should be kept in relative lockstep, with some important distinctions.
    /// 
    /// There is the 'current' grid that is editted with add/remove Object.
    /// This grid is used for deciding if new additions to the grid are even allowed. (Is there something already there, will it block pathing, etc)
    /// 
    /// The 'nav' grid is mainly handled by a worker thread, editted with add/remove from Navi.
    /// This should only have objects that are currently changing worker navigation.
    /// It takes buildDelay time for any changes made by add/remove from Navi to take effect.
    /// See buildDelay notes for why.
    /// 
    /// 
    /// For example, the path for adding a new tower is:
    ///     1. check 'canPlaceObject'
    ///     2. placeObject
    ///     3. let the tower build (while it's not changing pathing)
    ///     4. when the tower is 'finishing', addToNavi, tower will become active in 2.5s
    /// 
    /// To remove a tower, first class removeFromNavi, then removeObject
    /// </summary>
    class NavGrid
    {
        class NavMesh
        {
            class SearchNode
            {
                public GridLoc loc;
                public SearchNode direct;
                public SearchNode goTo;

                public SearchNode(GridLoc loc)
                {
                    this.loc = loc;
                }
            }

            /// <summary>
            /// Checks to see if we can place a new object defined by the arguments.
            /// map.travelNode[0] must be defined.
            /// </summary>
            public static bool canPlaceObject(MapObject[][] grid, int width, int height, MapObject[] travelPoints, 
                int objX, int objY, int objWidth, int objHeight, bool blocksPathing, bool mustBePathedTo)
            {
                if (!blocksPathing && !mustBePathedTo)
                    return true;

                if (blocksPathing && mustBePathedTo)
                    throw new ArgumentException("We're trying to path to an object that blocks pathing?");

                bool[][] searchNodes = new bool[height][];
                for (int i = 0; i < searchNodes.Length; i++)
                {
                    searchNodes[i] = new bool[width];
                }

                //do a breadth-first search starting from the spawn point
                //must be able to reach each travel point from spawn
                Queue<GridLoc> searchLocs = new Queue<GridLoc>();

                {
                    //add a starting location
                    GridLoc loc = travelPoints[0].getLocation();
                    searchNodes[loc.y][loc.x] = true;

                    if (blocksPathing)
                    {
                        foreach (GridLoc neighbor in getAllNeighborLocs(grid, width, height, loc, objX, objY, objWidth, objHeight))
                        {
                            //add a new fringe node if we haven't already been there, and we can path there
                            if (searchNodes[neighbor.y][neighbor.x] == false)
                            {
                                searchLocs.Enqueue(neighbor);
                            }
                        }
                    }
                    else
                    {
                        foreach (GridLoc neighbor in getAllNeighborLocs(grid, width, height, loc))
                        {
                            //add a new fringe node if we haven't already been there, and we can path there
                            if (searchNodes[neighbor.y][neighbor.x] == false)
                            {
                                searchLocs.Enqueue(neighbor);
                            }
                        }
                    }
                }

                while (searchLocs.Count != 0)
                {
                    GridLoc curLoc = searchLocs.Dequeue();

                    if (searchNodes[curLoc.y][curLoc.x])
                        continue; //we've already found a valid path to this node, keep going

                    //save this node, we've found the fastest way to reach it
                    searchNodes[curLoc.y][curLoc.x] = true;

                    if (mustBePathedTo && curLoc.y == objY && curLoc.x == objX)
                        return true;

                    //now add all neighboring nodes
                    if (blocksPathing)
                    {
                        foreach (GridLoc neighbor in getAllNeighborLocs(grid, width, height, curLoc, objX, objY, objWidth, objHeight))
                        {
                            //add a new fringe node if we haven't already been there, and we can path there
                            if (searchNodes[neighbor.y][neighbor.x] == false)
                            {
                                searchLocs.Enqueue(neighbor);
                            }
                        }
                    }
                    else
                    {
                        foreach (GridLoc neighbor in getAllNeighborLocs(grid, width, height, curLoc))
                        {
                            //add a new fringe node if we haven't already been there, and we can path there
                            if (searchNodes[neighbor.y][neighbor.x] == false)
                            {
                                searchLocs.Enqueue(neighbor);
                            }
                        }
                    }
                }

                //ok -- check to see what our pathing search found
                if (blocksPathing)
                {
                    //if this new object blocks pathing, make sure we can still reach all of the required points
                    for (int i = 1; i < travelPoints.Length; i++)
                    {
                        if (travelPoints[i] == null) //if we're checking this during init
                            continue;

                        GridLoc loc = travelPoints[i].getLocation();
                        if (searchNodes[loc.y][loc.x] == false)
                            return false;
                    }
                }
                else if (mustBePathedTo)
                {
                    //new object must be pathed to, make sure we can make it...
                    //we check during the search
                    return false;
                }
                else
                    throw new Exception("Somethin weird is goin on");

                return true;

            }

            /// <summary>
            /// For each cell in the map, this contains a 'destination' cell
            /// A unit should look up the cell where he is in this map, and go to where it points at.
            /// </summary>
            GridLoc[][] goToCell;

            public NavMesh(MapObject[][] navGrid, int navWidth, int navHeight, MapObject goal)
            {
                goToCell = new GridLoc[navHeight][];
                for (int i = 0; i < goToCell.Length; i++)
                    goToCell[i] = new GridLoc[navWidth];

                SearchNode[][] searchNodes = new SearchNode[navHeight][];
                for (int i = 0; i < searchNodes.Length; i++)
                {
                    searchNodes[i] = new SearchNode[navWidth];
                }

                //do a breadth-first search starting from the goal
                Queue<SearchNode> searchLocs = new Queue<SearchNode>();
                {
                    //add all of our goal positions as found and pointing to themselves
                    GridLoc loc = goal.getLocation();
                    for (int y = loc.y; y < loc.y + goal.getHeight(); y++)
                    {
                        for (int x = loc.x; x < loc.x + goal.getHeight(); x++)
                        {
                            GridLoc nodLoc = new GridLoc(x, y);
                            SearchNode n = new SearchNode(nodLoc);
                            searchNodes[y][x] = n;
                            n.direct = n;
                            n.goTo = n;

                            foreach (GridLoc neighbor in getAllNeighborLocs(navGrid, navWidth, navHeight, n.loc))
                            {
                                //add a new fringe node if we haven't already been there, and we can path there
                                if (searchNodes[neighbor.y][neighbor.x] == null)
                                {
                                    SearchNode newNode = new SearchNode(neighbor);
                                    newNode.goTo = n;
                                    newNode.direct = n;
                                    searchLocs.Enqueue(newNode);
                                }
                            }
                        }
                    }
                }

                while (searchLocs.Count != 0)
                {
                    SearchNode curNode = searchLocs.Dequeue();

                    if (searchNodes[curNode.loc.y][curNode.loc.x] != null)
                        continue; //we've already found a valid path to this node, keep going

                    //save this node, we've found the fastest way to reach it
                    searchNodes[curNode.loc.y][curNode.loc.x] = curNode;

                    //calc the curNode path
                    {
                        //combo mode
                        //calc the path as far as possible using direct paths
                        //then try to tighten it up using single-step goTo's.
                        
                        SearchNode nextNode = curNode.direct.direct;
                        while (nextNode != curNode.direct)
                        {
                            if (canPathTo(navGrid, curNode.loc, nextNode.loc))
                            {
                                curNode.direct = nextNode;
                                nextNode = nextNode.direct;
                            }
                            else
                                break;
                        }

                        //might be able to skip some canPathTo's here by
                        //first trying 8 nodes ahead, and halving until we only check one node at a time.
                        int numFrwd = 8;
                        nextNode = curNode.direct;
                        while (nextNode.goTo != curNode.direct)
                        {
                            for (int i = 0; i < numFrwd; i++)
                                nextNode = nextNode.goTo;

                            if (canPathTo(navGrid, curNode.loc, nextNode.loc))
                            {
                                curNode.direct = nextNode;
                            }
                            else
                            {
                                if (numFrwd == 1)
                                    break;

                                numFrwd = numFrwd / 2;
                                nextNode = curNode.direct;
                            }
                        }
                    }

                    //now add all neighboring nodes
                    foreach (GridLoc neighbor in getAllNeighborLocs(navGrid, navWidth, navHeight, curNode.loc))
                    {
                        //add a new fringe node if we haven't already been there, and we can path there
                        if (searchNodes[neighbor.y][neighbor.x] == null)
                        {
                            SearchNode newNode = new SearchNode(neighbor);
                            newNode.goTo = curNode;
                            newNode.direct = curNode;

                            searchLocs.Enqueue(newNode);
                        }
                    }
                }

                GridLoc badLoc = new GridLoc(-1, -1);
                for (int y = 0; y < navHeight; y++)
                {
                    for (int x = 0; x < navWidth; x++)
                    {
                        SearchNode n = searchNodes[y][x];
                        if (n == null) //we can't path here, so set the goTo location as the goal
                            goToCell[y][x] = badLoc; //if an enemy winds up here, he'll just walk through everything to the goal.
                        else
                            goToCell[y][x] = n.direct.loc;
                    }
                }
            }

            public GridLoc getGoToPos(GridLoc loc)
            {
                return goToCell[loc.y][loc.x];
            }

            #region spot testing for canpath
            /*
            public class testc : MapObject
            {
                public testc(GridLoc loc)
                    : base(loc, 1, 1)
                {

                }
                public override bool blocksPathing()
                {
                    return true;
                }

                public override void Dispose()
                {
                }

                public override bool mustBePathedTo()
                {
                    return false;
                }
            }

            public static void test ()
            {
                MapObject[][] testGrid = new MapObject[10][];
                for (int i = 0; i < 10; i++)
                    testGrid[i] = new MapObject[10];

                testGrid[5][2] = new testc(new GridLoc(2, 5));
                testGrid[4][2] = new testc(new GridLoc(2, 4));

                //bool testPath = canPathTo(testGrid, new GridLoc(2, 6), new GridLoc(0, 1));
                //bool rev = canPathTo(testGrid, new GridLoc(0, 1), new GridLoc(2, 6));

                bool test2 = canPathTo(testGrid, new GridLoc(2, 6), new GridLoc(0, 4));
                bool rev2 = canPathTo(testGrid, new GridLoc(0, 4), new GridLoc(2, 6));
            }
            */
            #endregion

            static bool canPathTo(MapObject[][] navGrid, GridLoc start, GridLoc end)
            {
                //do a line drawing algo from start to end
                if (Math.Abs(end.y - start.y) > Math.Abs(end.x - start.x))
                {
                    //this is 'steep', so we swap x/y for calcs
                    {
                        int p = start.x;
                        start.x = start.y;
                        start.y = p;

                        p = end.x;
                        end.x = end.y;
                        end.y = p;
                    }

                    if (start.x > end.x)
                    {//swap start and end, so end-start x is always positive
                        GridLoc t = start;
                        start = end;
                        end = t;
                    }

                    float dx = end.x - start.x;
                    float dy = end.y - start.y;
                    float grad = dy / dx;

                    int xend = start.x; //we always start in a pixel center...
                    float yend = start.y + grad * (0.5f); //if grad is 0, should always be start.y

                    int xpxl1 = xend;
                    int ypxl1 = (int)yend;

                    //first endpoint
                    {
                        float c2 = (float)(yend - Math.Truncate(yend));
                        float c1 = 1 - c2;

                        if (collides(navGrid, ypxl1, xpxl1, c1))
                            return false;
                        if (collides(navGrid, ypxl1 + 1, xpxl1, c2))
                            return false;
                    }
                    float intery = yend;

                    //second endpoint
                    xend = end.x;
                    yend = end.y + grad * (0.5f);

                    int xpxl2 = xend;
                    int ypxl2 = (int)yend;

                    /*
                    {
                        float c2 = (float)(yend - Math.Truncate(yend));
                        float c1 = 1 - c2;

                        if (collides(navGrid, ypxl2, xpxl2, c1))
                            return false;
                        if (collides(navGrid, ypxl2 + 1, xpxl2, c2))
                            return false;
                    }
                    */

                    //main loop
                    {
                        float c2 = (float)(intery - Math.Truncate(intery));
                        float c1 = 1 - c2;
                        int y = (int)intery;
                        for (int x = xpxl1 + 1; x <= xpxl2; x++)
                        {
                            if (collides(navGrid, y, x, c1))
                                return false;
                            if (collides(navGrid, y + 1, x, c2))
                                return false;

                            intery += grad;

                            c2 = (float)(intery - Math.Truncate(intery));
                            c1 = 1 - c2;
                            y = (int)intery;
                            if (collides(navGrid, y, x, c1))
                                return false;
                            if (collides(navGrid, y + 1, x, c2))
                                return false;
                        }
                    }
                }
                else
                {
                    if (start.x > end.x)
                    {//swap start and end
                        GridLoc t = start;
                        start = end;
                        end = t;
                    }

                    float dx = end.x - start.x;
                    float dy = end.y - start.y;
                    float grad = dy / dx;

                    int xend = start.x; //we always start in a pixel center...
                    float yend = start.y + grad * (0.5f); //if grad is 0, should always be start.y

                    int xpxl1 = xend;
                    int ypxl1 = (int)yend;

                    //first endpoint
                    {
                        float c2 = (float)(yend - Math.Truncate(yend));
                        float c1 = 1 - c2;
                        //check (xpxl1, ypxl1), rfpart(yend) * xgap
                        //check (xpxl1, ypxl1 + 1), fpart(yend) * xgap

                        if (collides(navGrid, xpxl1, ypxl1, c1))
                            return false;
                        if (collides(navGrid, xpxl1, ypxl1 + 1, c2))
                            return false;
                    }
                    float intery = yend;

                    //second endpoint
                    xend = end.x;
                    yend = end.y + grad * (0.5f);

                    int xpxl2 = xend;
                    int ypxl2 = (int)yend;

                    /*
                    {
                        float c2 = (float)(yend - Math.Truncate(yend));
                        float c1 = 1 - c2;
                        //check (xpxl2, ypxl2)
                        //check (xpxl2, ypxl2 + 1)

                        if (collides(navGrid, xpxl2, ypxl2, c1))
                            return false;
                        if (collides(navGrid, xpxl2, ypxl2 + 1, c2))
                            return false;
                    }
                    */

                    //main loop
                    {
                        float c2 = (float)(intery - Math.Truncate(intery));
                        float c1 = 1 - c2;
                        int y = (int)intery;
                        for (int x = xpxl1 + 1; x <= xpxl2; x++)
                        {
                            if (collides(navGrid, x, y, c1))
                                return false;
                            if (collides(navGrid, x, y + 1, c2))
                                return false;

                            intery += grad;

                            c2 = (float)(intery - Math.Truncate(intery));
                            c1 = 1 - c2;
                            y = (int)intery;
                            if (collides(navGrid, x, y, c1))
                                return false;
                            if (collides(navGrid, x, y + 1, c2))
                                return false;
                        }
                    }
                }

                return true;
            }

            static bool collides(MapObject[][] navGrid, int x, int y, float perc)
            {
                if (y >= navGrid.Length || x >= navGrid[0].Length)
                    return false;

                return perc > 0.001f && !isPathable(navGrid, x, y);
            }

            static IEnumerable<GridLoc> getAllNeighborLocs(MapObject[][] navGrid, int navWidth, int navHeight, GridLoc start)
            {
                bool top = start.y > 0 && isPathable(navGrid, start.x, start.y - 1);
                bool left = start.x > 0 && isPathable(navGrid, start.x - 1, start.y);
                bool bot = start.y < navHeight - 1 && isPathable(navGrid, start.x, start.y + 1);
                bool right = start.x < navWidth - 1 && isPathable(navGrid, start.x + 1, start.y);
                if (top)
                    yield return new GridLoc(start.x, start.y - 1);
                if (left)
                    yield return new GridLoc(start.x - 1, start.y);
                if (bot)
                    yield return new GridLoc(start.x, start.y + 1);
                if (right)
                {
                    yield return new GridLoc(start.x + 1, start.y);
                    if (top && isPathable(navGrid, start.x + 1, start.y - 1))
                        yield return new GridLoc(start.x + 1, start.y - 1);
                    if (bot && isPathable(navGrid, start.x + 1, start.y + 1))
                        yield return new GridLoc(start.x + 1, start.y + 1);
                }
                if (left)
                {
                    if (top && isPathable(navGrid, start.x - 1, start.y - 1))
                        yield return new GridLoc(start.x - 1, start.y - 1);
                    if (bot && isPathable(navGrid, start.x - 1, start.y + 1))
                        yield return new GridLoc(start.x - 1, start.y + 1);
                }
            }

            /// <summary>
            /// Gets all neighboring locations, assuming there is a new object that blocks pathing.
            /// </summary>
            static IEnumerable<GridLoc> getAllNeighborLocs(MapObject[][] navGrid, int navWidth, int navHeight,
                GridLoc start, int objX, int objY, int objWidth, int objHeight)
            {
                bool top = start.y > 0 && isPathable(navGrid, start.x, start.y - 1, objX, objY, objWidth, objHeight);
                bool left = start.x > 0 && isPathable(navGrid, start.x - 1, start.y, objX, objY, objWidth, objHeight);
                bool bot = start.y < navHeight - 1 && isPathable(navGrid, start.x, start.y + 1, objX, objY, objWidth, objHeight);
                bool right = start.x < navWidth - 1 && isPathable(navGrid, start.x + 1, start.y, objX, objY, objWidth, objHeight);
                if (top)
                    yield return new GridLoc(start.x, start.y - 1);
                if (left)
                    yield return new GridLoc(start.x - 1, start.y);
                if (bot)
                    yield return new GridLoc(start.x, start.y + 1);
                if (right)
                {
                    yield return new GridLoc(start.x + 1, start.y);
                    if (top && isPathable(navGrid, start.x + 1, start.y - 1, objX, objY, objWidth, objHeight))
                        yield return new GridLoc(start.x + 1, start.y - 1);
                    if (bot && isPathable(navGrid, start.x + 1, start.y + 1, objX, objY, objWidth, objHeight))
                        yield return new GridLoc(start.x + 1, start.y + 1);
                }
                if (left)
                {
                    if (top && isPathable(navGrid, start.x - 1, start.y - 1, objX, objY, objWidth, objHeight))
                        yield return new GridLoc(start.x - 1, start.y - 1);
                    if (bot && isPathable(navGrid, start.x - 1, start.y + 1, objX, objY, objWidth, objHeight))
                        yield return new GridLoc(start.x - 1, start.y + 1);
                }
            }

            static bool isPathable(MapObject[][] navGrid, int x, int y)
            {
                MapObject o = navGrid[y][x];

                return !(o != null && o.blocksPathing());
            }

            static bool isPathable(MapObject[][] navGrid, int x, int y, int objX, int objY, int objWidth, int objHeight)
            {
                MapObject o = navGrid[y][x];
                if (o != null && o.blocksPathing())
                    return false;

                //blocksPathing is here because sometimes we're pathing to this object, not pathing around it
                //pretty inefficient but eh
                return !(x >= objX && x < objX + objWidth
                        && y >= objY && y < objY + objHeight);
            }
        }

        //this is an 'animation' time before any add/del of an object actually takes affect
        //this is to give the backend time to calc the new pathing, given crap cpus and network latency
        //we don't really have a way to force the rest of the game to abide by this timer without thead-syncing hell
        //so, do your best everybody else
        public const float buildDelay = 2.5f;
        private const float navBin = 0.5f; //number of seconds per navigation update

        #region Multithreading Notes
        //this is very finely multithreaded
        //we're using a buffered strategy for multithreading
        //things that are shared between threads generally only need to write in one thread, read in the other
        //we don't want to use locks in methods that are used heavily, or any locks during the heavy processing
        //so, to talk between threads we use buffer versions of each variable, that get set/read with a lock, but should only be locked during the actual read/write
        //shared vars used by the ui thread are prefixed with ui_
        //buffer vars are prefixed with buf_
        //nav vars are prefixed with nav_
        #endregion

        //static vars
        readonly Map map;
        readonly Thread navThread;

        #region UI Vars
        //ui vars
        Vector2 spawnPos;
        TeleporterEntrance[] teleEntrances;

        NavMesh[] ui_meshes;
        
        MapObject[][] ui_grid;
        int ui_width;
        int ui_height;

        PathNode[] ui_travelNodes;
        MapObject[] ui_travelPoints; //includes all travelNodes and teleporter exits.

        public int NumTeles
        {
            get
            {
                return teleEntrances.Length;
            }
        }
        public int NumTravelNodes
        {
            get
            {
                return ui_travelNodes.Length;
            }
        }
        public IEnumerable<RoundedRectangle> mapObjectDisplays
        {
            get
            {
                foreach (TeleporterEntrance o in teleEntrances)
                    yield return o.rect;

                foreach (MapObject o in ui_travelPoints)
                    yield return o.rect;
            }
        }
        #endregion

        #region Shared Vars
        //shared vars
        private volatile bool runNavThread = true; //set to false to kill the nav thread
        private object navLock = new object(); //object to use for locking

        //newGrid tells the navThread there is a new level, grab the data here
        private bool buf_newGrid = false; 
        MapObject[][] buf_grid;
        int buf_width = 0;
        int buf_height = 0;
        MapObject[] buf_nodes = null;

        //navmesh output buffer
        NavMesh[] buf_meshes;

        //these are just shared, always lock while using these
        TimedQueue<MapObject> objsToAdd = new TimedQueue<MapObject>();
        TimedQueue<MapObject> objsToRemove = new TimedQueue<MapObject>();
        #endregion

        #region Nav Vars
        //nav thread vars

        //world definition for the nav thread
        MapObject[][] nav_grid;
        int nav_width = 0;
        int nav_height = 0;
        MapObject[] nav_nodes = null;

        //nav outut
        NavMesh[] nav_meshes;
        #endregion

        public NavGrid(Map map)
        {
            this.map = map;

            navThread = new Thread(NavRun);
            navThread.Start();
        }

        public void init(Random rand, int width, int height, int numNodes, int numTeles, int numRocks)
        {
            if (numNodes < 2)
                throw new ArgumentException("Must have at least two nodes; a start and an end. " + numNodes);

            //first clear all of the old stuff
            if (ui_travelPoints != null)
            {
                for (int i = 0; i < teleEntrances.Length; i++)
                    teleEntrances[i].Dispose();
            }

            ui_width = width;
            ui_height = height;

            ui_grid = new MapObject[height][];
            for (int i = 0; i < height; i++)
                ui_grid[i] = new MapObject[width];

            ui_travelNodes = new PathNode[numNodes];
            ui_travelPoints = new MapObject[numNodes + numTeles];
            teleEntrances = new TeleporterEntrance[numTeles];
            ui_meshes = new NavMesh[numNodes - 1];

            //decide on a 'minimum' distance between two consecutive nodes
            //for a 20x20 grid, this is ~6 units
            //for a 100x100 grid, this is ~28 units
            float minPathDist = (float)(Math.Sqrt(width * width + height * height) / 5);
            float minPathDistSquared = minPathDist * minPathDist;

            //this is the number of grid units that all nodes must be apart from eachother
            int minNodeSep = 1;

            //node sizes -- could change this per-node, too
            int nodeWidth = 2;
            int nodeHeight = 2;

            //first, add travel nodes -- make sure they're all pathable
            for (int i = 0; i < numNodes; i++)
            {
                int tests = 0;
                while (true)
                {
                    //anti infinite-loop
                    if (tests++ > 10000)
                    {
                        Logger.WriteLine(LogType.ERROR, "Infinite loop when placing travel nodes! Restarting with new, random, seed");
                        rand = new Random();
                        init(rand, width, height, numNodes, numTeles, numRocks);
                        return;
                    }

                    int locX = rand.Next(width - nodeWidth + 1);
                    int locY = rand.Next(height - nodeHeight + 1);

                    if (i > 0)
                    {
                        float dist = ui_travelNodes[i - 1].getLocation().DistSquared(locX, locY);

                        if (dist < minPathDistSquared)
                            continue;
                    }

                    //we want to test a larger area than where we're actually going to place the node
                    //the only other thing in the map at this point is other pathing nodes
                    //this ensures that none of the nodes are touching
                    //the weird max/min here is to ensure we're not testing off the grid
                    int testPosX = Math.Max(locX - minNodeSep, 0);
                    int testPosY = Math.Max(locY - minNodeSep, 0);
                    int testWidth = Math.Min(width - testPosX, nodeWidth + minNodeSep + locX - testPosX);
                    int testHeight = Math.Min(height - testPosY, nodeHeight + minNodeSep + locY - testPosY);

                    if (canPlaceObject(testPosX, testPosY, testWidth, testHeight, false, i != 0))
                    {
                        ui_travelNodes[i] = new PathNode(map, i, numNodes, new GridLoc(locX, locY), nodeWidth, nodeHeight);
                        _placeObject(ui_grid, ui_travelNodes[i]);
                        ui_travelPoints[i] = ui_travelNodes[i];
                        break;
                    }
                }
            }

            //teleporter exit size
            int teleExitWidth = 2;
            int teleExitHeight = 2;

            //then, add teleporter exits
            for (int i = 0; i < numTeles; i++)
            {
                //place the exits
                int tests = 0;
                while (true)
                {
                    //anti infinite-loop
                    if (tests++ > 10000)
                    {
                        Logger.WriteLine(LogType.ERROR, "Infinite loop when placing teleporter exits! Restarting with new, random, seed");
                        rand = new Random();
                        init(rand, width, height, numNodes, numTeles, numRocks);
                        return;
                    }

                    int locX = rand.Next(width - teleExitWidth + 1);
                    int locY = rand.Next(height - teleExitHeight + 1);

                    if (canPlaceObject(locX, locY, teleExitWidth, teleExitHeight, false, true))
                    {
                        var ext = new TeleporterExit(map, new GridLoc(locX, locY), teleExitWidth, teleExitHeight, i);
                        ui_travelPoints[numNodes + i] = ext;
                        _placeObject(ui_grid, ext);
                        break;
                    }
                }
            }
            
            //now add rock pathing blockers
            for (int i = 0; i < numRocks; i++)
            {
            }

            //now add teleporter entrances
            //we do this after rocks, because tele-entrances don't actually get placed into the navi grid
            //so rocks might get placed on top of tele entrances without a special check

            //decide on a 'minimum' distance for the teleporter to teleport
            float minTeleDist = (float)(Math.Sqrt(width * width + height * height) / 5);
            float minTeleDistSquared = minTeleDist * minTeleDist;

            //area around the entrance to check for other entrances, so they don't overlap
            int minEntSep = 1;

            //teleporter entrace size, can change per tele
            //don't have width/height, because we do a circle-test for hitting the entrance
            int teleEntSize = 2;

            for (int i = 0; i < numTeles; i++)
            {
                TeleporterExit ext = (TeleporterExit)ui_travelPoints[numNodes + i];
                int tests = 0;
                while (true)
                {
                    //anti infinite-loop
                    if (tests++ > 10000)
                    {
                        Logger.WriteLine(LogType.ERROR, "Infinite loop when placing teleporter entrances! Restarting with new, random, seed");
                        rand = new Random();
                        init(rand, width, height, numNodes, numTeles, numRocks);
                        return;
                    }

                    int locX = rand.Next(width - teleEntSize + 1);
                    int locY = rand.Next(height - teleEntSize + 1);
                    
                    //make sure we're far enough away from the exit
                    float dist = ext.getLocation().DistSquared(locX, locY);

                    if (dist < minTeleDistSquared)
                        continue;

                    //make sure there isn't another entrance too close to this one
                    if (i > 0)
                    {
                        //we want to test a larger area than where we're actually going to place the ent
                        //the weird max/min here is to ensure we're not testing off the grid
                        int testPosX = Math.Max(locX - minEntSep, 0);
                        int testPosY = Math.Max(locY - minEntSep, 0);
                        int testWidth = Math.Min(width - testPosX, teleEntSize + minEntSep + locX - testPosX);
                        int testHeight = Math.Min(height - testPosY, teleEntSize + minEntSep + locY - testPosY);

                        bool colDet = false;
                        for (int t = 0; t < i; t++)
                        {
                            var o = teleEntrances[t];

                            if (testPosX < o.loc.x + o.width
                                && o.loc.x < testPosX + testWidth
                                && testPosY < o.loc.y + o.height 
                                && o.loc.y < testPosY + testHeight)
                            {
                                colDet = true;
                                break;
                            }
                        }

                        if (colDet)
                            continue;
                    }

                    if (canPlaceObject(locX, locY, teleEntSize, teleEntSize, false, true))
                    {
                        teleEntrances[i] = new TeleporterEntrance(map, new GridLoc(locX, locY), teleEntSize, ext, i);
                        break;
                    }
                }
            }

            //done generating the grid, now update metadata
            GridLoc startPos = ui_travelNodes[0].getLocation();
            spawnPos = new Vector2(startPos.x + ui_travelNodes[0].getWidth() * 0.5f, startPos.y + ui_travelNodes[0].getHeight() * 0.5f) * map.gridScale;

            //tell the navigation to update...
            lock (navLock)
            {
                //we have a new level, so just trash all the old stuff
                //sync all of the world state to our thread
                buf_width = ui_width;
                buf_height = ui_height;

                buf_grid = new MapObject[buf_height][];
                for (int y = 0; y < buf_height; y++)
                {
                    buf_grid[y] = new MapObject[buf_width];
                    for (int x = 0; x < buf_width; x++)
                    {
                        buf_grid[y][x] = ui_grid[y][x];
                    }
                }

                buf_nodes = new PathNode[ui_travelNodes.Length - 1];
                buf_meshes = new NavMesh[buf_nodes.Length];

                for (int i = 0; i < buf_nodes.Length; i++)
                    buf_nodes[i] = ui_travelNodes[i + 1];

                objsToAdd.Clear();
                objsToRemove.Clear();

                buf_newGrid = true;
            }

        }

        public Vector2 getSpawnPos()
        {
            return spawnPos;
        }

        static bool canPlaceObject(MapObject[][] grid, int width, int height, MapObject[] travelPoints, 
            int objX, int objY, int objWidth, int objHeight, bool blocksPathing, bool mustBePathedTo)
        {
            //first, verify that nothing is in the way
            for (int y = objY; y < objY + objHeight; y++)
            {
                for (int x = objX; x < objX + objWidth; x++)
                {
                    if (grid[y][x] != null)
                        return false; //there's an object in the way!
                }
            }

            //if object blocks a path, verify that we're not going to completely block pathing
            if (!blocksPathing && !mustBePathedTo)
                return true;

            return NavMesh.canPlaceObject(grid, width, height, travelPoints, objX, objY, objWidth, objHeight, blocksPathing, mustBePathedTo);
        }

        public bool canPlaceObject(int objX, int objY, int objWidth, int objHeight, bool blocksPathing, bool mustBePathedTo)
        {
            return canPlaceObject(ui_grid, ui_width, ui_height, ui_travelPoints, objX, objY, objWidth, objHeight, blocksPathing, mustBePathedTo);
        }

        public bool canPlaceObject(MapObject obj)
        {
            GridLoc loc = obj.getLocation();
            return canPlaceObject(loc.x, loc.y, obj.getWidth(), obj.getHeight(), obj.blocksPathing(), obj.mustBePathedTo());
        }

        [Obsolete] //this should be handled by the map w/ an acceleration structure
        public void checkTeles(Enemy e)
        {
            for (int i = 0; i < teleEntrances.Length; i++)
            {
                TeleporterEntrance ent = teleEntrances[i];
                float d = (e.radius + ent.radius) * (e.radius + ent.radius);
                if (Vector2.DistanceSquared(ent.center, e.position) < d)
                {
                    if (!e.telesTaken[ent.num])
                    {
                        e.telesTaken[ent.num] = true;
                        GridLoc ext = ent.exit.getLocation();
                        e.Teleport(ext, ent.exit.getWidth(), ent.exit.getHeight());
                    }
                }
            }
        }

        [Obsolete] //this should be handled by the map w/ an acceleration structure
        public List<Tower> FindAllTowersTouchingBox(Vector2 start, Vector2 end)
        {
            HashSet<Tower> toReturn = new HashSet<Tower>();

            //use the map grid
            GridLoc startLoc = new GridLoc(
                Math.Max((int)(start.X / map.gridScale), 0),
                Math.Max((int)(start.Y / map.gridScale), 0));

            GridLoc endLoc = new GridLoc(
                Math.Min((int)(end.X / map.gridScale) + 1, ui_width),
                Math.Min((int)(end.Y / map.gridScale) + 1, ui_height));

            for (int y = startLoc.y; y < endLoc.y; y++)
            {
                for (int x = startLoc.x; x < endLoc.x; x++)
                {
                    MapObject o = ui_grid[y][x];
                    if (o != null && o is Tower)
                    {
                        toReturn.Add(o as Tower);
                    }
                }
            }

            return toReturn.ToList();
        }
        
        static void _placeObject(MapObject[][] grid, MapObject obj)
        {
            GridLoc loc = obj.getLocation();
            //place the object, update all grid refs to point to it
            for (int y = loc.y; y < loc.y + obj.getHeight(); y++)
            {
                for (int x = loc.x; x < loc.x + obj.getWidth(); x++)
                {
                    grid[y][x] = obj;
                }
            }
        }

        static void _removeObject(MapObject[][] grid, MapObject obj)
        {
            GridLoc loc = obj.getLocation();
            //place the object, update all grid refs to point to it
            for (int y = loc.y; y < loc.y + obj.getHeight(); y++)
            {
                for (int x = loc.x; x < loc.x + obj.getWidth(); x++)
                {
                    if (grid[y][x] != obj)
                        throw new InvalidOperationException("Removing an object from a position it isn't at?");
                    grid[y][x] = null;
                }
            }
        }

        public void placeObject(MapObject obj)
        {
#if DEBUG
            if (!canPlaceObject(obj))
                throw new InvalidOperationException("Can't place an object here, check first!");
#endif
            _placeObject(ui_grid, obj);
        }

        public void addToNavi(MapObject obj)
        {
            lock (navLock)
            {
                objsToAdd.Insert(obj, buildDelay);
            }
        }

        public void removeObject(MapObject obj)
        {
            _removeObject(ui_grid, obj);
        }

        public void removeFromNavi(MapObject obj)
        {
            lock (navLock)
            {
                //if we get some netlag and remove an object in animation time
                //try to delete it from the objsToAdd queue first
                if (objsToAdd.Contains(obj))
                {
                    objsToAdd.Remove(obj);
                }
                else
                    objsToRemove.Insert(obj, buildDelay);
            }
        }

        public GridLoc getNaviGoal(int numNode, GridLoc pos)
        {
            if (ui_meshes[numNode - 1] == null)
                return ui_travelNodes[numNode].getLocation();

            return ui_meshes[numNode - 1].getGoToPos(pos);
        }

        public GridLoc getNodePos(int node)
        {
            return ui_travelPoints[node].getLocation();
        }
        
        public void Update(float dt)
        {
            lock (navLock)
            {
                for (int i = 0; i < ui_meshes.Length; i++)
                {
                    ui_meshes[i] = buf_meshes[i];
                }
            }
        }

        //nav thread stuff
        private void NavRun()
        {
            #region notes
            //this thread should have a deep copy of the navigation grid
            //each path-cycle, it should check for changes compared to the map's grid
            //specifically, it's looking for MapObjects that are complete or will complete within the next cycle
            //(each mapobject should have an active/not tag + time-til-change or something?)
            //  should we store a list of upcoming changes directly? so nav update doesn't have to search?
            //  and a noti if we just completely reset the map
            //if it finds any changes, recalc pathing for new setup
            //after recalc, wait for end of full cycle-time, then give main thread new nav

            //shared objects --
            //runThread
            //forceSync
            //map's navgrid
            //map's width/height
            //map's travelNodes --- we don't care about travelPoints because the ui thread should veirfy that the path is valid
            //navmeshes inside each travelNode
            #endregion

            //nav thread variables
            bool recalc = false;
            Stopwatch threadTimer = new Stopwatch();
            threadTimer.Start();
            double time = 0;
            while (runNavThread)
            {
                time += threadTimer.Elapsed.TotalMilliseconds / 1000.0;
                threadTimer.Restart();

                float dt = navBin;
                while (time > dt)
                {
                    time -= dt;

                    lock (navLock)
                    {
                        //check to see if we have a new level
                        if (buf_newGrid)
                        {
                            nav_width = buf_width;
                            nav_height = buf_height;

                            nav_grid = buf_grid;

                            nav_nodes = buf_nodes;
                            nav_meshes = new NavMesh[buf_meshes.Length];

                            recalc = true;
                            buf_newGrid = false;
                        }

                        //check to see if there are any upcoming changes to the map...
                        objsToAdd.Update(dt);
                        while (objsToAdd.Count > 0 && objsToAdd.MinTimer() < navBin)
                        {
                            MapObject o = objsToAdd.Pop();
                            _placeObject(nav_grid, o);
                            recalc = true;
                        }

                        objsToRemove.Update(dt);
                        while (objsToRemove.Count > 0 && objsToRemove.MinTimer() < navBin)
                        {
                            MapObject o = objsToRemove.Pop();
                            _removeObject(nav_grid, o);
                            recalc = true;
                        }
                    }

                    if (recalc)
                    {
                        recalc = false;

                        //we're recalcing paths, so go ahead and do that
                        for (int i = 0; i < nav_meshes.Length; i++)
                        {
                            nav_meshes[i] = new NavMesh(nav_grid, nav_width, nav_height, nav_nodes[i]);
                        }

                        //now, sync with main thread
                        //could move this to the start of this loop
                        //it would make the meshes update on even navUpdate times, rather than as soon as we finish
                        //which would ~maybe~ help sync multiplayer a bit better. (and make nav updates more 'consistent')
                        lock (navLock)
                        {
                            //if a new grid was created while we were generating new paths, just ignore these results
                            if (!buf_newGrid)
                            {
                                for (int i = 0; i < nav_meshes.Length; i++)
                                {
                                    buf_meshes[i] = nav_meshes[i];
                                }
                            }
                        }
                    }
                }

                Thread.Sleep(1);
            }

            Logger.WriteLine(LogType.DEBUG, "Navigation thread is exiting");
        }

        public void Dispose()
        {
            runNavThread = false;
        }
    }
}
