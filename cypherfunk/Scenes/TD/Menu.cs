﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    class Menu
    {
        /// <summary>
        /// The displays/controls for the 'command card' portion of the menu
        /// </summary>
        abstract class CommandCard
        {
            /// <summary>
            /// Set to enable/disable the display/ui.
            /// All command cards should assume enabled is set to false on construction.
            /// </summary>
            public abstract bool enabled { set; }
        }

        /// <summary>
        /// Information about the round, to be displayed when the user has nothing else relevant selected.
        /// </summary>
        class RoundInfoCard : CommandCard
        {
            Menu menu;
            Map map;
            
            FontRenderer title;

            float enemyYPos;
            FontRenderer[] enemyTypes;
            FontRenderer[] enemyStats;

            public RoundInfoCard (Menu menu, Map map)
            {
                this.menu = menu;
                this.map = map;

                title = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                title.pos = menu.cmdPos;
                title.scale = menu.cmdHeaderSize;
                title.anchor = FontAnchor.TOP_LEFT;

                enemyYPos = title.pos.Y + title.scale + menu.cmdPadding;

                RoundChanged();

                enabled = false;
            }

            /// <summary>
            /// Menu should call this whenever the map has new information to display in this card.
            /// </summary>
            public void RoundChanged()
            {
                //cleanup previous round info
                if (enemyTypes != null)
                {
                    for (int i = 0; i < enemyTypes.Length; i++)
                    {
                        enemyTypes[i].Dispose();
                        enemyStats[i].Dispose();
                    }
                }

                title.text = "Round Info. Diff: " + map.mapDifficulty;

                int numTypes = map.enemiesToSpawn.Count;
                enemyTypes = new FontRenderer[numTypes];
                enemyStats = new FontRenderer[numTypes];

                float yPos = enemyYPos;
                for (int i = 0; i < numTypes; i++)
                {
                    enemyTypes[i] = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_128));
                    enemyTypes[i].text = map.enemiesToSpawn[i].GetTotalSpawns().ToString() + " " + Enemy.GetDisplayType(map.enemiesToSpawn[i].type);
                    enemyTypes[i].pos = new Vector2(menu.cmdPos.X, yPos);
                    enemyTypes[i].scale = menu.cmdDetailSize;
                    enemyTypes[i].anchor = FontAnchor.TOP_LEFT;

                    yPos += enemyTypes[i].scale + menu.cmdSmallPadding;

                    enemyStats[i] = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_128));
                    enemyStats[i].text = Enemy.GetHealth(map.enemiesToSpawn[i].type, map.enemiesToSpawn[i].diffMod) + " Health";
                    enemyStats[i].pos = new Vector2(menu.cmdPos.X + menu.cmdHorizTab, yPos);
                    enemyStats[i].scale = menu.cmdDetailSize;
                    enemyStats[i].anchor = FontAnchor.TOP_LEFT;

                    yPos += enemyStats[i].scale + menu.cmdPadding;
                }
            }

            public override bool enabled
            {
                set
                {
                    title.enabled = value;
                    for (int i = 0; i < enemyTypes.Length; i++)
                    {
                        enemyTypes[i].enabled = value;
                        enemyStats[i].enabled = value;
                    }
                }
            }
        }

        /// <summary>
        /// Information about the currently selected tower(s)
        /// Takes just a list of towers.
        /// If a single tower is selected, will display stats.
        /// If multiple towers are selected, will display an icon for each tower.
        ///     As well as 'subgroup' each tower based on type/upgrade status.
        /// 
        /// In either case, will also show available commands.
        /// Commands for all towers:
        ///     Attack (Attack -> Leftclick will make all towers override targetting and attack the player-sel target)
        ///     Targetting Modes (first, last, most hp, least hp, etc)
        /// 
        /// Commands per tower type: (Acts on currently selected subgroup)
        ///     Upgrade
        ///     Abilities
        ///     
        /// </summary>
        class TowerCard : CommandCard
        {
            Menu menu;
            Map map;

            FontRenderer title;
            FontRenderer info;
            BoxTextButton upgrade;
            BoxTextButton delete;
            BoxTextButton cancel;

            DashedCircle_3D selTowerRange;

            Tower _cur;
            Tower curTower
            {
                get
                {
                    return _cur;
                }
                set
                {
                    if (_cur != null)
                    {
                        _cur.stateChanged -= towerStateChanged;

                        selTowerRange.circles.Clear();
                    }

                    _cur = value;

                    if (_cur != null)
                    {
                        _cur.stateChanged += towerStateChanged;
                        calcState(title.enabled);

                        selTowerRange.circles.Add(new DashedCircle(
                            new Vector3(_cur.pos, 0),
                            _cur.range,
                            5f,
                            1f,
                            0f,
                            Color.LightGoldenrodYellow
                            ));
                    }
                }
            }

            public List<Tower> currentTowersSelected
            {
                set
                {
                    //this is a temporary setup where we pretend we can only select 1 tower, to make things easier
                    if (value != null && value.Count > 0)
                    {
                        curTower = value[0];
                        return;
                    }
                    else
                        curTower = null;
                }
            }

            private void towerStateChanged(Tower obj)
            {
                calcState(title.enabled);
            }

            public TowerCard(Menu menu, Map map)
            {
                this.menu = menu;
                this.map = map;

                selTowerRange = new DashedCircle_3D(map.stage, map.em, 2);
                map.em.addUpdateListener(0, rangeUpdate);

                title = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                title.pos = menu.cmdPos;
                title.scale = menu.cmdHeaderSize;
                title.anchor = FontAnchor.TOP_LEFT;

                info = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                info.pos = new Vector2(menu.cmdPos.X, menu.cmdPos.Y + title.scale + menu.cmdPadding);
                info.scale = menu.cmdDetailSize;
                info.anchor = FontAnchor.TOP_LEFT;
                info.text = "Tower Info Goes Here";

                upgrade = new BoxTextButton(map.stage, map.em, 1000, "Upgrade");
                upgrade.Position = new Vector2(menu.cmdPos.X, info.pos.Y + info.scale + menu.cmdPadding);
                upgrade.Scale = new Vector2(menu.cmdScale.X * 0.5f, menu.cmdHeaderSize);
                upgrade.onClick += onUpgrade;

                delete = new BoxTextButton(map.stage, map.em, 1000, "Delete");
                delete.Position = new Vector2(menu.cmdPos.X, upgrade.Position.Y + upgrade.Scale.Y + menu.cmdPadding);
                delete.Scale = new Vector2(menu.cmdScale.X * 0.5f, menu.cmdHeaderSize);
                delete.onClick += onDelete;

                cancel = new BoxTextButton(map.stage, map.em, 1000, "Cancel");
                cancel.Position = new Vector2(menu.cmdPos.X, info.pos.Y + info.scale + menu.cmdPadding);
                cancel.Scale = new Vector2(menu.cmdScale.X * 0.5f, menu.cmdHeaderSize);
                cancel.onClick += onCancel;

                enabled = false;
            }

            void rangeUpdate(float dt)
            {
                for (int i = 0; i < selTowerRange.circles.Count; i++)
                {
                    var c = selTowerRange.circles[i];

                    c.rot += dt * 0.25f;

                    selTowerRange.circles[i] = c;
                }
            }

            void onUpgrade(Button obj)
            {
                map.UpgradeTower(curTower);
                info.text = "Upgrade Cost: " + curTower.getUpgradeCost().ToString();
            }

            void onDelete(Button obj)
            {
                map.DeleteTower(curTower);
            }

            void onCancel(Button obj)
            {
                if (curTower.state == TowerState.BUILD)
                {
                    map.DeleteTower(curTower);
                    menu.towersSelected = null;
                }
                else if (curTower.state == TowerState.ACTIVE)
                {
                    map.DeleteTower(curTower);
                }
                else if (curTower.state == TowerState.DESTROY)
                {
                    map.CancelDelete(curTower);
                }
            }

            private void calcState(bool enabled)
            {
                info.enabled = false;
                upgrade.enabled = false;
                delete.enabled = false;
                cancel.enabled = false;
                selTowerRange.enabled = false;

                if (enabled && curTower != null)
                {
                    title.text = "State: " + curTower.state;
                    selTowerRange.enabled = true;

                    if (curTower.state == TowerState.ACTIVE)
                    {
                        upgrade.enabled = true;
                        delete.enabled = true;
                        info.enabled = true;
                        info.text = "Upgrade Cost: " + curTower.getUpgradeCost().ToString();
                    }
                    else if (curTower.state == TowerState.BUILD || curTower.state == TowerState.DESTROY)
                    {
                        cancel.enabled = true;
                    }
                    else if (curTower.state == TowerState.DEAD)
                    {
                        menu.towersSelected = null;
                    }
                }
            }
            
            public override bool enabled
            {
                set
                {
                    title.enabled = value;

                    if (!value && curTower != null)
                    {
                        curTower = null;
                    }

                    calcState(value);
                }
            }
        }

        /// <summary>
        /// Information about the tower *type*, when user mouseover/selects a tower-building button
        /// </summary>
        class TowerTypeCard : CommandCard
        {
            Menu menu;
            Map map;

            FontRenderer title;
            FontRenderer info;

            TowerTypes _cur;
            public TowerTypes cur
            {
                get
                {
                    return _cur;
                }
                set
                {
                    _cur = value;
                    title.text = value.ToString();
                    info.text = "Cost: " + Tower.GetTowerCost(_cur).ToString();
                }
            }

            public TowerTypeCard(Menu menu, Map map)
            {
                this.menu = menu;
                this.map = map;

                title = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                title.pos = menu.cmdPos;
                title.scale = menu.cmdHeaderSize;
                title.anchor = FontAnchor.TOP_LEFT;

                info = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                info.pos = new Vector2(menu.cmdPos.X, menu.cmdPos.Y + title.scale + menu.cmdPadding);
                info.scale = menu.cmdDetailSize;
                info.anchor = FontAnchor.TOP_LEFT;
                info.text = "Tower Info Goes Here";

                enabled = false;
            }

            public override bool enabled
            {
                set
                {
                    title.enabled = value;
                    info.enabled = value;
                }
            }
        }

        /// <summary>
        /// Information about the currently selected enemy.
        /// </summary>
        class EnemyInfoCard : CommandCard
        {
            Menu menu;
            Map map;

            FontRenderer title;
            FontRenderer info;

            Enemy _cur;
            public Enemy enemySelected
            {
                get
                {
                    return _cur;
                }
                set
                {
                    if (_cur != null)
                    {
                        _cur.healthChanged -= onEnemyHealthChange;
                    }

                    _cur = value;

                    if (_cur != null)
                    {
                        title.text = enemySelected.ToString();
                        info.text = "Hp: " + enemySelected.health + "/" + enemySelected.maxHealth;
                        enemySelected.healthChanged += onEnemyHealthChange;
                    }
                }
            }

            private void onEnemyHealthChange(Enemy obj)
            {
                info.text = "Hp: " + enemySelected.health + "/" + enemySelected.maxHealth;
            }

            public EnemyInfoCard(Menu menu, Map map)
            {
                this.menu = menu;
                this.map = map;

                title = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                title.pos = menu.cmdPos;
                title.scale = menu.cmdHeaderSize;
                title.anchor = FontAnchor.TOP_LEFT;

                info = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                info.pos = new Vector2(menu.cmdPos.X, menu.cmdPos.Y + title.scale + menu.cmdPadding);
                info.scale = menu.cmdDetailSize;
                info.anchor = FontAnchor.TOP_LEFT;
                info.text = "Enemy Info Goes Here";

                enabled = false;
            }

            public override bool enabled
            {
                set
                {
                    title.enabled = value;
                    info.enabled = value;

                    if (!value && enemySelected != null)
                    {
                        enemySelected = null;
                    }
                }
            }
        }

        GameStage stage;
        EventManager em;
        Map map;

        RoundedRectangle_2D background;
        //all of the menu options should be inside this area
        Vector2 bgPos;
        Vector2 bgScale;

        //menu stuff
        BoxTextButton[] towerButtons;
        BoxTextButton startRound;
        BoxTextButton newLevel;
        FontRenderer curRes;

        //command card stuff
        RoundedRectangle_2D cmdBackground;
        //all of the command card info should be inside this area
        Vector2 cmdPos;
        Vector2 cmdScale;
        //standard pixel scaling for command cards so they look somewhat uniform
        float cmdPadding;
        float cmdSmallPadding;
        float cmdHorizTab;
        float cmdHeaderSize;
        float cmdDetailSize;

        //command cards
        CommandCard _currCard = null;
        CommandCard currCard
        {
            get
            {
                return _currCard;
            }
            set
            {
                if (_currCard == value)
                    return;

                if (_currCard != null)
                    _currCard.enabled = false;


                if (value == null)
                {
                    _currCard = roundCard;
                }
                else
                    _currCard = value;

                if (_currCard != null)
                    _currCard.enabled = true;
            }
        }

        RoundInfoCard roundCard;
        TowerTypeCard towerTypeCard;
        TowerCard towerCard;
        EnemyInfoCard enemyCard;

        //State Variables
        public List<Tower> towersSelected
        {
            set
            {
                towerCard.currentTowersSelected = value;

                if (value != null)
                    currCard = towerCard;
                else if (currCard == towerCard)
                    currCard = null;
            }
        }
        public Enemy enemySelected
        {
            get
            {
                return enemyCard.enemySelected;
            }
            set
            {
                enemyCard.enemySelected = value;

                if (value != null)
                    currCard = enemyCard;
                else if (currCard == enemyCard)
                    currCard = null;
            }
        }

        int _resAvailable = 11;
        public int resAvailable
        {
            get
            {
                return _resAvailable;
            }
            set
            {
                _resAvailable = value;
                curRes.text = "Gold: " + _resAvailable.ToString();
            }
        }

        public Menu(GameStage stage, EventManager em, Map map)
        {
            this.stage = stage;
            this.em = em;
            this.map = map;

            background = new RoundedRectangle_2D(stage, em, -1024);
            background.borderColor = Color.White;
            background.borderThickness = 5;
            background.mainColor = new Color(0.2f, 0.2f, 0.2f, 1f);
            background.radius = 10;

            background.position = new Vector2(stage.settings.resWidth * 3 / 4, 0);
            background.scale = new Vector2(stage.settings.resWidth / 4, stage.settings.resHeight);

            bgPos = background.position + background.radius;
            bgScale = background.scale - background.radius * 2;

            //ui positioning
            //all ui should be partitioned based on % of space, to handle multi-resolution
            float xPos = bgPos.X;
            float yPos = bgPos.Y;

            {//create buttons
                float xScale = bgScale.X / 4;
                float yScale = bgScale.Y * 0.10849056603f; //should match xScale at 1080p, non-1080p will be non-square
                towerButtons = new BoxTextButton[(int)TowerTypes.TOWER_END];
                for (TowerTypes t = TowerTypes.TOWER_START + 1; t < TowerTypes.TOWER_END; t++)
                {
                    int i = (int)t;
                    towerButtons[i] = new BoxTextButton(stage, em, 2048, t.ToString());
                    towerButtons[i].Position = new Vector2(xPos, yPos);
                    towerButtons[i].Scale = new Vector2(xScale, yScale);
                    towerButtons[i].dat = t;
                    towerButtons[i].onClick += onTowerButtonClicked;
                    towerButtons[i].onMouseOver += onTowerButtonMouseover;

                    xPos += xScale;
                    if (i % 4 == 3)
                    {
                        xPos = bgPos.X;
                        yPos += yScale;
                    }
                }

                yPos += bgScale.Y * 0.01886792452f;
            }

            {//menu/control buttons

                startRound = new BoxTextButton(stage, em, 2048, "Start");
                startRound.Position = new Vector2(bgPos.X, yPos);
                startRound.Scale = new Vector2(bgScale.X / 3, bgScale.Y * 0.05424528301f);
                startRound.onClick += onStartRoundClick;

                newLevel = new BoxTextButton(stage, em, 2048, "Remake");
                newLevel.Position = new Vector2(bgPos.X + bgScale.X * 2 / 3f, yPos);
                newLevel.Scale = new Vector2(bgScale.X / 3, bgScale.Y * 0.05424528301f);
                newLevel.onClick += onNewLevelClick;

                yPos += startRound.Scale.Y + bgScale.Y * 0.01886792452f;
            }

            {//state info -- amount of gold

                curRes = new FontRenderer(map.stage, map.em, -1000, map.stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
                curRes.pos = new Vector2(bgPos.X, yPos);
                curRes.scale = bgScale.Y * 0.03773584905f;
                curRes.anchor = FontAnchor.TOP_LEFT;
                curRes.text = "Gold: ";

                yPos += curRes.scale + bgScale.Y * 0.01886792452f;
            }

            //command card background
            cmdBackground = new RoundedRectangle_2D(stage, em, -1023);
            cmdBackground.borderColor = Color.White;
            cmdBackground.borderThickness = 2;
            cmdBackground.mainColor = new Color(1f, 1f, 1f, 0f); //white transparent so it doesn't lerp to black
            cmdBackground.radius = 10;

            cmdBackground.position = new Vector2(background.position.X, yPos);
            cmdBackground.scale = new Vector2(background.scale.X, background.scale.Y - yPos);

            //use background radius here so everything has a uniform padding
            cmdPos = cmdBackground.position + background.radius;
            cmdScale = cmdBackground.scale - background.radius * 2;

            cmdPadding = cmdScale.Y * 0.03292181069f; //20pixels at 1080p
            cmdSmallPadding = cmdScale.Y * 0.03292181069f * 0.5f; //10 pixels
            cmdHeaderSize = cmdScale.Y * 0.06584362139f; //40 pixels at 1080p
            cmdDetailSize = cmdScale.Y * 0.04938271604f; //30 pixels at 1080p
            cmdHorizTab = cmdScale.X * 0.05f;

            {//the rest is the 'command card' area
                roundCard = new RoundInfoCard(this, map);
                towerTypeCard = new TowerTypeCard(this, map);
                towerCard = new TowerCard(this, map);
                enemyCard = new EnemyInfoCard(this, map);
            }
            currCard = roundCard;

            em.addEventHandler(0, onActionEvent);
        }

        private void onStartRoundClick(Button obj)
        {
            map.StartRound();
        }

        private void onNewLevelClick(Button obj)
        {
            map.MakeNewLevel(0);
        }

        void onTowerButtonMouseover(Button obj, bool over)
        {
            //get the tower type selected from the button data
            TowerTypes t = (TowerTypes)obj.dat;

            if (over)
            {
                towerTypeCard.cur = t;
                currCard = towerTypeCard;
            }
            else if (towerTypeCard.cur == t)
            {
                if (map.curTower <= TowerTypes.TOWER_START || map.curTower >= TowerTypes.TOWER_END)
                {
                    currCard = null;
                }
                else
                { //we have a tower selected to build, so leave the towertype card on building thing
                    towerTypeCard.cur = map.curTower;
                    currCard = towerTypeCard;
                }
            }
        }

        void onTowerButtonClicked(Button obj)
        {
            //get the tower type selected from the button data
            TowerTypes t = (TowerTypes)obj.dat;

            //pass this off to the scene to handle
            map.TowerSelected(t);
        }

        public void TowerSelectionChanged(TowerTypes type)
        {
            for (TowerTypes t = TowerTypes.TOWER_START + 1; t < TowerTypes.TOWER_END; t++)
            {
                int i = (int)t;

                if (type == t)
                    towerButtons[i].MainColor = Color.Red;
                else
                    towerButtons[i].MainColor = Color.Black;
            }

            if (type > TowerTypes.TOWER_START && type < TowerTypes.TOWER_END)
            {
                towerTypeCard.cur = type;
                currCard = towerTypeCard;
            }
            else if (currCard == towerTypeCard)
                currCard = null;
        }

        bool onActionEvent(ActionEventArgs args)
        {
            if (args.action >= ActionType.BUILD_TOWER_0 && args.action <= ActionType.BUILD_TOWER_11)
            {
                int i = (int)args.action - (int)ActionType.BUILD_TOWER_0;
                map.TowerSelected((TowerTypes)i);
                return true;
            }
            return false;
        }

        public void RoundChanged()
        {
            roundCard.RoundChanged();
            currCard = roundCard;
        }

        internal void playNotEnoughResAnim()
        {
            Logger.WriteLine(LogType.DEBUG, "Not enough resources to do whatever you were trying to do");
        }
    }
}
