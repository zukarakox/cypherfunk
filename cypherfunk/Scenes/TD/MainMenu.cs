﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using deck;
using log;
using assetmanager;

namespace cypherfunk
{
    class MainMenu : Scene
    {
        ICamera cam;

        GameStage stage;

        public MainMenu(GameStage stage)
        {
            this.stage = stage;
            cam = new FPVCamera(stage.settings.resWidth / (float)stage.settings.resHeight, Vector3.UnitY, 0, 0);
        }

        public HashSet<Asset> getPreloadAssetList()
        {
            return new HashSet<Asset>()
            {
                Asset.B_FONT,
                Asset.B_COLOR,
                Asset.TEX_DUCK,
                Asset.SH_FONT_SDF,
                Asset.F_CALLI_SDF_128,
                Asset.SH_POS_TEX,
                Asset.VB_QUAD_POS_TEX_UNIT
            };
        }

        public void Preload(EventManager em)
        {
            var font = new FontRenderer(stage, em, 0, stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
            font.pos = new Vector2(stage.settings.resWidth / 2, stage.settings.resHeight / 2);
            font.scale = stage.settings.resHeight / 8;
            font.text = "Cypherfunk Software";
            font.anchor = FontAnchor.CENTER_CENTER;

            var tex = new TexturedQuad_2D(stage, em, 0, stage.Assets.getAsset(TextureAssets.DUCK));
            tex.scale = new Vector2(400, 400);
            tex.position = new Vector2(font.pos.X - tex.scale.X / 2, font.pos.Y - font.scale / 2 - tex.scale.Y);
        }

        public float loadTime()
        {
            return 5f;
        }

        public void LoadUpdate(float dt)
        {
        }

        public void LoadEnd()
        {
        }

        public HashSet<Asset> getAssetList()
        {
            return new HashSet<Asset>()
            {
                (Asset)ShaderAssets.POS_TEX,
                (Asset)ShaderAssets.POS_NORM_TEX,
                (Asset)TextureAssets.DUCK,
                (Asset)TextureAssets.WHITEPIXEL,
                (Asset)VertexBufferAssets.QUAD_POS_TEX_UNIT,
                (Asset)VertexBufferAssets.CIRCLE_POS_TEX_UNIT,
                (Asset)VertexBufferAssets.CIRCLE_POS_TEX_NORM_UNIT,
                (Asset)FontAssets.CALLI_SDF_128,
                (Asset)FontAssets.SEGOEUI_SDF_128,
            };
        }

        public bool draw3D()
        {
            return false;
        }

        public void Load(EventManager em)
        {
            FontRenderer font = new FontRenderer(stage, em, 0, stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
            font.scale = 128f;
            font.pos = new Vector2(10, 10);
            font.anchor = FontAnchor.TOP_LEFT;
            font.text = "Hi!";
            
            Button button = new BoxTextButton(stage, em, 0, "Start Game");
            button.Position = new Vector2(10, 300);
            button.Scale = new Vector2(400, 50);

            button.onClick += onStartGame;
        }

        private void onStartGame(Button obj)
        {
            stage.switchToScene(Scenes.TD_SCENE);
        }

        public void Update(float dt)
        {
        }

        public ICamera getCamera()
        {
            return cam;
        }

        public void Dispose()
        {
        }
    }
}
