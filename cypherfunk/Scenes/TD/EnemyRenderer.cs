﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using SharpDX;
using SharpDX.Direct3D11;
using Color = SharpDX.Color;
using SharpDX.DXGI;
using assetmanager;

namespace cypherfunk
{
    [StructLayout(LayoutKind.Explicit, Size = 64)]
    struct EnemyVSData
    {
        [FieldOffset(0)]
        public Vector4 centerRadius;
        [FieldOffset(16)]
        public Vector4 centerColor;
        [FieldOffset(32)]
        public Vector4 borderColor;
        [FieldOffset(48)]
        public float borderWidth;
        [FieldOffset(52)]
        public float time;
        [FieldOffset(56)]
        public float shieldMult;
        [FieldOffset(60)]
        float padding;
    }

    [StructLayout(LayoutKind.Explicit, Size = 16)]
    struct EnemyDrawData
    {
        [FieldOffset(0)]
        public Vector3 cameraForward;
        [FieldOffset(12)]
        private float padding;
    }

    struct EnemyRender
    {//different struct than LineSegmentData in case I want to add something to the shader
        public Vector3 center;
        public float radius;
        public Vector4 centerColor;
        public Vector4 borderColor;
        public float borderWidth;
        public bool hasShield;
        public float time;

        public EnemyRender(Vector3 center, float radius, Color centerColor, Color borderColor, float borderWidth, bool hasShield, float time)
        {
            this.center = center;
            this.radius = radius;
            this.centerColor = Texture.convertToLinear(centerColor);
            this.borderColor = Texture.convertToLinear(borderColor);
            this.borderWidth = borderWidth;
            this.hasShield = hasShield;
            this.time = time;
        }
    }

    class EnemyRenderer : IDisposable
    {
        public Shader shader;
        private ConstBuffer<EnemyVSData> circleBuf;
        private ConstBuffer<EnemyDrawData> drawBuf;
        private ConstBuffer<ushort> indexBuffer;

        public bool enabled = true;

        public List<EnemyRender> circles = new List<EnemyRender>();

        GameStage stage;
        EventManager em;

        public EnemyRenderer(GameStage stage, EventManager em, int priority)
        {
            this.stage = stage;
            this.em = em;

            shader = stage.Assets.getAsset(ShaderAssets.ENEMY);
            drawBuf = stage.Assets.getAsset<EnemyDrawData>(BufferAssets.ENEMY_DRAW);
            circleBuf = stage.Assets.getAsset<EnemyVSData>(BufferAssets.ENEMY_DATA);
            indexBuffer = stage.Assets.getAsset<ushort>(BufferAssets.QUAD_INDEX);

            em.addDrawPostProc(priority, Draw3D);
        }

        void Draw3D()
        {
            if (!enabled)
                return;

            shader.Bind(stage.Context);
            stage.Context.VertexShader.SetShaderResource(0, circleBuf.srv);
            stage.Context.InputAssembler.SetIndexBuffer(indexBuffer.buf, Format.R16_UInt, 0);

            drawBuf.dat[0].cameraForward = stage.cam.getForwardVec();
            drawBuf.Write(stage.Context);

            stage.Context.VertexShader.SetConstantBuffer(1, drawBuf.buf);

            int index = 0;

            while (index < circles.Count)
            {
                int loops = Math.Min(index + circleBuf.numElements, circles.Count) - index;

                for (int i = 0; i < loops; i++)
                {
                    circleBuf.dat[i].centerRadius = new Vector4(circles[i + index].center, circles[i + index].radius);
                    circleBuf.dat[i].centerColor = circles[i + index].centerColor;
                    circleBuf.dat[i].borderColor = circles[i + index].borderColor;
                    circleBuf.dat[i].borderWidth = circles[i + index].borderWidth;
                    circleBuf.dat[i].time = circles[i + index].time;
                    circleBuf.dat[i].shieldMult = circles[i + index].hasShield ? 1.0f : 0.0f;
                }

                circleBuf.Write(stage.Context, 0, loops);

                stage.Context.DrawIndexed(loops * 6, 0, 0);

                index += loops;
            }
        }

        public void Dispose()
        {
            em.removePostProc(Draw3D);
        }
    }
}
