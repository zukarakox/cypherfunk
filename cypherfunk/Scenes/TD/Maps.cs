﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using SharpDX;
using assetmanager;
using log;

namespace cypherfunk.TD
{
    struct EnemySpawnInfo
    {
        public IList<EnemyAffixes> type;
        public int num;
        public float spawnTime;

        /// <summary>
        /// Difficult mod for deciding strength of the enemy spawned. (Amount of hp, or strength of certain characteristics)
        /// </summary>
        public float diffMod;

        /// <summary>
        /// Number of enemies to spawn per batch.
        /// Must be >= 1.
        /// </summary>
        public int batchNum;

        /// <summary>
        /// Amount of time between spawning each individual enemy in a batch.
        /// Not used if batchNum is 1.
        /// </summary>
        public float batchTime;

        public EnemySpawnInfo(IList<EnemyAffixes> type, int num, float diffMod)
        {
            this.type = type;
            this.num = num;
            this.diffMod = diffMod;

            spawnTime = Enemy.GetSpawnTime(type, diffMod);
            batchNum = Enemy.GetSpawnCount(type);
            batchTime = 0.04f;
        }

        public int GetTotalSpawns()
        {
            return num * batchNum;
        }
    }
    
    class Map
    {
        #region Scene State Vars
        public GameStage stage;
        public EventManager em;
        TDScene scene;
        Menu menu;

        OrthoCamera cam;
        Vector2 mapBounds;
        #endregion

        #region World Definition
        //amount of 'world space' a single grid unit takes up
        int _gridScale = 10;
        float _towerRefund = 1f;
        int _worldWidth;// = width * gridScale;
        int _worldHeight;// = width * gridScale;
        public float mapDifficulty = 0;

        public int gridScale
        {
            get
            {
                return _gridScale;
            }
        }
        public float towerRefundAmount
        {
            get
            {
                return _towerRefund;
            }
        }
        public int worldWidth
        {
            get
            {
                return _worldWidth;
            }
        }
        public int worldHeight
        {
            get
            {
                return _worldHeight;
            }
        }
        public int NumTeles
        {
            get
            {
                return navGrid.NumTeles;
            }
        }
        public int NumTravelNodes
        {
            get
            {
                return navGrid.NumTravelNodes;
            }
        }

        //width and height are sized in grid units
        int _width;
        int _height;

        public int width
        {
            get
            {
                return _width;
            }
        }
        public int height
        {
            get
            {
                return _height;
            }
        }

        NavGrid navGrid;
        #endregion

        #region World state
        List<Enemy> enemies = new List<Enemy>();
        List<Tower> towers = new List<Tower>();
        List<Bullet> bullets = new List<Bullet>();

        List<Tower> buildingTowers = new List<Tower>();

        public List<TowerBoostZone> towerBoosts = new List<TowerBoostZone>();
        public List<EnemyBoostZone> enemyBoosts = new List<EnemyBoostZone>();

        //type/number/difficulty of enemies that will be spawned during the current wave
        public List<EnemySpawnInfo> enemiesToSpawn = new List<EnemySpawnInfo>();
        bool roundInProgress = false;
        #endregion

        #region Display Vars
        RoundedRectangle_3D disp;
        Lines_3D gridLines;
        Lines_3D detailedGridLines;

        EnemyRenderer enemyDisp;
        RoundedRectangleBatch_3D towerDisp;
        RoundedRectangleBatch_3D bulletDisp;
        RoundedRectangleBatch_3D mapObjectDisp;
        DashedCircle_3D boostDisp;
        #endregion

        public Map(GameStage stage, EventManager em, TDScene scene)
        {
            this.scene = scene;
            this.stage = stage;
            this.em = em;

            cam = scene.cam;
            mapBounds = new Vector2(stage.settings.resWidth * 3 / 4, stage.settings.resHeight);

            #region 3D Display Init
            disp = new RoundedRectangle_3D(stage, em, -1024);
            disp.radius = 10;
            disp.borderThickness = 2;
            disp.mainColor = Color.Transparent;
            disp.borderColor = Color.White;

            gridLines = new Lines_3D(stage, em, -1000);
            gridLines.color = Color.White;
            gridLines.lineWidth = 1f;

            detailedGridLines = new Lines_3D(stage, em, -1048);
            detailedGridLines.color = Color.Wheat;
            detailedGridLines.lineWidth = 0.25f;
            detailedGridLines.enabled = false;

            boostDisp = new DashedCircle_3D(stage, em, -1000);
            mapObjectDisp = new RoundedRectangleBatch_3D(stage, em, -700);
            towerDisp = new RoundedRectangleBatch_3D(stage, em, -600);
            enemyDisp = new EnemyRenderer(stage, em, -500);
            bulletDisp = new RoundedRectangleBatch_3D(stage, em, -400);

            em.addDrawPostProc(-131072, DrawUpdate);
            #endregion

            #region 2D Display Init
            selectionBox = new RoundedRectangle_2D(stage, em, -2048);
            selectionBox.borderColor = new Color(0, 1f, 0, 1f);
            selectionBox.mainColor = new Color(0, 1f, 0, 0);
            selectionBox.radius = 2;
            selectionBox.borderThickness = 1;
            selectionBox.enabled = false;
            #endregion

            em.addEventHandler((int)InterfacePriority.MEDIUM, onMouseMove);
            em.addEventHandler((int)InterfacePriority.MEDIUM, onAction);
            em.addEventHandler((int)InterfacePriority.MEDIUM, onKeyboardEvent);

            menu = new Menu(stage, em, this);

            navGrid = new NavGrid(this);

            MakeNewLevel(0);
        }

        #region Tower Building
        //information about the tower the user is currently trying to build
        bool _canBuildAtCurrentLoc;
        bool isShiftDown = false;

        TowerTypes _curTower = TowerTypes.TOWER_START;
        TowerPreview towerPreview;
        public TowerTypes curTower
        {
            get
            {
                return _curTower;
            }
            set
            {
                if (_curTower == value)
                    return;

                _curTower = value;
                curTowerLoc = new GridLoc(-1, -1);
                canBuildAtCurrentLoc = false;

                if (_curTower > TowerTypes.TOWER_START && _curTower < TowerTypes.TOWER_END)
                {
                    if (towerPreview != null)
                        towerPreview.Dispose();

                    towerPreview = TowerPreview.CreatePreview(this, _curTower);
                    towerPreview.SetPreview(false);
                    towerPreview.enabled = false; //need a valid position before we can display it
                }
                else
                {
                    towerPreview.Dispose();
                    towerPreview = null;
                }

                menu.TowerSelectionChanged(_curTower);
            }
        }

        GridLoc _curTowerLoc;
        GridLoc curTowerLoc
        {
            get
            {
                return _curTowerLoc;
            }
            set
            {
                _curTowerLoc = value;

                if (_curTowerLoc.x >= 0
                    && _curTowerLoc.y >= 0)
                {
                    towerPreview.position = _curTowerLoc;
                    towerPreview.enabled = true;
                }
                else
                {
                    if (towerPreview != null)
                        towerPreview.enabled = false;
                }
            }
        }

        bool canBuildAtCurrentLoc
        {
            get
            {
                return _canBuildAtCurrentLoc;
            }
            set
            {
                if (_canBuildAtCurrentLoc == value)
                    return;

                _canBuildAtCurrentLoc = value;

                //set the display color
                if (towerPreview != null && curTower > TowerTypes.TOWER_START)
                {
                    towerPreview.SetPreview(_canBuildAtCurrentLoc);
                }
            }
        }

        public void TowerSelected(TowerTypes type)
        {
            curTower = type;
        }
        #endregion

        #region Mouse Selection
        bool _selectionDrag = false;
        bool selectionDrag
        {
            get
            {
                return _selectionDrag;
            }
            set
            {
                _selectionDrag = value;

                selectionBox.enabled = value;
                
                selectionBox.position = CameraHelper.getScreenSpace(startSel, stage.settings.resWidth, stage.settings.resHeight, cam.ViewProjMatrix());
                selectionBox.scale = Vector2.Zero;
            }
        }
        Vector2 startSel; //the original position of the mouse for selection drag -- in world coordinates incase the camera moves
        RoundedRectangle_2D selectionBox;
        #endregion

        #region Mouse Control
        bool onMouseMove(PointerEventArgs args)
        {
            //we don't want to deal with *most* mouse events that take place outside of the map view
            bool outOfMapBounds = args.x > mapBounds.X || args.y > mapBounds.Y;

            #region Mouse Move
            if (args.type == PointerEventType.MOVE)
            {
                #region Edge of Screen
                if (args.windowInFocus)
                {
                    isMouseOnLeftEdge = args.x <= 20;
                    isMouseOnRightEdge = args.x >= stage.settings.resWidth - 20;
                    isMouseOnTopEdge = args.y <= 20;
                    isMouseOnBottomEdge = args.y >= stage.settings.resHeight - 20;
                }
                else
                {
                    isMouseOnLeftEdge = false;
                    isMouseOnRightEdge = false;
                    isMouseOnTopEdge = false;
                    isMouseOnBottomEdge = false;
                }
                #endregion

                #region Tower Building
                if (curTower > TowerTypes.TOWER_START)
                {
                    if (outOfMapBounds)
                    {
                        curTowerLoc = new GridLoc(-1, -1);
                        return false;
                    }

                    Vector3 world = CameraHelper.getWorldSpace(args.x, args.y, stage.settings.resWidth, stage.settings.resHeight, cam.InvViewProjMatrix());

                    int towerWidth = towerPreview.width;
                    int towerHeight = towerPreview.height;

                    bool widthEven = towerWidth % 2 == 0;
                    bool heightEven = towerHeight % 2 == 0;

                    //place the tower's center where the mouse is
                    //if the width/height is odd, this is the center tile
                    //if the width/height is even, this is between the two center tiles
                    //clamp to map edges
                    int x;
                    int y;

                    if (widthEven)
                        x = Math.Min(Math.Max((int)Math.Round(world.X / gridScale) - towerWidth / 2, 0), width - towerWidth);
                    else
                        x = Math.Min(Math.Max((int)(world.X / gridScale) - towerWidth / 2, 0), width - towerWidth);

                    if (heightEven)
                        y = Math.Min(Math.Max((int)Math.Round(world.Y / gridScale) - towerHeight / 2, 0), height - towerHeight);
                    else
                        y = Math.Min(Math.Max((int)(world.Y / gridScale) - towerHeight / 2, 0), height - towerHeight);

                    if (x >= 0 && x <= width - towerWidth
                        && y >= 0 && y <= height - towerWidth)
                    {
                        if (x != curTowerLoc.x || y != curTowerLoc.y)
                        {
                            curTowerLoc = new GridLoc(x, y);
                            canBuildAtCurrentLoc = Tower.GetTowerCost(curTower) <= menu.resAvailable && 
                                navGrid.canPlaceObject(x, y, towerWidth, towerHeight, towerPreview.blocksPathing, false);
                        }
                    }
                    else
                    {
                        curTowerLoc = new GridLoc(-1, -1);
                    }
                    return false;
                }
                #endregion

                if (selectionDrag)
                {
                    Vector2 screen = CameraHelper.getScreenSpace(startSel, stage.settings.resWidth, stage.settings.resHeight, cam.ViewProjMatrix());

                    //make a bounding box from the mouse drag
                    Vector2 st = new Vector2(Math.Min(screen.X, args.x), Math.Min(screen.Y, args.y));
                    Vector2 en = new Vector2(Math.Max(screen.X, args.x), Math.Max(screen.Y, args.y));

                    selectionBox.position = st;
                    selectionBox.scale = en - st;

                    return true;
                }
            }
            #endregion

            #region Ending Selection Box, Button Event
            if (args.type == PointerEventType.BUTTON)
            {
                if (selectionDrag && !args.isDown && args.button == PointerButton.LEFT)
                {//handle the end of the selection box
                    selectionDrag = false;
                    Vector3 world = CameraHelper.getWorldSpace(args.x, args.y, stage.settings.resWidth, stage.settings.resHeight, cam.InvViewProjMatrix());

                    //make a bounding box from the mouse drag
                    Vector2 st = new Vector2(Math.Min(world.X, startSel.X), Math.Min(world.Y, startSel.Y));
                    Vector2 en = new Vector2(Math.Max(world.X, startSel.X), Math.Max(world.Y, startSel.Y));

                    //we want to find all relevant things touching this bounding box
                    //priority is selecting all of our towers, enemies, then map-objects like nodes, teleporters, and/or blockers
                    //towers can be multi-selected, other things just select a single thing closest to the start pos?
                    List<Tower> selTowers = FindAllTowersTouchingBox(st, en);

                    if (selTowers.Count > 0)
                    {
                        //we want selection circles around all of the selected towers
                        menu.towersSelected = selTowers;
                        return true;
                    }
                    menu.towersSelected = null;

                    if (roundInProgress)
                    {
                        Enemy selEnemy = FindNearestSelectableEnemyTouchingBox(st, en);
                        if (selEnemy != null)
                        {
                            menu.enemySelected = selEnemy;
                            return true;
                        }
                    }
                    menu.enemySelected = null;

                    return true;
                }
            }
            #endregion

            if (outOfMapBounds)
                return false;

            #region Mouse Button
            if (args.type == PointerEventType.BUTTON)
            {
                //if we have a tower we're looking to build...
                if (curTower > TowerTypes.TOWER_START && args.isDown)
                #region Tower Building
                {
                    //on left mouse, we're trying to place it down
                    if (args.button == PointerButton.LEFT)
                    {
                        if (canBuildAtCurrentLoc)
                        {//if we can build it, build it!
                            Tower newTower = PlaceNewTower(towerPreview.position, curTower);
                            if (!isShiftDown) //then deselect the tower, unless shift is down
                            {
                                curTower = TowerTypes.TOWER_START;
                                if (newTower != null)
                                    menu.towersSelected = new List<Tower>() { newTower };
                            }
                            canBuildAtCurrentLoc = false;
                        }
                        //if we can't build it at the current location, assume the user misclicked or something
                        //and don't do anything
                        return true;
                    }
                    else if (args.button == PointerButton.RIGHT)
                    { //on right mouse, cancel the selection
                        curTower = TowerTypes.TOWER_START;
                        return true;
                    }
                }
                #endregion

                
                if (args.isDown && args.button == PointerButton.LEFT)
                #region Starting a selection box
                {//handle creating a selection box
                    selectionDrag = true;

                    Vector3 world = CameraHelper.getWorldSpace(args.x, args.y, stage.settings.resWidth, stage.settings.resHeight, cam.InvViewProjMatrix());
                    startSel = new Vector2(world.X, world.Y);
                    return true;
                }
                #endregion
            }
            #endregion

            #region MouseWheel
            if (args.type == PointerEventType.MOUSEWHEEL)
            {
                //make the camera zoom-in/out on the world, centered on the mouse position
                //camera zoom should be clamped at a min/max value to avoid bad
                //max zoom-out should never display the world in less pixels than it is
                //should save about ~25% of the screen on the right for menu
                //so if we have a 1000 x 1000 world on a 800x600 display
                //max zoom out is then dispaying only part of the world, a 600x600 section
                //we don't want to display the world any smaller than that, because lines and things will stop rendering :(
                zoomChanged(Math.Sign(args.wheelClicks), args.x, args.y);
                return true;
            }
            #endregion
            return false;
        }
        #endregion

        #region Camera Calcs
        bool isMouseOnLeftEdge = false;
        bool isMouseOnRightEdge = false;
        bool isMouseOnTopEdge = false;
        bool isMouseOnBottomEdge = false;

        void zoomChanged(int dir, int mouseX, int mouseY)
        {
            //clamp zoom
            int zoomPre = zoom;
            zoom = Math.Max(Math.Min(zoom + dir, 14), 0);
            if (zoomPre == zoom)
                return;

            calcZoom(mouseX, mouseY);
        }

        int zoom = 0;
        public void calcZoom(int mouseX, int mouseY)
        {
            //first, calc the max zoom out distance at zoom = 0
            int screenWidth = stage.settings.resWidth * 3 / 4;
            int screenHeight = stage.settings.resHeight;

            float boundsX = screenWidth;
            float boundsY = screenHeight;

            //if our world size is less than half the screen size, don't zoom out to full screen size
            while (boundsX / 2 > worldWidth && boundsY / 2 > worldHeight)
            {
                boundsX /= 2;
                boundsY /= 2;
            }

            //if zoom isn't 0, zoom in!
            for (int i = 0; i < zoom; i++)
            {
                boundsX *= 0.85f;
                boundsY *= 0.85f;
            }

            //boundsX is * 4 / 3 here because we're reserving 1/4 of the screen for menu space
            //but the camera doesn't know that
            //maybe i should just set the viewport
            Vector2 prevScale = cam.scale;
            cam.scale = new Vector2(boundsX * 4 / 3, boundsY);

            //just set camera position to evenly balance the map in the middle
            cam.pos = new Vector2(cam.pos.X + (prevScale.X - cam.scale.X) * mouseX / stage.settings.resWidth,
                cam.pos.Y + (prevScale.Y - cam.scale.Y) * mouseY / stage.settings.resHeight);

            clampCameraPos();

            detailedGridLines.enabled = Math.Max(boundsX, boundsY) < gridScale * 25;
        }

        void clampCameraPos()
        {
            //now clamp position to inside the viewable bounds
            Vector2 pos = new Vector2(Math.Min(Math.Max(cam.pos.X, 0) + cam.scale.X * 3 / 4, worldWidth) - cam.scale.X * 3 / 4,
                                Math.Min(Math.Max(cam.pos.Y, 0) + cam.scale.Y, worldHeight) - cam.scale.Y);

            if (cam.scale.X * 3f / 4f > worldWidth)
                pos.X = (worldWidth - cam.scale.X * 3f / 4f) / 2.0f;
            if (cam.scale.Y > worldHeight)
                pos.Y = (worldHeight - cam.scale.Y) / 2.0f;

            cam.pos = pos;
        }
        #endregion

        #region Key event handling
        bool onKeyboardEvent(KeyData data, bool isDown)
        {
            if (data.k == System.Windows.Forms.Keys.ShiftKey)
            {
                isShiftDown = isDown;
                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Q && isDown)
            {
                //TEMP
                detailedGridLines.lineSegments.Clear();
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        GridLoc t = new GridLoc(x, y);
                        GridLoc e = getNaviGoal(1, t);

                        if (e.x < 0 || e.y < 0)
                            continue;

                        Vector2 st = new Vector2((t.x + 0.5f) * gridScale, (t.y + 0.5f) * gridScale);
                        Vector2 en = new Vector2((e.x + 0.5f) * gridScale, (e.y + 0.5f) * gridScale);

                        detailedGridLines.lineSegments.Add(new LineSegment(new Vector3(st, -80), new Vector3(en, -80)));
                    }
                }

                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Left && isDown)
            {
                mapDifficulty -= 0.25f;
                ChangeSpawnStats();
                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Right && isDown)
            {
                mapDifficulty += 0.25f;
                ChangeSpawnStats();
                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Up && isDown)
            {
                curAffix = curAffix + 1;
                if (curAffix >= EnemyAffixes.AFFIX_END)
                    curAffix = EnemyAffixes.AFFIX_END - 1;
                ChangeSpawnStats();
                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Down && isDown)
            {
                curAffix = curAffix - 1;
                if (curAffix < EnemyAffixes.AFFIX_START)
                    curAffix = EnemyAffixes.AFFIX_START;
                ChangeSpawnStats();
                return true;
            }

            if (data.k == System.Windows.Forms.Keys.Enter && isDown)
            {
                START_TEST();
                return true;
            }

            return false;
        }
        #endregion

        #region Action Event handling
        bool onAction(ActionEventArgs args)
        {
            if (args.action == ActionType.ESCAPE)
            {
                stage.Exit();
                return true;
            }

            return false;
        }
        #endregion

        #region Round Controls
        float spawner = 0f;
        int enemiesLeaked = 0;
        int curEnemySpawn = 0;
        int numSpawned = 0;
        int batchNum = 0; //enemy we're spawning inside this current batch
        EnemyAffixes curAffix = EnemyAffixes.AFFIX_START;

        bool BALANCE_TEST = false;
        void START_TEST()
        {
            BALANCE_TEST = true;
            mapDifficulty = 0;
            curAffix = EnemyAffixes.AFFIX_START;
            ChangeSpawnStats();
            Logger.WriteLine(LogType.DEBUG, "START TEST");
            GameStage.DEBUG_TIDI = 1000f;
            StartRound();
        }

        void END_TEST()
        {
            Logger.WriteLine(LogType.DEBUG, "END TEST");
            GameStage.DEBUG_TIDI = 1f;
            BALANCE_TEST = false;
        }

        public void StartRound()
        {
            if (!roundInProgress)
            {
                //clear the pather enemies
                foreach (Enemy e in enemies)
                    e.ReachedEnd();
                enemies.Clear();

                foreach (Tower t in towers)
                    t.OnRoundStart();

                foreach (Tower t in buildingTowers)
                    t.OnRoundStart();

                enemiesLeaked = 0;
                spawner = 0;
                batchNum = 0;
                curEnemySpawn = 0;
                numSpawned = 0;
                roundInProgress = true;
            }
        }

        void EndRound()
        {
            if (roundInProgress)
            {
                roundInProgress = false;
                spawner = 0;

                foreach (Enemy e in enemies)
                    e.ReachedEnd();
                enemies.Clear();
                foreach (Bullet b in bullets)
                    b.Dispose();
                bullets.Clear();

                foreach (Tower t in towers)
                    t.OnRoundEnd();

                foreach (Tower t in buildingTowers)
                    t.OnRoundEnd();

                ChangeSpawnStats();
                menu.RoundChanged();

                if (BALANCE_TEST)
                {
                    if (enemiesLeaked == 0)
                    {
                        mapDifficulty += 0.1f;
                        ChangeSpawnStats();
                        StartRound();
                    }
                    else
                    {
                        Logger.WriteLine(LogType.DEBUG, (mapDifficulty - 0.1f).ToString());
                        curAffix = curAffix + 1;
                        if (curAffix == EnemyAffixes.AFFIX_END)
                        {
                            curAffix = EnemyAffixes.AFFIX_END - 1;
                            //done!
                            END_TEST();
                        }
                        else
                        {
                            mapDifficulty = 0;
                            ChangeSpawnStats();
                            StartRound();
                        }

                    }
                }
            }
            else
            {
                Logger.WriteLine(LogType.DEBUG, "Trying to end round while round isn't in progress?");
            }
        }

        void ChangeSpawnStats()
        {
            if (!roundInProgress)
            {
                for (int i = 0; i < enemiesToSpawn.Count; i++)
                {
                    if (curAffix > EnemyAffixes.AFFIX_START)
                        enemiesToSpawn[i] = (new EnemySpawnInfo(new List<EnemyAffixes>() { curAffix }, 10, mapDifficulty));
                    else
                        enemiesToSpawn[i] = (new EnemySpawnInfo(new List<EnemyAffixes>(), 10, mapDifficulty));
                }

                menu.RoundChanged();
            }
        }
        #endregion

        #region Level Gen
        public void MakeNewLevel(int seed)
        {
            //reset states
            roundInProgress = false;
            spawner = 0;
            enemiesToSpawn.Clear();

            //dispose and clear all the old crap
            foreach (Enemy e in enemies)
                e.ReachedEnd();
            enemies.Clear();
            foreach (Tower t in towers)
                t.Dispose();
            towers.Clear();
            foreach (Tower t in buildingTowers)
                t.Dispose();
            buildingTowers.Clear();
            foreach (Bullet b in bullets)
                b.Dispose();
            bullets.Clear();

            foreach (TowerBoostZone z in towerBoosts)
                z.Dispose();
            towerBoosts.Clear();
            foreach (EnemyBoostZone z in enemyBoosts)
                z.Dispose();
            enemyBoosts.Clear();

            //reset UI states
            menu.towersSelected = null;

            //init new stuff!
            Random rand;
            if (seed == 0)
                rand = new Random();
            else
                rand = new Random(seed);

            // ------------------           gen new world
            //vars that can change, but we're going to leave them constant for now
            _gridScale = 10;
            _towerRefund = 1.0f;

            //high-level world generation
            //the Important Decisions are handled here, actually generating stuff comes later

            int numNodes;
            int numRocks = 0;
            int numTeleporters = 0;
            int numBoosts = 0;
            float[] boostRad;
            float[] boostStr;
            int numSpawns;
            int[] eneAffixes;
            float[] eneDiff;
            int[] eneNum;

            #region Parameter Gen
            if (mapDifficulty < 10)
            {
                _width = rand.Next(15, 21);
                _height = rand.Next(15, 21);

                numNodes = 3;
                numRocks = 0;

                numSpawns = 1;
                eneNum = new int[numSpawns];
                eneAffixes = new int[numSpawns];
                eneDiff = new float[numSpawns];

                for (int i = 0; i < numSpawns; i++)
                {
                    eneDiff[i] = rand.NextFloat(3, 4);
                    eneNum[i] = rand.Next(10, 20);
                    eneAffixes[i] = 0;
                }

                int t = rand.Next(0, 3);
                if (t == 0)
                    numTeleporters = 1;
                else if (t == 1)
                    numBoosts = 1;
                else if (t == 2)
                    eneAffixes[0] = 1;
                else
                    throw new InvalidOperationException("Rand out of range?");

                boostStr = new float[numBoosts];
                boostRad = new float[numBoosts];
                for (int i = 0; i < numBoosts; i++)
                {
                    boostRad[i] = rand.NextFloat(10, 40);
                    boostStr[i] = rand.NextFloat(1.2f, 2f);
                }
            }
            else if (mapDifficulty < 20)
            {
                _width = rand.Next(21, 25);
                _height = rand.Next(21, 25);

                numNodes = rand.Next(3, 5);
                numRocks = 0;

                numSpawns = 1;
                eneNum = new int[numSpawns];
                eneAffixes = new int[numSpawns];
                eneDiff = new float[numSpawns];

                for (int i = 0; i < numSpawns; i++)
                {
                    eneDiff[i] = rand.NextFloat(5, 7);
                    eneNum[i] = rand.Next(10, 20);
                    eneAffixes[i] = 0;
                }

                int t = rand.Next(0, 3);
                if (t == 0)
                    numTeleporters = 1;
                else if (t == 1)
                    numBoosts = 1;
                else if (t == 2)
                    eneAffixes[0] = 1;
                else
                    throw new InvalidOperationException("Rand out of range?");

                boostStr = new float[numBoosts];
                boostRad = new float[numBoosts];
                for (int i = 0; i < numBoosts; i++)
                {
                    boostRad[i] = rand.NextFloat(10, 40);
                    boostStr[i] = rand.NextFloat(1.2f, 2f);
                }
            }
            else //max level
            {
                _width = rand.Next(46, 51);
                _height = rand.Next(46, 51);

                numNodes = rand.Next(6, 11);
                numRocks = 0;

                numTeleporters = rand.Next(1, 4);

                numSpawns = rand.Next(1, 4);
                eneNum = new int[numSpawns];
                eneAffixes = new int[numSpawns];
                eneDiff = new float[numSpawns];

                for (int i = 0; i < numSpawns; i++)
                {
                    eneDiff[i] = rand.NextFloat(400, 500);
                    eneNum[i] = rand.Next(400, 500);
                    eneAffixes[i] = 3;
                }

                numBoosts = rand.Next(1, 4);
                boostStr = new float[numBoosts];
                boostRad = new float[numBoosts];
                for (int i = 0; i < numBoosts; i++)
                {
                    boostRad[i] = rand.NextFloat(10, 40);
                    boostStr[i] = rand.NextFloat(1.2f, 2f);
                }
            }
            #endregion

            //update vars changed by high level world gen
            _worldWidth = width * gridScale;
            _worldHeight = height * gridScale;

            //decide how many resources player should have for this round
            menu.resAvailable = width * height * 2;

            //init grid
            navGrid.init(rand, width, height, numNodes, numTeleporters, numRocks);

            //add in boosts
            for (int i = 0; i < numBoosts; i++)
            {
                Vector2 pos = new Vector2(
                    rand.NextFloat(0, 1) * worldWidth,
                    rand.NextFloat(0, 1) * worldHeight
                    );

                int type = rand.Next(0, 4);
                if (type == 0)
                    enemyBoosts.Add(EnemyBoostZone.makeEnemySpeedBoost(this, pos, boostRad[i], boostStr[i]));
                else if (type == 1)
                    towerBoosts.Add(TowerBoostZone.makeDamageBoost(this, pos, boostRad[i], boostStr[i]));
                else if (type == 2)
                    towerBoosts.Add(TowerBoostZone.makeRangeBoost(this, pos, boostRad[i], boostStr[i]));
                else if (type == 3)
                    towerBoosts.Add(TowerBoostZone.makeAttackSpeedBoost(this, pos, boostRad[i], boostStr[i]));
                else
                    throw new InvalidOperationException("Rand out of range?: " + type);
            }

            //setup enemies
            for (int i = 0; i < numSpawns; i++)
            {
                List<EnemyAffixes> affixSet = new List<EnemyAffixes>();

                for (int t = 0; t < eneAffixes[i]; t++)
                {
                    EnemyAffixes toAdd = (EnemyAffixes)rand.Next(0, (int)EnemyAffixes.AFFIX_END);
                    if (!affixSet.Contains(toAdd))
                        affixSet.Add(toAdd);
                    else
                        t--;
                }

                enemiesToSpawn.Add(new EnemySpawnInfo(affixSet, eneNum[i], eneDiff[i]));
            }

            #region display setup
            //setup background
            disp.scale = new Vector2(width * gridScale, height * gridScale);
            disp.pos = new Vector3(0, 0, -500);

            //setup main grid lines
            gridLines.lineSegments.Clear();
            for (int x = 10; x < width; x += 10)
            {
                gridLines.lineSegments.Add(new LineSegment(new Vector3(x * gridScale, gridLines.lineWidth, -80),
                    new Vector3(x * gridScale, height * gridScale - gridLines.lineWidth, -80)));
            }
            for (int y = 10; y < height; y += 10)
            {
                gridLines.lineSegments.Add(new LineSegment(new Vector3(gridLines.lineWidth, y * gridScale, -80),
                    new Vector3(width * gridScale - gridLines.lineWidth, y * gridScale, -80)));
            }

            //setup detailed grid lines
            detailedGridLines.lineSegments.Clear();
            for (int x = 1; x < width; x++)
            {
                if (x % 10 == 0)
                    continue;

                detailedGridLines.lineSegments.Add(new LineSegment(new Vector3(x * gridScale, detailedGridLines.lineWidth, -80),
                    new Vector3(x * gridScale, height * gridScale - detailedGridLines.lineWidth, -80)));
            }

            for (int y = 1; y < height; y++)
            {
                if (y % 10 == 0)
                    continue;

                detailedGridLines.lineSegments.Add(new LineSegment(new Vector3(detailedGridLines.lineWidth, y * gridScale, -80),
                    new Vector3(width * gridScale - detailedGridLines.lineWidth, y * gridScale, -80)));
            }
            #endregion

            zoom = 0;
            calcZoom(0, 0);
            menu.RoundChanged();
        }
        #endregion

        #region Updates
        public void Update(float dt)
        {
            #region Camera Update
            if (isMouseOnBottomEdge || isMouseOnLeftEdge || isMouseOnRightEdge || isMouseOnTopEdge)
            {
                Vector2 camPos = cam.pos;
                if (isMouseOnRightEdge)
                    camPos.X += cam.scale.X * 0.5f * dt;
                if (isMouseOnLeftEdge)
                    camPos.X -= cam.scale.X * 0.5f * dt;
                if (isMouseOnBottomEdge)
                    camPos.Y += cam.scale.Y * 0.5f * dt;
                if (isMouseOnTopEdge)
                    camPos.Y -= cam.scale.Y * 0.5f * dt;
                cam.pos = camPos;
                clampCameraPos();
            }
            #endregion

            navGrid.Update(dt);

            #region Round Update
            if (roundInProgress)
            {
                spawner += dt;
                if (spawner > 0f && curEnemySpawn < enemiesToSpawn.Count)
                {
                    SpawnEnemy(enemiesToSpawn[curEnemySpawn].type, enemiesToSpawn[curEnemySpawn].diffMod);

                    batchNum++;
                    if (batchNum >= enemiesToSpawn[curEnemySpawn].batchNum)
                    {
                        spawner = -enemiesToSpawn[curEnemySpawn].spawnTime;
                        batchNum = 0;
                        numSpawned++;
                    }
                    else
                        spawner = -enemiesToSpawn[curEnemySpawn].batchTime;

                    if (numSpawned >= enemiesToSpawn[curEnemySpawn].num)
                    {
                        curEnemySpawn++;
                        numSpawned = 0;
                    }
                }
                for (int i = 0; i < enemies.Count; i++)
                {
                    Enemy e = enemies[i];

                    e.UpdateEffects(dt);

                    if (e.health <= 0)
                    {//enemy died
                        e.Died();
                        enemies.RemoveAt(i--);
                    }
                    else if (e.Update(dt))
                    {//enemy made it to the end
                        enemiesLeaked += e.ReachedEnd();
                        enemies.RemoveAt(i--);
                    }

                    //otherwise, see if the enemy stepped in anything interesting
                    navGrid.checkTeles(e);
                }

                if (curEnemySpawn >= enemiesToSpawn.Count && enemies.Count == 0)
                {
                    //round won!
                    EndRound();
                }

                foreach (Tower t in towers)
                {
                    t.Update(dt);
                }

                UpdateBuildingTowers(dt);

                for (int i = 0; i < bullets.Count; i++)
                {
                    if (bullets[i].Update(dt))
                    {
                        bullets[i].Dispose();
                        bullets.RemoveAt(i--);
                    }
                }
            }
            #endregion
            #region Build-time Update
            else //round *not* in progress
            {
                spawner += dt;
                if (spawner > 1f)
                {
                    spawner = 0;
                    SpawnTracer();
                }

                for (int i = 0; i < enemies.Count; i++)
                {
                    Enemy e = enemies[i];
                    
                    if (e.Update(dt))
                    {//enemy made it to the end
                        e.ReachedEnd();
                        enemies.RemoveAt(i--);
                    }

                    //otherwise, see if the enemy stepped in anything interesting
                    navGrid.checkTeles(e);
                }

                UpdateBuildingTowers(dt);
            }
            #endregion
        }

        private void UpdateBuildingTowers(float dt)
        {
            for (int i = 0; i < buildingTowers.Count; i++)
            {
                Tower t = buildingTowers[i];
                t.BuildUpdate(dt);
                if (t.state == TowerState.ACTIVE)
                {
                    if (!t.inNavi)
                    {
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower switched to active state, but isn't in navigation?");
                        navGrid.addToNavi(t);
                        t.inNavi = true;
                    }
                    towers.Add(t);
                    buildingTowers.RemoveAt(i--);
                }
                else if (t.state == TowerState.FINISH_BUILD && !t.inNavi)
                {
                    navGrid.addToNavi(t);
                    t.inNavi = true;
                }
                else if (t.state == TowerState.FINISH_DESTROY && t.inNavi)
                {
                    navGrid.removeFromNavi(t);
                    t.inNavi = false;
                }
                else if (t.state == TowerState.DEAD)
                {
                    if (t.inNavi)
                    {
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower switched to dead state, but is still in navigation?");
                        navGrid.removeFromNavi(t);
                        t.inNavi = false;
                    }

                    menu.resAvailable = menu.resAvailable + (int)(t.totalCost * towerRefundAmount);
                    buildingTowers.RemoveAt(i--);
                    navGrid.removeObject(t);
                    t.Dispose();
                }
            }
        }
        #endregion

        #region Search Functions
        IEnumerable<Enemy> targetableEnemies
        {
            get
            {
                foreach (Enemy e in enemies)
                {
                    if (e.health > 0 && !e.isInvuln)
                        yield return e;
                }
            }
        }

        IEnumerable<Enemy> enemiesInRange(Vector2 pos, float maxDist)
        {
            foreach (Enemy e in targetableEnemies)
            {
                float dist = Vector2.DistanceSquared(pos, e.position);
                if (dist < (maxDist + e.radius) * (maxDist + e.radius))
                {
                    yield return e;
                }
            }
        }

        public Enemy FindNearestEnemy(Vector2 pos, float maxDist)
        {
            Enemy toReturn = null;
            float minDist = maxDist * maxDist;

            foreach (Enemy e in enemiesInRange(pos, maxDist))
            {
                float dist = Vector2.DistanceSquared(pos, e.position);
                if (dist < minDist || toReturn == null)
                {
                    minDist = dist;
                    toReturn = e;
                }
            }

            return toReturn;
        }

        public Enemy FindClosestToEndEnemy(Vector2 pos, float maxDist)
        {
            Enemy toReturn = null;
            float distToEnd = 0;

            foreach (Enemy e in enemiesInRange(pos, maxDist))
            {
                float dist = e.GetEstDistToEnd();
                if (dist < distToEnd || toReturn == null)
                {
                    distToEnd = dist;
                    toReturn = e;
                }
            }

            return toReturn;
        }

        public Enemy FindClosestToEndEnemyWithWeakerPoison(Vector2 pos, float maxDist, PoisonEffect effect)
        {
            Enemy toReturn = null;
            float distToEnd = 0;
            float retDamage = 0;
            float effDamage = effect.dps * effect.time;

            foreach (Enemy e in enemiesInRange(pos, maxDist))
            {
                float dist = e.GetEstDistToEnd();

                float dmg; //damage the enemy will take if we apply our effect
                if (e.poisonEffect.dps > effect.dps)
                    dmg = 0; //if the current poison on the mob does more dps than our poison, we'll do 0 damage, because it won't apply the effect.
                else //otherwise, we'll deal our full damage minus the damage we're going to 'remove' from the mob
                    dmg = effDamage - e.poisonEffect.dps * Math.Max(e.poisonEffect.time, 0); 

                if (toReturn == null)
                {//if we haven't selected a target yet, just pick the first one
                    distToEnd = dist;
                    retDamage = dmg;
                    toReturn = e;
                }
                else if (dmg > retDamage)
                {//if we can deal more total damage by targetting this mob, do it
                    distToEnd = dist;
                    retDamage = dmg;
                    toReturn = e;
                }
                else if (dmg == retDamage && dist < distToEnd)
                {//if damage output is the same, target the closest to the end
                    distToEnd = dist;
                    retDamage = dmg;
                    toReturn = e;
                }
            }

            return toReturn;
        }

        public Enemy FindNearestEnemyIgnoringEnemies(Vector2 pos, float maxDist, List<Enemy> toIgnore)
        {
            Enemy toReturn = null;
            float minDist = maxDist * maxDist;

            foreach (Enemy e in enemiesInRange(pos, maxDist))
            {
                if (toIgnore.Contains(e))
                    continue;

                float dist = Vector2.DistanceSquared(pos, e.position);
                if (dist < minDist || toReturn == null)
                {
                    minDist = dist;
                    toReturn = e;
                }
            }
            
            return toReturn;
        }

        public bool AnyEnemyInRange(Vector2 pos, float maxDist)
        {
            foreach (Enemy e in enemiesInRange(pos, maxDist))
            {
                return true;
            }

            return false;
        }

        public List<Enemy> FindAllEnemiesTouchingBox(Vector2 start, Vector2 end)
        {
            List<Enemy> toReturn = new List<Enemy>();

            foreach (Enemy e in targetableEnemies)
            {
                if (e.position.X + e.radius >= start.X && e.position.X - e.radius < end.X
                    && e.position.Y + e.radius >= start.Y && e.position.Y - e.radius < end.Y)
                {
                    toReturn.Add(e);
                }
            }

            return toReturn;
        }

        public List<Tower> FindAllTowersTouchingBox(Vector2 start, Vector2 end)
        {
            return navGrid.FindAllTowersTouchingBox(start, end);
        }

        public Enemy FindNearestSelectableEnemyTouchingBox(Vector2 start, Vector2 end)
        {
            Enemy toReturn = null;
            float minDist = Vector2.DistanceSquared(start, end);
            
            foreach (Enemy e in enemies)
            {
                float dist = Vector2.DistanceSquared(start, e.position);
                //first check if the enemy is close enough to possibly be inside the box and closer than current closer
                if (dist < minDist)
                {
                    //then verify it's atually inside the box
                    if (e.position.X >= start.X && e.position.X < end.X
                        && e.position.Y >= start.Y && e.position.Y < end.Y)
                    {
                        minDist = dist;
                        toReturn = e;
                    }
                }
            }

            return toReturn;
        }
        #endregion

        #region Game Effects -- Spawn bullets/enemies, apply damage/effect
        public void FirePewPewBullet(Enemy target, Vector2 startPos, float damage)
        {
            bullets.Add(new PewPewBullet(this, target, startPos, damage));
        }

        public void FireBwooshBullet(Vector2 target, Vector2 startPos, float damage)
        {
            bullets.Add(new BwooshBullet(this, target, startPos, damage));
        }

        public void FirePoisonBullet(Enemy target, Vector2 startPos, PoisonEffect effect)
        {
            bullets.Add(new PoisonBullet(this, target, startPos, effect));
        }

        public CarrierBullet FireCarrierBullet(Enemy target, Vector2 startPos, float damage)
        {
            CarrierBullet toRet = new CarrierBullet(this, target, startPos, damage);
            bullets.Add(toRet);

            return toRet;
        }

        public void SpawnEnemy(IList<EnemyAffixes> type, float diff)
        {
            Vector2 pos = navGrid.getSpawnPos();
            Enemy.CreateEnemy(type, this, pos, diff, 1);
        }

        public void SpawnTracer()
        {
            Vector2 pos = navGrid.getSpawnPos();
            Enemy.CreateEnemy(null, this, pos, 0f, 1);
        }

        public void AddEnemy(Enemy e)
        {
            enemies.Add(e);
        }

        public GridLoc getNaviGoal(int node, GridLoc pos)
        {
            return navGrid.getNaviGoal(node, pos);
        }

        public GridLoc getNodePos(int node)
        {
            return navGrid.getNodePos(node);
        }

        public void DamageArea(Vector2 pos, float range, float damage, DamageSource source)
        {
            foreach (Enemy e in enemiesInRange(pos, range))
            {
                e.DealDamage(damage, source);
            }
        }

        public void ApplyAOEEffect(Vector2 pos, float range, EnemyEffect effect)
        {
            foreach (Enemy e in enemiesInRange(pos, range))
            {
                effect.Apply(e);
            }
        }
        #endregion

        #region Add/Del Towers
        public void UpgradeTower(Tower t)
        {
            int cost = t.getUpgradeCost();
            if (cost > menu.resAvailable)
            {
                menu.playNotEnoughResAnim();
                return;
            }

            menu.resAvailable = menu.resAvailable - cost;

            t.Upgrade();
        }

        public Tower PlaceNewTower(GridLoc pos, TowerTypes type)
        {
            int cost = Tower.GetTowerCost(type);
            if (cost > menu.resAvailable)
            {
                menu.playNotEnoughResAnim();
                return null;
            }

            menu.resAvailable = menu.resAvailable - cost;

            //placeObject will make sure the position is valid
            Tower t = Tower.CreateTower(this, pos, type);

            buildingTowers.Add(t);
            navGrid.placeObject(t);

            if (roundInProgress)
                t.OnRoundStart();

            return t;
        }

        public void DeleteTower(Tower t)
        {
            switch (t.state)
            {
                case TowerState.BUILD:
                    menu.resAvailable = menu.resAvailable + t.totalCost;
                    buildingTowers.Remove(t);
                    navGrid.removeObject(t);
                    t.Dispose();
                    break;
                case TowerState.FINISH_BUILD:
                    //this shouldn't ever happen during a single-player game
                    //this *may* happen during a multiplayer game where a player cancels a building at the last second
                    //local pathing may do weird stuff for a second, if there was Significant latency delay and we used up all of our buffer time :(
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower being removed during finish time");
                    menu.resAvailable = menu.resAvailable + t.totalCost;
                    navGrid.removeFromNavi(t);
                    buildingTowers.Remove(t);
                    navGrid.removeObject(t);
                    t.Dispose();
                    break;
                case TowerState.ACTIVE:
                    //the tower is fully built, so rather than deleting it directly, we want to start 'destryoing' the tower
                    //the player can cancel the destruction during this time, the tower blocks pathing but does not shoot
                    towers.Remove(t);
                    buildingTowers.Add(t);
                    t.state = TowerState.DESTROY;
                    break;
                case TowerState.DESTROY:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Destryoing a tower at DESTROY?");
                    break;
                case TowerState.FINISH_DESTROY:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Destryoing a tower at FINISH_DESTROY?");
                    break;
                case TowerState.DEAD:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Destryoing a tower at DEAD?");
                    break;
            }
        }

        public void CancelDelete(Tower t)
        {
            switch (t.state)
            {
                case TowerState.BUILD:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Cancel destroy a tower at BUILD?");
                    break;
                case TowerState.FINISH_BUILD:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Cancel destroy a tower at FINISH_BUILD?");
                    break;
                case TowerState.ACTIVE:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Cancel destroy a tower at ACTIVE?");
                    break;
                case TowerState.DESTROY:
                    if (buildingTowers.Remove(t))
                    {
                        towers.Add(t);
                        t.state = TowerState.ACTIVE;
                    }
                    else
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower in destroy state wasn't in buildingTowers list in CancelDelete?");
                    break;
                case TowerState.FINISH_DESTROY:
                    if (buildingTowers.Remove(t))
                    {
                        towers.Add(t);
                        t.state = TowerState.ACTIVE;
                        if (!t.inNavi)
                            navGrid.addToNavi(t);
                        else
                            Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower wasn't in navi in finish_destroy state in canceldelete?");
                    }
                    else
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "Tower in finish_destroy state wasn't in buildingTowers list in CancelDelete?");
                    break;
                case TowerState.DEAD:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Cancel destroy a tower at DEAD?");
                    break;
            }
        }
        #endregion
        
        void DrawUpdate()
        {
            //setup all of our rendering primitives here
            //alternatively, we could actually just do ~all of our rendering here
            //which woulve save some struct writes and whatnot
            boostDisp.circles.Clear();
            foreach (BoostZone o in towerBoosts)
                boostDisp.circles.Add(o.disp);
            foreach (BoostZone o in enemyBoosts)
                boostDisp.circles.Add(o.disp);

            mapObjectDisp.rects.Clear();
            foreach (RoundedRectangle o in navGrid.mapObjectDisplays)
                mapObjectDisp.rects.Add(o);

            towerDisp.rects.Clear();
            foreach (Tower t in towers)
            {
                towerDisp.rects.Add(t.rect);
            }

            foreach (Tower t in buildingTowers)
            {
                towerDisp.rects.Add(t.rect);
            }

            enemyDisp.circles.Clear();
            foreach (Enemy e in enemies)
                enemyDisp.circles.Add(e.getDisplay());

            bulletDisp.rects.Clear();
            foreach (Bullet b in bullets)
            {
                bulletDisp.rects.Add(b.disp);
            }
        }

        public void Dispose()
        {
            em.removePostProc(DrawUpdate);
            navGrid.Dispose();
        }
    }
}
