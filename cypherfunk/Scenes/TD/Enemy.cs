﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharpDX;
using assetmanager;

namespace cypherfunk.TD
{
    enum EnemyAffixes
    {
        AFFIX_START = -1, //start of spawnable affixes, -1 so the first affix is at 0
        SWARMER,
        FAST,
        CANTSTOP, //wont stop
        SHIELDED,
        PLATED,
        REGEN,
        FLYING,
        BUS,
        BOSS,
        SHADE,
        ZOOM,
        SPAWNER,
        AFFIX_END, //end of spawnable affixes
        SPAWNED,
        BUS_RIDER,
    }

    enum DamageSource
    {
        NORMAL,
        DOT
    }

    class Enemy
    {
        #region Attribute Calc
        public static float GetHealth(IList<EnemyAffixes> type, float diff)
        {
            return GetBaseHealth(diff) * GetHealthMultiplier(type);
        }

        static float GetBaseHealth(float diff)
        {
            return 100 + 100 * diff;
        }

        static float GetHealthMultiplier(IList<EnemyAffixes> type)
        {
            float toRet = 1f;
            if (type.Contains(EnemyAffixes.SWARMER))
                toRet *= 0.35f;

            if (type.Contains(EnemyAffixes.BUS_RIDER))
                toRet *= 0.20f;

            if (type.Contains(EnemyAffixes.SPAWNED))
                toRet *= 0.15f;

            if (type.Contains(EnemyAffixes.FAST))
                toRet *= 0.55f;

            if (type.Contains(EnemyAffixes.SHIELDED))
                toRet *= 0.60f;

            if (type.Contains(EnemyAffixes.PLATED))
                toRet *= 0.40f;

            if (type.Contains(EnemyAffixes.FLYING))
                toRet *= 0.35f;

            if (type.Contains(EnemyAffixes.BOSS))
                toRet *= 2.00f;

            if (type.Contains(EnemyAffixes.SHADE))
                toRet *= 0.70f;

            if (type.Contains(EnemyAffixes.ZOOM))
                toRet *= 0.80f;

            if (type.Contains(EnemyAffixes.REGEN))
                toRet *= 0.70f;

            if (type.Contains(EnemyAffixes.BUS))
                toRet *= 0.40f;

            if (type.Contains(EnemyAffixes.SPAWNER))
                toRet *= 0.60f;

            return toRet;
        }

        static float GetRadius(IList<EnemyAffixes> type)
        {
            float toRet = 0.375f;
            if (type.Contains(EnemyAffixes.SWARMER) || type.Contains(EnemyAffixes.BUS_RIDER) || type.Contains(EnemyAffixes.SPAWNED))
                toRet *= 2 / 3f;

            if (type.Contains(EnemyAffixes.BOSS))
                toRet *= 4 / 3f;

            return toRet;
        }

        public static float GetSpeed(IList<EnemyAffixes> type, float diff)
        {
            return GetBaseSpeed(diff) * GetSpeedMultiplier(type);
        }

        static float GetBaseSpeed(float diff)
        {
            return 40f;
        }

        static float GetSpeedMultiplier(IList<EnemyAffixes> type)
        {
            float toRet = 1f;
            if (type.Contains(EnemyAffixes.FAST))
                toRet *= 2.0f;

            if (type.Contains(EnemyAffixes.SPAWNED))
                toRet *= 1.1f;

            if (type.Contains(EnemyAffixes.BOSS))
                toRet *= 0.65f;

            if (type.Contains(EnemyAffixes.ZOOM))
                toRet *= 0.75f;

            if (type.Contains(EnemyAffixes.SPAWNER))
                toRet *= 0.75f;

            if (type.Contains(EnemyAffixes.FLYING))
                toRet *= 0.75f;

            return toRet;
        }

        public static int GetSpawnCount(IList<EnemyAffixes> type)
        {
            if (type.Contains(EnemyAffixes.SWARMER))
                return 4;
            return 1;
        }

        static int GetShieldedAmount(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.SHIELDED))
                return 2 + (int)(diff * 2);

            return 0;
        }

        static float GetPlatedAmount(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.PLATED))
                return 2 + diff * 2;

            return 0;
        }

        static float GetRegen(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.REGEN))
                return 2 + diff * 5;

            return 0;
        }

        static float GetZoomFactor(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.ZOOM))
                return 4;

            return 0;
        }

        public static float GetSpawnTime(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.BOSS))
                return 0.75f;

            return 0.2f;
        }

        static float GetShadeLength(IList<EnemyAffixes> type, float diff)
        {
            return 5;
        }

        static float GetSpawnCD(IList<EnemyAffixes> type, float diff)
        {
            return 2.5f;
        }

        static int GetBusAmount(IList<EnemyAffixes> type, float diff)
        {
            return 4;
        }

        static int GetNumLives(IList<EnemyAffixes> type, float diff)
        {
            if (type.Contains(EnemyAffixes.BUS))
                return 1 + GetBusAmount(type, diff);

            return 1;
        }

        public static string GetDisplayType(IList<EnemyAffixes> type)
        {
            if (type.Count == 0)
                return "Basic";
            if (type.Count == 1)
                return type[0].ToString();

            string toRet = type[0].ToString();
            for (int i = 1; i < type.Count; i++)
                toRet += " " + type[i].ToString();

            return toRet;
        }
        #endregion

        /// <summary>
        /// Creates an enemy based on the current difficulty and affix list provided.
        /// Affix list can be null, will spawn a pather enemy.
        /// Should only spawn pathers during build phase.
        /// </summary>
        public static void CreateEnemy(IList<EnemyAffixes> type, Map map, Vector2 pos, float diff, int goalNode)
        {
            if (type == null)
            {//if we have no type, spawn a pather enemy
                map.AddEnemy(new Enemy(map, pos, null, 0, goalNode));
                return;
            }

            map.AddEnemy(new Enemy(map, pos, type, diff, goalNode));
        }

        public readonly float maxHealth;
        private float _health;
        private float myHealth
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;

                if (healthChanged != null)
                    healthChanged(this);
            }
        }
        public float health
        {
            get
            {
                return myHealth;
            }
        }

        public void DealDamage(float damage, DamageSource source)
        {
            switch (source)
            {
                case DamageSource.NORMAL:
                    if (damage > plating)
                    {
                        if (shields > 0)
                        {
                            shields--;
                            return;
                        }

                        myHealth -= (damage - plating);
                    }
                    break;
                case DamageSource.DOT:
                    myHealth -= damage;
                    break;
                default:
                    throw new InvalidOperationException("Missing damage source case in DealDamage");
            }
        }

        public bool isDead = false;
        public bool isInvuln = false;

        int curGoalNode = 1;
        float distTravelledToNode = 0;

        float diff;
        float baseSpeed;

        int numLives = 1;

        bool affectedByCold = true;
        public ColdEffect coldEffect;
        public PoisonEffect poisonEffect;
        bool affectedByStun = true;
        public StunEffect stunEffect;

        #region Affix Vars
        float plating;
        int shields;
        float regen;
        bool isFlying;
        float zoomFactor;

        //spawner vars
        bool spawns = false;
        IList<EnemyAffixes> spawnType;
        float spawnTimer = 0;
        readonly float spawnCD;

        //bus vars
        bool bus = false;
        IList<EnemyAffixes> busType;
        int busAmount;

        //shade vars
        bool shades;
        float shadeLength;
        float shadeTimer;
        #endregion

        public bool[] telesTaken;

        //position in the world
        public Vector2 position;
        public float radius;

        public event Action<Enemy> healthChanged;

        Map map;
        IList<EnemyAffixes> affixes;

        //display stuff
        Color borderColor;
        Color centerColor;
        float borderSize;
        float animTimer = 0;

        protected Enemy(Map map, Vector2 pos, IList<EnemyAffixes> affixes, float diff, int goalNode)
        {
            this.map = map;
            this.affixes = affixes;
            this.diff = diff;
            this.curGoalNode = goalNode;
            position = pos;

            telesTaken = new bool[map.NumTeles];
            for (int x = 0; x < telesTaken.Length; x++)
                telesTaken[x] = false;

            if (affixes != null)
            {
                myHealth = GetHealth(affixes, diff);
                baseSpeed = GetSpeed(affixes, diff);
                radius = GetRadius(affixes) * map.gridScale;

                affectedByCold = !affixes.Contains(EnemyAffixes.CANTSTOP);
                affectedByStun = !affixes.Contains(EnemyAffixes.CANTSTOP);

                shields = GetShieldedAmount(affixes, diff);
                plating = GetPlatedAmount(affixes, diff);
                regen = GetRegen(affixes, diff);

                spawns = affixes.Contains(EnemyAffixes.SPAWNER);
                if (spawns)
                {
                    spawnCD = GetSpawnCD(affixes, diff);
                    spawnTimer = -spawnCD;

                    spawnType = new List<EnemyAffixes>();
                    for (int i = 0; i < affixes.Count; i++)
                    {
                        if (affixes[i] != EnemyAffixes.SPAWNER && affixes[i] != EnemyAffixes.BUS)
                            spawnType.Add(affixes[i]);
                    }
                    spawnType.Add(EnemyAffixes.SPAWNED);
                }

                bus = affixes.Contains(EnemyAffixes.BUS);
                if (bus)
                {
                    busAmount = GetBusAmount(affixes, diff);

                    busType = new List<EnemyAffixes>();
                    for (int i = 0; i < affixes.Count; i++)
                    {
                        if (affixes[i] != EnemyAffixes.BUS && affixes[i] != EnemyAffixes.SPAWNER)
                            busType.Add(affixes[i]);
                    }
                    busType.Add(EnemyAffixes.BUS_RIDER);
                }

                isFlying = affixes.Contains(EnemyAffixes.FLYING);

                shades = affixes.Contains(EnemyAffixes.SHADE);
                if (shades)
                {
                    shadeLength = GetShadeLength(affixes, diff);
                    shadeTimer = -shadeLength;
                }

                zoomFactor = GetZoomFactor(affixes, diff);

                numLives = GetNumLives(affixes, diff);

                //display stuff
                centerColor = Color.Black;
                borderColor = Color.Red;
                borderSize = radius * 0.2f;

                if (affixes.Contains(EnemyAffixes.PLATED))
                {
                    borderColor = Color.SlateGray;
                    borderSize = radius * 0.4f;
                }

                if (affixes.Contains(EnemyAffixes.FLYING))
                {
                    centerColor = Color.Yellow;
                }

                animTimer = new Random().NextFloat(0, 10);
            }
            else
            { //pather enemy
                myHealth = 1f;
                baseSpeed = 150f;
                radius = map.gridScale / 2f;

                centerColor = new Color(255, 0, 0, 100);
                borderColor = centerColor;
                borderSize = 0;
            }

            maxHealth = myHealth;

            updateGoalLoc();
        }

        public void UpdateEffects(float dt)
        {
            coldEffect.Update(dt, this);
            poisonEffect.Update(dt, this);
            stunEffect.Update(dt, this);
        }

        public EnemyRender getDisplay()
        {
            Color center = centerColor;
            Color outer = borderColor;

            if (isInvuln)
            {
                center.A = 100;
                outer.A = 100;
            }

            return new EnemyRender(
                    new Vector3(position, 0),
                    radius,
                    center,
                    outer,
                    borderSize,
                    shields > 0,
                    animTimer
                    );
        }

        //returns true if we've reached the end of the maze
        public bool Update(float dt)
        {
            myHealth = Math.Min(myHealth + regen * dt, maxHealth);
            animTimer += dt;

            if (shades)
            {
                shadeTimer += dt;
                isInvuln = shadeTimer > 0;

                if (shadeTimer > shadeLength)
                    shadeTimer = -shadeLength;
            }

            baseSpeed += dt * zoomFactor;

            bool toRet = UpdatePos(dt);

            if (spawns)
            {
                spawnTimer += dt;
                if (spawnTimer > 0)
                {
                    spawnTimer = -spawnCD;
                    CreateEnemy(spawnType, map, position, diff, curGoalNode);
                }
            }
            return toRet;
        }

        /// <summary>
        /// Teleport to the given teleporter exit!
        /// </summary>
        public void Teleport(GridLoc loc, int width, int height)
        {
            position = new Vector2((loc.x + width / 2f) * map.gridScale, (loc.y + height / 2f) * map.gridScale);

            updateGoalLoc();
        }
        
        private void updateGoalLoc()
        {
            GridLoc goalLoc;
            if (isFlying)
            {
                goalLoc = map.getNodePos(curGoalNode);
            }
            else
            {
                GridLoc myLoc = new GridLoc((int)(position.X / map.gridScale), (int)(position.Y / map.gridScale));
                goalLoc = map.getNaviGoal(curGoalNode, myLoc);

                if (goalLoc.x < 0 || goalLoc.y < 0)
                {
                    //if the goal loc is invalid, keep our current path, unless we're at our current goal
                    if (position != goalPos)
                        return;

                    goalLoc = map.getNodePos(curGoalNode);
                }
            }

            goalPos = new Vector2(goalLoc.x + 0.5f, goalLoc.y + 0.5f) * map.gridScale;
        }

        Vector2 goalPos;
        bool UpdatePos(float dt)
        {
            if (affectedByStun && stunEffect.time > 0)
                return false;
            
            float speed = baseSpeed;
            if (affectedByCold && coldEffect.time > 0)
                speed *= coldEffect.strength;

            foreach (EnemyBoostZone z in map.enemyBoosts)
            {
                if (z.touching(this))
                {
                    speed *= z.speedBoost;
                }
            }
            
            Vector2 d = goalPos - position;
            float dist = d.Length();

            //if we're not near or inside of our goal cell, update our goal position
            if (dist > map.gridScale * 2)
            {
                updateGoalLoc();
                d = goalPos - position;
                dist = d.Length();
            }

            if (dist < speed * dt)
            {//we reached our goal position, find a new one
                position = goalPos;
                distTravelledToNode += dist;
                updateGoalLoc();

                //calc remaining time we didn't use to reach goal
                float r = dt - dist / speed;

                //if we're at the final obj...
                if (goalPos == position)
                {
                    curGoalNode++;
                    distTravelledToNode = 0;
                    if (curGoalNode == map.NumTravelNodes)
                        return true;
                }

                //update pos again with remaining time
                return UpdatePos(r);
            }
            else
            {
                distTravelledToNode += speed * dt;
                position = position + d / dist * speed * dt;
            }

            return false;
        }

        internal float GetEstDistToEnd()
        {
            return (map.NumTravelNodes - curGoalNode - 1) * 10000 - distTravelledToNode;
        }

        public void Died()
        {
            isDead = true;

            if (bus)
            {
                GridLoc myLoc = new GridLoc((int)(position.X / map.gridScale), (int)(position.Y / map.gridScale));
                Vector2 cent = new Vector2(myLoc.x + 0.5f, myLoc.y + 0.5f) * map.gridScale;
                Vector2 offset = new Vector2(map.gridScale * 0.25f, 0);

                for (int i = 0; i < busAmount; i++)
                {

                    CreateEnemy(busType, map, 
                        cent + Vector2.Transform(offset, Quaternion.RotationAxis(Vector3.UnitZ, (float)(i * (Math.PI * 2) / busAmount))), 
                        diff, curGoalNode);
                }
            }
        }

        public int ReachedEnd()
        {
            isDead = true;

            return numLives;
        }
    }
}
