﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk.TD
{

    struct StunEffect : EnemyEffect
    {
        public float time;
        public float damage;

        public StunEffect(float time, float damage)
        {
            this.time = time;
            this.damage = damage;
        }

        public void Apply(Enemy e)
        {
            e.DealDamage(damage, DamageSource.NORMAL);

            e.stunEffect.time = Math.Max(time, e.stunEffect.time);
        }

        public void Update(float dt, Enemy e)
        {
            time -= dt;
        }
    }
}
