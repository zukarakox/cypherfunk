﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk.TD
{
    struct PoisonEffect : EnemyEffect
    {
        public float dps;
        public float time;

        public PoisonEffect(float dps, float time)
        {
            this.time = time;
            this.dps = dps;
        }

        public void Apply(Enemy e)
        {
            PoisonEffect c = e.poisonEffect;
            if (c.time >= 0)
            {
                if (c.dps > dps)
                    return;
            }

            c.time = time;
            c.dps = dps;
            e.poisonEffect = c;
        }

        public void Update(float dt, Enemy e)
        {
            time -= dt;

            if (time > 0)
            {
                e.DealDamage(dps * dt, DamageSource.DOT);
            }
        }
    }
}
