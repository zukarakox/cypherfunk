﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk.TD
{
    interface EnemyEffect
    {
        /// <summary>
        /// Called during the initial application of the effect.
        /// </summary>
        void Apply(Enemy e);

        /// <summary>
        /// Called every frame.
        /// </summary>
        void Update(float dt, Enemy e);
    }
}
