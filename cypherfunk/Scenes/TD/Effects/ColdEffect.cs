﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cypherfunk.TD
{

    struct ColdEffect : EnemyEffect
    {
        public float strength;
        public float time;
        public float damage;

        public ColdEffect(float strength, float time, float damage)
        {
            this.strength = strength;
            this.time = time;
            this.damage = damage;
        }

        public void Apply(Enemy e)
        {
            e.DealDamage(damage, DamageSource.NORMAL);

            ColdEffect c = e.coldEffect;
            if (c.time >= 0)
            {
                if (c.strength < strength)
                    return;
            }

            c.time = time;
            c.strength = strength;
            e.coldEffect = c;
        }

        public void Update(float dt, Enemy e)
        {
            time -= dt;
        }
    }
}
