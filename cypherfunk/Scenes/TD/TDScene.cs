﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using assetmanager;
using SharpDX;
using cypherfunk.TD;
using log;

namespace cypherfunk
{
    class TDScene : Scene
    {
        public OrthoCamera cam;
        GameStage stage;
        EventManager em;
        public Map currentLevel;

        public TDScene(GameStage stage)
        {
            this.stage = stage;
            
            cam = new OrthoCamera(Vector2.Zero, 1920f, 1080f);
        }

        #region Preload
        public HashSet<Asset> getPreloadAssetList()
        {
            return new HashSet<Asset>()
            {
                Asset.B_FONT,
                Asset.B_COLOR,
                Asset.TEX_DUCK,
                Asset.SH_FONT_SDF,
                Asset.F_CALLI_SDF_128,
                Asset.SH_POS_TEX,
                Asset.VB_QUAD_POS_TEX_UNIT
            };
        }

        FontRenderer loadFont;
        float loadTimer = 0;
        public void Preload(EventManager pre_em)
        {
            loadFont = new FontRenderer(stage, pre_em, 0, stage.Assets.getAsset(FontAssets.CALLI_SDF_128));
            loadFont.pos = new Vector2(10, stage.settings.resHeight - 10);
            loadFont.scale = stage.settings.resHeight / 8;
            loadFont.text = "Loading";
            loadFont.anchor = FontAnchor.BOTTOM_LEFT;
        }

        public float loadTime()
        {
            return 0f;
        }

        public void LoadUpdate(float dt)
        {
            loadTimer += dt;

            if (loadTimer > 4)
                loadTimer = 0;

            if (loadTimer > 3)
                loadFont.text = "Loading...";
            else if (loadTimer > 2)
                loadFont.text = "Loading..";
            else if (loadTimer > 1)
                loadFont.text = "Loading.";
            else
                loadFont.text = "Loading";
        }
        #endregion

        public void LoadEnd()
        {
            loadFont.Dispose();
            loadFont = null;

            stage.ConstrainMouseToWindow();
        }

        public HashSet<Asset> getAssetList()
        {
            return new HashSet<Asset>()
            {
                Asset.SH_ROUNDED_RECTANGLE_2D,
                Asset.B_ROUNDED_RECT,
                Asset.SH_POS_NORM_TEX,
                Asset.VB_CIRLCE_POS_TEX_NORM_UNIT,
                Asset.B_GRADIENT_CIRCLE,
                Asset.B_QUAD_INDEX,
                Asset.B_GRADIENT_CIRCLE_DATA,
                Asset.SH_GRADIENT_CIRCLE,
                Asset.SH_DASHED_CIRCLE,
                Asset.SH_BORDERED_CIRCLE,
                Asset.SH_LINE_COLORED,
                Asset.B_LINE_COLORED_SEGMENT,
                Asset.B_LINE_COLORED
            };
        }

        public void Load(EventManager load_em)
        {
            this.em = load_em;

            currentLevel = new Map(stage, load_em, this);
        }

        public void Update(float dt)
        {
            currentLevel.Update(dt);
        }

        public ICamera getCamera()
        {
            return cam;
        }

        public bool draw3D()
        {
            return true;
        }

        public void Dispose()
        {
            stage.StopConstrainingMouseToWindow();
            currentLevel.Dispose();
        }
    }
}
