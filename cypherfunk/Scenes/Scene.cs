﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using assetmanager;

namespace cypherfunk
{
    enum Scenes
    {
        TEST_SCENE,
        MAIN_MENU,
        TD_SCENE
    }

    interface Scene
    {
        void Update(float dt);

        /// <summary>
        /// Called once around Load time.
        /// Scene must use the camera returned by this method for the entirety of the scene.
        /// </summary>
        ICamera getCamera();

        /// <summary>
        /// Should return the list of assets this scene is expecting to load.
        /// If an asset isn't added to this list, but is referenced during load,
        /// the asset will be loaded there.
        /// 
        /// Generally better to include everything possible in this list, because the AssetManager will
        /// dispose of potentially unused assets before the load begins.
        /// </summary>
        HashSet<Asset> getAssetList();
        
        /// <summary>
        /// Should return the list of assets this scene wants during load time.
        /// </summary>
        HashSet<Asset> getPreloadAssetList();

        /// <summary>
        /// Minimum time in seconds to spend on loading. (Useful for splash screens)
        /// </summary>
        /// <returns></returns>
        float loadTime();

        /// <summary>
        /// Initialize preload assets here.
        /// </summary>
        void Preload(EventManager em);

        /// <summary>
        /// Update called during load
        /// </summary>
        void LoadUpdate(float dt);

        /// <summary>
        /// Called when load ends. Good place to clean up variables used during load.
        /// </summary>
        void LoadEnd();

        /// <summary>
        /// Initialize scene assets here. All calls to use an asset must be performed here, or returned from getAssetList, before they can be used anywhere else.
        /// Generally the good place to hook UI events, too.
        /// </summary>
        void Load(EventManager em);

        /// <summary>
        /// Whether this scene should draw 3D at all, or if it just needs the 2D rendering step.
        /// Is checked every frame.
        /// </summary>
        /// <returns></returns>
        bool draw3D();

        void Dispose();
    }
}
