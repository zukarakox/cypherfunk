﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX;
using deck;
using log;

using assetmanager;

namespace cypherfunk
{
    class TestPlayer
    {
        GameStage stage;
        EventManager em;

        FPVCamera cam;
        TexturedQuad_2D playerSprite;

        bool isForwardDown = false;
        bool isBackDown = false;
        bool isLeftDown = false;
        bool isRightDown = false;

        public TestPlayer(GameStage stage, EventManager em, FPVCamera cam)
        {
            this.stage = stage;
            this.em = em;
            this.cam = cam;

            playerSprite = new TexturedQuad_2D(stage, em, -1024, stage.Assets.getAsset(TextureAssets.DUCK));
            playerSprite.position = new Vector2(400, 400);
            playerSprite.scale = new Vector2(100, 100);

            em.addUpdateListener(0, onUpdate);
            em.addEventHandler((int)InterfacePriority.MEDIUM, onAction);
            em.addEventHandler((int)InterfacePriority.HIGHEST, onPointerEvent);
        }

        bool onPointerEvent(PointerEventArgs args)
        {
            if (args.type == PointerEventType.AIM)
            {
                cam.yaw -= args.aimDeltaX;
                cam.pitch -= args.aimDeltaY;
                return true;
            }
            return false;
        }

        bool onAction(ActionEventArgs args)
        {
            /*
            if (args.action == ActionType.MOVE_FORWARD)
            {
                isForwardDown = args.buttonDown;
                return true;
            }
            if (args.action == ActionType.MOVE_BACK)
            {
                isBackDown = args.buttonDown;
                return true;
            }
            if (args.action == ActionType.MOVE_LEFT)
            {
                isLeftDown = args.buttonDown;
                return true;
            }
            if (args.action == ActionType.MOVE_RIGHT)
            {
                isRightDown = args.buttonDown;
                return true;
            }
            if (args.action == ActionType.ENTER_FPV && args.buttonDown)
            {
                stage.EnterFPVMode();
                return true;
            }
            if (args.action == ActionType.LEAVE_FPV && args.buttonDown)
            {
                stage.LeaveFPVMode();
                return true;
            }
            if (args.action == ActionType.FIRE)
            {
                if (args.buttonDown)
                    playerSprite.scale = playerSprite.scale + 100;
                else
                    playerSprite.scale = playerSprite.scale - 100;
                return true;
            }
            */

            return false;
        }

        void onUpdate(float dt)
        {
            float speed = 100;
            Vector2 vel = new Vector2();
            if (isLeftDown)
                vel.X -= speed;
            if (isRightDown)
                vel.X += speed;
            if (isForwardDown)
                vel.Y -= speed;
            if (isBackDown)
                vel.Y += speed;

            playerSprite.position = playerSprite.position + vel * dt;
        }
    }

    class TestScene : Scene, ICypherFromIce, ICypherFromProxy, ICypherFromFreeside
    {
        FPVCamera cam;
        ulong playerID = 0;
        float time = 0;

        GameStage stage;
        CyphertoIceClient iceClient;
        CyphertoProxyClient proxyClient;
        CyphertoFreesideClient freesideClient;

        TexturedCircle_2D spinDuck;

        FontRenderer[] fonts;
        Textbox textbox;

        PointLight[] lights;

        DropDownMenu dropMenu;
        CycleMenu cycleMenu;
        
        public TestScene(GameStage stage)
        {
            this.stage = stage;
            cam = new FPVCamera(stage.settings.resWidth / (float)stage.settings.resHeight, Vector3.UnitY, 0, 0);
        }

        public float loadTime()
        {
            return 0f;
        }

        public HashSet<Asset> getAssetList()
        {
            return new HashSet<Asset>()
            {
                (Asset)ShaderAssets.POS_TEX,
                (Asset)ShaderAssets.POS_NORM_TEX,
                (Asset)TextureAssets.DUCK,
                (Asset)TextureAssets.WHITEPIXEL,
                (Asset)VertexBufferAssets.QUAD_POS_TEX_UNIT,
                (Asset)VertexBufferAssets.CIRCLE_POS_TEX_UNIT,
                (Asset)VertexBufferAssets.CIRCLE_POS_TEX_NORM_UNIT,
                (Asset)FontAssets.CALLI_BMP_128,
                (Asset)FontAssets.CALLI_BMP_64,
                (Asset)FontAssets.CALLI_BMP_32,
                (Asset)FontAssets.CALLI_BMP_16,
                (Asset)FontAssets.CALLI_SDF_128,
                (Asset)FontAssets.CALLI_SDF_64,
                (Asset)FontAssets.CALLI_SDF_32,
                (Asset)FontAssets.CALLI_SDF_16,
                (Asset)FontAssets.SEGOEUI_BMP_128,
                (Asset)FontAssets.SEGOEUI_BMP_64,
                (Asset)FontAssets.SEGOEUI_BMP_32,
                (Asset)FontAssets.SEGOEUI_BMP_16,
                (Asset)FontAssets.SEGOEUI_SDF_128,
                (Asset)FontAssets.SEGOEUI_SDF_64,
                (Asset)FontAssets.SEGOEUI_SDF_32,
                (Asset)FontAssets.SEGOEUI_SDF_16
            };
        }

        public HashSet<Asset> getPreloadAssetList()
        {
            return new HashSet<Asset>()
            {
                (Asset)ShaderAssets.POS_TEX,
                (Asset)TextureAssets.DUCK,
                (Asset)VertexBufferAssets.QUAD_POS_TEX_UNIT,
                (Asset)BufferAssets.ROUNDED_RECT,
                (Asset)FontAssets.SEGOEUI_SDF_128,
                (Asset)ShaderAssets.ROUNDED_RECTANGLE_2D,
                (Asset)ShaderAssets.FONT_SDF,
                (Asset)BufferAssets.FONT,
                (Asset)BufferAssets.COLOR,
            };
        }

        public void Preload(EventManager em)
        {
            Texture t_duck = stage.Assets.getAsset(TextureAssets.DUCK);
            var testQuad = new TexturedQuad_2D(stage, em, 0, t_duck);
            testQuad.position = new Vector2(10, 10);
            testQuad.scale = new Vector2(1000, 1000);
        }

        public void LoadUpdate(float dt)
        {

        }

        public void LoadEnd()
        {
        }

        public bool draw3D()
        {
            return true;
        }

        public void Load(EventManager em)
        {
            var player = new TestPlayer(stage, em, cam);

            Shader s_posTex = stage.Assets.getAsset(ShaderAssets.POS_TEX);
            Texture t_duck = stage.Assets.getAsset(TextureAssets.DUCK);
            Texture t_pixel = stage.Assets.getAsset(TextureAssets.WHITEPIXEL);
            VertexBuffer vb_quad = stage.Assets.getAsset(VertexBufferAssets.QUAD_POS_TEX_UNIT);

            //mip ducks
            var testQuad = new TexturedQuad_2D(stage, em, 0, t_duck);
            testQuad.position = new Vector2(10, 10);
            testQuad.scale = new Vector2(testQuad.tex.width, testQuad.tex.height);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_duck);
            testQuad.position = new Vector2(276, 10);
            testQuad.scale = new Vector2(testQuad.tex.width / 2f, testQuad.tex.height / 2f);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_duck);
            testQuad.position = new Vector2(414, 10);
            testQuad.scale = new Vector2(testQuad.tex.width / 4f, testQuad.tex.height / 4f);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_duck);
            testQuad.position = new Vector2(488, 10);
            testQuad.scale = new Vector2(testQuad.tex.width / 8f, testQuad.tex.height / 8f);

            testQuad = new TexturedQuad_2D(stage, em, 0, stage.Assets.getAsset(TextureAssets.TEST));
            testQuad.shader = stage.Assets.getAsset(ShaderAssets.TEST);
            testQuad.position = new Vector2(10, 300);
            testQuad.scale = new Vector2(400, 50);

            //spin duck
            spinDuck = new TexturedCircle_2D(stage, em, 0, t_duck);
            spinDuck.position = new Vector2(600.5f, 200.5f);
            spinDuck.scale = new Vector2(281, 247);
            spinDuck.rot = (float)(Math.PI / 3);

            //ground duck
            var a = new TexturedCircle_MRT(stage, em, 0, t_duck);
            a.position = new Vector3(0, 0, 0);
            a.scale = new Vector2(20f, 20f);
            a.face = new Vector3(0, 1, 0);
            a.face.Normalize();

            //Lights
            MakeLights(em);

            //font tests
            loadFonts(em);

            //font scales
            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 10);
            testQuad.scale = new Vector2(10, 128);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 148);
            testQuad.scale = new Vector2(10, 64);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 222);
            testQuad.scale = new Vector2(10, 32);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 264);
            testQuad.scale = new Vector2(10, 16);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 290);
            testQuad.scale = new Vector2(10, 8);

            //sdf scales
            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 436 - 128);
            testQuad.scale = new Vector2(10, 128);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 510 - 64);
            testQuad.scale = new Vector2(10, 64);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 552 - 32);
            testQuad.scale = new Vector2(10, 32);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 578 - 16);
            testQuad.scale = new Vector2(10, 16);

            testQuad = new TexturedQuad_2D(stage, em, 0, t_pixel);
            testQuad.position = new Vector2(788, 596 - 8);
            testQuad.scale = new Vector2(10, 8);

            //ui crap
            textbox = new Textbox(stage, em, 0);
            textbox.Position = new Vector2(10, 400);
            textbox.Scale = new Vector2(500, 40);

            textbox.onEnterPressed += onTestEnter;

            var b = new BoxTextButton(stage, em, 0, "Apply");
            b.Position = new Vector2(520, 400);
            b.Scale = new Vector2(120, 40);

            b.onClick += onTestApply;

            b = new BoxTextButton(stage, em, 0, "Color Cycle");
            b.Position = new Vector2(520, 460);
            b.Scale = new Vector2(200, 40);
            onTestClick(null);

            b.onClick += onTestClick;
            
            string[] fontNames = new string[]
            {
                "CALLI 128",
                "CALLI 64",
                "CALLI 32",
                "CALLI 16",
                "SEGOE UI 128",
                "SEGOE UI 64",
                "SEGOE UI 32",
                "SEGOE UI 16"
            };

            dropMenu = new DropDownMenu(stage, em, 0, fontNames, 2);
            dropMenu.Position = new Vector2(520, 340);
            dropMenu.Scale = new Vector2(240, 40);

            dropMenu.onSelectionChange += onDropSel;

            var slider = new Slider(stage, em, 0, 10, 128, 2000);
            slider.Position = new Vector2(10, 460);
            slider.Scale = new Vector2(400, 40);

            slider.onValueChanged += onSlider;

            scroll = new ScrollBar(stage, em, 0);
            scroll.Position = new Vector2(1890, 10);
            scroll.Scale = new Vector2(20, 600);
            scroll.scrollPos = new Vector2(800, 0);
            scroll.scrollScale = new Vector2(1090, 620);

            mainFontWindow = totFontHeight();
            scroll.percentOfScreen = 1f;

            scroll.onValueChanged += onScroll;

            cycleMenu = new CycleMenu(stage, em, 0, fontNames, 2);
            cycleMenu.Position = new Vector2(10, 510);
            cycleMenu.Scale = new Vector2(400, 40);
            cycleMenu.onSelectionChange += onDropSel;
        }

        ScrollBar scroll;

        private void onScroll(float val)
        {
            float height = totFontHeight();
            for (int x = 0; x < fonts.Length; x++)
            {
                fonts[x].offset.Y = -val * (height - mainFontWindow);
            }
        }

        float mainFontWindow;
        private float totFontHeight()
        {
            float toReturn = 0;
            for (int x = 0; x < fonts.Length; x++)
            {
                toReturn += fonts[x].scale + 10;
            }

            return toReturn;
        }

        private void MakeLights(EventManager em)
        {
            Color[] colors = new Color[] { Color.Red, Color.Green, Color.Blue, Color.Plum, Color.Yellow, Color.Orange, Color.OldLace };
            Vector3 pos = new Vector3(0, 4, 5);
            Matrix rot = Matrix.RotationY((float)(Math.PI * 2 / colors.Length));

            lights = new PointLight[colors.Length];
            for (int x = 0; x < lights.Length; x++)
            {
                lights[x] = new PointLight(stage, em, pos, colors[x], 7f, 1f);
                pos = Vector3.TransformCoordinate(pos, rot);
            }
        }

        private void onSlider(float val)
        {
            float scale = val;
            Vector2 pos = new Vector2(800, 0);

            for (int x = 0; x < 10; x++)
            {
                if (x == 5)
                {
                    scale = val;
                }

                pos.Y += scale + 10;
                fonts[x].pos = pos;
                fonts[x].scale = scale;

                scale /= 2;
            }

            scroll.percentOfScreen = mainFontWindow / pos.Y;
        }

        int curFontSelection = 2;
        private void onDropSel(int sel)
        {
            if (sel == curFontSelection)
                return;

            Font bmp_font;
            Font sdf_font;

            switch (sel)
            {
                case 0: //calli 128
                    bmp_font = stage.Assets.getAsset(FontAssets.CALLI_BMP_128);
                    sdf_font = stage.Assets.getAsset(FontAssets.CALLI_SDF_128);
                    break;
                case 1: //calli 64
                    bmp_font = stage.Assets.getAsset(FontAssets.CALLI_BMP_64);
                    sdf_font = stage.Assets.getAsset(FontAssets.CALLI_SDF_64);
                    break;
                case 2: //calli 32
                    bmp_font = stage.Assets.getAsset(FontAssets.CALLI_BMP_32);
                    sdf_font = stage.Assets.getAsset(FontAssets.CALLI_SDF_32);
                    break;
                case 3: //calli 16
                    bmp_font = stage.Assets.getAsset(FontAssets.CALLI_BMP_16);
                    sdf_font = stage.Assets.getAsset(FontAssets.CALLI_SDF_16);
                    break;
                case 4: //segoe 128
                    bmp_font = stage.Assets.getAsset(FontAssets.SEGOEUI_BMP_128);
                    sdf_font = stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_128);
                    break;
                case 5: //segoe 64
                    bmp_font = stage.Assets.getAsset(FontAssets.SEGOEUI_BMP_64);
                    sdf_font = stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_64);
                    break;
                case 6: //segoe 32
                    bmp_font = stage.Assets.getAsset(FontAssets.SEGOEUI_BMP_32);
                    sdf_font = stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_32);
                    break;
                case 7: //segoe 16
                    bmp_font = stage.Assets.getAsset(FontAssets.SEGOEUI_BMP_16);
                    sdf_font = stage.Assets.getAsset(FontAssets.SEGOEUI_SDF_16);
                    break;
                default:
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Something broke in your dumb selection menu: " + sel);
                    return;
            }

            for (int x = 0; x < fonts.Length; x++)
            {
                if (x < 5)
                    fonts[x].font = bmp_font;
                else
                    fonts[x].font = sdf_font;
            }

            curFontSelection = sel;
            cycleMenu.Selection = sel;
            dropMenu.Selection = sel;
        }

        private void onTestApply(Button obj)
        {
            onTestEnter();
        }

        private void onTestEnter()
        {
            for (int x = 0; x < fonts.Length; x++)
            {
                fonts[x].text = textbox.text;
            }
        }

        Color[] fontColors = new Color[] { Color.White, Color.Red, Color.SkyBlue, Color.Green };
        int fontColorIndex = 0;
        private void onTestClick(Button obj)
        {
            for (int x= 0; x < fonts.Length; x++)
            {
                fonts[x].color = fontColors[fontColorIndex];
            }
            
            fontColorIndex = (fontColorIndex + 1) % fontColors.Length;
        }

        private void loadFonts(EventManager em)
        {
            Font bmp_font = stage.Assets.getAsset(FontAssets.CALLI_BMP_32);
            Font sdf_font = stage.Assets.getAsset(FontAssets.CALLI_SDF_32);
            fonts = new FontRenderer[10];

            Vector2 pos = new Vector2(800, 0);
            Font curFont = bmp_font;
            float scale = 128f;

            for (int x = 0; x < 10; x++)
            {
                if (x == 5)
                {
                    curFont = sdf_font;
                    scale = 128;
                }

                pos.Y += scale + 10;

                fonts[x] = new FontRenderer(stage, em, 0, curFont);
                fonts[x].text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mauris libero, placerat ut vehicula vel, vulputate eu nisi. Sed vestibulum ut velit vel pellentesque.";
                fonts[x].anchor = FontAnchor.BOTTOM_LEFT;
                fonts[x].pos = pos;
                fonts[x].scale = scale;

                scale /= 2;
            }

            float totH = totFontHeight();
            for (int x = 0; x < fonts.Length; x++)
            {
                fonts[x].boundsPos = new Vector2(800, 0);
                fonts[x].boundsScale = new Vector2(1050, totH);
            }
        }

        public void Update(float dt)
        {
            if (iceClient == null && proxyClient == null)
            {
                time += dt;
                if (time > 5)
                {
                    time = 0;
                    iceClient = new CyphertoIceClient(this, new PublicKey());
                }
            }
            if (iceClient != null)
                iceClient.Update();
            if (proxyClient != null)
                proxyClient.Update();

            spinDuck.rot += dt;

            Matrix rot = Matrix.RotationY(dt);
            for (int x = 0; x < lights.Length; x++)
            {
                lights[x].pos = Vector3.TransformCoordinate(lights[x].pos, rot);
            }
        }

        public void proxy_onConnectionChange(CyphertoProxyClient conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED:
                    Logger.WriteLine(LogType.DEBUG, "Connected to proxy!!!");
                    proxyClient.sendRequestGameServer();
                    break;
                case ConnectionStatus.DISCONNECTED:
                    Logger.WriteLine(LogType.DEBUG, "Disconnected from proxy, shoot!");
                    break;
            }
        }

        public void proxy_GameServerResponse(CyphertoProxyClient conn, bool success, string reason)
        {
            if (!success)
            {
                Logger.WriteLine(LogType.DEBUG, "Finding gameserve failed: " + reason);
            }
            else
            {
                Logger.WriteLine(LogType.DEBUG, "Found gameserver?");
            }
        }

        public void ice_LoginResponse(CyphertoIceClient conn, bool success, UInt256? symID, SymKey? symKey, ulong? playerID, string host, int? port, string reason)
        {
            if (success)
            {
                Logger.WriteLine(LogType.DEBUG, "Successful Login, connecting to proxy...");

                this.playerID = playerID.Value;
                proxyClient = new CyphertoProxyClient(this, port.Value, host, symKey.Value, symID.Value, this.playerID, this, out freesideClient);
                iceClient.Disconnect("done");
                iceClient = null;
            }
            else
            {
                Logger.WriteLine(LogType.DEBUG, "Unsuccessful Login: " + reason);
            }
        }

        public void ice_NewUserResponse(CyphertoIceClient conn, bool success, string reason)
        {
            if (success)
            {
                Logger.WriteLine(LogType.DEBUG, "Successful new user");
                iceClient.sendLoginRequest("username", "password");
            }
            else
            {
                Logger.WriteLine(LogType.DEBUG, "Unsuccessful account creation: " + reason);
            }
        }

        public void ice_onConnectionChange(CyphertoIceClient conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED:
                    Logger.WriteLine(LogType.DEBUG, "Connected to ice");
                    iceClient.sendNewUserRequest("username", "password");
                    break;
                case ConnectionStatus.DISCONNECTED:
                    Logger.WriteLine(LogType.DEBUG, "Disconnected from ice");
                    break;
            }
        }

        public void freeside_onConnectionChange(CyphertoFreesideClient conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Connection to freeside change. New Status: " + status);
            if (status == ConnectionStatus.CONNECTED)
            {
                freesideClient.sendPlayerInput("420 blazeit", 420);
            }
        }

        public void freeside_ObjPos(CyphertoFreesideClient conn, string msg, int a)
        {
            Logger.WriteLine(LogType.DEBUG, "Got message from freeisde?: " + msg + " " + a);
        }

        public ICamera getCamera()
        {
            return cam;
        }

        public void Dispose()
        {
            //TODO, probably never.
        }
    }
}
