﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using log;

namespace deck
{
    public enum ConnectionStatus { CONNECTED, DISCONNECTED }
    public enum MessageDeliveryType
    {
        UNRELIABLE = NetDeliveryMethod.Unreliable,
        UNRELIABLE_SEQUENCED = NetDeliveryMethod.UnreliableSequenced,
        RELIABLE_UNORDERED = NetDeliveryMethod.ReliableUnordered,
        RELIABLE_SEQUENCED = NetDeliveryMethod.ReliableSequenced,
        RELIABLE_ORDERED = NetDeliveryMethod.ReliableOrdered
    }

    public struct UInt256
    {
        private byte[] dat;

        public UInt256(byte[] dat)
        {
            this.dat = dat;
        }

        internal byte[] Bytes { get { return dat; } }

        public override string ToString()
        {
            string toReturn = "";
            for (int x = 0; x < dat.Length; x++)
                toReturn += dat[x] + " ";

            return toReturn;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int p = 16777619;
                int hash = (int)2166136261;

                for (int i = 0; i < dat.Length; i++)
                    hash = (hash ^ dat[i]) * p;

                hash += hash << 13;
                hash ^= hash >> 7;
                hash += hash << 3;
                hash ^= hash >> 17;
                hash += hash << 5;
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is UInt256)
            {
                var o = (UInt256)obj;
                for (int x = 0; x < dat.Length; x++)
                    if (o.dat[x] != dat[x])
                        return false;

                return true;
            }
            return false;
        }
    }

    public struct SymKey
    {
        internal static readonly int ByteLength = 64;

        private byte[] dat;

        public SymKey(byte[] dat)
        {
            if (dat.Length != ByteLength)
                throw new ArgumentException("Byte array provided to SymKey is not of SymKeylength " + dat.Length);

            this.dat = dat;
        }

        internal byte[] Bytes { get { return dat; } }
    }

    public struct PublicKey
    {
        internal static readonly int ByteLength = 64;

        private byte[] dat;

        public PublicKey(byte[] dat)
        {
            if (dat.Length != ByteLength)
                throw new ArgumentException("Byte array provided to SymKey is not of SymKeylength " + dat.Length);

            this.dat = dat;
        }

        internal byte[] Bytes { get { return dat; } }
    }

    public struct PrivateKey
    {
        internal static readonly int ByteLength = 64;

        private byte[] dat;

        public PrivateKey(byte[] dat)
        {
            if (dat.Length != ByteLength)
                throw new ArgumentException("Byte array provided to SymKey is not of SymKeylength " + dat.Length);

            this.dat = dat;
        }

        internal byte[] Bytes { get { return dat; } }
    }

    internal delegate void MessageReceive(NetConn remote, NetMsgIn msg);
    public delegate void ConnectionStatusChanged(NetConn remote, ConnectionStatus newStatus);
    internal delegate bool SecureConnectionApproval(UInt256 id, NetConn remote, NetMsgIn msg);
    internal delegate bool ConnectionApproval(NetConn remote, NetMsgIn msg);
    public delegate SymKey? GetSymKey(UInt256 id);
    internal delegate void WriteHandshake(NetMsgOut msg);

    /// <summary>
    /// Accesses cyberspace!
    /// </summary>
    public abstract class Deck
    {
        private static readonly Random rand = new Random();
        public static void GenerateSymKey(out UInt256 symID, out SymKey symKey)
        {//TODO: NOT GOOD RANDOM. MAKE THESE ACTUALLY GOOD CRYPTO RANDOM
            symID = new UInt256(new byte[32]);
            rand.NextBytes(symID.Bytes);

            symKey = new SymKey(new byte[SymKey.ByteLength]);
            rand.NextBytes(symKey.Bytes);
        }

        public static byte[] hashPassword(string password)
        {//TODO: NOT GOOD HASH. MAKE GOOD CRYPTO HASH
            byte[] toReturn = new byte[32];
            int hash = password.GetHashCode();
            int t = 1;
            for (int x = 0; x < toReturn.Length; x++)
            {
                toReturn[x] = (byte)(hash * t * t);
                t += toReturn[x];
            }

            return toReturn;
        }

        /// <summary>
        /// Creates a connection to service without any form of encryption or authentication. Make sure this is only used internally on protected systems.
        /// </summary>
        internal static ClientDeck CreateInternalClient(string serviceID, string host, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
        {
            return new InsecureClientDeck(serviceID, host, port, onReceive, onChange);
        }

        /// <summary>
        /// Creates a connection to service without any form of encryption or authentication. Make sure this is only used internally on protected systems.
        /// </summary>
        internal static ClientDeck CreateInternalClient(string serviceID, string host, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, WriteHandshake writeHandshake)
        {
            return new InsecureClientDeck(serviceID, host, port, onReceive, onChange, writeHandshake);
        }

        /// <summary>
        /// Creates a connection to a service with authentication and encryption.
        /// Uses TLS to verify connection, using provided PublicKey. (Okay it actually doesn't do shit right now, should probably fix that.)
        /// </summary>
        internal static ClientDeck CreateSecureClient(string serviceID, string host, int port, PublicKey pubKey, MessageReceive onReceive, ConnectionStatusChanged onChange)
        {
            return new SecureClientDeck(serviceID, host, port, onReceive, onChange);
        }

        /// <summary>
        /// Creates a secure connection to a server with a prenegotiated symkey and id.
        /// WriteHandshake should add in any information you need to send the server so that it will accept your connection.
        /// Just needs to be ~something~ so that the symkey can be verified as correct.
        /// </summary>
        /// <returns></returns>
        internal static ClientDeck CreateSecureClient(string serviceID, string host, int port, SymKey sym, UInt256 id, MessageReceive onReceive, ConnectionStatusChanged onChange, WriteHandshake writeHandshake)
        {
            return new EncryptClientDeck(serviceID, host, port, sym, id, onReceive, onChange, writeHandshake);
        }

        /// <summary>
        /// Creates a service host without any form of encryption or authentication. Make sure this is only done on ports NOT available to the public.
        /// </summary>
        internal static Deck CreateInternalServer(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
        {
            return new InsecureServerDeck(serviceID, port, onReceive, onChange);
        }

        /// <summary>
        /// Creates a service host without any form of encryption or authentication. Make sure this is only done on ports NOT available to the public.
        /// </summary>
        internal static Deck CreateInternalServer(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, ConnectionApproval onConnection)
        {
            return new InsecureServerDeck(serviceID, port, onReceive, onChange, onConnection);
        }

        /// <summary>
        /// Creates a service host that authenticates to clients and encrypts all messages. Must handle client authentication after connection.
        /// Proves identity using provided private key. (Doesn't actually do anything right now)
        /// </summary>
        internal static Deck CreateSecureServer(string serviceID, int port, PrivateKey privKey, MessageReceive onReceive, ConnectionStatusChanged onChange)
        {
            return new SecureServerDeck(serviceID, port, onReceive, onChange);
        }

        /// <summary>
        /// Creates a service host that both authenticates to clients, and authenticates clients. All traffic is encrypted.
        /// The GetSymKey method is called when a new connection has started. It expect to recieve a symkey based on a ulong.
        /// This symkey should have been decided beforehand, where the ulong is just an indentifier so the server knows which symkey.
        /// Everything after that initial ulong should be encrypted using the symkey.
        /// 
        /// Will only work with EncryptClientDecks.
        /// </summary>
        internal static Deck CreateSecureServer(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, SecureConnectionApproval onConnection, GetSymKey getSymKey)
        {
            return new EncryptServerDeck(serviceID, port, onReceive, onChange, onConnection, getSymKey);
        }

        private readonly MessageReceive onReceive;
        private readonly ConnectionStatusChanged onChange;
        private readonly ConnectionApproval onApproval;
        protected internal NetPeer peer;

        private readonly NetMsgIn incMsg = new NetMsgIn();
        private readonly NetMsgOut outMsg = new NetMsgOut();

        private readonly Dictionary<NetConnection, NetConn> activeConnections = new Dictionary<NetConnection, NetConn>();

        internal Deck(MessageReceive onReceive, ConnectionStatusChanged onChange)
        {
            this.onReceive = onReceive;
            this.onChange = onChange;
        }

        internal Deck(MessageReceive onReceive, ConnectionStatusChanged onChange, ConnectionApproval onApproval)
        {
            this.onReceive = onReceive;
            this.onChange = onChange;
            this.onApproval = onApproval;
        }

        internal NetMsgOut StartMessage(int size)
        {
            outMsg.wrap(peer.CreateMessage(size));
            return outMsg;
        }

        internal virtual void EncryptMessage(NetConn conn, NetMsgOut msg)
        {//do nothing! here to be overriden.

        }

        internal void SendMessage(NetConn conn, MessageDeliveryType type, int channel)
        {
            if (outMsg.msg == null)
                throw new NullReferenceException("can't send a message that wasn't started");

            EncryptMessage(conn, outMsg);

            peer.SendMessage(outMsg.msg, conn.remote, (NetDeliveryMethod)type, channel);
            outMsg.msg = null;
        }

        /// <summary>
        /// Internal send message. Does NOT encrypt before sending.
        /// </summary>
        internal void SendMessage(NetConnection conn, MessageDeliveryType type, int channel)
        {
            if (outMsg.msg == null)
                throw new NullReferenceException("can't send a message that wasn't started");

            peer.SendMessage(outMsg.msg, conn, (NetDeliveryMethod)type, channel);
            outMsg.msg = null;
        }

        internal void Update()
        {
            NetIncomingMessage msg;
            while ((msg = peer.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        incMsg.wrap(msg);
                        _onReceive(activeConnections[msg.SenderConnection], incMsg);
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        string reason = msg.ReadString();

                        switch (status)
                        {
                            case NetConnectionStatus.Connected:
                                {
                                    if (activeConnections.ContainsKey(msg.SenderConnection))
                                    {//we already had the connection from the connection approval.
                                        _onChange(activeConnections[msg.SenderConnection], ConnectionStatus.CONNECTED);
                                    }
                                    else
                                    {
                                        NetConn conn = new NetConn(msg.SenderConnection);
                                        activeConnections.Add(msg.SenderConnection, conn);
                                        _onChange(conn, ConnectionStatus.CONNECTED);
                                    }
                                    break;
                                }
                            case NetConnectionStatus.Disconnected:
                                {
                                    NetConn sender;
                                    if (msg.SenderConnection != null && activeConnections.TryGetValue(msg.SenderConnection, out sender))
                                    {
                                        _onChange(sender, ConnectionStatus.DISCONNECTED);
                                        activeConnections.Remove(msg.SenderConnection);
                                    }
                                    else
                                    {//well, something disconnected vOv
                                        _onChange(null, ConnectionStatus.DISCONNECTED);
                                    }
                                    break;
                                }
                            case NetConnectionStatus.RespondedConnect:
                                break; //can pretty much ignore this, we'll get a Connected status in a bit.
                            case NetConnectionStatus.Disconnecting:
                            case NetConnectionStatus.InitiatedConnect:
                            case NetConnectionStatus.None:
                            case NetConnectionStatus.ReceivedInitiation:
                            case NetConnectionStatus.RespondedAwaitingApproval:
                            default:
                                Logger.WriteLine(LogType.POSSIBLE_ERROR, "Unhandled status change: " + status.ToString() + " Reason: " + reason);
                                break;

                        }
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        {
                            NetConn conn = new NetConn(msg.SenderConnection);
                            incMsg.wrap(msg);
                            if (_onApproval(conn, incMsg))
                            {
                                activeConnections.Add(msg.SenderConnection, conn);
                                msg.SenderConnection.Approve();
                            }
                            else
                            {
                                msg.SenderConnection.Deny();
                            }

                            break;
                        }
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                        Logger.WriteLine(LogType.DEBUG, msg.ReadString());
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, msg.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        Logger.WriteLine(LogType.ERROR, msg.ReadString());
                        break;
                    case NetIncomingMessageType.ConnectionLatencyUpdated:
                    case NetIncomingMessageType.UnconnectedData:
                    case NetIncomingMessageType.DiscoveryRequest:
                    case NetIncomingMessageType.DiscoveryResponse:
                    case NetIncomingMessageType.NatIntroductionSuccess:
                    case NetIncomingMessageType.Receipt:
                    default:
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "Unhandled type: " + msg.MessageType);
                        break;
                }

                incMsg.msg = null;

                if (msg.LengthBits != msg.Position)
                    Logger.WriteLine(LogType.ERROR, "Didn't use all of the data in a message? " + msg.MessageType);
                peer.Recycle(msg);
            }
        }

        internal virtual void _onReceive(NetConn conn, NetMsgIn msg)
        {
            onReceive(conn, msg);
        }

        internal virtual void _onChange(NetConn conn, ConnectionStatus status)
        {
            onChange(conn, status);
        }

        internal virtual bool _onApproval(NetConn conn, NetMsgIn msg)
        {
            return onApproval(conn, msg);
        }

        public void close(string bye)
        {
            peer.Shutdown(bye);
        }
    }

    internal abstract class ClientDeck : Deck
    {
        private NetConn conn;
        internal ClientDeck(MessageReceive onReceive, ConnectionStatusChanged onChange)
            : base(onReceive, onChange)
        {
        }

        public void SendMessage(MessageDeliveryType type, int channel)
        {
            if (conn == null)
                throw new InvalidOperationException("sending a message without being connected!");

            SendMessage(conn, type, channel);
        }

        internal override void _onChange(NetConn conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED://hide CONNECTED status, as we actually need to do some more handshaking.
                    //send our handshake here...
                    if (this.conn != null)
                        Logger.WriteLine(LogType.POSSIBLE_ERROR, "ClientDeck in weird state. Just connected to something while we already have a connection?");

                    this.conn = conn;
                    break;
                case ConnectionStatus.DISCONNECTED:
                    this.conn = null;
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }

            base._onChange(conn, status);
        }
    }
    
    internal class InsecureClientDeck : ClientDeck
    {
        internal InsecureClientDeck(string serviceID, string host, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
            : this(serviceID, host, port, onReceive, onChange, null)
        {
        }
        
        internal InsecureClientDeck(string serviceID, string host, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, WriteHandshake handshake)
            : base(onReceive, onChange)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);

            peer = new NetClient(config);

            peer.Start();

            if (handshake != null)
            {
                NetMsgOut msgOut = StartMessage(30);
                handshake(msgOut);

                peer.Connect(host, port, msgOut.msg);
                msgOut.msg = null;
            }
            else
            {
                peer.Connect(host, port);
            }
        }
    }

    internal class EncryptClientDeck : ClientDeck
    {
        private byte[] sym;
        /// <summary>
        /// Creates a secure connection to a remote host.
        /// </summary>
        internal EncryptClientDeck(string serviceID, string host, int port, SymKey sym, UInt256 id, MessageReceive onReceive, ConnectionStatusChanged onChange, WriteHandshake handshake)
            : base(onReceive, onChange)
        {
            this.sym = sym.Bytes;
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);

            peer = new NetClient(config);

            peer.Start();

            NetMsgOut msgOut = StartMessage(30);
            msgOut.WriteUInt256(id);

            int pos = msgOut.getBytePos();
            handshake(msgOut);
            EncryptMessage(msgOut.getData(), pos, msgOut.getBytePos() - pos, sym.Bytes);

            peer.Connect(host, port, msgOut.msg);
            msgOut.msg = null;
        }

        internal override void _onReceive(NetConn conn, NetMsgIn msg)
        {
            byte[] data = msg.getData();
            DecryptMessage(data, 0, msg.getByteLen(), sym);

            base._onReceive(conn, msg);
        }

        internal override void EncryptMessage(NetConn conn, NetMsgOut msg)
        {
            byte[] data = msg.getData();
            EncryptMessage(data, 0, msg.getBytePos(), sym);
        }

        private void EncryptMessage(byte[] msg, int pos, int len, byte[] sym)
        {
            int s = 0;
            for (int x = 0; x < len; x++)
                msg[pos + x] ^= sym[(s++ % sym.Length)];
        }

        private void DecryptMessage(byte[] msg, int pos, int len, byte[] sym)
        {
            int s = 0;
            for (int x = 0; x < len; x++)
                msg[pos + x] ^= sym[(s++ % sym.Length)];
        }
    }

    internal class SecureClientDeck : ClientDeck
    {
        private bool waitingForHandshake = true;
        
        /// <summary>
        /// Creates a secure connection to a remote host.
        /// </summary>
        internal SecureClientDeck(string serviceID, string host, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
            : base(onReceive, onChange)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);

            peer = new NetClient(config);

            peer.Start();
            peer.Connect(host, port);
        }

        internal override void _onReceive(NetConn conn, NetMsgIn msg)
        {
            if (waitingForHandshake)
            {
                string str = msg.ReadString();
                if (str.Equals("Thanks man, it was really difficult!"))
                {
                    waitingForHandshake = false;
                    base._onChange(conn, ConnectionStatus.CONNECTED);
                }
                else
                {
                    close("very rude");
                }
            }
            else
            {
                byte[] data = msg.msg.Data;
                for (int x = 0; x < msg.msg.LengthBytes; x++)
                    data[x] ^= 0xAA;

                base._onReceive(conn, msg);
            }
        }

        internal override void _onChange(NetConn conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED://hide CONNECTED status, as we actually need to do some more handshaking.
                    //send our handshake here...
                    waitingForHandshake = true;
                    NetMsgOut toSend = StartMessage(30);
                    toSend.WriteString("Hey bro! Super great encryption!");
                    SendMessage(conn.remote, MessageDeliveryType.RELIABLE_ORDERED, 0);
                    break;
                case ConnectionStatus.DISCONNECTED:
                    waitingForHandshake = true;
                    base._onChange(conn, status);
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }
        }

        internal override void EncryptMessage(NetConn conn, NetMsgOut msg)
        {
            byte[] data = msg.getData();
            for (int x = 0; x < msg.getBytePos(); x++)
                data[x] ^= 0xAA;
        }
    }

    internal class InsecureServerDeck : Deck
    {
        /// <summary>
        /// Creates an insecure host.
        /// </summary>
        internal InsecureServerDeck(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
            : base(onReceive, onChange)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);
            config.Port = port;
            config.PingInterval = 5;
            config.ConnectionTimeout = 30;

            peer = new NetServer(config);
            peer.Start();
        }

        internal InsecureServerDeck(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, ConnectionApproval onConnection)
            : base(onReceive, onChange, onConnection)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);
            config.Port = port;
            config.PingInterval = 5;
            config.ConnectionTimeout = 30;
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

            peer = new NetServer(config);
            peer.Start();
        }
    }

    internal class EncryptServerDeck : Deck
    {
        private Dictionary<NetConn, SymKey> symkeys = new Dictionary<NetConn, SymKey>();
        private GetSymKey getSymKey;
        private SecureConnectionApproval onConnection;

        /// <summary>
        /// Creates a secure host with a symkey decided in advance. The symkey authenticates both the server and the client.
        /// The connection attempt should be encrypted, but should include some cleartext data to identify the sender.
        /// The GetSymKey method should read the first few bytes of the message and return the symkey.
        /// The rest of the message will then be decrypted, and get passed to the ConnectionApproval method.
        /// 
        /// This *should* actually be secure, once the Encrypt/DecryptMessage functions are actually using proper encryption.
        /// </summary>
        internal EncryptServerDeck(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange, SecureConnectionApproval onConnection, GetSymKey getSymKey)
            : base(onReceive, onChange)
        {
            this.getSymKey = getSymKey;
            this.onConnection = onConnection;

            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);
            config.Port = port;
            config.PingInterval = 5;
            config.ConnectionTimeout = 30;
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

            peer = new NetServer(config);
            peer.Start();
        }

        internal override void _onReceive(NetConn conn, NetMsgIn msg)
        {
            byte[] data = msg.getData();
            int pos = msg.getBytePos();
            int len = msg.getByteLen() - pos;
            byte[] sym = symkeys[conn].Bytes;

            DecryptMessage(data, pos, len, sym);
            base._onReceive(conn, msg);
        }

        internal override void _onChange(NetConn conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED:
                    base._onChange(conn, status);
                    break;
                case ConnectionStatus.DISCONNECTED:
                    symkeys.Remove(conn);
                    base._onChange(conn, status);
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }
        }

        internal override bool _onApproval(NetConn conn, NetMsgIn msg)
        {
            UInt256 id = msg.ReadUInt256();
            SymKey? sym = getSymKey(id);
            
            if (!sym.HasValue)
                return false;

            byte[] data = msg.getData();
            int pos = msg.getBytePos();
            int len = msg.getByteLen() - pos;
            DecryptMessage(data, pos, len, sym.Value.Bytes);

            bool toReturn = onConnection(id, conn, msg);

            if (toReturn)
            {
                symkeys.Add(conn, sym.Value);
            }

            return toReturn;
        }

        internal override void EncryptMessage(NetConn conn, NetMsgOut msg)
        {
            byte[] sym = symkeys[conn].Bytes;
            byte[] data = msg.msg.Data;
            int s = 0;

            for (int x = 0; x < msg.msg.LengthBytes; x++)
                data[x] ^= sym[(s++ % sym.Length)];
        }

        private void DecryptMessage(byte[] msg, int pos, int len, byte[] sym)
        {
            int s = 0;
            for (int x = 0; x < len; x++)
                msg[pos + x] ^= sym[(s++ % sym.Length)];
        }

    }

    internal class SecureServerDeck : Deck
    {
        private readonly HashSet<NetConn> handshaking = new HashSet<NetConn>();
        private readonly HashSet<NetConn> validConns = new HashSet<NetConn>();

        /// <summary>
        /// Creates a secure host that authenticates to clients but DOES NOT authenticate the client.
        /// Currently doesn't really auth anything.
        /// </summary>
        internal SecureServerDeck(string serviceID, int port, MessageReceive onReceive, ConnectionStatusChanged onChange)
            : base(onReceive, onChange)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(serviceID);
            config.Port = port;
            config.PingInterval = 5;
            config.ConnectionTimeout = 30;

            peer = new NetServer(config);
            peer.Start();
        }

        internal override void _onReceive(NetConn conn, NetMsgIn msg)
        {
            if (handshaking.Contains(conn))
            {
                string str = msg.ReadString();
                if (str.Equals("Hey bro! Super great encryption!"))
                {
                    handshaking.Remove(conn);
                    validConns.Add(conn);

                    NetMsgOut toSend = StartMessage(30);
                    toSend.WriteString("Thanks man, it was really difficult!");
                    SendMessage(conn.remote, MessageDeliveryType.RELIABLE_ORDERED, 0);

                    base._onChange(conn, ConnectionStatus.CONNECTED);
                }
                else
                {
                    Logger.WriteLine(LogType.POSSIBLE_ERROR, "Invalid secure handshake: " + str);
                    conn.remote.Disconnect("no sneaky!");
                }
            }
            else if (validConns.Contains(conn))
            {
                byte[] data = msg.msg.Data;
                for (int x = 0; x < msg.msg.LengthBytes; x++)
                    data[x] ^= 0xAA;

                base._onReceive(conn, msg);
            }
        }

        internal override void _onChange(NetConn conn, ConnectionStatus status)
        {
            switch (status)
            {
                case ConnectionStatus.CONNECTED://hide CONNECTED status, as we actually need to do some more handshaking.
                    handshaking.Add(conn);
                    validConns.Remove(conn);
                    break;
                case ConnectionStatus.DISCONNECTED:
                    handshaking.Remove(conn);
                    validConns.Remove(conn);
                    base._onChange(conn, status);
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }
        }

        internal override void EncryptMessage(NetConn conn, NetMsgOut msg)
        {
            byte[] data = msg.msg.Data;
            for (int x = 0; x < msg.msg.LengthBytes; x++)
                data[x] ^= 0xAA;
        }
    }
}
