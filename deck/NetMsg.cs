﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using log;

namespace deck
{
    public class NetConn
    {
        internal NetConnection remote;

        public object userData;
        internal NetConn(NetConnection remote)
        {
            this.remote = remote;
        }

        internal long UID
        {
            get
            {
                return remote.RemoteUniqueIdentifier;
            }
        }

        public void Disconnect(string msg)
        {
            remote.Disconnect(msg);
        }
    }

    internal class NetMsgIn
    {
        internal NetIncomingMessage msg;
        internal NetMsgIn()
        {
        }

        internal void wrap(NetIncomingMessage msg)
        {
            if (this.msg != null)
                Logger.WriteLine(LogType.ERROR, "Incoming message non-null. Didn't finish a message?");

            this.msg = msg;
        }

        internal MessageDeliveryType getDeliveryMethod()
        {
            return (MessageDeliveryType)msg.DeliveryMethod;
        }

        internal int getDeliveryChannel()
        {
            return msg.SequenceChannel;
        }

        internal byte[] getData()
        {
            return msg.Data;
        }

        internal int getBytePos()
        {
            return msg.PositionInBytes;
        }

        internal int getByteLen()
        {
            return msg.LengthBytes;
        }

        internal byte[] ReadRawBytes(out int offset, out int len)
        {
            offset = msg.PositionInBytes;
            len = msg.LengthBytes - offset;

            msg.Position = msg.LengthBits;
            return msg.Data;
        }

        internal long getSenderUID()
        {
            return msg.SenderConnection.RemoteUniqueIdentifier;
        }

        internal string ReadString()
        {
            return msg.ReadString();
        }

        internal ulong ReadUInt64()
        {
            return msg.ReadUInt64();
        }

        internal uint ReadUInt32()
        {
            return msg.ReadUInt32();
        }

        internal int ReadInt32()
        {
            return msg.ReadInt32();
        }

        internal UInt256 ReadUInt256()
        {
            return new UInt256(msg.ReadBytes(32));
        }

        internal SymKey ReadSymKey()
        {
            return new SymKey(msg.ReadBytes(SymKey.ByteLength));
        }

        internal ushort ReadUInt16()
        {
            return msg.ReadUInt16();
        }

        internal byte[] ReadByteArray()
        {
            int len = msg.ReadInt32();
            return msg.ReadBytes(len);
        }

        internal bool ReadBoolean()
        {
            return msg.ReadBoolean();
        }
    }

    internal class NetMsgOut
    {
        internal NetOutgoingMessage msg;
        internal NetMsgOut()
        {
        }

        internal void wrap(NetOutgoingMessage msg)
        {
            if (this.msg != null)
                Logger.WriteLine(LogType.ERROR, "Outgoing message non-null. Didn't send an allocated message?");

            this.msg = msg;
        }

        internal byte[] getData()
        {
            return msg.Data;
        }

        internal int getBytePos()
        {
            return msg.LengthBytes;
        }

        internal void WriteString(string str)
        {
            msg.Write(str);
        }

        internal void WriteUInt64(ulong l)
        {
            msg.Write(l);
        }

        internal void WriteUInt32(uint i)
        {
            msg.Write(i);
        }

        internal void WriteInt32(int i)
        {
            msg.Write(i);
        }

        internal void WriteUInt16(ushort s)
        {
            msg.Write(s);
        }

        /// <summary>
        /// This doesn't write the array length, only the bytes, so ReadBytes() won't work on the recieving end.
        /// </summary>
        internal void WriteRawBytes(byte[] b, int pos, int len)
        {
            msg.Write(b, pos, len);
        }

        internal void WriteByteArray(byte[] b)
        {
            WriteInt32(b.Length);
            msg.Write(b);
        }

        internal void WriteUInt256(UInt256 i)
        {
            msg.Write(i.Bytes);
        }

        internal void WriteSymKey(SymKey key)
        {
            msg.Write(key.Bytes);
        }

        internal void WriteBoolean(bool b)
        {
            msg.Write(b);
        }
    }
}
