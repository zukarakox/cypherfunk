﻿#Protocol file format: lines starting in # are comments and ignored.
#Blanks lines are significant and signify the next block of information
#ALL NETWORK SERVICES -- this should list all clients/hosts
ice
auth
proxy
proxymaster
freeside
freesidemaster
cypher

#CONNECTIONS -- a list of connections, in the form of [client], [server][, opts]*
#An opt of just a number is taken as the communication port
#An opt of tls/sym/local defines the communication encryption
#An opt of 'pass [service]' defines a proxy connection of [client], [server], pass [middle], where client and server connect through a previously-existing middleman
#Proxy connections can only have one hop and MUST already have a client, middle AND middle, server connection set
#An opt of a url is the connection address for the client
cypher, ice, tls, 8008, ice.cypherfunk.com
cypher, proxy, sym
ice, auth, local, 8010, auth.cypherfunk.com
ice, proxymaster, local, 8011, proxymaster.cypherfunk.com
proxy, proxymaster, local, 8012, proxymaster.cypherfunk.com
proxy, freeside, local
proxy, freesidemaster, local, 8014, freesidemaster.cypherfunk.com
freeside, freesidemaster, local, 8015, freesidemaster.cypherfunk.com
cypher, freeside, pass proxy

#Protocols -- the actual data being sent back and forth
#A line starting with c, sets the current 'connection' for messages, as [sender], [reciever]
#A line starting with m, starts a new message, until the next blank line.
#Each line after starting a message, til the next blank line, is a message part.
#You can have an empty message with no parts.
#Each part is either a type or an 'if' statement
#if syntax is 'if BOOL_VAR {' EXACTLY
#if ends on a line of '}' or '} else {'
#BOOL_VAR must be a previously-defined boolean message part
#if always checks for true
#valid types can be found in the T4 source, in the 'MessageType' enum. Actual syntax must be checked in some later switch case
#------------------------------------------------------------------CYPHER TO ICE---------------------------------------------------------
c, cypher, ice
m, LoginRequest, RELIABLE_ORDERED 0
string username
string password

m, NewUserRequest, RELIABLE_ORDERED 0
string username
string password

#-------------------------------------------------------------------ICE TO CYPHER---------------------------------------------------------
c, ice, cypher
m, LoginResponse, RELIABLE_ORDERED 0
bool success
if success {
	uint256 symID
	symkey symKey
	ulong playerID
	string host
	int port
} else {
	string reason
}

m, NewUserResponse, RELIABLE_ORDERED 0
bool success
if success { 
} else {
	string reason
}

#------------------------------------------------------------------ICE TO AUTH---------------------------------------------------------
c, ice, auth
m, LoginRequest, RELIABLE_UNORDERED 0
uint reqID
string username
byte[] password

m, NewUserRequest, RELIABLE_UNORDERED 0
uint reqID
string username
byte[] password

#-------------------------------------------------------------------AUTH TO ICE---------------------------------------------------------
c, auth, ice
m, LoginResponse, RELIABLE_UNORDERED 0
uint reqID
bool success
if success {
	ulong playerID
} else {
	string reason
}

m, NewUserResponse, RELIABLE_UNORDERED 0
uint reqID
bool success
if success { 
} else {
	string reason
}

#------------------------------------------------------------------ICE TO PROXYMASTER---------------------------------------------------------
c, ice, proxymaster
m, FindProxy, RELIABLE_UNORDERED 0
uint reqID
uint256 symID
symkey symKey
ulong playerID

#------------------------------------------------------------------PROXYMASTER TO ICE---------------------------------------------------------
c, proxymaster, ice
m, FindProxyResponse, RELIABLE_UNORDERED 0
uint reqID
bool success
if success {
	string hostname
	int port
} else {
	string reason
}

#------------------------------------------------------------------PROXYMASTER TO PROXY---------------------------------------------------------
c, proxymaster, proxy
m, NewUser, RELIABLE_ORDERED 0
uint256 symID
symkey symKey
ulong playerID

#------------------------------------------------------------------PROXY TO PROXYMASTER---------------------------------------------------------
c, proxy, proxymaster
m, PublicInformation, RELIABLE_ORDERED 0
string publicHost
int publicPort

#------------------------------------------------------------------CYPHER TO FREESIDE---------------------------------------------------------
c, cypher, freeside
m, PlayerInput, UNRELIABLE_SEQUENCED 0
string hi
int b

#------------------------------------------------------------------FREESIDE TO CYPHER---------------------------------------------------------
c, freeside, cypher
m, ObjPos, UNRELIABLE_SEQUENCED 0
string sup
int a

#------------------------------------------------------------------CYPHER TO PROXY---------------------------------------------------------
c, cypher, proxy
m, RequestGameServer, RELIABLE_ORDERED 0

#------------------------------------------------------------------PROXY TO CYPHER---------------------------------------------------------
c, proxy, cypher
m, GameServerResponse, RELIABLE_ORDERED 0
bool success
if success {
} else {
	string reason
}

#------------------------------------------------------------------PROXY TO FREESIDEMASTER---------------------------------------------------------
c, proxy, freesidemaster
m, RequestGameServer, RELIABLE_ORDERED 0
ulong playerID

#------------------------------------------------------------------FREESIDERMASTER TO PROXY---------------------------------------------------------
c, freesidemaster, proxy
m, GameServerResponse, RELIABLE_ORDERED 0
ulong playerID
bool success
if success {
	string hostname
	int port
} else {
	string reason
}

#------------------------------------------------------------------FREESIDEMASTER TO FREESIDE---------------------------------------------------------
c, freesidemaster, freeside

#------------------------------------------------------------------FREESIDE TO FREESIDEMASTER---------------------------------------------------------
c, freeside, freesidemaster
m, PublicInformation, RELIABLE_ORDERED 0
string privHost
int privPort
