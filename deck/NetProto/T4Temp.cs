﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// This class isn't actually compiled. It's just a manual mirror of the helpercode in the T4 message generator.
/// When working on the codegen, you can copy/paste it here to get some intellisense.
/// 
/// </summary>
namespace deck.AutoGen
{
    public class TestClass
    {
        public class Service
        {
            public readonly Dictionary<Service, Connection> connections = new Dictionary<Service, Connection>();
            public readonly string name;
            public readonly string capName;

            public Service(string name)
            {
                this.name = name;

                capName = name.ToUpper().Substring(0, 1) + name.Substring(1);
            }

            public void addConn(Connection conn)
            {
                if (conn.cli != this && conn.serv != this)
                    throw new Exception("adding a connection to a service that doesn't use this service?");

                if (conn.cli == this)
                    connections.Add(conn.serv, conn);
                else
                    connections.Add(conn.cli, conn);
            }
        }

        public enum CONN_TYPE { NONE, TLS, SYM, LOCAL, PASSTHRU }
        public class Connection
        {
            public readonly Service cli;
            public readonly Service serv;

            public readonly string host; //can be null
            public readonly int port;

            public readonly CONN_TYPE type;
            public readonly Service pass;

            public List<Message> messages = new List<Message>();

            public Connection(Service cli, Service serv, string host, int port, CONN_TYPE type, Service pass)
            {
                this.cli = cli;
                this.serv = serv;
                this.host = host;
                this.port = port;
                this.type = type;
                this.pass = pass;

                if (pass != null && type != CONN_TYPE.PASSTHRU)
                    throw new Exception("pass isn't null but we're not a passthru conn");
                if (pass == null && type == CONN_TYPE.PASSTHRU)
                    throw new Exception("pass is null, but we're a passthru!");
            }

            public void addMessage(Message m)
            {
                if (!((m.from == cli && m.to == serv) || (m.to == cli && m.from == serv)))
                    throw new Exception("adding a message to wrong connection");

                messages.Add(m);
            }

            public bool needsHost()
            {
                return host == null;
            }

            public bool needsPort()
            {
                return port < 0;
            }

            public bool needsSymKey()
            {
                return type == CONN_TYPE.SYM;
            }

            public bool needsPubKey()
            {
                return type == CONN_TYPE.TLS;
            }

            public string getDeckCreationClient()
            {
                bool needHost = needsHost();
                bool needPort = needsPort();
                bool needSymKey = needsSymKey();
                bool needPubKey = needsPubKey();

                string toReturn = "deck = Deck.";

                switch (type)
                {
                    case CONN_TYPE.LOCAL:
                        toReturn += "CreateInternalClient";
                        break;
                    case CONN_TYPE.TLS:
                        toReturn += "CreateSecureClient";
                        break;
                    case CONN_TYPE.SYM:
                        toReturn += "CreateSecureClient";
                        break;
                    case CONN_TYPE.PASSTHRU:
                        return "throw new Exception(\"passthru connections not implemented yet\");";
                    default:
                        throw new Exception("missing case");
                }

                toReturn += "(\"" + serv.name + "\"";

                if (needHost)
                    toReturn += ", host";
                else
                    toReturn += ", \"" + host + "\"";

                if (needPort)
                    toReturn += ", port";
                else
                    toReturn += ", " + port;

                if (needPubKey)
                    toReturn += ", pubKey";

                if (needSymKey)
                    toReturn += ", symKey, symID";

                toReturn += ", onRec, onChg";

                if (needSymKey)
                    toReturn += ", writeHandshake";

                toReturn += ");";

                return toReturn;
            }

            public string getDeckCreationServer()
            {
                bool needPort = needsPort();
                bool needPubKey = needsPubKey();
                bool needSymKey = needsSymKey();

                string toReturn = "deck = Deck.";

                switch (type)
                {
                    case CONN_TYPE.LOCAL:
                        toReturn += "CreateInternalServer";
                        break;
                    case CONN_TYPE.TLS:
                        toReturn += "CreateSecureServer";
                        break;
                    case CONN_TYPE.SYM:
                        toReturn += "CreateSecureServer";
                        break;
                    case CONN_TYPE.PASSTHRU:
                        return "throw new Exception(\"passthru connections not implemented yet\");";
                    default:
                        throw new Exception("missing case");
                }

                toReturn += "(\"" + serv.name + "\"";

                if (needPort)
                    toReturn += ", port";
                else
                    toReturn += ", " + port;

                if (needPubKey)
                    toReturn += ", privKey";

                toReturn += ", onRec, onChg";

                if (needSymKey)
                    toReturn += ", onConn, getSymKey";

                toReturn += ");";

                return toReturn;
            }
        }

        public enum MESSAGE_PROTO { NONE, RELIABLE_ORDERED, RELIABE_SEQUENCED, RELIABLE_UNORDERED, UNRELIABLE_SEQUENCED, UNRELIABLE }
        public class Message
        {
            public readonly Service from;
            public readonly Service to;

            public readonly string name;
            public readonly List<MessagePart> messageParts;

            public readonly MESSAGE_PROTO proto;
            public readonly int channel;

            public Message(Service from, Service to, string name, List<MessagePart> parts, MESSAGE_PROTO proto, int channel)
            {
                this.from = from;
                this.to = to;
                this.name = name;
                messageParts = parts;
                this.proto = proto;
                this.channel = channel;
            }

            public string getMethodArguments()
            {//should return a string 
                string toReturn = "NetConn conn";
                _getMethodArguments(ref toReturn, messageParts, false);
                return toReturn;
            }

            private void _getMethodArguments(ref string toReturn, List<MessagePart> parts, bool inIf)
            {
                for (int x = 0; x < parts.Count; x++)
                {
                    if (parts[x].condi == null)
                    {
                        toReturn += ", " + parts[x].getArg(inIf);
                    }
                    else
                    {
                        _getMethodArguments(ref toReturn, parts[x].ifParts, true);
                        if (parts[x].elseParts != null)
                            _getMethodArguments(ref toReturn, parts[x].elseParts, true);
                    }
                }
            }

            public string getMethodCall()
            {//should return a string 
                string toReturn = "conn";
                _getMethodCall(ref toReturn, messageParts);
                return toReturn;
            }

            private void _getMethodCall(ref string toReturn, List<MessagePart> parts)
            {
                for (int x = 0; x < parts.Count; x++)
                {
                    if (parts[x].condi == null)
                    {
                        toReturn += ", " + parts[x].name;
                    }
                    else
                    {
                        _getMethodCall(ref toReturn, parts[x].ifParts);
                        if (parts[x].elseParts != null)
                            _getMethodCall(ref toReturn, parts[x].elseParts);
                    }
                }
            }

            public List<string> getArgDecl()
            {//should return a string for each messagepart declaring all of the variables
                List<string> toReturn = new List<string>();

                _getArgDecl(toReturn, messageParts, false);

                return toReturn;
            }

            private void _getArgDecl(List<string> toReturn, List<MessagePart> parts, bool inIf)
            {
                for (int x = 0; x < parts.Count; x++)
                {
                    if (parts[x].condi == null)
                    {
                        toReturn.Add(parts[x].getArg(inIf) + parts[x].getDefaultVal(inIf) + ";");
                    }
                    else
                    {
                        _getArgDecl(toReturn, parts[x].ifParts, true);
                        if (parts[x].elseParts != null)
                            _getArgDecl(toReturn, parts[x].elseParts, true);
                    }
                }
            }

            public List<string> getParse()
            {//should return a string for each messagepart declaring all of the variables
                List<string> toReturn = new List<string>();

                _getParse(toReturn, messageParts);

                return toReturn;
            }

            private void _getParse(List<string> toReturn, List<MessagePart> parts)
            {
                for (int x = 0; x < parts.Count; x++)
                {
                    if (parts[x].condi == null)
                    {
                        toReturn.Add(parts[x].name + " = msg." + parts[x].getParse() + ";");
                    }
                    else
                    {
                        toReturn.Add("if (" + parts[x].condi.name + ") {");
                        _getParse(toReturn, parts[x].ifParts);
                        toReturn.Add("}");
                        if (parts[x].elseParts != null)
                        {
                            toReturn.Add("else { ");
                            _getParse(toReturn, parts[x].elseParts);
                            toReturn.Add("}");
                        }
                    }
                }
            }

            public List<string> getWrite(uint num)
            {//should return a string for each messagepart declaring all of the variables
                List<string> toReturn = new List<string>();

                toReturn.Add("var msg = deck.StartMessage(64);");
                toReturn.Add("msg.WriteUInt32(" + num + ");");
                _getWrite(toReturn, messageParts, false);

                return toReturn;
            }

            private void _getWrite(List<string> toReturn, List<MessagePart> parts, bool inIf)
            {
                for (int x = 0; x < parts.Count; x++)
                {
                    if (parts[x].condi == null)
                    {
                        toReturn.Add("msg." + parts[x].getWrite(inIf) + parts[x].name + ");");
                    }
                    else
                    {
                        toReturn.Add("if (" + parts[x].condi.name + ") {");
                        _getWrite(toReturn, parts[x].ifParts, true);
                        toReturn.Add("}");
                        if (parts[x].elseParts != null)
                        {
                            toReturn.Add("else { ");
                            _getWrite(toReturn, parts[x].elseParts, true);
                            toReturn.Add("}");
                        }
                    }
                }
            }

            public string getMessageProtoType()
            {
                switch (proto)
                {
                    case MESSAGE_PROTO.NONE:
                        throw new Exception("Message proto not set");
                    case MESSAGE_PROTO.RELIABE_SEQUENCED:
                        return "MessageDeliveryType.RELIABLE_SEQUENCED";
                    case MESSAGE_PROTO.RELIABLE_ORDERED:
                        return "MessageDeliveryType.RELIABLE_ORDERED";
                    case MESSAGE_PROTO.RELIABLE_UNORDERED:
                        return "MessageDeliveryType.RELIABLE_UNORDERED";
                    case MESSAGE_PROTO.UNRELIABLE:
                        return "MessageDeliveryType.UNRELIABLE";
                    case MESSAGE_PROTO.UNRELIABLE_SEQUENCED:
                        return "MessageDeliveryType.UNRELIABLE_SEQUENCED";
                    default:
                        throw new Exception("Missing case in get proto");
                }

                throw new Exception("switch case didn't return in message proto?");
            }

            public string getMessageChannel()
            {
                return channel.ToString();
            }
        }

        public static MESSAGE_PROTO parseProto(string proto)
        {
            if (proto == "RELIABE_SEQUENCED")
                return MESSAGE_PROTO.RELIABE_SEQUENCED;
            if (proto == "RELIABLE_ORDERED")
                return MESSAGE_PROTO.RELIABLE_ORDERED;
            if (proto == "RELIABLE_UNORDERED")
                return MESSAGE_PROTO.RELIABLE_UNORDERED;
            if (proto == "UNRELIABLE")
                return MESSAGE_PROTO.UNRELIABLE;
            if (proto == "UNRELIABLE_SEQUENCED")
                return MESSAGE_PROTO.UNRELIABLE_SEQUENCED;


            throw new Exception("couldn't parse proto string");
        }

        public enum MessageType { STRING, ULONG, SYMKEY, BOOL, INT, IF }
        public class MessagePart
        {
            public readonly string name;
            public readonly MessageType type;

            public readonly MessagePart condi;
            public readonly List<MessagePart> ifParts;
            public readonly List<MessagePart> elseParts;

            public MessagePart(string name, MessageType type)
            {
                this.name = name;
                this.type = type;

                condi = null;
                ifParts = null;
                elseParts = null;
            }

            public MessagePart(MessagePart condi, List<MessagePart> ifParts, List<MessagePart> elseParts)
            {
                name = null;
                type = MessageType.IF;

                this.condi = condi;
                this.ifParts = ifParts;
                this.elseParts = elseParts;
            }

            public string getArg(bool inIf)
            {
                return getTypeArg() + getCanNull(inIf) + " " + name;
            }

            private string getTypeArg()
            {
                switch (type)
                {
                    case MessageType.STRING:
                        return "string";
                    case MessageType.ULONG:
                        return "ulong";
                    case MessageType.SYMKEY:
                        return "SymKey";
                    case MessageType.BOOL:
                        return "bool";
                    case MessageType.INT:
                        return "int";
                    case MessageType.IF:
                        throw new Exception("calling typearg on type if part");
                }

                throw new Exception("missing type case");
            }

            private string getCanNull(bool inIf)
            {
                if (!inIf)
                    return "";

                switch (type)
                {
                    case MessageType.STRING:
                        return "";
                    case MessageType.ULONG:
                        return "?";
                    case MessageType.SYMKEY:
                        return "?";
                    case MessageType.BOOL:
                        return "?";
                    case MessageType.INT:
                        return "?";
                    case MessageType.IF:
                        throw new Exception("calling canNull on type if part");
                }

                throw new Exception("missing type case");
            }

            public string getDefaultVal(bool inIf)
            {
                if (inIf)
                    return "= null";

                return "";
            }

            public string getParse()
            {
                switch (type)
                {
                    case MessageType.STRING:
                        return "ReadString()";
                    case MessageType.ULONG:
                        return "ReadUInt64()";
                    case MessageType.SYMKEY:
                        return "ReadSymKey()";
                    case MessageType.BOOL:
                        return "ReadBoolean()";
                    case MessageType.INT:
                        return "ReadInt32()";
                    case MessageType.IF:
                        throw new Exception("calling getParse on type if part");
                }

                throw new Exception("missing type case");
            }

            public string getWrite(bool inIf)
            {
                if (inIf)
                {
                    switch (type)
                    {
                        case MessageType.STRING:
                            return "WriteString(";
                        case MessageType.ULONG:
                            return "WriteUInt64((ulong)";
                        case MessageType.SYMKEY:
                            return "WriteSymKey((SymKey)";
                        case MessageType.BOOL:
                            return "WriteBoolean((bool)";
                        case MessageType.INT:
                            return "WriteInt32((int)";
                        case MessageType.IF:
                            throw new Exception("calling getWrite on type if part");
                    }
                }
                else
                {
                    switch (type)
                    {
                        case MessageType.STRING:
                            return "WriteString(";
                        case MessageType.ULONG:
                            return "WriteUInt64(";
                        case MessageType.SYMKEY:
                            return "WriteSymKey(";
                        case MessageType.BOOL:
                            return "WriteBoolean(";
                        case MessageType.INT:
                            return "WriteInt32(";
                        case MessageType.IF:
                            throw new Exception("calling getWrite on type if part");
                    }
                }

                throw new Exception("missing type case");
            }

        }

        private static MessageType parseMessageType(string type)
        {
            if (type == "string")
                return MessageType.STRING;
            else if (type == "ulong")
                return MessageType.ULONG;
            else if (type == "symkey")
                return MessageType.SYMKEY;
            else if (type == "bool")
                return MessageType.BOOL;
            else if (type == "int")
                return MessageType.INT;

            throw new Exception("not a valid message part type " + type);
        }

        private static string getNextLine(string[] lines, ref int l)
        {
            while (l < lines.Length)
            {
                string line = lines[l++];
                if (line.StartsWith("#"))
                    continue;

                return line;
            }

            return null;
        }

        public static Dictionary<string, Service> getServices(string filename)
        {
            string[] lines = System.IO.File.ReadAllLines(filename);
            int l = 0;

            Dictionary<string, Service> services = new Dictionary<string, Service>();

            string line = null;
            while (true)
            {//populate services
                line = getNextLine(lines, ref l).Trim();

                if (line == "")
                    break;

                services.Add(line, new Service(line));
            }

            while (true)
            {//populate connections
                line = getNextLine(lines, ref l).Trim();

                if (line == "")
                    break;

                string[] dat = line.Split(',');
                for (int x = 0; x < dat.Length; x++)
                    dat[x] = dat[x].Trim();

                Service cli = services[dat[0]];
                Service serv = services[dat[1]];

                CONN_TYPE type = CONN_TYPE.NONE;
                Service pass = null;
                int port = -1;
                int tempPort = -1;
                string host = null;
                for (int x = 2; x < dat.Length; x++)
                {
                    if (dat[x] == "tls")
                        type = CONN_TYPE.TLS;
                    else if (dat[x] == "sym")
                        type = CONN_TYPE.SYM;
                    else if (dat[x] == "local")
                        type = CONN_TYPE.LOCAL;
                    else if (dat[x].StartsWith("pass"))
                    {
                        type = CONN_TYPE.PASSTHRU;

                        string s = dat[x].Substring(dat[x].IndexOf(' ') + 1);
                        pass = services[s];
                    }
                    else if (Int32.TryParse(dat[x], out tempPort))
                    {
                        port = tempPort;
                    }
                    else
                    {
                        host = dat[x];
                    }
                }

                Connection conn = new Connection(cli, serv, host, port, type, pass);

                cli.addConn(conn);
                serv.addConn(conn);
            }

            Service sender = null;
            Service recv = null;
            Connection curConn = null;
            while (true)
            {//populate messages
                line = getNextLine(lines, ref l);

                if (line == null)
                    break;

                line = line.Trim();

                if (line.StartsWith("c"))
                {//new sender/recv
                    string[] dat = line.Split(',');
                    for (int x = 0; x < dat.Length; x++)
                        dat[x] = dat[x].Trim();

                    sender = services[dat[1]];
                    recv = services[dat[2]];
                    curConn = sender.connections[recv];
                }
                else if (line.StartsWith("m"))
                {//new message
                    string[] dat = line.Split(',');
                    for (int x = 0; x < dat.Length; x++)
                        dat[x] = dat[x].Trim();

                    string name = dat[1];

                    int sp = dat[2].IndexOf(' ');
                    string proto = dat[2].Substring(0, sp).Trim();
                    string chan = dat[2].Substring(sp + 1).Trim();


                    curConn.addMessage(new Message(sender, recv, name, parseMessage(lines, ref l), parseProto(proto), int.Parse(chan)));
                }
            }

            return services;
        }

        static List<MessagePart> parseMessage(string[] lines, ref int l)
        {
            List<MessagePart> parts = new List<MessagePart>();

            while (true)
            {//parse message -- must end on blank newline, or null
                string line = getNextLine(lines, ref l);

                if (line == null)
                    break;

                line = line.Trim();
                if (line == "")
                    break;

                //first see if we have any control structures
                if (line.StartsWith("if"))
                {//if statement, need to parse if block
                    string[] p = line.Split(' ');
                    string ifName = p[1];

                    if (p[2] != "{")
                        throw new Exception("missing bracket on if");

                    MessagePart condiOn = null;
                    for (int x = 0; x < parts.Count; x++)
                    {
                        if (parts[x].name.Equals(ifName))
                        {
                            condiOn = parts[x];
                            break;
                        }
                    }

                    if (condiOn == null)
                        throw new Exception("condiOn null in bad place, cant find conditional for if statement");
                    if (condiOn.type != MessageType.BOOL)
                        throw new Exception("condiOn not a boolean?");

                    List<MessagePart> ifParts = parseMessage(lines, ref l);

                    List<MessagePart> elseParts = null;
                    line = lines[l - 1].Trim(); //see if we have an else...
                    if (line == "} else {")
                    {
                        elseParts = parseMessage(lines, ref l);
                    }

                    parts.Add(new MessagePart(condiOn, ifParts, elseParts));
                }
                else if (line.StartsWith("}"))
                {
                    return parts;
                }
                else
                {
                    int space = line.IndexOf(' ');
                    MessageType partType = parseMessageType(line.Substring(0, space));
                    string partName = line.Substring(space + 1).Trim();

                    parts.Add(new MessagePart(partName, partType));
                }
            }

            return parts;
        }
    }
}
