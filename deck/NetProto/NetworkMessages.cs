﻿
//THIS IS A MACHINE GENERATED FILE.
//CHANGES MUST BE MADE IN THE TEMPLATE FILE.
//TO ADD MESSAGES, EDIT THE ASSOCIATED CONFIG FILE. (THE NAME OF WHICH SHOULD BE AT THE TOP OF THE TEMPLATE)
//AFTER EDITING THE CONFIG FILE, RE-RUN THE TEMPLATE GENERATOR (BUILD -> TRANSFORM ALL T4 TEMPLATES, OR MAKE A CHANGE HERE AND SAVE IT)
//TO ADD A NEW TYPE TO THE MESSAGE PROTOCL, GO TO THE MESSAGEPART CLASS IN THE TEMPLATE AND ADD THE APPROPRIATE IMPLEMENTATION.
//YOU CAN GET INTELLISENSE WHILE EDITTING BY COPY/PASTING THE BOTTOM COLLECTION OF CODE TO T4TEMP.CS
using System;
using log;
using System.Collections.Generic;

namespace deck
{
    internal delegate void MsgHandle(NetConn remote, NetMsgIn msg);
    internal delegate void ProxClientHandle(NetMsgIn msg);
    internal delegate void ProxHandle(ProxConn conn, NetMsgIn msg);

    public class ProxConn
    {
        internal NetConn netConn;
        public readonly ulong playerID;
        internal ProxConn(NetConn netConn, ulong playerID)
        {
            this.netConn = netConn;
            this.playerID = playerID;
        }
    }
	
					public class IcetoCypherHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IIceFromCypher impl;
												private Deck deck;
												
						public IcetoCypherHost(IIceFromCypher impl, PrivateKey privKey)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[2];
																	handlers[0] = onLoginRequest; 
																			handlers[1] = onNewUserRequest; 
									
							deck = Deck.CreateSecureServer("ice", 8008, privKey, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.cypher_onConnectionChange(conn, status);
						}
						
															private void onLoginRequest(NetConn conn, NetMsgIn msg)
									{
																					string username;
																						string password;
											
																					username = msg.ReadString();
																						password = msg.ReadString();
											
										impl.cypher_LoginRequest(conn, username, password);
									} 
																	private void onNewUserRequest(NetConn conn, NetMsgIn msg)
									{
																					string username;
																						string password;
											
																					username = msg.ReadString();
																						password = msg.ReadString();
											
										impl.cypher_NewUserRequest(conn, username, password);
									} 
																	public void sendLoginResponse(NetConn conn, bool success, UInt256? symID, SymKey? symKey, ulong? playerID, string host, int? port, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteBoolean(success);
																						if (success) {
																						msg.WriteUInt256((UInt256)symID);
																						msg.WriteSymKey((SymKey)symKey);
																						msg.WriteUInt64((ulong)playerID);
																						msg.WriteString(host);
																						msg.WriteInt32((int)port);
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	public void sendNewUserResponse(NetConn conn, bool success, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(1);
																						msg.WriteBoolean(success);
																						if (success) {
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
													}

					public interface IIceFromCypher
					{
												void cypher_onConnectionChange(NetConn conn, ConnectionStatus status);
															void cypher_LoginRequest(NetConn conn, string username, string password); 
																	void cypher_NewUserRequest(NetConn conn, string username, string password); 
													}
				
					public class IcetoAuthClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IIceFromAuth impl;
												private ClientDeck deck;
																		public object userData;
						
						public IcetoAuthClient(IIceFromAuth impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[2];
																	handlers[0] = onLoginResponse; 
																			handlers[1] = onNewUserResponse; 
									
							deck = Deck.CreateInternalClient("auth", "auth.cypherfunk.com", 8010, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.auth_onConnectionChange(this, status);
						}
						
															public void sendLoginRequest(uint reqID, string username, byte[] password)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt32(reqID);
																						msg.WriteString(username);
																						msg.WriteByteArray(password);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
																	public void sendNewUserRequest(uint reqID, string username, byte[] password)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(1);
																						msg.WriteUInt32(reqID);
																						msg.WriteString(username);
																						msg.WriteByteArray(password);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
																	private void onLoginResponse(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						bool success;
																						ulong? playerID= null;
																						string reason= null;
											
																					reqID = msg.ReadUInt32();
																						success = msg.ReadBoolean();
																						if (success) {
																						playerID = msg.ReadUInt64();
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.auth_LoginResponse(this, reqID, success, playerID, reason);
									} 
																	private void onNewUserResponse(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						bool success;
																						string reason= null;
											
																					reqID = msg.ReadUInt32();
																						success = msg.ReadBoolean();
																						if (success) {
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.auth_NewUserResponse(this, reqID, success, reason);
									} 
													}

					public interface IIceFromAuth
					{
												void auth_onConnectionChange(IcetoAuthClient client, ConnectionStatus status);
															void auth_LoginResponse(IcetoAuthClient client, uint reqID, bool success, ulong? playerID, string reason); 
																	void auth_NewUserResponse(IcetoAuthClient client, uint reqID, bool success, string reason); 
													}
				
					public class IcetoProxymasterClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IIceFromProxymaster impl;
												private ClientDeck deck;
																		public object userData;
						
						public IcetoProxymasterClient(IIceFromProxymaster impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onFindProxyResponse; 
									
							deck = Deck.CreateInternalClient("proxymaster", "proxymaster.cypherfunk.com", 8011, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.proxymaster_onConnectionChange(this, status);
						}
						
															public void sendFindProxy(uint reqID, UInt256 symID, SymKey symKey, ulong playerID)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt32(reqID);
																						msg.WriteUInt256(symID);
																						msg.WriteSymKey(symKey);
																						msg.WriteUInt64(playerID);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
																	private void onFindProxyResponse(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						bool success;
																						string hostname= null;
																						int? port= null;
																						string reason= null;
											
																					reqID = msg.ReadUInt32();
																						success = msg.ReadBoolean();
																						if (success) {
																						hostname = msg.ReadString();
																						port = msg.ReadInt32();
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.proxymaster_FindProxyResponse(this, reqID, success, hostname, port, reason);
									} 
													}

					public interface IIceFromProxymaster
					{
												void proxymaster_onConnectionChange(IcetoProxymasterClient client, ConnectionStatus status);
															void proxymaster_FindProxyResponse(IcetoProxymasterClient client, uint reqID, bool success, string hostname, int? port, string reason); 
													}
				
					public class AuthtoIceHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IAuthFromIce impl;
												private Deck deck;
												
						public AuthtoIceHost(IAuthFromIce impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[2];
																	handlers[0] = onLoginRequest; 
																			handlers[1] = onNewUserRequest; 
									
							deck = Deck.CreateInternalServer("auth", 8010, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.ice_onConnectionChange(conn, status);
						}
						
															private void onLoginRequest(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						string username;
																						byte[] password;
											
																					reqID = msg.ReadUInt32();
																						username = msg.ReadString();
																						password = msg.ReadByteArray();
											
										impl.ice_LoginRequest(conn, reqID, username, password);
									} 
																	private void onNewUserRequest(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						string username;
																						byte[] password;
											
																					reqID = msg.ReadUInt32();
																						username = msg.ReadString();
																						password = msg.ReadByteArray();
											
										impl.ice_NewUserRequest(conn, reqID, username, password);
									} 
																	public void sendLoginResponse(NetConn conn, uint reqID, bool success, ulong? playerID, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt32(reqID);
																						msg.WriteBoolean(success);
																						if (success) {
																						msg.WriteUInt64((ulong)playerID);
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
																	public void sendNewUserResponse(NetConn conn, uint reqID, bool success, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(1);
																						msg.WriteUInt32(reqID);
																						msg.WriteBoolean(success);
																						if (success) {
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
													}

					public interface IAuthFromIce
					{
												void ice_onConnectionChange(NetConn conn, ConnectionStatus status);
															void ice_LoginRequest(NetConn conn, uint reqID, string username, byte[] password); 
																	void ice_NewUserRequest(NetConn conn, uint reqID, string username, byte[] password); 
													}
				
					public class ProxytoCypherHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxyFromCypher impl;
												private Deck deck;
												
						public ProxytoCypherHost(IProxyFromCypher impl, int port, GetSymKey getSymKey)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[3];
														handlers[0] = onProxyMessage;
							handlers[1] = onProxyDisc;
																	handlers[2] = onRequestGameServer; 
									
							deck = Deck.CreateSecureServer("proxy", port, onRec, onChg, onConn, getSymKey);
													}
						
						private bool onConn(UInt256 symID, NetConn conn, NetMsgIn msg)
						{
							ulong playerID = msg.ReadUInt64();

							return impl.cypher_onSymConnApproval(symID, conn, playerID);
						}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.cypher_onConnectionChange(conn, status);
						}
												private void onProxyMessage(NetConn conn, NetMsgIn msg)
						{
							int offset;
							int len;
							byte[] dat = msg.ReadRawBytes(out offset, out len);
							MessageDeliveryType type = msg.getDeliveryMethod();
							int chan = msg.getDeliveryChannel();

							impl.cypher_onProxyMessage(conn, dat, offset, len, type, chan);
						}

						private void onProxyDisc(NetConn conn, NetMsgIn msg)
						{
							string reason = msg.ReadString();
							impl.cypher_onProxyDisc(conn, reason);
						}

						public void sendProxyMessage(NetConn conn, byte[] forwardedMsg, int pos, int len, MessageDeliveryType proto, int chan)
						{
							var msg = deck.StartMessage(64);
							msg.WriteUInt32(0);
							msg.WriteRawBytes(forwardedMsg, pos, len);
							deck.SendMessage(conn, proto, chan);
						}

						public void sendProxyConnection(NetConn conn)
						{
							var msg = deck.StartMessage(4);
							msg.WriteUInt32(1);
							deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
						}

						public void sendProxyDisconnect(NetConn conn)
						{
							var msg = deck.StartMessage(4);
							msg.WriteUInt32(2);
							deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
						}
						
															private void onRequestGameServer(NetConn conn, NetMsgIn msg)
									{
										
										
										impl.cypher_RequestGameServer(conn);
									} 
																	public void sendGameServerResponse(NetConn conn, bool success, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(3);
																						msg.WriteBoolean(success);
																						if (success) {
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
													}

					public interface IProxyFromCypher
					{
												bool cypher_onSymConnApproval(UInt256 symID, NetConn conn, ulong playerID);
												void cypher_onProxyMessage(NetConn conn, byte[] msg, int pos, int len, MessageDeliveryType type, int chan);
						void cypher_onProxyDisc(NetConn conn, string reason);
												void cypher_onConnectionChange(NetConn conn, ConnectionStatus status);
															void cypher_RequestGameServer(NetConn conn); 
													}
				
					public class ProxytoProxymasterClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxyFromProxymaster impl;
												private ClientDeck deck;
																		public object userData;
						
						public ProxytoProxymasterClient(IProxyFromProxymaster impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onNewUser; 
									
							deck = Deck.CreateInternalClient("proxymaster", "proxymaster.cypherfunk.com", 8012, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.proxymaster_onConnectionChange(this, status);
						}
						
															private void onNewUser(NetConn conn, NetMsgIn msg)
									{
																					UInt256 symID;
																						SymKey symKey;
																						ulong playerID;
											
																					symID = msg.ReadUInt256();
																						symKey = msg.ReadSymKey();
																						playerID = msg.ReadUInt64();
											
										impl.proxymaster_NewUser(this, symID, symKey, playerID);
									} 
																	public void sendPublicInformation(string publicHost, int publicPort)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteString(publicHost);
																						msg.WriteInt32(publicPort);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
													}

					public interface IProxyFromProxymaster
					{
												void proxymaster_onConnectionChange(ProxytoProxymasterClient client, ConnectionStatus status);
															void proxymaster_NewUser(ProxytoProxymasterClient client, UInt256 symID, SymKey symKey, ulong playerID); 
													}
				
					public class ProxytoFreesideClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxyFromFreeside impl;
												private ClientDeck deck;
																		public object userData;
						
						public ProxytoFreesideClient(IProxyFromFreeside impl, int port, string host)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[2];
														handlers[0] = onProxyMessage;
							handlers[1] = onProxyDisc;
							
							deck = Deck.CreateInternalClient("freeside", host, port, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.freeside_onConnectionChange(this, status);
						}
												private void onProxyMessage(NetConn conn, NetMsgIn msg)
						{
							ulong pid = msg.ReadUInt64();
							int offset;
							int len;
							byte[] dat = msg.ReadRawBytes(out offset, out len);
							MessageDeliveryType type = msg.getDeliveryMethod();
							int chan = msg.getDeliveryChannel();

							impl.freeside_onProxyMessage(this, pid, dat, offset, len, type, chan);
						}

						private void onProxyDisc(NetConn conn, NetMsgIn msg)
						{
							ulong pid = msg.ReadUInt64();
							string reason = msg.ReadString();
							impl.freeside_onProxyDisc(this, pid, reason);
						}

						public void sendProxyMessage(ulong pid, byte[] forwardedMsg, int pos, int len, MessageDeliveryType proto, int chan)
						{
							var msg = deck.StartMessage(64);
							msg.WriteUInt32(0);
							msg.WriteUInt64(pid);
							msg.WriteRawBytes(forwardedMsg, pos, len);
							deck.SendMessage(proto, chan);
						}

						public void sendProxyConnection(ulong pid)
						{
							var msg = deck.StartMessage(12);
							msg.WriteUInt32(1);
							msg.WriteUInt64(pid);
							deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
						}

						public void sendProxyDisconnect(ulong pid)
						{
							var msg = deck.StartMessage(12);
							msg.WriteUInt32(2);
							msg.WriteUInt64(pid);
							deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
						}
						
											}

					public interface IProxyFromFreeside
					{
												void freeside_onProxyMessage(ProxytoFreesideClient client, ulong pid, byte[] msg, int pos, int len, MessageDeliveryType type, int chan);
						void freeside_onProxyDisc(ProxytoFreesideClient client, ulong pid, string reason);
												void freeside_onConnectionChange(ProxytoFreesideClient client, ConnectionStatus status);
											}
				
					public class ProxytoFreesidemasterClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxyFromFreesidemaster impl;
												private ClientDeck deck;
																		public object userData;
						
						public ProxytoFreesidemasterClient(IProxyFromFreesidemaster impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onGameServerResponse; 
									
							deck = Deck.CreateInternalClient("freesidemaster", "freesidemaster.cypherfunk.com", 8014, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.freesidemaster_onConnectionChange(this, status);
						}
						
															public void sendRequestGameServer(ulong playerID)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt64(playerID);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	private void onGameServerResponse(NetConn conn, NetMsgIn msg)
									{
																					ulong playerID;
																						bool success;
																						string hostname= null;
																						int? port= null;
																						string reason= null;
											
																					playerID = msg.ReadUInt64();
																						success = msg.ReadBoolean();
																						if (success) {
																						hostname = msg.ReadString();
																						port = msg.ReadInt32();
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.freesidemaster_GameServerResponse(this, playerID, success, hostname, port, reason);
									} 
													}

					public interface IProxyFromFreesidemaster
					{
												void freesidemaster_onConnectionChange(ProxytoFreesidemasterClient client, ConnectionStatus status);
															void freesidemaster_GameServerResponse(ProxytoFreesidemasterClient client, ulong playerID, bool success, string hostname, int? port, string reason); 
													}
				
					public class ProxymastertoIceHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxymasterFromIce impl;
												private Deck deck;
												
						public ProxymastertoIceHost(IProxymasterFromIce impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onFindProxy; 
									
							deck = Deck.CreateInternalServer("proxymaster", 8011, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.ice_onConnectionChange(conn, status);
						}
						
															private void onFindProxy(NetConn conn, NetMsgIn msg)
									{
																					uint reqID;
																						UInt256 symID;
																						SymKey symKey;
																						ulong playerID;
											
																					reqID = msg.ReadUInt32();
																						symID = msg.ReadUInt256();
																						symKey = msg.ReadSymKey();
																						playerID = msg.ReadUInt64();
											
										impl.ice_FindProxy(conn, reqID, symID, symKey, playerID);
									} 
																	public void sendFindProxyResponse(NetConn conn, uint reqID, bool success, string hostname, int? port, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt32(reqID);
																						msg.WriteBoolean(success);
																						if (success) {
																						msg.WriteString(hostname);
																						msg.WriteInt32((int)port);
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_UNORDERED, 0);
																			} 
													}

					public interface IProxymasterFromIce
					{
												void ice_onConnectionChange(NetConn conn, ConnectionStatus status);
															void ice_FindProxy(NetConn conn, uint reqID, UInt256 symID, SymKey symKey, ulong playerID); 
													}
				
					public class ProxymastertoProxyHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IProxymasterFromProxy impl;
												private Deck deck;
												
						public ProxymastertoProxyHost(IProxymasterFromProxy impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onPublicInformation; 
									
							deck = Deck.CreateInternalServer("proxymaster", 8012, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.proxy_onConnectionChange(conn, status);
						}
						
															public void sendNewUser(NetConn conn, UInt256 symID, SymKey symKey, ulong playerID)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt256(symID);
																						msg.WriteSymKey(symKey);
																						msg.WriteUInt64(playerID);
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	private void onPublicInformation(NetConn conn, NetMsgIn msg)
									{
																					string publicHost;
																						int publicPort;
											
																					publicHost = msg.ReadString();
																						publicPort = msg.ReadInt32();
											
										impl.proxy_PublicInformation(conn, publicHost, publicPort);
									} 
													}

					public interface IProxymasterFromProxy
					{
												void proxy_onConnectionChange(NetConn conn, ConnectionStatus status);
															void proxy_PublicInformation(NetConn conn, string publicHost, int publicPort); 
													}
				
					public class FreesidetoProxyHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IFreesideFromProxy impl;
												private Deck deck;
												private readonly FreesidetoCypherHost proxy;
												private readonly Dictionary<ulong, ProxConn> idToProxConn = new Dictionary<ulong, ProxConn>();
												
						public FreesidetoProxyHost(IFreesideFromProxy impl, int port, IFreesideFromCypher proxyImpl, out FreesidetoCypherHost proxyOut)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[3];
														handlers[0] = onProxyMessage;
							handlers[1] = onProxyConn;
							handlers[2] = onProxyDisc;
							
							deck = Deck.CreateInternalServer("freeside", port, onRec, onChg);
														proxy = new FreesidetoCypherHost(proxyImpl, this);
							proxyOut = proxy;
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							if (status == ConnectionStatus.DISCONNECTED)
							{
								foreach (ulong pid in idToProxConn.Keys)
								{
									ProxConn c = idToProxConn[pid];
									proxy.onChg(c, ConnectionStatus.DISCONNECTED);
								}
								idToProxConn.Clear();
							}
							impl.proxy_onConnectionChange(conn, status);
						}

						private void onProxyMessage(NetConn conn, NetMsgIn msg)
						{
							ulong pid = msg.ReadUInt64();
							ProxConn c = idToProxConn[pid];

							proxy.onRec(c, msg);
						}

						private void onProxyConn(NetConn conn, NetMsgIn msg)
						{
							ulong pid = msg.ReadUInt64();
							ProxConn c = new ProxConn(conn, pid);
							idToProxConn.Add(pid, c);

							proxy.onChg(c, ConnectionStatus.CONNECTED);
						}

						private void onProxyDisc(NetConn conn, NetMsgIn msg)
						{
							ulong pid = msg.ReadUInt64();
							ProxConn c = idToProxConn[pid];
							idToProxConn.Remove(pid);

							proxy.onChg(c, ConnectionStatus.DISCONNECTED);
						}

						internal NetMsgOut startProxyMessage(ProxConn conn, int siz)
						{
							var toReturn = deck.StartMessage(siz);
							toReturn.WriteUInt32(0);
							toReturn.WriteUInt64(conn.playerID);

							return toReturn;
						}

						internal void sendProxyMessage(ProxConn conn, MessageDeliveryType type, int chan)
						{
							deck.SendMessage(conn.netConn, type, chan);
						}

						internal void proxDisc(ProxConn conn, string reason)
						{
							idToProxConn.Remove(conn.playerID);

							var msg = deck.StartMessage(64);
							msg.WriteUInt32(1);
							msg.WriteUInt64(conn.playerID);
							msg.WriteString(reason);
							deck.SendMessage(conn.netConn, MessageDeliveryType.RELIABLE_ORDERED, 0);
						}
						
											}

					public interface IFreesideFromProxy
					{
												void proxy_onConnectionChange(NetConn conn, ConnectionStatus status);
											}
				
					public class FreesidetoFreesidemasterClient
					{
						private readonly MsgHandle[] handlers;
						private readonly IFreesideFromFreesidemaster impl;
												private ClientDeck deck;
																		public object userData;
						
						public FreesidetoFreesidemasterClient(IFreesideFromFreesidemaster impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[0];
							
							deck = Deck.CreateInternalClient("freesidemaster", "freesidemaster.cypherfunk.com", 8015, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.freesidemaster_onConnectionChange(this, status);
						}
						
															public void sendPublicInformation(string privHost, int privPort)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteString(privHost);
																						msg.WriteInt32(privPort);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
													}

					public interface IFreesideFromFreesidemaster
					{
												void freesidemaster_onConnectionChange(FreesidetoFreesidemasterClient client, ConnectionStatus status);
											}
				
					public class FreesidetoCypherHost
					{
						private readonly ProxHandle[] handlers;
						private readonly IFreesideFromCypher impl;
												private readonly FreesidetoProxyHost proxy;
												
						internal FreesidetoCypherHost(IFreesideFromCypher impl, FreesidetoProxyHost proxy)
						{
							this.impl = impl;
							this.proxy = proxy;
							handlers = new ProxHandle[1];
																	handlers[0] = onPlayerInput; 
															}
						
						internal void onRec(ProxConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								disconnect(conn, "malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						internal void onChg(ProxConn conn, ConnectionStatus status)
						{
							impl.cypher_onConnectionChange(conn, status);
						}

						public void disconnect(ProxConn conn, string reason)
						{
							proxy.proxDisc(conn, reason);
						}
						
															private void onPlayerInput(ProxConn conn, NetMsgIn msg)
									{
																					string hi;
																						int b;
											
																					hi = msg.ReadString();
																						b = msg.ReadInt32();
											
										impl.cypher_PlayerInput(conn, hi, b);
									} 
																	public void sendObjPos(ProxConn conn, string sup, int a)
									{
																					var msg = proxy.startProxyMessage(conn, 64);
																						msg.WriteUInt32(0);
																						msg.WriteString(sup);
																						msg.WriteInt32(a);
																					proxy.sendProxyMessage(conn, MessageDeliveryType.UNRELIABLE_SEQUENCED, 0);
																			} 
													}

					public interface IFreesideFromCypher
					{
												void cypher_onConnectionChange(ProxConn conn, ConnectionStatus status);
															void cypher_PlayerInput(ProxConn conn, string hi, int b); 
													}
				
					public class FreesidemastertoProxyHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IFreesidemasterFromProxy impl;
												private Deck deck;
												
						public FreesidemastertoProxyHost(IFreesidemasterFromProxy impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onRequestGameServer; 
									
							deck = Deck.CreateInternalServer("freesidemaster", 8014, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.proxy_onConnectionChange(conn, status);
						}
						
															private void onRequestGameServer(NetConn conn, NetMsgIn msg)
									{
																					ulong playerID;
											
																					playerID = msg.ReadUInt64();
											
										impl.proxy_RequestGameServer(conn, playerID);
									} 
																	public void sendGameServerResponse(NetConn conn, ulong playerID, bool success, string hostname, int? port, string reason)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteUInt64(playerID);
																						msg.WriteBoolean(success);
																						if (success) {
																						msg.WriteString(hostname);
																						msg.WriteInt32((int)port);
																						}
																						else { 
																						msg.WriteString(reason);
																						}
																					deck.SendMessage(conn, MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
													}

					public interface IFreesidemasterFromProxy
					{
												void proxy_onConnectionChange(NetConn conn, ConnectionStatus status);
															void proxy_RequestGameServer(NetConn conn, ulong playerID); 
													}
				
					public class FreesidemastertoFreesideHost
					{
						private readonly MsgHandle[] handlers;
						private readonly IFreesidemasterFromFreeside impl;
												private Deck deck;
												
						public FreesidemastertoFreesideHost(IFreesidemasterFromFreeside impl)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[1];
																	handlers[0] = onPublicInformation; 
									
							deck = Deck.CreateInternalServer("freesidemaster", 8015, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Close(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.freeside_onConnectionChange(conn, status);
						}
						
															private void onPublicInformation(NetConn conn, NetMsgIn msg)
									{
																					string privHost;
																						int privPort;
											
																					privHost = msg.ReadString();
																						privPort = msg.ReadInt32();
											
										impl.freeside_PublicInformation(conn, privHost, privPort);
									} 
													}

					public interface IFreesidemasterFromFreeside
					{
												void freeside_onConnectionChange(NetConn conn, ConnectionStatus status);
															void freeside_PublicInformation(NetConn conn, string privHost, int privPort); 
													}
				
					public class CyphertoIceClient
					{
						private readonly MsgHandle[] handlers;
						private readonly ICypherFromIce impl;
												private ClientDeck deck;
																		public object userData;
						
						public CyphertoIceClient(ICypherFromIce impl, PublicKey pubKey)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[2];
																	handlers[0] = onLoginResponse; 
																			handlers[1] = onNewUserResponse; 
									
							deck = Deck.CreateSecureClient("ice", "ice.cypherfunk.com", 8008, pubKey, onRec, onChg);
													}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							impl.ice_onConnectionChange(this, status);
						}
						
															public void sendLoginRequest(string username, string password)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteString(username);
																						msg.WriteString(password);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	public void sendNewUserRequest(string username, string password)
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(1);
																						msg.WriteString(username);
																						msg.WriteString(password);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	private void onLoginResponse(NetConn conn, NetMsgIn msg)
									{
																					bool success;
																						UInt256? symID= null;
																						SymKey? symKey= null;
																						ulong? playerID= null;
																						string host= null;
																						int? port= null;
																						string reason= null;
											
																					success = msg.ReadBoolean();
																						if (success) {
																						symID = msg.ReadUInt256();
																						symKey = msg.ReadSymKey();
																						playerID = msg.ReadUInt64();
																						host = msg.ReadString();
																						port = msg.ReadInt32();
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.ice_LoginResponse(this, success, symID, symKey, playerID, host, port, reason);
									} 
																	private void onNewUserResponse(NetConn conn, NetMsgIn msg)
									{
																					bool success;
																						string reason= null;
											
																					success = msg.ReadBoolean();
																						if (success) {
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.ice_NewUserResponse(this, success, reason);
									} 
													}

					public interface ICypherFromIce
					{
												void ice_onConnectionChange(CyphertoIceClient client, ConnectionStatus status);
															void ice_LoginResponse(CyphertoIceClient client, bool success, UInt256? symID, SymKey? symKey, ulong? playerID, string host, int? port, string reason); 
																	void ice_NewUserResponse(CyphertoIceClient client, bool success, string reason); 
													}
				
					public class CyphertoProxyClient
					{
						private readonly MsgHandle[] handlers;
						private readonly ICypherFromProxy impl;
												private ClientDeck deck;
												private readonly CyphertoFreesideClient proxy;
												private bool proxyConnected = false;
																		public object userData;
						
						public CyphertoProxyClient(ICypherFromProxy impl, int port, string host, SymKey symKey, UInt256 symID, ulong playerID, ICypherFromFreeside proxyImpl, out CyphertoFreesideClient proxyOut)
						{
							this.impl = impl;
							
							handlers = new MsgHandle[4];
														handlers[0] = onProxyMessage;
							handlers[1] = onProxyConn;
							handlers[2] = onProxyDisc;
																	handlers[3] = onGameServerResponse; 
									
							this.playerID = playerID;
							
							deck = Deck.CreateSecureClient("proxy", host, port, symKey, symID, onRec, onChg, writeHandshake);
														proxy = new CyphertoFreesideClient(proxyImpl, this);
							proxyOut = proxy;
													}
						
						private readonly ulong playerID;
						private void writeHandshake(NetMsgOut msg)
						{
							msg.WriteUInt64(playerID);
						}
						
						public void Update()
						{
							deck.Update();
						}
						
						public void Disconnect(string reason)
						{
							deck.close(reason);
						}
						
						private void onRec(NetConn conn, NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								conn.Disconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(conn, msg);
						}
						
						private void onChg(NetConn conn, ConnectionStatus status)
						{
							if (status == ConnectionStatus.DISCONNECTED)
							{
								if (proxyConnected)
								{
									proxyConnected = false;
									proxy.onChg(ConnectionStatus.DISCONNECTED);
								}
							}

							impl.proxy_onConnectionChange(this, status);
						}

						private void onProxyMessage(NetConn conn, NetMsgIn msg)
						{
							proxy.onRec(msg);
						}

						private void onProxyConn(NetConn conn, NetMsgIn msg)
						{
							proxyConnected = true;
							proxy.onChg(ConnectionStatus.CONNECTED);
						}

						private void onProxyDisc(NetConn conn, NetMsgIn msg)
						{
							proxyConnected = false;
							proxy.onChg(ConnectionStatus.DISCONNECTED);
						}

						internal void proxyDisconnect(string reason)
						{
							if (proxyConnected)
							{
								proxyConnected = false;
								proxy.onChg(ConnectionStatus.DISCONNECTED);

								var msg = deck.StartMessage(64);
								msg.WriteUInt32(1);
								msg.WriteString(reason);
								deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
							}
						}

						internal NetMsgOut startProxyMessage(int siz)
						{
							var msg = deck.StartMessage(siz);
							msg.WriteUInt32(0);
							return msg;
						}

						internal void sendProxyMessage(MessageDeliveryType type, int chan)
						{
							deck.SendMessage(type, chan);
						}
						
															public void sendRequestGameServer()
									{
																					var msg = deck.StartMessage(64);
																						msg.WriteUInt32(2);
																					deck.SendMessage(MessageDeliveryType.RELIABLE_ORDERED, 0);
																			} 
																	private void onGameServerResponse(NetConn conn, NetMsgIn msg)
									{
																					bool success;
																						string reason= null;
											
																					success = msg.ReadBoolean();
																						if (success) {
																						}
																						else { 
																						reason = msg.ReadString();
																						}
											
										impl.proxy_GameServerResponse(this, success, reason);
									} 
													}

					public interface ICypherFromProxy
					{
												void proxy_onConnectionChange(CyphertoProxyClient client, ConnectionStatus status);
															void proxy_GameServerResponse(CyphertoProxyClient client, bool success, string reason); 
													}
				
					public class CyphertoFreesideClient
					{
						private readonly ProxClientHandle[] handlers;
						private readonly ICypherFromFreeside impl;
												private readonly CyphertoProxyClient proxy;
																		public object userData;
						
						internal CyphertoFreesideClient(ICypherFromFreeside impl, CyphertoProxyClient proxy)
						{
							this.impl = impl;
							this.proxy = proxy;
							handlers = new ProxClientHandle[1];
																	handlers[0] = onObjPos; 
															}
						
						internal void onRec(NetMsgIn msg)
						{
							uint cmd = msg.ReadUInt32();
							if (cmd < 0 || cmd >= handlers.Length)
							{
								Logger.WriteLine(LogType.ERROR, "Malformed message, invalid msg id " + cmd);
								proxy.proxyDisconnect("malformed message");
								return;
							}

							handlers[cmd].Invoke(msg);
						}
						
						internal void onChg(ConnectionStatus status)
						{
							impl.freeside_onConnectionChange(this, status);
						}
						
															public void sendPlayerInput(string hi, int b)
									{
																					var msg = proxy.startProxyMessage(64);
																						msg.WriteUInt32(0);
																						msg.WriteString(hi);
																						msg.WriteInt32(b);
																					proxy.sendProxyMessage(MessageDeliveryType.UNRELIABLE_SEQUENCED, 0);
																			} 
																	private void onObjPos(NetMsgIn msg)
									{
																					string sup;
																						int a;
											
																					sup = msg.ReadString();
																						a = msg.ReadInt32();
											
										impl.freeside_ObjPos(this, sup, a);
									} 
													}

					public interface ICypherFromFreeside
					{
												void freeside_onConnectionChange(CyphertoFreesideClient client, ConnectionStatus status);
															void freeside_ObjPos(CyphertoFreesideClient client, string sup, int a); 
													}
				}
