﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace log
{
    public enum LogType
    {
        POSSIBLE_ERROR,
        ERROR,
        DEBUG
    }
    public abstract class Logger
    {
        public static void WriteLine(LogType type, string s)
        {
            ConsoleColor fg = ConsoleColor.White;
            ConsoleColor bg = ConsoleColor.Black;

            switch (type)
            {
                case LogType.DEBUG:
                    fg = ConsoleColor.White;
                    bg = ConsoleColor.Black;
                    break;
                case LogType.ERROR:
                    fg = ConsoleColor.White;
                    bg = ConsoleColor.Red;
                    break;
                case LogType.POSSIBLE_ERROR:
                    fg = ConsoleColor.Red;
                    bg = ConsoleColor.Black;
                    break;
            }

            Console.ForegroundColor = fg;
            Console.BackgroundColor = bg;
            Console.WriteLine(s);
        }
    }
}
