﻿using System;
using deck;
using log;
using System.Threading;
using System.Collections.Generic;

namespace auth
{
    class AccountInformation
    {
        internal byte[] pwHash;
        internal ulong playerID;

        internal AccountInformation(byte[] pwHash, ulong playerID)
        {
            this.pwHash = pwHash;
            this.playerID = playerID;
        }
    }

    class AccountDatabase
    {
        Dictionary<string, AccountInformation> passwordDB = new Dictionary<string, AccountInformation>();
        private static ulong pUID = 1;


        public bool checkLogin(string username, byte[] hash, out ulong playerID, out string reason)
        {
            AccountInformation acc;
            if (passwordDB.TryGetValue(username, out acc))
            {   
                if (hashEquals(acc.pwHash, hash))
                {
                    playerID = acc.playerID;
                    reason = "";
                    return true;
                }
                else
                {
                    reason = "invalid password";
                }
            }
            else
            {
                reason = "invalid usename";
            }

            playerID = 0;
            return false;
        }

        public bool addAccount(string username, byte[] hash, out string reason)
        {
            if (passwordDB.ContainsKey(username))
            {
                reason = "username already exists";
                return false;
            }

            ulong playerID = pUID++;
            AccountInformation acc = new AccountInformation(hash, playerID);
            passwordDB.Add(username, acc);
            reason = "";

            return true;
        }
        private bool hashEquals(byte[] a, byte[] b)
        {
            if (a.Length != b.Length)
                return false;

            bool toReturn = true;
            for (int x = 0; x < a.Length; x++)
            {
                if (a[x] != b[x])
                    toReturn = false;
            }

            return toReturn;
        }
    }
    /// <summary>
    /// Has the secure database of username/passwords. Only talks to the login servers, providing a yes/no response on auth attemtps.
    /// Also sends the account UID on success.
    /// </summary>
    class Auth : IAuthFromIce
    {
        static void Main(string[] args)
        {
            new Auth();
        }

        AccountDatabase db;
        AuthtoIceHost host;
        Auth()
        {
            db = new AccountDatabase();
            host = new AuthtoIceHost(this);

            new Thread(Run).Start();
        }

        public void ice_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Auth client connection change. New Status: " + status);
            switch (status)
            {
                case ConnectionStatus.CONNECTED:
                    break;
                case ConnectionStatus.DISCONNECTED:
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }
        }

        public void ice_LoginRequest(NetConn conn, uint reqID, string username, byte[] password)
        {
            ulong pID;
            string reason;
            bool success = db.checkLogin(username, password, out pID, out reason);

            host.sendLoginResponse(conn, reqID, success, pID, reason);
        }

        public void ice_NewUserRequest(NetConn conn, uint reqID, string username, byte[] password)
        {
            string reason;
            bool success = db.addAccount(username, password, out reason);

            host.sendNewUserResponse(conn, reqID, success, reason);
        }

        /*
        void onReceive(NetConn conn, NetMsgIn msg)
        {
            IceToAuth cmd = (IceToAuth)msg.ReadUInt32();
            switch (cmd)
            {
                case IceToAuth.LOGIN:
                    {
                        break;
                    }
                case IceToAuth.NEWUSER:
                    {
                        break;
                    }
                default:
                    Logger.WriteLine("Invalid command packet: " + cmd);
                    conn.Disconnect("Invalid packet");
                    break;
            }
        }
        */

        void Run()
        {
            while (true)
            {
                host.Update();
                Thread.Sleep(1);
            }
        }
    }
}
