﻿using deck;
using log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ice
{
    class ClientInformation
    {
        //player information
        internal string username;
        internal ulong playerID;

        //internal state
        internal uint reqID;
        internal UInt256 symID;
        internal SymKey symKey;

        //connection
        internal NetConn conn;

        public ClientInformation(NetConn conn)
        {
            this.conn = conn;
            conn.userData = this;
        }
    }

    class Ice : IIceFromAuth, IIceFromProxymaster, IIceFromCypher
    {
        static void Main(string[] args)
        {
            new Ice();
        }

        IcetoProxymasterClient proxyClient;
        IcetoAuthClient authClient;

        IcetoCypherHost iceHost;

        uint loginReqID = 0;
        Dictionary<uint, ClientInformation> requestToClient = new Dictionary<uint, ClientInformation>();
        Ice()
        {
            authClient = new IcetoAuthClient(this);
            proxyClient = new IcetoProxymasterClient(this);
            iceHost = new IcetoCypherHost(this, new PrivateKey());

            new Thread(Run).Start();
        }

        public void proxymaster_onConnectionChange(IcetoProxymasterClient conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Proxy connection change. New Status: " + status);
        }

        public void proxymaster_FindProxyResponse(IcetoProxymasterClient conn, uint reqID, bool success, string hostname, int? port, string reason)
        {
            ClientInformation cli;
            if (!requestToClient.TryGetValue(reqID, out cli))
            {
                Logger.WriteLine(LogType.POSSIBLE_ERROR, "Request ID that we don't have... " + reqID);
                return;
            }

            requestToClient.Remove(reqID);

            iceHost.sendLoginResponse(cli.conn, success, cli.symID, cli.symKey, cli.playerID, hostname, port, reason);
        }

        public void auth_onConnectionChange(IcetoAuthClient conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Auth connection change. New Status: " + status);
        }

        public void auth_LoginResponse(IcetoAuthClient conn, uint reqID, bool success, ulong? playerID, string reason)
        {
            ClientInformation cli;
            if (!requestToClient.TryGetValue(reqID, out cli))
            {
                Logger.WriteLine(LogType.POSSIBLE_ERROR, "Request ID that we don't have... " + reqID);
                return;
            }

            requestToClient.Remove(reqID);

            if (success)
            {
                cli.playerID = (ulong)playerID;
                cli.reqID = loginReqID++;
                requestToClient.Add(cli.reqID, cli);

                Deck.GenerateSymKey(out cli.symID, out cli.symKey);
                
                proxyClient.sendFindProxy(cli.reqID, cli.symID, cli.symKey, cli.playerID);
            }
            else
            {
                iceHost.sendLoginResponse(cli.conn, false, null, null, null, null, null, reason);
            }
        }

        public void auth_NewUserResponse(IcetoAuthClient conn, uint reqID, bool success, string reason)
        {
            ClientInformation cli;
            if (!requestToClient.TryGetValue(reqID, out cli))
            {
                Logger.WriteLine(LogType.POSSIBLE_ERROR, "Request ID that we don't have... " + reqID);
                return;
            }

            requestToClient.Remove(reqID);

            iceHost.sendNewUserResponse(cli.conn, success, reason);
        }

        public void cypher_LoginRequest(NetConn conn, string username, string password)
        {
            ClientInformation cli;
            if (conn != null && conn.userData is ClientInformation)
            {
                cli = (ClientInformation)conn.userData;
                cli.username = username;
                byte[] pwHash = Deck.hashPassword(password);
                cli.reqID = loginReqID++;
                requestToClient.Add(cli.reqID, cli);

                authClient.sendLoginRequest(cli.reqID, cli.username, pwHash);
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Message from non-client connection? " + conn);
            }
        }

        public void cypher_NewUserRequest(NetConn conn, string username, string password)
        {
            ClientInformation cli;
            if (conn != null && conn.userData is ClientInformation)
            {
                cli = (ClientInformation)conn.userData;
                cli.username = username;
                byte[] pwHash = Deck.hashPassword(password);
                cli.reqID = loginReqID++;
                requestToClient.Add(cli.reqID, cli);

                authClient.sendNewUserRequest(cli.reqID, cli.username, pwHash);
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Message from non-client connection? " + conn);
            }
        }

        public void cypher_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Client connection change from " + conn + ". New Status: " + status);
            switch (status)
            {
                case ConnectionStatus.CONNECTED:
                    new ClientInformation(conn); //this sets conn.userData to ClientInformation
                    break;
                case ConnectionStatus.DISCONNECTED:
                    break;
                default:
                    throw new NotImplementedException("Missing case: " + status);
            }
        }

        void Run()
        {
            while (true)
            {
                iceHost.Update();
                authClient.Update();
                proxyClient.Update();
                Thread.Sleep(1);
            }
        }
    }
}
