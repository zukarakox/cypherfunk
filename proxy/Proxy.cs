﻿using System;
using deck;
using log;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proxy
{
    internal class ClientInformation
    {
        internal UInt256 symID;
        internal SymKey symKey;
        internal ulong playerID;

        internal NetConn conn;
        internal ProxytoFreesideClient freesideClient;

        /// <summary>
        /// Initially used to countdown until we drop the expected connection.
        /// </summary>
        internal float timer = 0;

        internal ClientInformation(UInt256 symID, SymKey symKey, ulong playerID)
        {
            this.symID = symID;
            this.symKey = symKey;
            this.playerID = playerID;
        }
    }

    internal class FreesideInformation
    {
        internal List<ClientInformation> clientsWaitingForConnect = new List<ClientInformation>();
        internal bool isConnected = false;

        internal List<ClientInformation> connectedClients = new List<ClientInformation>();

        internal FreesideInformation()
        {

        }
    }

    /// <summary>
    /// Maintains a connection to the proxymaster server, responding to healthchecks.
    /// Proxymaster will send information about a player about to connect.
    /// Player will connect, use symkey that proxymaster sends.
    /// Proxy will then forward an unencryped stream to the appropriate game serves for the client.
    /// </summary>
    internal class Proxy : IProxyFromCypher, IProxyFromFreeside, IProxyFromFreesidemaster, IProxyFromProxymaster
    {
        static void Main(string[] args)
        {
            new Proxy("proxy1.cypherfunk.com", 8009);
        }

        Dictionary<UInt256, ClientInformation> expectedConnections = new Dictionary<UInt256, ClientInformation>();
        List<ClientInformation> expectingClients = new List<ClientInformation>();

        ProxytoProxymasterClient pmClient; //proxymaster connection
        ProxytoFreesidemasterClient fmClient; //freesidemaster connection
        ProxytoCypherHost cypherHost;
        List<ProxytoFreesideClient> freesideClients = new List<ProxytoFreesideClient>();

        Dictionary<ulong, ClientInformation> playerIDToClient = new Dictionary<ulong, ClientInformation>();

        string publicHostname;
        int publicPort;

        Proxy(string publicHostname, int publicPort)
        {
            this.publicHostname = publicHostname;
            this.publicPort = publicPort;

            pmClient = new ProxytoProxymasterClient(this);
            fmClient = new ProxytoFreesidemasterClient(this);
            cypherHost = new ProxytoCypherHost(this, publicPort, getClientSymKey);

            new Thread(Run).Start();
        }

        SymKey? getClientSymKey(UInt256 id)
        {
            ClientInformation cli;
            if (expectedConnections.TryGetValue(id, out cli))
            {
                return cli.symKey;
            }

            return null;
        }

        void Run()
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            long cur;
            long last = 0;
            while (true)
            {
                cur = timer.ElapsedMilliseconds;
                float dt = (cur - last) / 1000f;
                last = cur;

                fmClient.Update();
                pmClient.Update();
                cypherHost.Update();
                foreach (ProxytoFreesideClient c in freesideClients)
                    c.Update();

                for (int x = 0; x < expectingClients.Count; x++)
                {
                    ClientInformation cli = expectingClients[x];
                    cli.timer += dt;
                    if (cli.timer > 30)
                    {//timeout!
                        Logger.WriteLine(LogType.DEBUG, "Expected connection timeout! " + cli.playerID);
                        expectedConnections.Remove(cli.symID);
                        expectingClients.RemoveAt(x--);
                        continue;
                    }
                }

                Thread.Sleep(1);
            }
        }

        public bool cypher_onSymConnApproval(UInt256 symID, NetConn conn, ulong playerID)
        {
            ClientInformation cli;
            if (expectedConnections.TryGetValue(symID, out cli))
            {
                if (playerID == cli.playerID)
                {
                    expectedConnections.Remove(symID);
                    expectingClients.Remove(cli);
                    cli.conn = conn;
                    conn.userData = cli;
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        public void cypher_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Connection to cypher change. New Status: " + status);
            if (status == ConnectionStatus.CONNECTED)
            {//woo.
                if (conn.userData is ClientInformation)
                {
                    ClientInformation cli = (ClientInformation)conn.userData;
                    playerIDToClient.Add(cli.playerID, cli);
                }
                else
                {
                    Logger.WriteLine(LogType.ERROR, "Connecting to cypher but we don't have a clientinformation?");
                    conn.Disconnect("missing data, weird conn");
                }
            }
            else if (status == ConnectionStatus.DISCONNECTED)
            {
                if (conn.userData is ClientInformation)
                {
                    ClientInformation cli = (ClientInformation)conn.userData;
                    playerIDToClient.Remove(cli.playerID);

                    if (cli.freesideClient != null)
                    {
                        cli.freesideClient.sendProxyDisconnect(cli.playerID);
                        cli.freesideClient = null;
                    }
                }
                else
                {
                    Logger.WriteLine(LogType.ERROR, "Disconnecting from cypher but we don't have a clientinformation?");
                }
            }
        }

        public void cypher_onProxyMessage(NetConn conn, byte[] msg, int pos, int len, MessageDeliveryType type, int chan)
        {
            if (conn.userData is ClientInformation)
            {
                ClientInformation cli = (ClientInformation)conn.userData;
                if (cli.freesideClient != null)
                {
                    cli.freesideClient.sendProxyMessage(cli.playerID, msg, pos, len, type, chan);
                }
                else
                {
                    Logger.WriteLine(LogType.ERROR, "Proxy message from cypher but we don't have a freeside client?");
                    cypherHost.sendProxyDisconnect(conn);
                }
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Proxy message from cypher but we don't have a clientinformation?");
                conn.Disconnect("missing data, weird conn");
            }
        }

        public void cypher_onProxyDisc(NetConn conn, string reason)
        {
            if (conn.userData is ClientInformation)
            {
                ClientInformation cli = (ClientInformation)conn.userData;
                if (cli.freesideClient != null)
                {
                    cli.freesideClient.sendProxyDisconnect(cli.playerID);
                    cli.freesideClient = null;
                }
                else
                {
                    Logger.WriteLine(LogType.ERROR, "Proxy disc from cypher but we don't have a freeside client?");
                    //guess it'd be kinda redundant to tell it to proxyDisc here...
                }
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Proxy disc from cypher but we don't have a clientinformation?");
                conn.Disconnect("missing data, weird conn");
            }
        }

        public void cypher_RequestGameServer(NetConn conn)
        {
            ClientInformation cli = (ClientInformation)conn.userData;
            fmClient.sendRequestGameServer(cli.playerID);
        }

        public void freeside_onConnectionChange(ProxytoFreesideClient conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Connection to freeside change. New Status: " + status);
            if (status == ConnectionStatus.CONNECTED)
            {//woo.
                FreesideInformation fInf = (FreesideInformation)conn.userData;
                fInf.isConnected = true;
                foreach (ClientInformation cli in fInf.clientsWaitingForConnect)
                {
                    cli.freesideClient = conn;
                    conn.sendProxyConnection(cli.playerID);
                    cypherHost.sendProxyConnection(cli.conn);
                    fInf.connectedClients.Add(cli);
                }
                fInf.clientsWaitingForConnect.Clear();
            }
            else if (status == ConnectionStatus.DISCONNECTED)
            {
                FreesideInformation fInf = (FreesideInformation)conn.userData;
                fInf.isConnected = false;

                //first, tell all of the waiting clients, if any, that the request failed.
                foreach (ClientInformation cli in fInf.clientsWaitingForConnect)
                {
                    cypherHost.sendGameServerResponse(cli.conn, false, "Lost connectiont to game server");
                }
                fInf.clientsWaitingForConnect.Clear();

                //secondly, tell all connected clients, if any, that the proxy connection is dead
                foreach (ClientInformation cli in fInf.connectedClients)
                {
                    cypherHost.sendProxyDisconnect(cli.conn);
                    cli.freesideClient = null;
                }
                fInf.connectedClients.Clear();

                freesideClients.Remove(conn);
            }
        }

        public void freeside_onProxyMessage(ProxytoFreesideClient client, ulong pid, byte[] msg, int pos, int len, MessageDeliveryType type, int chan)
        {
            ClientInformation cli;
            if (playerIDToClient.TryGetValue(pid, out cli))
            {
                if (cli.conn != null)
                {
                    cypherHost.sendProxyMessage(cli.conn, msg, pos, len, type, chan);
                }
                else
                {//this is really weird. should probably just nuke things.
                    Logger.WriteLine(LogType.ERROR, "Proxy message from freeside but we don't have a client connection?");
                    client.sendProxyDisconnect(pid);
                }
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Proxy message from freeside, but we don't have that client information?");
                client.sendProxyDisconnect(pid);
            }
        }

        public void freeside_onProxyDisc(ProxytoFreesideClient client, ulong pid, string reason)
        {
            ClientInformation cli;
            Logger.WriteLine(LogType.DEBUG, "freeside is sending a proxy disc: " + reason);
            if (playerIDToClient.TryGetValue(pid, out cli))
            {
                if (cli.conn != null)
                {
                    cypherHost.sendProxyDisconnect(cli.conn);
                }
                else
                {//this is really weird. should probably just nuke things.
                    Logger.WriteLine(LogType.ERROR, "Proxy disc from freeside but we don't have a client connection?");
                }
            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Proxy disc from freeside, but we don't have that client information?");
            }
        }

        public void proxymaster_onConnectionChange(ProxytoProxymasterClient client, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Connection to proxymaster change. New Status: " + status);

            if (status == ConnectionStatus.CONNECTED)
            {
                pmClient.sendPublicInformation(publicHostname, publicPort);
            }
        }

        public void proxymaster_NewUser(ProxytoProxymasterClient client, UInt256 symID, SymKey symKey, ulong playerID)
        {
            expectedConnections.Add(symID, new ClientInformation(symID, symKey, playerID));
        }

        public void freesidemaster_onConnectionChange(ProxytoFreesidemasterClient client, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Connection to freesidemaster change. New Status: " + status);
        }

        Dictionary<string, ProxytoFreesideClient> hostnameToFreeside = new Dictionary<string, ProxytoFreesideClient>();
        public void freesidemaster_GameServerResponse(ProxytoFreesidemasterClient client, ulong playerID, bool success, string hostname, int? port, string reason)
        {
            if (success)
            {
                ClientInformation cInf;
                if (!playerIDToClient.TryGetValue(playerID, out cInf))
                {
                    Logger.WriteLine(LogType.ERROR, "Gameserver response can't find matching player id?");
                    return;
                }

                Logger.WriteLine(LogType.DEBUG, "Freeside master gameserver response success");
                ProxytoFreesideClient cli;
                FreesideInformation fInf;
                if (!hostnameToFreeside.TryGetValue(hostname, out cli))
                {
                    cli = new ProxytoFreesideClient(this, port.Value, hostname);
                    hostnameToFreeside.Add(hostname, cli);
                    freesideClients.Add(cli);

                    fInf = new FreesideInformation();
                    cli.userData = fInf;
                }
                else
                {
                    fInf = (FreesideInformation)cli.userData;
                }

                if (!fInf.isConnected)
                {
                    fInf.clientsWaitingForConnect.Add(cInf);
                }
                else
                {
                    cInf.freesideClient = cli;
                    cli.sendProxyConnection(playerID);
                    cypherHost.sendProxyConnection(cInf.conn);
                    fInf.connectedClients.Add(cInf);
                }

            }
            else
            {
                Logger.WriteLine(LogType.ERROR, "Freeside master gameserver response failed: " + reason);
            }
        }
    }
}
