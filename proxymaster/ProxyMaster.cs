﻿using System;
using deck;
using log;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace proxymaster
{
    class Proxy
    {
        internal NetConn conn;
        internal string publicHostname;
        internal int publicPort;

        internal Proxy(NetConn conn, string publicHostname, int publicPort)
        {
            this.conn = conn;
            this.publicHostname = publicHostname;
            this.publicPort = publicPort;
        }
    }

    /// <summary>
    /// Handles the list of proxy servers. Does health checks to ensure they're working.
    /// Gives a proxy server to use when the ice service has successfully auth'd a player.
    /// </summary>
    class ProxyMaster : IProxymasterFromIce, IProxymasterFromProxy
    {
        static void Main(string[] args)
        {
            new ProxyMaster();
        }


        ProxymastertoIceHost iceHost;
        ProxymastertoProxyHost proxyHost;

        List<Proxy> proxies = new List<Proxy>();
        ProxyMaster()
        {
            iceHost = new ProxymastertoIceHost(this);
            proxyHost = new ProxymastertoProxyHost(this);

            new Thread(Run).Start();
        }

        void Run()
        {
            while (true)
            {
                proxyHost.Update();
                iceHost.Update();
                Thread.Sleep(1);
            }
        }

        public void proxy_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Proxymaster proxy connection change. New Status: " + status);
            if (status == ConnectionStatus.DISCONNECTED)
            {
                //remove this from our proxy list if it exists
                for (int x = 0; x < proxies.Count; x++)
                {
                    if (proxies[x].conn == conn)
                    {
                        proxies.RemoveAt(x);
                        break;
                    }
                }
            }
        }

        public void proxy_PublicInformation(NetConn conn, string publicHost, int publicPort)
        {
            for (int x = 0; x < proxies.Count; x++)
            {
                if (proxies[x].conn == conn)
                {
                    conn.Disconnect("already got information? sent twice?");
                    return;
                }
            }

            Proxy p = new Proxy(conn, publicHost, publicPort);
            proxies.Add(p);
        }

        public void ice_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Proxymaster ice connection change. New Status: " + status);
        }

        public void ice_FindProxy(NetConn conn, uint reqID, UInt256 symID, SymKey symKey, ulong playerID)
        {

            if (proxies.Count == 0)
            {
                //failed oops, tell ice
                iceHost.sendFindProxyResponse(conn, reqID, false, null, null, "No available proxies.");
            }
            else
            {
                //TODO: pick proxy based on some load measurement
                Proxy proxy = proxies[(int)(reqID % proxies.Count)];

                //tell proxy new user incoming
                proxyHost.sendNewUser(proxy.conn, symID, symKey, playerID);

                //give ice the public proxy hostname
                iceHost.sendFindProxyResponse(conn, reqID, true, proxy.publicHostname, proxy.publicPort, null);
            }
        }
    }
}
