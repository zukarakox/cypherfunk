﻿using System;
using deck;
using log;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace freesidemaster
{
    class Freeside
    {
        internal NetConn conn;
        internal string privHostname;
        internal int privPort;

        internal Freeside(NetConn conn, string privHostname, int privPort)
        {
            this.conn = conn;
            this.privHostname = privHostname;
            this.privPort = privPort;
        }
    }

    /// <summary>
    /// Maintains list of freeside servers. Does healthchecks to ensure functionality.
    /// Mostly coordinates game functions that aren't the game itself.
    /// Finds a game server for the player, when joining a not-match zone, including working as a queue when servers are full.
    /// Coordinates matchmaking (and choosing which server the match takes place on)
    /// </summary>
    class FreesideMaster : IFreesidemasterFromFreeside, IFreesidemasterFromProxy
    {
        static void Main(string[] args)
        {
            new FreesideMaster();
        }

        FreesidemastertoFreesideHost freesideHost;
        FreesidemastertoProxyHost proxyHost;

        List<Freeside> freesides = new List<Freeside>();
        FreesideMaster()
        {
            freesideHost = new FreesidemastertoFreesideHost(this);
            proxyHost = new FreesidemastertoProxyHost(this);

            new Thread(Run).Start();
        }

        void Run()
        {
            while (true)
            {
                freesideHost.Update();
                proxyHost.Update();
                Thread.Sleep(1);
            }
        }

        public void freeside_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Freeside Connection to Freeside Master change: " + status);
            if (status == ConnectionStatus.DISCONNECTED)
            {
                //remove this from our proxy list if it exists
                for (int x = 0; x < freesides.Count; x++)
                {
                    if (freesides[x].conn == conn)
                    {
                        freesides.RemoveAt(x);
                        break;
                    }
                }
            }
        }

        public void freeside_PublicInformation(NetConn conn, string privHost, int privPort)
        {
            for (int x = 0; x < freesides.Count; x++)
            {
                if (freesides[x].conn == conn)
                {
                    conn.Disconnect("already got information? sent twice?");
                    return;
                }
            }
            
            freesides.Add(new Freeside(conn, privHost, privPort));
        }

        public void proxy_onConnectionChange(NetConn conn, ConnectionStatus status)
        {
            Logger.WriteLine(LogType.DEBUG, "Proxy Connection to Freeside Master change: " + status);
        }

        public void proxy_RequestGameServer(NetConn conn, ulong playerID)
        {

            if (freesides.Count == 0)
            {
                //failed oops, tell ice
                proxyHost.sendGameServerResponse(conn, playerID, false, null, null, "No available servers");
            }
            else
            {
                //should probably actually pick the gameserver based on something but
                //whatever.
                Freeside freeside = freesides[0];

                //give the information for the freeside back to proxy
                proxyHost.sendGameServerResponse(conn, playerID, true, freeside.privHostname, freeside.privPort, null);
            }
        }
    }
}
